unit Testes.Colecoes.VetorValidacao;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes;

type
  TVetorValidacao = class
  private
    fTotal    : Integer;
    fValores  : TArray<Integer>;
    fColecao  ,
    fInverso  ,
    fAleatorio: TEnumerable<Integer>;
  public
    constructor Create(const total: Integer);
    destructor  Destroy; override;

    function validarCrescente  (const vetor: TArray<Integer>): Boolean; overload;
    function validarDecrescente(const vetor: TArray<Integer>): Boolean; overload;
    function validarValores    (const vetor: TArray<Integer>): Boolean; overload;
    function validarInverso    (const vetor: TArray<Integer>): Boolean; overload;
    function validarConteudo   (const vetor: TArray<Integer>): Boolean; overload;

    function validarCrescente  (const colecao: IEnumeravel<Integer>): Boolean; overload;
    function validarDecrescente(const colecao: IEnumeravel<Integer>): Boolean; overload;
    function validarValores    (const colecao: IEnumeravel<Integer>): Boolean; overload;
    function validarInverso    (const colecao: IEnumeravel<Integer>): Boolean; overload;
    function validarConteudo   (const colecao: IEnumeravel<Integer>): Boolean; overload;

    property total    : Integer              read fTotal;
    property valores  : TEnumerable<Integer> read fColecao;
    property inverso  : TEnumerable<Integer> read fInverso;
    property aleatorio: TEnumerable<Integer> read fAleatorio;
  end;

implementation
uses
  FractalLotus.Fundacao.Ordenacoes.Aleatorio,
  FractalLotus.Fundacao.Ordenacoes.Validador;

type
  TEnumerador = class(TEnumerator<Integer>)
  strict private
    fIndex: Integer;
    fVetor: TVetorValidacao;

    function lerTotal: Integer; inline;
  protected
    function DoGetCurrent: Integer; override;
    function DoMoveNext  : Boolean; override;

    function GetIndex(const relativo: Integer): Integer; virtual; abstract;

    property total: Integer read lerTotal;
  public
    constructor Create(const vetor: TVetorValidacao); virtual;
  end;

  TEnumeradorValores = class(TEnumerador)
  protected
    function GetIndex(const relativo: Integer): Integer; override;
  end;

  TEnumeradorInverso = class(TEnumerador)
  protected
    function GetIndex(const relativo: Integer): Integer; override;
  end;

  TEnumeradorAleatorio = class(TEnumerador)
  private
    fIndices: TArray<Integer>;
  protected
    function GetIndex(const relativo: Integer): Integer; override;
  public
    constructor Create(const vetor: TVetorValidacao); override;
    destructor  Destroy; override;
  end;

  TColecaoValores<T: TEnumerador> = class abstract (TEnumerable<Integer>)
  private
    fVetor: TVetorValidacao;
  protected
    function DoGetEnumerator: TEnumerator<Integer>; override;
  public
    constructor Create(const vetor: TVetorValidacao);
  end;

{ TEnumerador }

constructor TEnumerador.Create(const vetor: TVetorValidacao);
begin
  fVetor := vetor;
  fIndex := -1;
end;

function TEnumerador.DoGetCurrent: Integer;
var
  indice: Integer;
begin
  indice := GetIndex(fIndex);
  Result := fVetor.fValores[indice];
end;

function TEnumerador.DoMoveNext: Boolean;
begin
  Inc(fIndex);

  Result := fIndex < total;
end;

function TEnumerador.lerTotal: Integer;
begin
  Result := fVetor.total;
end;

{ TEnumeradorValores }

function TEnumeradorValores.GetIndex(const relativo: Integer): Integer;
begin
  Result := relativo;
end;

{ TEnumeradorInverso }

function TEnumeradorInverso.GetIndex(const relativo: Integer): Integer;
begin
  Result := total - relativo - 1;
end;

{ TEnumeradorAleatorio }

constructor TEnumeradorAleatorio.Create(const vetor: TVetorValidacao);
var
  i: Integer;
begin
  inherited Create(vetor);

  SetLength(fIndices, vetor.total);
  for i := 0 to vetor.total - 1 do
    fIndices[i] := i;

  with TOrdenacaoAleatoria<Integer>.Create do
    try
      ordenar(fIndices);
    finally
      Free;
    end;
end;

destructor TEnumeradorAleatorio.Destroy;
begin
  SetLength(fIndices, 0);

  inherited Destroy;
end;

function TEnumeradorAleatorio.GetIndex(const relativo: Integer): Integer;
begin
  Result := fIndices[relativo];
end;

{ TColecaoValores<T> }

constructor TColecaoValores<T>.Create(const vetor: TVetorValidacao);
begin
  fVetor := vetor;
end;

function TColecaoValores<T>.DoGetEnumerator: TEnumerator<Integer>;
begin
  Result := T.Create(fVetor);
end;

{ TVetorValidacao }

constructor TVetorValidacao.Create(const total: Integer);
var
  i: Integer;
begin
  fTotal := total;

  SetLength(fValores, total);

  for i := 1 to total do
    fValores[i - 1] := i;

  with TOrdenacaoAleatoria<Integer>.Create do
    try
      ordenar(fValores);
    finally
      Free;
    end;

  fColecao   := TColecaoValores<TEnumeradorValores  >.Create(Self);
  fInverso   := TColecaoValores<TEnumeradorInverso  >.Create(Self);
  fAleatorio := TColecaoValores<TEnumeradorAleatorio>.Create(Self);
end;

destructor TVetorValidacao.Destroy;
begin
  fAleatorio.Free;
  fInverso  .Free;
  fColecao  .Free;

  SetLength(fValores, 0);

  inherited Destroy;
end;

function TVetorValidacao.validarConteudo(const vetor: TArray<Integer>): Boolean;
var
  i: Integer;
  valores: TArray<Integer>;
begin
  if (Length(vetor) <> total) then
    Exit(False);

  Result  := True;
  valores := vetor;

  TArray.Sort<Integer>(valores);

  for i := 1 to total do
    if not Result then
      Break
    else
      Result := vetor[i - 1] = i;
end;

function TVetorValidacao.validarConteudo(
  const colecao: IEnumeravel<Integer>): Boolean;
begin
  Result := validarConteudo(colecao.ToArray);
end;

function TVetorValidacao.validarCrescente(
  const vetor: TArray<Integer>): Boolean;
begin
  with TValidadorOrdenacaoAscendente<Integer>.Create do
    try
      Result := (Length(vetor) = total) and validar(vetor);
    finally
      Free;
    end;
end;

function TVetorValidacao.validarCrescente(
  const colecao: IEnumeravel<Integer>): Boolean;
begin
  Result := validarCrescente(colecao.ToArray);
end;

function TVetorValidacao.validarDecrescente(
  const colecao: IEnumeravel<Integer>): Boolean;
begin
  Result := validarDecrescente(colecao.ToArray);
end;

function TVetorValidacao.validarDecrescente(
  const vetor: TArray<Integer>): Boolean;
begin
  with TValidadorOrdenacaoDescendente<Integer>.Create do
    try
      Result := (Length(vetor) = total) and validar(vetor);
    finally
      Free;
    end;
end;

function TVetorValidacao.validarInverso(const vetor: TArray<Integer>): Boolean;
var
  i: Integer;
begin
  Result := Length(vetor) = total;

  for i := 0 to total - 1 do
    if not Result then
      Break
    else
      Result := vetor[i] = fValores[total - i - 1];
end;

function TVetorValidacao.validarInverso(
  const colecao: IEnumeravel<Integer>): Boolean;
begin
  Result := validarInverso(colecao.ToArray);
end;

function TVetorValidacao.validarValores(const vetor: TArray<Integer>): Boolean;
var
  i: Integer;
begin
  Result := Length(vetor) = total;

  for i := 0 to total - 1 do
    if not Result then
      Break
    else
      Result := vetor[i] = fValores[i];
end;

function TVetorValidacao.validarValores(
  const colecao: IEnumeravel<Integer>): Boolean;
begin
  Result := validarValores(colecao.ToArray);
end;

end.
