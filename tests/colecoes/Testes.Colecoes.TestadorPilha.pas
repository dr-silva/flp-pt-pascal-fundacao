unit Testes.Colecoes.TestadorPilha;

interface
uses
  DUnitX.TestFramework,
  Testes.Colecoes.TestadorEnumeravel,
  FractalLotus.Fundacao.Cerne.Colecoes;

type
  TTestadorPilha = class abstract (TTestadorEnumeravel)
  private
    function pilha: IEmpilhavel<Integer>; inline;
  public
    [Test]
    procedure testarEmpilhamento;
    [Test]
    procedure testarDesempilhamento;
  end;

  [TestFixture]
  TTestadorPilhaVetorial = class (TTestadorPilha)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;

  [TestFixture]
  TTestadorPilhaEncadeada = class (TTestadorPilha)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;

implementation
uses
  FractalLotus.Fundacao.Colecoes.PilhaVetorial,
  FractalLotus.Fundacao.Colecoes.PilhaEncadeada;

{ TTestadorPilha }

function TTestadorPilha.pilha: IEmpilhavel<Integer>;
begin
  Result := colecao as IEmpilhavel<Integer>;
end;

procedure TTestadorPilha.testarDesempilhamento;
var
  index,
  valor: Integer;
  itens: TArray<Integer>;
begin
  pilha.limpar;

  if (colecao.total <> 0) then
    Assert.Fail('A cole��o n�o est� vazia.');

  for valor in vetor.valores do
    pilha.empilhar(valor);

  SetLength(itens, colecao.total);
  index := 0;

  while not colecao.vazio do
  begin
    itens[index] := pilha.desempilhar;
    Inc(index);
  end;

  Assert.IsTrue(vetor.validarInverso(itens));
end;

procedure TTestadorPilha.testarEmpilhamento;
var
  valor: Integer;
begin
  pilha.limpar;

  if (colecao.total <> 0) then
    Assert.Fail('A cole��o n�o est� vazia.');

  for valor in vetor.valores do
    pilha.empilhar(valor);

  Assert.IsTrue(vetor.validarConteudo(colecao));
end;

{ TTestadorPilhaVetorial }

function TTestadorPilhaVetorial.criarColecao: IEnumeravel<Integer>;
begin
  Result := TPilhaVetorial<Integer>.Create(vetor.total);
end;

{ TTestadorPilhaEncadeada }

function TTestadorPilhaEncadeada.criarColecao: IEnumeravel<Integer>;
begin
  Result := TPilhaEncadeada<Integer>.Create;
end;

end.
