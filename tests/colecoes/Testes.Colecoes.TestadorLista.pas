unit Testes.Colecoes.TestadorLista;

interface
uses
  DUnitX.TestFramework,
  Testes.Colecoes.TestadorEnumeravel,
  FractalLotus.Fundacao.Cerne.Colecoes;

type
  TTestadorLista = class abstract (TTestadorEnumeravel)
  protected
    function lista: IListavel<Integer>; inline;
  public
    [Test]
    procedure testarInsercao;
    [Test]
    procedure testarRemocao;
  end;

  [TestFixture]
  TTestadorListaVetorial = class (TTestadorLista)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;

  [TestFixture]
  TTestadorListaEncadeada = class (TTestadorLista)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;

implementation
uses
  FractalLotus.Fundacao.Colecoes.ListaVetorial,
  FractalLotus.Fundacao.Colecoes.ListaEncadeada;

{ TTestadorLista }

function TTestadorLista.lista: IListavel<Integer>;
begin
  Result := colecao as IListavel<Integer>;
end;

procedure TTestadorLista.testarInsercao;
var
  valor: Integer;
begin
  lista.limpar;

  if not colecao.vazio then
    Assert.Fail('A cole��o devia estar vazia.');

  for valor in vetor.valores do
    lista.adicionar(valor);

  Assert.IsTrue(vetor.validarValores(colecao));
end;

procedure TTestadorLista.testarRemocao;
var
  index, i,
  valor: Integer;
  itens: TArray<Integer>;
begin
  lista.limpar;

  if not colecao.vazio then
    Assert.Fail('A cole��o devia estar vazia.');

  for valor in vetor.valores do
    lista.adicionar(valor);

  SetLength(itens, colecao.total);
  index := 0;

  for i := colecao.total -1 downto 0 do
  begin
    itens[index] := lista[i];

    lista.remover(i);

    Inc(index);
  end;

  if not colecao.vazio then
    Assert.Fail('A cole��o devia estar vazia.');

  Assert.IsTrue(vetor.validarInverso(itens));
end;

{ TTestadorListaVetorial }

function TTestadorListaVetorial.criarColecao: IEnumeravel<Integer>;
begin
  Result := TListaVetorial<Integer>.Create(vetor.total + vetor.total div 2);
end;

{ TTestadorListaEncadeada }

function TTestadorListaEncadeada.criarColecao: IEnumeravel<Integer>;
begin
  Result := TListaEncadeada<Integer>.Create;
end;

end.
