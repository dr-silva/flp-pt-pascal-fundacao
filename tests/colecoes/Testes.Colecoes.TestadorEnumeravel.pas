unit Testes.Colecoes.TestadorEnumeravel;

interface
uses
  System.Generics.Collections,
  Testes.Colecoes.VetorValidacao,
  FractalLotus.Fundacao.Cerne.Colecoes;

type
  TTestadorEnumeravel = class abstract
  strict private
    const TOTAL = 100;
  private
    fVetor  : TVetorValidacao;
    fColecao: IEnumeravel<Integer>;
  protected
    function criarColecao: IEnumeravel<Integer>; virtual; abstract;

    property vetor  : TVetorValidacao      read fVetor;
    property colecao: IEnumeravel<Integer> read fColecao;
  public
    constructor Create;
    destructor  Destroy; override;
  end;

implementation

{ TTestadorEnumeravel }

constructor TTestadorEnumeravel.Create;
begin
  fVetor   := TVetorValidacao.Create(TOTAL);
  fColecao := criarColecao;
end;

destructor TTestadorEnumeravel.Destroy;
begin
  fColecao := nil;
  fVetor.Free;

  inherited Destroy;
end;

end.
