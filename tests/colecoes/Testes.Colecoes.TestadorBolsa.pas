unit Testes.Colecoes.TestadorBolsa;

interface
uses
  DUnitX.TestFramework,
  Testes.Colecoes.TestadorEnumeravel,
  FractalLotus.Fundacao.Cerne.Colecoes;

type
  {$region 'Abstra��es'}
  TTestadorBolsa = class abstract (TTestadorEnumeravel)
  protected
    function bolsa: IGuardavel<Integer>; inline;

    function validar(const itens: TArray<Integer>): Boolean; virtual;
  public
    [Test]
    procedure testarAdicao;
  end;

  TTestadorBolsaOrdenada = class abstract (TTestadorBolsa)
  protected
    function validar(const itens: TArray<Integer>): Boolean; override;
  end;
  {$endregion}

  {$region 'Bolsas Normais'}
  [TestFixture]
  TTestadorBolsaVetorial = class (TTestadorBolsa)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;

  [TestFixture]
  TTestadorBolsaEncadeada = class (TTestadorBolsa)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;
  {$endregion}

  {$region 'Bolsas Ordenadas'}
  [TestFixture]
  TTestadorBolsaOrdenadaVetorial = class (TTestadorBolsaOrdenada)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;

  [TestFixture]
  TTestadorBolsaOrdenadaEncadeada = class (TTestadorBolsaOrdenada)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;
  {$endregion}

implementation
uses
  FractalLotus.Fundacao.Colecoes.BolsaVetorial,
  FractalLotus.Fundacao.Colecoes.BolsaEncadeada,
  FractalLotus.Fundacao.Colecoes.BolsaOrdenadaVetorial,
  FractalLotus.Fundacao.Colecoes.BolsaOrdenadaEncadeada;

{ TTestadorBolsa }

function TTestadorBolsa.bolsa: IGuardavel<Integer>;
begin
  Result := colecao as IGuardavel<Integer>;
end;

procedure TTestadorBolsa.testarAdicao;
var
  valor: Integer;
begin
  if not colecao.vazio then
    Assert.Fail('A cole��o devia estar vazia.');

  for valor in vetor.valores do
    bolsa.guardar(valor);

  Assert.IsTrue(validar(colecao.ToArray));
end;

function TTestadorBolsa.validar(const itens: TArray<Integer>): Boolean;
begin
  Result := vetor.validarValores(itens);
end;

{ TTestadorBolsaOrdenada }

function TTestadorBolsaOrdenada.validar(const itens: TArray<Integer>): Boolean;
begin
  Result := vetor.validarCrescente(itens);
end;

{ TTestadorBolsaVetorial }

function TTestadorBolsaVetorial.criarColecao: IEnumeravel<Integer>;
begin
  Result := TBolsaVetorial<Integer>.Create(vetor.total);
end;

{ TTestadorBolsaEncadeada }

function TTestadorBolsaEncadeada.criarColecao: IEnumeravel<Integer>;
begin
  Result := TBolsaEncadeada<Integer>.Create;
end;

{ TTestadorBolsaOrdenadaVetorial }

function TTestadorBolsaOrdenadaVetorial.criarColecao: IEnumeravel<Integer>;
begin
  Result := TBolsaOrdenadaVetorial<Integer>.Create(vetor.total);
end;

{ TTestadorBolsaOrdenadaEncadeada }

function TTestadorBolsaOrdenadaEncadeada.criarColecao: IEnumeravel<Integer>;
begin
  Result := TBolsaOrdenadaEncadeada<Integer>.Create;
end;

end.
