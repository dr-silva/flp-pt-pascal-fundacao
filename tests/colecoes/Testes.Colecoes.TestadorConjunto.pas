unit Testes.Colecoes.TestadorConjunto;

interface
uses
  DUnitX.TestFramework,
  Testes.Colecoes.TestadorEnumeravel,
  FractalLotus.Fundacao.Cerne.Colecoes;

type
  {$region 'Abstrações'}
  TTestadorConjunto = class (TTestadorEnumeravel)
  private
    function conjunto: IDistinguivel<Integer>; inline;
  protected
    function validarInsercao: Boolean; virtual; abstract;
    function validarConjunto(const itens: TArray<Integer>): Boolean;
  public
    [Test]
    procedure testarInsercao;
    [Test]
    procedure testarExclusao;
    [Test]
    procedure testarUniao_1;
    [Test]
    procedure testarUniao_2;
    [Test]
    procedure testarUniao_3;
    [Test]
    procedure testarExcecao_1;
    [Test]
    procedure testarExcecao_2;
    [Test]
    procedure testarExcecao_3;
    [Test]
    procedure testarInterseccao_1;
    [Test]
    procedure testarInterseccao_2;
    [Test]
    procedure testarInterseccao_3;
  end;

  TTestadorConjuntoHash = class abstract (TTestadorConjunto)
  protected
    function validarInsercao: Boolean; override;
  end;

  TTestadorConjuntoOrdenado = class abstract (TTestadorConjunto)
  protected
    function validarInsercao: Boolean; override;
  end;
  {$endregion}

  {$region 'Conjuntos Hash'}
  [TestFixture]
  TTestadorConjuntoHashEncadeado = class (TTestadorConjuntoHash)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;

  [TestFixture]
  TTestadorConjuntoHashSondado = class (TTestadorConjuntoHash)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;
  {$endregion}

  {$region 'Conjuntos Ordenados'}
  [TestFixture]
  TTestadorConjuntoOrdenadoVetorial = class (TTestadorConjuntoOrdenado)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;

  [TestFixture]
  TTestadorConjuntoOrdenadoEncadeado = class (TTestadorConjuntoOrdenado)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;
  {$endregion}

implementation
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Colecoes.PilhaVetorial,
  FractalLotus.Fundacao.Colecoes.ConjuntoHashEncadeado,
  FractalLotus.Fundacao.Colecoes.ConjuntoHashSondado,
  FractalLotus.Fundacao.Colecoes.ConjuntoOrdenadoVetorial,
  FractalLotus.Fundacao.Colecoes.ConjuntoOrdenadoEncadeado;

{ TTestadorConjunto }

function TTestadorConjunto.conjunto: IDistinguivel<Integer>;
begin
  Result := colecao as IDistinguivel<Integer>;
end;

procedure TTestadorConjunto.testarExcecao_1;
const
  VETOR: TArray<Integer> = [1, 3, 5, 7, 9];
var
  i: Integer;
  pilha: IEmpilhavel<Integer>;
begin
  pilha := TPilhaVetorial<Integer>.Create(10);
  try
    conjunto.limpar;

    for i := 0 to 9 do
    begin
      conjunto.adicionar(i);
      pilha   .empilhar (i * 2);
    end;

    conjunto.excetuar(pilha);
    Assert  .IsTrue( validarConjunto(VETOR) );
  finally
    pilha := nil;
  end;
end;

procedure TTestadorConjunto.testarExcecao_2;
const
  VETOR: TArray<Integer> = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
var
  i: Integer;
  pilha: IEmpilhavel<Integer>;
begin
  pilha := TPilhaVetorial<Integer>.Create(10);
  try
    conjunto.limpar;

    for i := 0 to 9 do
      conjunto.adicionar(i);

    conjunto.excetuar(pilha);
    Assert  .IsTrue( validarConjunto(VETOR) );
  finally
    pilha := nil;
  end;
end;

procedure TTestadorConjunto.testarExcecao_3;
const
  VETOR: TArray<Integer> = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
var
  i: Integer;
  pilha: IEmpilhavel<Integer>;
begin
  pilha := nil;
  try
    conjunto.limpar;

    for i := 0 to 9 do
      conjunto.adicionar(i);

    conjunto.excetuar(pilha);
    Assert  .IsTrue( validarConjunto(VETOR) );
  finally
    pilha := nil;
  end;
end;

procedure TTestadorConjunto.testarExclusao;
var
  index,
  valor: Integer;
  itens: TArray<Integer>;
begin
  conjunto.limpar;

  if not colecao.vazio then
    Assert.Fail('O conjunto devia estar vazio.');

  for valor in vetor.valores do
    conjunto.adicionar(valor);

  SetLength(itens, colecao.total);
  index := 0;

  for valor in vetor.aleatorio do
    if conjunto.remover(valor) then
    begin
      itens[index] := valor;
      Inc(index);
    end;

  if not colecao.vazio then
    Assert.Fail('O conjunto devia estar vazio.');

  Assert.IsTrue(vetor.validarConteudo(itens));
end;

procedure TTestadorConjunto.testarInsercao;
var
  valor: Integer;
begin
  conjunto.limpar;

  if not colecao.vazio then
    Assert.Fail('O conjunto devia estar vazio.');

  for valor in vetor.valores do
    conjunto.adicionar(valor);

  Assert.IsTrue(validarInsercao);
end;

procedure TTestadorConjunto.testarInterseccao_1;
const
  VETOR: TArray<Integer> = [0, 2, 4, 6, 8];
var
  i: Integer;
  pilha: IEmpilhavel<Integer>;
begin
  pilha := TPilhaVetorial<Integer>.Create(10);
  try
    conjunto.limpar;

    for i := 0 to 9 do
    begin
      conjunto.adicionar(i);
      pilha   .empilhar (i * 2);
    end;

    conjunto.interseccionar(pilha);
    Assert  .IsTrue( validarConjunto(VETOR) );
  finally
    pilha := nil;
  end;
end;

procedure TTestadorConjunto.testarInterseccao_2;
const
  VETOR: TArray<Integer> = [];
var
  i: Integer;
  pilha: IEmpilhavel<Integer>;
begin
  pilha := TPilhaVetorial<Integer>.Create(10);
  try
    conjunto.limpar;

    for i := 0 to 9 do
      conjunto.adicionar(i);

    conjunto.interseccionar(pilha);
    Assert  .IsTrue( validarConjunto(VETOR) );
  finally
    pilha := nil;
  end;
end;

procedure TTestadorConjunto.testarInterseccao_3;
const
  VETOR: TArray<Integer> = [];
var
  i: Integer;
  pilha: IEmpilhavel<Integer>;
begin
  pilha := TPilhaVetorial<Integer>.Create(10);
  try
    conjunto.limpar;

    for i := 0 to 9 do
      conjunto.adicionar(i);

    conjunto.interseccionar(pilha);
    Assert  .IsTrue( validarConjunto(VETOR) );
  finally
    pilha := nil;
  end;
end;

procedure TTestadorConjunto.testarUniao_1;
const
  VETOR: TArray<Integer> = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 18];
var
  i: Integer;
  pilha: IEmpilhavel<Integer>;
begin
  pilha := TPilhaVetorial<Integer>.Create(10);
  try
    conjunto.limpar;

    for i := 0 to 9 do
    begin
      conjunto.adicionar(i);
      pilha   .empilhar (i * 2);
    end;

    conjunto.unir(pilha);
    Assert  .IsTrue( validarConjunto(VETOR) );
  finally
    pilha := nil;
  end;
end;

procedure TTestadorConjunto.testarUniao_2;
const
  VETOR: TArray<Integer> = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
var
  i: Integer;
  pilha: IEmpilhavel<Integer>;
begin
  pilha := TPilhaVetorial<Integer>.Create(10);
  try
    conjunto.limpar;

    for i := 0 to 9 do
      conjunto.adicionar(i);

    conjunto.unir(pilha);
    Assert  .IsTrue( validarConjunto(VETOR) );
  finally
    pilha := nil;
  end;
end;

procedure TTestadorConjunto.testarUniao_3;
const
  VETOR: TArray<Integer> = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
var
  i: Integer;
  pilha: IEmpilhavel<Integer>;
begin
  pilha := nil;
  try
    conjunto.limpar;

    for i := 0 to 9 do
      conjunto.adicionar(i);

    conjunto.unir(pilha);
    Assert  .IsTrue( validarConjunto(VETOR) );
  finally
    pilha := nil;
  end;
end;

function TTestadorConjunto.validarConjunto(
  const itens: TArray<Integer>): Boolean;
var
  i, total: Integer;
  valores : TArray<Integer>;
begin
  total   := colecao.total;
  Result  := Length(itens) = total;
  valores := colecao.ToArray;

  if Result then
  begin
    TArray.Sort<Integer>(valores);

    for i := 0 to total - 1 do
      if not Result then
        Break
      else
        Result := itens[i] = valores[i];
  end;
end;

{ TTestadorConjuntoHash }

function TTestadorConjuntoHash.validarInsercao: Boolean;
begin
  Result := vetor.validarConteudo(colecao);
end;

{ TTestadorConjuntoOrdenado }

function TTestadorConjuntoOrdenado.validarInsercao: Boolean;
begin
  Result := vetor.validarCrescente(colecao);
end;

{ TTestadorConjuntoHashEncadeado }

function TTestadorConjuntoHashEncadeado.criarColecao: IEnumeravel<Integer>;
begin
  Result := TConjuntoHashEncadeado<Integer>.Create(vetor.total);
end;

{ TTestadorConjuntoHashSondado }

function TTestadorConjuntoHashSondado.criarColecao: IEnumeravel<Integer>;
begin
  Result := TConjuntoHashSondado<Integer>.Create(vetor.total);
end;

{ TTestadorConjuntoOrdenadoVetorial }

function TTestadorConjuntoOrdenadoVetorial.criarColecao: IEnumeravel<Integer>;
begin
  Result := TConjuntoOrdenadoVetorial<Integer>.Create(vetor.total);
end;

{ TTestadorConjuntoOrdenadoEncadeado }

function TTestadorConjuntoOrdenadoEncadeado.criarColecao: IEnumeravel<Integer>;
begin
  Result := TConjuntoOrdenadoEncadeado<Integer>.Create;
end;

end.
