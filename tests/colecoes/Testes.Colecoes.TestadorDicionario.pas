unit Testes.Colecoes.TestadorDicionario;

interface
uses
  DUnitX.TestFramework,
  Testes.Colecoes.TestadorEnumeravel,
  FractalLotus.Fundacao.Cerne.Colecoes;

type
  {$region 'Abstrações'}
  TTestadorDicionario = class abstract (TTestadorEnumeravel)
  private
    fDicionario: IDicionarizavel<Integer, Double>;
  protected
    function criarColecao   : IEnumeravel<Integer>; override;
    function criarDicionario: IDicionarizavel<Integer, Double>; virtual; abstract;
    function validarInsercao: Boolean;                          virtual; abstract;
  public
    [Test]
    procedure testarInsercao;
    [Test]
    procedure testarRemocao;
  end;

  TTestadorDicionarioHash = class abstract (TTestadorDicionario)
  protected
    function validarInsercao: Boolean; override;
  end;

  TTestadorDicionarioOrdenado = class abstract (TTestadorDicionario)
  protected
    function validarInsercao: Boolean; override;
  end;
  {$endregion}

  {$region 'Dicionários Hash'}
  [TestFixture]
  TTestadorDicionarioHashSondado = class (TTestadorDicionarioHash)
  protected
    function criarDicionario: IDicionarizavel<Integer, Double>; override;
  end;

  [TestFixture]
  TTestadorDicionarioHashEncadeado = class (TTestadorDicionarioHash)
  protected
    function criarDicionario: IDicionarizavel<Integer, Double>; override;
  end;
  {$endregion}

  {$region 'Dicionários Ordenados'}
  [TestFixture]
  TTestadorDicionarioOrdenadoVetorial = class (TTestadorDicionarioOrdenado)
  protected
    function criarDicionario: IDicionarizavel<Integer, Double>; override;
  end;

  [TestFixture]
  TTestadorDicionarioOrdenadoEncadeado = class (TTestadorDicionarioOrdenado)
  protected
    function criarDicionario: IDicionarizavel<Integer, Double>; override;
  end;
  {$endregion}

implementation
uses
  FractalLotus.Fundacao.Colecoes.DicionarioHashSondado,
  FractalLotus.Fundacao.Colecoes.DicionarioHashEncadeado,
  FractalLotus.Fundacao.Colecoes.DicionarioOrdenadoVetorial,
  FractalLotus.Fundacao.Colecoes.DicionarioOrdenadoEncadeado;

{ TTestadorDicionario }

function TTestadorDicionario.criarColecao: IEnumeravel<Integer>;
begin
  fDicionario := criarDicionario;
  Result      := fDicionario.chaves;
end;

procedure TTestadorDicionario.testarInsercao;
var
  valor: Integer;
begin
  fDicionario.limpar;

  if not fDicionario.vazio then
    Assert.Fail('O dicionário devia estar vazio.');

  for valor in vetor.valores do
    fDicionario.adicionar(valor, valor / 100);

  Assert.IsTrue(validarInsercao);
end;

procedure TTestadorDicionario.testarRemocao;
var
  index,
  valor: Integer;
  itens: TArray<Integer>;
begin
  fDicionario.limpar;

  if not fDicionario.vazio then
    Assert.Fail('O dicionário devia estar vazio.');

  for valor in vetor.valores do
    fDicionario.adicionar(valor, valor / 100);

  SetLength(itens, fDicionario.total);
  index := 0;

  for valor in vetor.aleatorio do
    if fDicionario.remover(valor) then
    begin
      itens[index] := valor;
      Inc(index);
    end;

  if not fDicionario.vazio then
    Assert.Fail('O dicionário devia estar vazio.');

  Assert.IsTrue(vetor.validarConteudo(itens));
end;

{ TTestadorDicionarioHash }

function TTestadorDicionarioHash.validarInsercao: Boolean;
begin
  Result := vetor.validarConteudo(colecao);
end;

{ TTestadorDicionarioOrdenado }

function TTestadorDicionarioOrdenado.validarInsercao: Boolean;
begin
  Result := vetor.validarCrescente(colecao);
end;

{ TTestadorDicionarioHashSondado }

function TTestadorDicionarioHashSondado.criarDicionario: IDicionarizavel<Integer, Double>;
begin
  Result := TDicionarioHashSondado<Integer, Double>.Create(vetor.total);
end;

{ TTestadorDicionarioHashEncadeado }

function TTestadorDicionarioHashEncadeado.criarDicionario: IDicionarizavel<Integer, Double>;
begin
  Result := TDicionarioHashEncadeado<Integer, Double>.Create(vetor.total);
end;

{ TTestadorDicionarioOrdenadoVetorial }

function TTestadorDicionarioOrdenadoVetorial.criarDicionario: IDicionarizavel<Integer, Double>;
begin
  Result := TDicionarioOrdenadoVetorial<Integer, Double>.Create(vetor.total);
end;

{ TTestadorDicionarioOrdenadoEncadeado }

function TTestadorDicionarioOrdenadoEncadeado.criarDicionario: IDicionarizavel<Integer, Double>;
begin
  Result := TDicionarioOrdenadoEncadeado<Integer, Double>.Create;
end;

end.
