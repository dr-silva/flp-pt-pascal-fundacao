unit Testes.Colecoes.TestadorFila;

interface
uses
  DUnitX.TestFramework,
  Testes.Colecoes.TestadorEnumeravel,
  FractalLotus.Fundacao.Cerne.Colecoes;

type
  {$region 'Abstra��es'}
  TTestadorFila = class abstract (TTestadorEnumeravel)
  protected
    function fila: IEnfileiravel<Integer>; inline;

    function validarEnfileiramento   (const itens: TArray<Integer>): Boolean; virtual;
    function validarDesenfileiramento(const itens: TArray<Integer>): Boolean; virtual;
  public
    [Test]
    procedure testarEnfileiramento;
    [Test]
    procedure testarDesenfileiramento;
  end;

  TTestadorFilaPrioridadeMinima = class abstract (TTestadorFila)
  protected
    function validarDesenfileiramento(const itens: TArray<Integer>): Boolean; override;
  end;

  TTestadorFilaPrioridadeMaxima = class abstract (TTestadorFila)
  protected
    function validarDesenfileiramento(const itens: TArray<Integer>): Boolean; override;
  end;
  {$endregion}

  {$region 'Filas Normais'}
  [TestFixture]
  TTestadorFilaVetorial = class(TTestadorFila)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;

  [TestFixture]
  TTestadorFilaEncadeada = class(TTestadorFila)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;
  {$endregion}

  {$region 'Filas Prioridade M�nima'}
  [TestFixture]
  TTestadorFilaPrioridadeMinimaVetorial = class(TTestadorFilaPrioridadeMinima)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;

  [TestFixture]
  TTestadorFilaPrioridadeMinimaEncadeada = class(TTestadorFilaPrioridadeMinima)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;
  {$endregion}

  {$region 'Filas Prioridade M�xima'}
  [TestFixture]
  TTestadorFilaPrioridadeMaximaVetorial = class(TTestadorFilaPrioridadeMaxima)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;

  [TestFixture]
  TTestadorFilaPrioridadeMaximaEncadeada = class(TTestadorFilaPrioridadeMaxima)
  protected
    function criarColecao: IEnumeravel<Integer>; override;
  end;
  {$endregion}

implementation
uses
  FractalLotus.Fundacao.Colecoes.FilaVetorial,
  FractalLotus.Fundacao.Colecoes.FilaEncadeada,
  FractalLotus.Fundacao.Colecoes.FilaPrioridadeVetorial,
  FractalLotus.Fundacao.Colecoes.FilaPrioridadeEncadeada;

{ TTestadorFila }

function TTestadorFila.fila: IEnfileiravel<Integer>;
begin
  Result := colecao as IEnfileiravel<Integer>;
end;

procedure TTestadorFila.testarDesenfileiramento;
var
  index,
  valor: Integer;
  itens: TArray<Integer>;
begin
  fila.limpar;

  if not colecao.vazio then
    Assert.Fail('A cole��o devia estar vazia.');

  for valor in vetor.valores do
    fila.enfileirar(valor);

  SetLength(itens, colecao.total);
  index := 0;

  while not colecao.vazio do
  begin
    itens[index] := fila.desenfileirar;
    Inc(index);
  end;

  Assert.IsTrue(validarDesenfileiramento(itens));
end;

procedure TTestadorFila.testarEnfileiramento;
var
  valor: Integer;
begin
  fila.limpar;

  if not colecao.vazio then
    Assert.Fail('A cole��o devia estar vazia.');

  for valor in vetor.valores do
    fila.enfileirar(valor);

  Assert.IsTrue(validarEnfileiramento(colecao.ToArray));
end;

function TTestadorFila.validarDesenfileiramento(
  const itens: TArray<Integer>): Boolean;
begin
  Result := vetor.validarValores(itens);
end;

function TTestadorFila.validarEnfileiramento(
  const itens: TArray<Integer>): Boolean;
begin
  Result := vetor.validarConteudo(itens);
end;

{ TTestadorFilaPrioridadeMinima }

function TTestadorFilaPrioridadeMinima.validarDesenfileiramento(
  const itens: TArray<Integer>): Boolean;
begin
  Result := vetor.validarCrescente(itens);
end;

{ TTestadorFilaPrioridadeMaxima }

function TTestadorFilaPrioridadeMaxima.validarDesenfileiramento(
  const itens: TArray<Integer>): Boolean;
begin
  Result := vetor.validarDecrescente(itens);
end;

{ TTestadorFilaVetorial }

function TTestadorFilaVetorial.criarColecao: IEnumeravel<Integer>;
begin
  Result := TFilaVetorial<Integer>.Create(vetor.total);
end;

{ TTestadorFilaEncadeada }

function TTestadorFilaEncadeada.criarColecao: IEnumeravel<Integer>;
begin
  Result := TFilaEncadeada<Integer>.Create;
end;

{ TTestadorFilaPrioridadeMinimaVetorial }

function TTestadorFilaPrioridadeMinimaVetorial.criarColecao: IEnumeravel<Integer>;
begin
  Result := TFilaPrioridadeMinimaVetorial<Integer>.Create(vetor.total);
end;

{ TTestadorFilaPrioridadeMinimaEncadeada }

function TTestadorFilaPrioridadeMinimaEncadeada.criarColecao: IEnumeravel<Integer>;
begin
  Result := TFilaPrioridadeMinimaEncadeada<Integer>.Create;
end;

{ TTestadorFilaPrioridadeMaximaVetorial }

function TTestadorFilaPrioridadeMaximaVetorial.criarColecao: IEnumeravel<Integer>;
begin
  Result := TFilaPrioridadeMaximaVetorial<Integer>.Create(vetor.total);
end;

{ TTestadorFilaPrioridadeMaximaEncadeada }

function TTestadorFilaPrioridadeMaximaEncadeada.criarColecao: IEnumeravel<Integer>;
begin
  Result := TFilaPrioridadeMaximaEncadeada<Integer>.Create;
end;

end.
