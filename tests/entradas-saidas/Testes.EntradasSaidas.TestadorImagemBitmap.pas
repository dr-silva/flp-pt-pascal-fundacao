unit Testes.EntradasSaidas.TestadorImagemBitmap;

interface
uses
  DUnitX.TestFramework,
  FractalLotus.Fundacao.EntradasSaidas.ImagemBitmap;

type
  [TestFixture]
  TTestadorImagemBitmap = class
  private
    procedure validarImagens(const ativo, passivo: TImagemBitmap);
  public
    [Test]
    procedure testarNegativo;
    [Test]
    procedure testarEspelhoHorizontal;
    [Test]
    procedure testarEspelhoVertical;
    [Test]
    procedure testarRotacaoEsquerda;
    [Test]
    procedure testarRotacaoDireita;
  end;

implementation

const
  CAMINHO    = '..\..\..\..\tests\recursos\';
  ORIGINAL_1 = CAMINHO + 'original-1.png';
  ORIGINAL_2 = CAMINHO + 'original-2.png';
  NEGATIVO   = CAMINHO + 'neg.png';
  HORIZONTAL = CAMINHO + 'esp-horz.png';
  VERTICAL   = CAMINHO + 'esp-vert.png';
  ESQUERDA   = CAMINHO + 'rot-esq.png';
  DIREITA    = CAMINHO + 'rot-dir.png';

{ TTestadorImagemBitmap }

procedure TTestadorImagemBitmap.testarEspelhoHorizontal;
var
  ativo  ,
  passivo: TImagemBitmap;
begin
  ativo   := TImagemBitmap.Create(ORIGINAL_2);
  passivo := TImagemBitmap.Create(HORIZONTAL);

  try
    ativo.espelharHorizontalmente;

    validarImagens(ativo, passivo);
  finally
    ativo  .Free;
    passivo.Free;
  end;
end;

procedure TTestadorImagemBitmap.testarEspelhoVertical;
var
  ativo  ,
  passivo: TImagemBitmap;
begin
  ativo   := TImagemBitmap.Create(ORIGINAL_1);
  passivo := TImagemBitmap.Create(VERTICAL);

  try
    ativo.espelharVerticalmente;

    validarImagens(ativo, passivo);
  finally
    ativo  .Free;
    passivo.Free;
  end;
end;

procedure TTestadorImagemBitmap.testarNegativo;
var
  ativo  ,
  passivo: TImagemBitmap;
begin
  ativo   := TImagemBitmap.Create(ORIGINAL_1);
  passivo := TImagemBitmap.Create(NEGATIVO);

  try
    ativo.negativar;

    validarImagens(ativo, passivo);
  finally
    ativo  .Free;
    passivo.Free;
  end;
end;

procedure TTestadorImagemBitmap.testarRotacaoDireita;
var
  ativo  ,
  passivo: TImagemBitmap;
begin
  ativo   := TImagemBitmap.Create(ORIGINAL_1);
  passivo := TImagemBitmap.Create(DIREITA);

  try
    ativo.rotarParaDireita;

    validarImagens(ativo, passivo);
  finally
    ativo  .Free;
    passivo.Free;
  end;
end;

procedure TTestadorImagemBitmap.testarRotacaoEsquerda;
var
  ativo  ,
  passivo: TImagemBitmap;
begin
  ativo   := TImagemBitmap.Create(ORIGINAL_1);
  passivo := TImagemBitmap.Create(ESQUERDA);

  try
    ativo.rotarParaEsquerda;

    validarImagens(ativo, passivo);
  finally
    ativo  .Free;
    passivo.Free;
  end;
end;

procedure TTestadorImagemBitmap.validarImagens(const ativo,
  passivo: TImagemBitmap);
var
  isTrue: Boolean;
  linha ,
  coluna: Integer;
begin
  isTrue := (ativo.altura  = passivo.altura) and
            (ativo.largura = passivo.largura);

  for linha := 0 to ativo.altura - 1 do
    for coluna := 0 to ativo.largura - 1 do
      if not isTrue then
        Assert.Fail
      else
        isTrue := ativo[linha, coluna] = passivo[linha, coluna];
end;

end.
