unit Testes.Ordenacoes.TestadorAleatorio;

interface
uses
  DUnitX.TestFramework,
  Testes.Ordenacoes.TestadorOrdenacao,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

type
  [TestFixture]
  TTestadorAleatorio = class
  private
    fValidador: IValidadorOrdenacao<Integer>;
    fOrdenador: IOrdenacao         <Integer>;

    function criarVetor(const total: Integer): TArray<Integer>;
    function ordenar   (const total: Integer): Boolean;
  public
    constructor Create;
    destructor  Destroy; override;

    [Test]
    procedure testarNulo;
    [Test]
    procedure testarVazio;
    [Test]
    procedure testarNaoVazio;
  end;

implementation
uses
  FractalLotus.Fundacao.Ordenacoes.Validador,
  FractalLotus.Fundacao.Ordenacoes.Aleatorio;

{ TTestadorAleatorio }

constructor TTestadorAleatorio.Create;
begin
  fValidador := TValidadorOrdenacaoAscendente<Integer>.Create;
  fOrdenador := TOrdenacaoAleatoria          <Integer>.Create;
end;

function TTestadorAleatorio.criarVetor(const total: Integer): TArray<Integer>;
var
  i: Integer;
begin
  if (total < 0) then
    Exit(nil);

  SetLength(Result, total);

  for i := 0 to total - 1 do
    Result[i] := i + 1;
end;

destructor TTestadorAleatorio.Destroy;
begin
  fValidador := nil;
  fOrdenador := nil;

  inherited Destroy;
end;

function TTestadorAleatorio.ordenar(const total: Integer): Boolean;
var
  vetor: TArray<Integer>;
begin
  vetor  := criarVetor        (total);
            fOrdenador.ordenar(vetor);
  Result := fValidador.validar(vetor);
end;

procedure TTestadorAleatorio.testarNaoVazio;
begin
  Assert.IsFalse(ordenar(100));
end;

procedure TTestadorAleatorio.testarNulo;
begin
  Assert.IsTrue(ordenar(-1));
end;

procedure TTestadorAleatorio.testarVazio;
begin
  Assert.IsTrue(ordenar(0));
end;

end.
