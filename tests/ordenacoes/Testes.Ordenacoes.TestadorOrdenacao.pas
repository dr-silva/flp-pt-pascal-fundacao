unit Testes.Ordenacoes.TestadorOrdenacao;

interface
uses
  System.Generics.Defaults,
  System.Generics.Collections,
  DUnitX.TestFramework,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

type
  TTestadorOrdenacao = class abstract
  private
    fAleatorio : IComparer  <Integer>;
    fComparador: IComparador<Integer>;

    function criarValidador(const crescente: Boolean): IValidadorOrdenacao<Integer>;
    function criarVetor    (const total    : Integer): TArray<Integer>;
  protected
    function criarOrdenador(const crescente: Boolean): IOrdenacao<Integer>; virtual; abstract;
    function ordenar(const total: Integer; const crescente: Boolean): Boolean;
  public
    constructor Create;
    destructor  Destroy; override;

    [Test]
    procedure testarCrescenteNulo;
    [Test]
    procedure testarCrescenteVazio;
    [Test]
    procedure testarCrescenteNaoVazio;
    [Test]
    procedure testarDecrescenteNulo;
    [Test]
    procedure testarDecrescenteVazio;
    [Test]
    procedure testarDecrescenteNaoVazio;
  end;

  TComparadorAleatorio = class(TInterfacedObject, IComparer<Integer>)
  private
    fDicionario: TDictionary<Integer, Double>;
  public
    constructor Create;
    destructor  Destroy; override;

    function Compare(const Left, Right: Integer): Integer;
  end;

implementation
uses
  FractalLotus.Fundacao.Ordenacoes.Validador;

{ TTestadorOrdenacao }

constructor TTestadorOrdenacao.Create;
begin
  fAleatorio  := TComparadorAleatorio.Create;
  fComparador := TFabricaComparador.criar<Integer>();
end;

function TTestadorOrdenacao.criarValidador(
  const crescente: Boolean): IValidadorOrdenacao<Integer>;
begin
  if crescente then
    Result := TValidadorOrdenacaoAscendente<Integer>.Create
  else
    Result := TValidadorOrdenacaoDescendente<Integer>.Create;
end;

function TTestadorOrdenacao.criarVetor(const total: Integer): TArray<Integer>;
var
  i: Integer;
begin
  if (total < 0) then
    Exit(nil);

  SetLength(Result, total);

  for i := 0 to total - 1 do
    Result[i] := i + 1;

  TArray.Sort<Integer>(Result, fAleatorio);
end;

destructor TTestadorOrdenacao.Destroy;
begin
  fAleatorio  := nil;
  fComparador := nil;

  inherited Destroy;
end;

function TTestadorOrdenacao.ordenar(const total: Integer;
  const crescente: Boolean): Boolean;
var
  ordenador: IOrdenacao         <Integer>;
  validador: IValidadorOrdenacao<Integer>;
  vetor    : TArray<Integer>;
begin
  ordenador := criarOrdenador(crescente);
  validador := criarValidador(crescente);
  vetor     := criarVetor    (total);
  try
    ordenador.ordenar(vetor);

    Result := validador.validar(vetor);
  finally
    ordenador := nil;
    validador := nil;
  end;
end;

procedure TTestadorOrdenacao.testarCrescenteNaoVazio;
begin
  Assert.IsTrue(ordenar(100, True));
end;

procedure TTestadorOrdenacao.testarCrescenteNulo;
begin
  Assert.IsTrue(ordenar(-1, True));
end;

procedure TTestadorOrdenacao.testarCrescenteVazio;
begin
  Assert.IsTrue(ordenar(0, True));
end;

procedure TTestadorOrdenacao.testarDecrescenteNaoVazio;
begin
  Assert.IsTrue(ordenar(100, False));
end;

procedure TTestadorOrdenacao.testarDecrescenteNulo;
begin
  Assert.IsTrue(ordenar(-1, False));
end;

procedure TTestadorOrdenacao.testarDecrescenteVazio;
begin
  Assert.IsTrue(ordenar(0, False));
end;

{ TComparadorAleatorio }

function TComparadorAleatorio.Compare(const Left, Right: Integer): Integer;
var
  valorEsq,
  valorDir: Double;
begin
  if not fDicionario.TryGetValue(Left, valorEsq) then
  begin
    valorEsq := Random;
    fDicionario.Add(Left, valorEsq);
  end;

  if not fDicionario.TryGetValue(Right, valorDir) then
  begin
    valorDir := Random;
    fDicionario.Add(Right, valorDir);
  end;

  if (valorEsq < valorDir) then
    Result := -1
  else if (valorEsq > valorDir) then
    Result := +1
  else
    Result := 0;
end;

constructor TComparadorAleatorio.Create;
begin
  fDicionario := TDictionary<Integer, Double>.Create;
end;

destructor TComparadorAleatorio.Destroy;
begin
  fDicionario.Free;

  inherited Destroy;
end;

end.
