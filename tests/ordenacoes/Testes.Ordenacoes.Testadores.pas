unit Testes.Ordenacoes.Testadores;

interface
uses
  DUnitX.TestFramework,
  Testes.Ordenacoes.TestadorOrdenacao,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

type
  [TestFixture]
  TTestadorInsercao = class(TTestadorOrdenacao)
  protected
    function criarOrdenador(const crescente: Boolean): IOrdenacao<Integer>; override;
  end;

  [TestFixture]
  TTestadorSelecao = class(TTestadorOrdenacao)
  protected
    function criarOrdenador(const crescente: Boolean): IOrdenacao<Integer>; override;
  end;

  [TestFixture]
  TTestadorShell = class(TTestadorOrdenacao)
  protected
    function criarOrdenador(const crescente: Boolean): IOrdenacao<Integer>; override;
  end;

  [TestFixture]
  TTestadorHeap = class(TTestadorOrdenacao)
  protected
    function criarOrdenador(const crescente: Boolean): IOrdenacao<Integer>; override;
  end;

  [TestFixture]
  TTestadorMerge = class(TTestadorOrdenacao)
  protected
    function criarOrdenador(const crescente: Boolean): IOrdenacao<Integer>; override;
  end;

  [TestFixture]
  TTestadorQuick = class(TTestadorOrdenacao)
  protected
    function criarOrdenador(const crescente: Boolean): IOrdenacao<Integer>; override;
  end;

  [TestFixture]
  TTestadorQuick3Way = class(TTestadorOrdenacao)
  protected
    function criarOrdenador(const crescente: Boolean): IOrdenacao<Integer>; override;
  end;

  [TestFixture]
  TTestadorContagem = class(TTestadorOrdenacao)
  protected
    function criarOrdenador(const crescente: Boolean): IOrdenacao<Integer>; override;
  end;

  [TestFixture]
  TTestadorBalde = class(TTestadorOrdenacao)
  protected
    function criarOrdenador(const crescente: Boolean): IOrdenacao<Integer>; override;
  end;

implementation
uses
  FractalLotus.Fundacao.Ordenacoes.Insercao,
  FractalLotus.Fundacao.Ordenacoes.Selecao,
  FractalLotus.Fundacao.Ordenacoes.Shell,
  FractalLotus.Fundacao.Ordenacoes.Heap,
  FractalLotus.Fundacao.Ordenacoes.Merge,
  FractalLotus.Fundacao.Ordenacoes.Quick,
  FractalLotus.Fundacao.Ordenacoes.Quick3Way,
  FractalLotus.Fundacao.Ordenacoes.Contagem,
  FractalLotus.Fundacao.Ordenacoes.Balde;

{ TTestadorInsercao }

function TTestadorInsercao.criarOrdenador(
  const crescente: Boolean): IOrdenacao<Integer>;
begin
  if crescente then
    Result := TOrdenacaoPorInsercaoAscendente<Integer>.Create
  else
    Result := TOrdenacaoPorInsercaoDescendente<Integer>.Create;
end;

{ TTestadorSelecao }

function TTestadorSelecao.criarOrdenador(
  const crescente: Boolean): IOrdenacao<Integer>;
begin
  if crescente then
    Result := TOrdenacaoPorSelecaoAscendente<Integer>.Create
  else
    Result := TOrdenacaoPorSelecaoDescendente<Integer>.Create;
end;

{ TTestadorShell }

function TTestadorShell.criarOrdenador(
  const crescente: Boolean): IOrdenacao<Integer>;
begin
  if crescente then
    Result := TOrdenacaoShellAscendente<Integer>.Create
  else
    Result := TOrdenacaoShellDescendente<Integer>.Create;
end;

{ TTestadorHeap }

function TTestadorHeap.criarOrdenador(
  const crescente: Boolean): IOrdenacao<Integer>;
begin
  if crescente then
    Result := TOrdenacaoHeapAscendente<Integer>.Create
  else
    Result := TOrdenacaoHeapDescendente<Integer>.Create;
end;

{ TTestadorMerge }

function TTestadorMerge.criarOrdenador(
  const crescente: Boolean): IOrdenacao<Integer>;
begin
  if crescente then
    Result := TOrdenacaoMergeAscendente<Integer>.Create
  else
    Result := TOrdenacaoMergeDescendente<Integer>.Create;
end;

{ TTestadorQuick }

function TTestadorQuick.criarOrdenador(
  const crescente: Boolean): IOrdenacao<Integer>;
begin
  if crescente then
    Result := TOrdenacaoQuickAscendente<Integer>.Create
  else
    Result := TOrdenacaoQuickDescendente<Integer>.Create;
end;

{ TTestadorQuick3Way }

function TTestadorQuick3Way.criarOrdenador(
  const crescente: Boolean): IOrdenacao<Integer>;
begin
  if crescente then
    Result := TOrdenacaoQuick3WayAscendente<Integer>.Create
  else
    Result := TOrdenacaoQuick3WayDescendente<Integer>.Create;
end;

{ TTestadorContagem }

function projetarContagem(const valor: Integer): Integer;
begin
  Result := valor;
end;

function TTestadorContagem.criarOrdenador(
  const crescente: Boolean): IOrdenacao<Integer>;
begin
  if crescente then
    Result := TOrdenacaoPorContagemAscendente<Integer>.Create(projetarContagem)
  else
    Result := TOrdenacaoPorContagemDescendente<Integer>.Create(projetarContagem);
end;

{ TTestadorBalde }

function projetarBalde(const valor: Integer): Double;
begin
  Result := valor / 101;
end;

function TTestadorBalde.criarOrdenador(
  const crescente: Boolean): IOrdenacao<Integer>;
begin
  if crescente then
    Result := TOrdenacaoPorBaldeAscendente<Integer>.Create(projetarBalde)
  else
    Result := TOrdenacaoPorBaldeDescendente<Integer>.Create(projetarBalde);
end;

end.
