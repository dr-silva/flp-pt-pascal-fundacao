unit Testes.Matematica.TestadorComplexo;

interface
uses
  DUnitX.TestFramework,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes,
  FractalLotus.Fundacao.Matematica.Complexo;

type
  [TestFixture]
  TTestadorComplexo = class
  private const
    ESQUERDA: TComplexo = (re:  2; im:  3);
    DIREITA : TComplexo = (re: -4; im: -4);
    INTEIRO : Integer   = 2;
    REAL    : Double    = 3.0;
  private
    fComparador: IComparadorIgualdade<TComplexo>;

    procedure validar(const esperado, calculado: TComplexo);
  public
    constructor Create;
    destructor  Destroy; override;

    [Test]
    procedure testarPositivo;
    [Test]
    procedure testarNegativo;
    [Test]
    procedure testarAdicao_1;
    [Test]
    procedure testarAdicao_2;
    [Test]
    procedure testarAdicao_3;
    [Test]
    procedure testarAdicao_4;
    [Test]
    procedure testarAdicao_5;
    [Test]
    procedure testarSubtracao_1;
    [Test]
    procedure testarSubtracao_2;
    [Test]
    procedure testarSubtracao_3;
    [Test]
    procedure testarSubtracao_4;
    [Test]
    procedure testarSubtracao_5;
    [Test]
    procedure testarMultiplicaca_1;
    [Test]
    procedure testarMultiplicaca_2;
    [Test]
    procedure testarMultiplicaca_3;
    [Test]
    procedure testarMultiplicaca_4;
    [Test]
    procedure testarMultiplicaca_5;
    [Test]
    procedure testarDivisao_1;
    [Test]
    procedure testarDivisao_2;
    [Test]
    procedure testarDivisao_3;
    [Test]
    procedure testarDivisao_4;
    [Test]
    procedure testarDivisao_5;
    [Test]
    procedure testarDivisaoZero_1;
    [Test]
    procedure testarDivisaoZero_2;
    [Test]
    procedure testarDivisaoZero_3;
    [Test]
    procedure testarExponenciacao_1;
    [Test]
    procedure testarExponenciacao_2;
    [Test]
    procedure testarLogaritmo_1;
    [Test]
    procedure testarLogaritmo_2;
    [Test]
    procedure testarSeno;
    [Test]
    procedure testarCosseno;
    [Test]
    procedure testarTangente;
    [Test]
    procedure testarCossecante;
    [Test]
    procedure testarSecante;
    [Test]
    procedure testarCotangente;
    [Test]
    procedure testarSenoHiperbolico;
    [Test]
    procedure testarCossenoHiperbolico;
    [Test]
    procedure testarTangenteHiperbolica;
    [Test]
    procedure testarCossecanteHiperbolica;
    [Test]
    procedure testarSecanteHiperbolica;
    [Test]
    procedure testarCotangenteHiperbolica;
  end;

implementation
uses
  System.SysUtils;

{ TTestadorComplexo }

constructor TTestadorComplexo.Create;
begin
  fComparador := TComplexo.comparadorIgualdade;
end;

destructor TTestadorComplexo.Destroy;
begin
  fComparador := nil;

  inherited Destroy;
end;

procedure TTestadorComplexo.testarAdicao_1;
const
  ESPERADO: TComplexo = (re: -2; im: -4);
begin
  validar(ESPERADO, INTEIRO + DIREITA);
end;

procedure TTestadorComplexo.testarAdicao_2;
const
  ESPERADO: TComplexo = (re: -1; im: -4);
begin
  validar(ESPERADO, REAL + DIREITA);
end;

procedure TTestadorComplexo.testarAdicao_3;
const
  ESPERADO: TComplexo = (re: 4; im: 3);
begin
  validar(ESPERADO, ESQUERDA + INTEIRO);
end;

procedure TTestadorComplexo.testarAdicao_4;
const
  ESPERADO: TComplexo = (re: 5; im: 3);
begin
  validar(ESPERADO, ESQUERDA + REAL);
end;

procedure TTestadorComplexo.testarAdicao_5;
const
  ESPERADO: TComplexo = (re: -2; im: -1);
begin
  validar(ESPERADO, ESQUERDA + DIREITA);
end;

procedure TTestadorComplexo.testarCossecante;
const
  ESPERADO: TComplexo = (re: 0.09047321; im: 0.041200986);
begin
  validar(ESPERADO, TComplexo.csc(ESQUERDA));
end;

procedure TTestadorComplexo.testarCossecanteHiperbolica;
const
  ESPERADO: TComplexo = (re: -0.272548661; im: -0.040300579);
begin
  validar(ESPERADO, TComplexo.csch(ESQUERDA));
end;

procedure TTestadorComplexo.testarCosseno;
const
  ESPERADO: TComplexo = (re: -4.189625691; im: -9.109227894);
begin
  validar(ESPERADO, TComplexo.cos(ESQUERDA));
end;

procedure TTestadorComplexo.testarCossenoHiperbolico;
const
  ESPERADO: TComplexo = (re: -3.724545505; im: 0.51182257);
begin
  validar(ESPERADO, TComplexo.cosh(ESQUERDA));
end;

procedure TTestadorComplexo.testarCotangente;
const
  ESPERADO: TComplexo = (re: -0.00373971; im: -0.996757797);
begin
  validar(ESPERADO, TComplexo.cot(ESQUERDA));
end;

procedure TTestadorComplexo.testarCotangenteHiperbolica;
const
  ESPERADO: TComplexo = (re: 1.035746638; im: 0.010604783);
begin
  validar(ESPERADO, TComplexo.coth(ESQUERDA));
end;

procedure TTestadorComplexo.testarDivisaoZero_1;
const
  ZERO: Integer = 0;
var
  calculado: TComplexo;
begin
  calculado := ESQUERDA / ZERO;

  if not TComplexo.eNaN(calculado) then
    Assert.Fail('Divis�o por 0 devia retornar um valor n�o-num�rico');
end;

procedure TTestadorComplexo.testarDivisaoZero_2;
const
  ZERO: Double = 0;
var
  calculado: TComplexo;
begin
  calculado := ESQUERDA / ZERO;

  if not TComplexo.eNaN(calculado) then
    Assert.Fail('Divis�o por 0 devia retornar um valor n�o-num�rico');
end;

procedure TTestadorComplexo.testarDivisaoZero_3;
var
  calculado: TComplexo;
begin
  calculado := ESQUERDA / TComplexo.ZERO;

  if not TComplexo.eNaN(calculado) then
    Assert.Fail('Divis�o por 0 devia retornar um valor n�o-num�rico');
end;

procedure TTestadorComplexo.testarDivisao_1;
const
  ESPERADO: TComplexo = (re: -0.25; im: 0.25);
begin
  validar(ESPERADO, INTEIRO / DIREITA);
end;

procedure TTestadorComplexo.testarDivisao_2;
const
  ESPERADO: TComplexo = (re: -0.375; im: 0.375);
begin
  validar(ESPERADO, REAL / DIREITA);
end;

procedure TTestadorComplexo.testarDivisao_3;
const
  ESPERADO: TComplexo = (re: 1; im: 1.5);
begin
  validar(ESPERADO, ESQUERDA / INTEIRO);
end;

procedure TTestadorComplexo.testarDivisao_4;
const
  ESPERADO: TComplexo = (re: 0.6666666667; im: 1);
begin
  validar(ESPERADO, ESQUERDA / REAL);
end;

procedure TTestadorComplexo.testarDivisao_5;
const
  ESPERADO: TComplexo = (re: -0.625; im: -0.125);
begin
  validar(ESPERADO, ESQUERDA / DIREITA);
end;

procedure TTestadorComplexo.testarExponenciacao_1;
const
  ESPERADO: TComplexo = (re: -7.315110095; im: 1.042743656);
begin
  validar(ESPERADO, TComplexo.exp(ESQUERDA));
end;

procedure TTestadorComplexo.testarExponenciacao_2;
const
  ESPERADO: TComplexo = (re: -0.281851193; im: -0.107283263);
begin
  validar(ESPERADO, TComplexo.exp(ESQUERDA, DIREITA));
end;

procedure TTestadorComplexo.testarLogaritmo_1;
const
  ESPERADO: TComplexo = (re: 1.282474679; im: 0.982793723);
begin
  validar(ESPERADO, TComplexo.ln(ESQUERDA));
end;

procedure TTestadorComplexo.testarLogaritmo_2;
const
  ESPERADO: TComplexo = (re: -0.035736229; im: -1.809839357);
begin
  validar(ESPERADO, TComplexo.log(ESQUERDA, DIREITA));
end;

procedure TTestadorComplexo.testarMultiplicaca_1;
const
  ESPERADO: TComplexo = (re: -8; im: -8);
begin
  validar(ESPERADO, INTEIRO * DIREITA);
end;

procedure TTestadorComplexo.testarMultiplicaca_2;
const
  ESPERADO: TComplexo = (re: -12; im: -12);
begin
  validar(ESPERADO, REAL * DIREITA);
end;

procedure TTestadorComplexo.testarMultiplicaca_3;
const
  ESPERADO: TComplexo = (re: 4; im: 6);
begin
  validar(ESPERADO, ESQUERDA * INTEIRO);
end;

procedure TTestadorComplexo.testarMultiplicaca_4;
const
  ESPERADO: TComplexo = (re: 6; im: 9);
begin
  validar(ESPERADO, ESQUERDA * REAL);
end;

procedure TTestadorComplexo.testarMultiplicaca_5;
const
  ESPERADO: TComplexo = (re: 4; im: -20);
begin
  validar(ESPERADO, ESQUERDA * DIREITA);
end;

procedure TTestadorComplexo.testarNegativo;
const
  ESPERADO: TComplexo = (re: -2; im: -3);
begin
  validar(ESPERADO, -ESQUERDA);
end;

procedure TTestadorComplexo.testarPositivo;
const
  ESPERADO: TComplexo = (re: 2; im: 3);
begin
  validar(ESPERADO, +ESQUERDA);
end;

procedure TTestadorComplexo.testarSecante;
const
  ESPERADO: TComplexo = (re: -0.041674964; im: 0.090611137);
begin
  validar(ESPERADO, TComplexo.sec(ESQUERDA));
end;

procedure TTestadorComplexo.testarSecanteHiperbolica;
const
  ESPERADO: TComplexo = (re: -0.263512975; im: -0.036211637);
begin
  validar(ESPERADO, TComplexo.sech(ESQUERDA));
end;

procedure TTestadorComplexo.testarSeno;
const
  ESPERADO: TComplexo = (re: 9.154499147; im: -4.16890696);
begin
  validar(ESPERADO, TComplexo.sen(ESQUERDA));
end;

procedure TTestadorComplexo.testarSenoHiperbolico;
const
  ESPERADO: TComplexo = (re: -3.59056459; im: 0.530921086);
begin
  validar(ESPERADO, TComplexo.senh(ESQUERDA));
end;

procedure TTestadorComplexo.testarSubtracao_1;
const
  ESPERADO: TComplexo = (re: 6; im: 4);
begin
  validar(ESPERADO, INTEIRO - DIREITA);
end;

procedure TTestadorComplexo.testarSubtracao_2;
const
  ESPERADO: TComplexo = (re: 7; im: 4);
begin
  validar(ESPERADO, REAL - DIREITA);
end;

procedure TTestadorComplexo.testarSubtracao_3;
const
  ESPERADO: TComplexo = (re: 0; im: 3);
begin
  validar(ESPERADO, ESQUERDA - INTEIRO);
end;

procedure TTestadorComplexo.testarSubtracao_4;
const
  ESPERADO: TComplexo = (re: -1; im: 3);
begin
  validar(ESPERADO, ESQUERDA - REAL);
end;

procedure TTestadorComplexo.testarSubtracao_5;
const
  ESPERADO: TComplexo = (re: 6; im: 7);
begin
  validar(ESPERADO, ESQUERDA - DIREITA);
end;

procedure TTestadorComplexo.testarTangente;
const
  ESPERADO: TComplexo = (re: -0.003764026; im: 1.003238627);
begin
  validar(ESPERADO, TComplexo.tan(ESQUERDA));
end;

procedure TTestadorComplexo.testarTangenteHiperbolica;
const
  ESPERADO: TComplexo = (re: 0.965385879; im: -0.009884375);
begin
  validar(ESPERADO, TComplexo.tanh(ESQUERDA));
end;

procedure TTestadorComplexo.validar(const esperado, calculado: TComplexo);
begin
  Assert.IsTrue
  (
    fComparador.Equals(esperado, calculado),
    string.Format('expected <%s>, but calculated <%s>', [esperado.toString, calculado.toString])
  );
end;

end.
