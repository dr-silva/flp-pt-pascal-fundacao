unit Testes.Matematica.TestadorGCLGeradores;

interface
uses
  DUnitX.TestFramework,
  Testes.Matematica.TestadorGCLInterface;

type
  [TestFixture]
  TTestadorGCLAppleCarbon = class (TTestadorGCLInterface)
  protected
    function gerador: string; override;
  end;

  [TestFixture]
  TTestadorGCLBorlandC = class (TTestadorGCLInterface)
  protected
    function gerador: string; override;
  end;

  [TestFixture]
  TTestadorGCLBorlandPascal = class (TTestadorGCLInterface)
  protected
    function gerador: string; override;
  end;

  [TestFixture]
  TTestadorGCLCpp11 = class (TTestadorGCLInterface)
  protected
    function gerador: string; override;
  end;

  [TestFixture]
  TTestadorGCLGLibC = class (TTestadorGCLInterface)
  protected
    function gerador: string; override;
  end;

  [TestFixture]
  TTestadorGCLMMIX = class (TTestadorGCLInterface)
  protected
    function gerador: string; override;
  end;

  [TestFixture]
  TTestadorGCLNewLib = class (TTestadorGCLInterface)
  protected
    function gerador: string; override;
  end;

  [TestFixture]
  TTestadorGCLNumericalRecipes = class (TTestadorGCLInterface)
  protected
    function gerador: string; override;
  end;

  [TestFixture]
  TTestadorGCLOpenVms = class (TTestadorGCLInterface)
  protected
    function gerador: string; override;
  end;

  [TestFixture]
  TTestadorGCLPosix = class (TTestadorGCLInterface)
  protected
    function gerador: string; override;
  end;

  [TestFixture]
  TTestadorGCLRtlUniform = class (TTestadorGCLInterface)
  protected
    function gerador: string; override;
  end;

implementation

{ TTestadorGCLAppleCarbon }

function TTestadorGCLAppleCarbon.gerador: string;
begin
  Result := 'apple-carbon';
end;

{ TTestadorGCLBorlandC }

function TTestadorGCLBorlandC.gerador: string;
begin
  Result := 'borland-c';
end;

{ TTestadorGCLBorlandPascal }

function TTestadorGCLBorlandPascal.gerador: string;
begin
  Result := 'borland-pascal';
end;

{ TTestadorGCLCpp11 }

function TTestadorGCLCpp11.gerador: string;
begin
  Result := 'cpp11';
end;

{ TTestadorGCLGLibC }

function TTestadorGCLGLibC.gerador: string;
begin
  Result := 'glibc';
end;

{ TTestadorGCLMMIX }

function TTestadorGCLMMIX.gerador: string;
begin
  Result := 'mmix';
end;

{ TTestadorGCLNewLib }

function TTestadorGCLNewLib.gerador: string;
begin
  Result := 'new-lib';
end;

{ TTestadorGCLNumericalRecipes }

function TTestadorGCLNumericalRecipes.gerador: string;
begin
  Result := 'numerical-recipes';
end;

{ TTestadorGCLOpenVms }

function TTestadorGCLOpenVms.gerador: string;
begin
  Result := 'open-vms';
end;

{ TTestadorGCLPosix }

function TTestadorGCLPosix.gerador: string;
begin
  Result := 'posix';
end;

{ TTestadorGCLRtlUniform }

function TTestadorGCLRtlUniform.gerador: string;
begin
  Result := 'rtl-uniform';
end;

end.
