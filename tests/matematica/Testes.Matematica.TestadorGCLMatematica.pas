unit Testes.Matematica.TestadorGCLMatematica;

interface
uses
  DUnitX.TestFramework,
  Testes.Matematica.TestadorGCL,
  FractalLotus.Fundacao.Matematica;

type
  [TestFixture]
  TTestadorGCLMatematica = class (TTestadorGCL)
  protected
    function gerarUniforme(const limite: Integer): Integer; overload; override;
    function gerarUniforme                       : Double;  overload; override;
  end;

implementation

{ TTestadorGCLMatematica }

function TTestadorGCLMatematica.gerarUniforme: Double;
begin
  Result := TMatematica.uniforme;
end;

function TTestadorGCLMatematica.gerarUniforme(const limite: Integer): Integer;
begin
  Result := TMatematica.uniforme(limite);
end;

end.
