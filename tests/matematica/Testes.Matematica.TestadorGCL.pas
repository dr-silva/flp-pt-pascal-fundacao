unit Testes.Matematica.TestadorGCL;

interface
uses
  DUnitX.TestFramework;

type
  TTestadorGCL = class abstract
  private const
    DELTA_INT   : Double  = 10;
    DELTA_DOUBLE: Double  = 1 / 12;
    TOTAL       : Integer = 1000;
    LIMITE      : Integer = 500;
  protected
    function gerarUniforme(const limite: Integer): Integer; overload; virtual; abstract;
    function gerarUniforme                       : Double;  overload; virtual; abstract;
  public
    [Test]
    procedure testarInteger;
    [Test]
    procedure testarDouble;
    [Test]
    procedure testarErro;
  end;

implementation
uses
  System.SysUtils;

{ TTestadorGCL }

procedure TTestadorGCL.testarDouble;
var
  i    : Integer;
  soma ,
  valor: Double;
begin
  soma := 0;

  for i := 1 to TOTAL do
  begin
    valor := gerarUniforme;

    if ((valor < 0) or (valor >= LIMITE)) then
      Assert.Fail('Valor double fora do limite.');

    soma := soma + valor;
  end;

  Assert.AreEqual(0.5, soma / TOTAL, DELTA_DOUBLE);
end;

procedure TTestadorGCL.testarErro;
begin
  try
    gerarUniforme(0);

    Assert.Fail('Era para ter levantado uma exce��o.');
  except
    on e: EArgumentOutOfRangeException do
    begin
      // do nothing
    end;
    on e: Exception do
      Assert.Fail('Erro desconhecido: ' + e.Message);
  end;
end;

procedure TTestadorGCL.testarInteger;
var
  i,
  valor: Integer;
  soma : Double;
begin
  soma := 0;

  for i := 1 to TOTAL do
  begin
    valor := gerarUniforme(LIMITE);

    if ((valor < 0) or (valor >= LIMITE)) then
      Assert.Fail('Valor inteiro fora do limite.');

    soma := soma + valor;
  end;

  Assert.AreEqual((LIMITE - 1) / 2, soma / TOTAL, DELTA_INT);
end;

end.
