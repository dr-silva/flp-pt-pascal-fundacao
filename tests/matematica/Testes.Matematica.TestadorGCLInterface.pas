unit Testes.Matematica.TestadorGCLInterface;

interface
uses
  DUnitX.TestFramework,
  Testes.Matematica.TestadorGCL,
  FractalLotus.Fundacao.Matematica.GeradorCongruencial,
  FractalLotus.Fundacao.Matematica.FabricaGeradorCongruencial;

type
  TTestadorGCLInterface = class abstract (TTestadorGCL)
  private
    fGerador: IGeradorCongruencial;
  protected
    function gerarUniforme(const limite: Integer): Integer; overload; override;
    function gerarUniforme                       : Double;  overload; override;
    function gerador                             : string;  virtual;  abstract;
  public
    constructor Create;
    destructor  Destroy; override;
  end;

implementation

{ TTestadorGCLInterface }

constructor TTestadorGCLInterface.Create;
begin
  fGerador := TFabricaGeradorCongruencial.criar
  (
    gerador, TTipoGeradorCongruencial.tgcLinear, 20210927
  );
end;

destructor TTestadorGCLInterface.Destroy;
begin
  fGerador := nil;

  inherited Destroy;
end;

function TTestadorGCLInterface.gerarUniforme(const limite: Integer): Integer;
begin
  Result := fGerador.uniforme(limite);
end;

function TTestadorGCLInterface.gerarUniforme: Double;
begin
  Result := fGerador.uniforme;
end;

end.
