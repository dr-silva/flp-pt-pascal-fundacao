program tstFundacao;

{$IFNDEF TESTINSIGHT}
{$APPTYPE CONSOLE}
{$ENDIF}
{$STRONGLINKTYPES ON}
uses
  System.SysUtils,
  {$IFDEF TESTINSIGHT}
  TestInsight.DUnitX,
  {$ELSE}
  DUnitX.Loggers.Console,
  DUnitX.Loggers.Xml.NUnit,
  {$ENDIF }
  DUnitX.TestFramework,
  Testes.Ordenacoes.TestadorOrdenacao in 'tests\ordenacoes\Testes.Ordenacoes.TestadorOrdenacao.pas',
  Testes.Ordenacoes.Testadores in 'tests\ordenacoes\Testes.Ordenacoes.Testadores.pas',
  Testes.Ordenacoes.TestadorAleatorio in 'tests\ordenacoes\Testes.Ordenacoes.TestadorAleatorio.pas',
  Testes.Colecoes.VetorValidacao in 'tests\colecoes\Testes.Colecoes.VetorValidacao.pas',
  Testes.Colecoes.TestadorEnumeravel in 'tests\colecoes\Testes.Colecoes.TestadorEnumeravel.pas',
  Testes.Colecoes.TestadorPilha in 'tests\colecoes\Testes.Colecoes.TestadorPilha.pas',
  Testes.Colecoes.TestadorFila in 'tests\colecoes\Testes.Colecoes.TestadorFila.pas',
  Testes.Colecoes.TestadorBolsa in 'tests\colecoes\Testes.Colecoes.TestadorBolsa.pas',
  Testes.Colecoes.TestadorLista in 'tests\colecoes\Testes.Colecoes.TestadorLista.pas',
  Testes.Colecoes.TestadorDicionario in 'tests\colecoes\Testes.Colecoes.TestadorDicionario.pas',
  Testes.Colecoes.TestadorConjunto in 'tests\colecoes\Testes.Colecoes.TestadorConjunto.pas',
  Testes.Matematica.TestadorComplexo in 'tests\matematica\Testes.Matematica.TestadorComplexo.pas',
  Testes.Matematica.TestadorGCL in 'tests\matematica\Testes.Matematica.TestadorGCL.pas',
  Testes.Matematica.TestadorGCLMatematica in 'tests\matematica\Testes.Matematica.TestadorGCLMatematica.pas',
  Testes.Matematica.TestadorGCLInterface in 'tests\matematica\Testes.Matematica.TestadorGCLInterface.pas',
  Testes.Matematica.TestadorGCLGeradores in 'tests\matematica\Testes.Matematica.TestadorGCLGeradores.pas',
  Testes.EntradasSaidas.TestadorImagemBitmap in 'tests\entradas-saidas\Testes.EntradasSaidas.TestadorImagemBitmap.pas';

{$IFNDEF TESTINSIGHT}
var
  runner: ITestRunner;
  results: IRunResults;
  logger: ITestLogger;
  nunitLogger : ITestLogger;
{$ENDIF}
begin
{$IFDEF TESTINSIGHT}
  TestInsight.DUnitX.RunRegisteredTests;
{$ELSE}
  ReportMemoryLeaksOnShutdown := True;
  try
    //Check command line options, will exit if invalid
    TDUnitX.CheckCommandLine;
    //Create the test runner
    runner := TDUnitX.CreateRunner;
    //Tell the runner to use RTTI to find Fixtures
    runner.UseRTTI := True;
    //When true, Assertions must be made during tests;
    runner.FailsOnNoAsserts := False;
    //Aways wait
    TDUnitX.Options.ExitBehavior := TDUnitXExitBehavior.Pause;
    //The randomness is totally necessary
    Randomize;

    //tell the runner how we will log things
    //Log to the console window if desired
    if TDUnitX.Options.ConsoleMode <> TDunitXConsoleMode.Off then
    begin
      logger := TDUnitXConsoleLogger.Create(TDUnitX.Options.ConsoleMode = TDunitXConsoleMode.Quiet);
      runner.AddLogger(logger);
    end;
    //Generate an NUnit compatible XML File
    nunitLogger := TDUnitXXMLNUnitFileLogger.Create(TDUnitX.Options.XMLOutputFile);
    runner.AddLogger(nunitLogger);

    //Run tests
    results := runner.Execute;
    if not results.AllPassed then
      System.ExitCode := EXIT_ERRORS;

    {$IFNDEF CI}
    //We don't want this happening when running under CI.
    if TDUnitX.Options.ExitBehavior = TDUnitXExitBehavior.Pause then
    begin
      System.Write('Done.. press <Enter> key to quit.');
      System.Readln;
    end;
    {$ENDIF}
  except
    on E: Exception do
      System.Writeln(E.ClassName, ': ', E.Message);
  end;
  IsConsole := False;
{$ENDIF}
end.
