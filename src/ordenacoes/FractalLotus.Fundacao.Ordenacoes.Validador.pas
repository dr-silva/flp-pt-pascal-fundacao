(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Ordenacoes.Validador.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Ordena��es da Funda��o: aqui ser�o declaradas as classes que
 *            implementam a interface IValidadorOrdena��o<T>, ela valida se um
 *            array est� ordenado corretamente, tem fun��o de teste.
 *            Para maiores informa��es sobre os classificadores vistos aqui,
 *            consulte a documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Ordenacoes.Validador;

interface
uses
  Ordenacoes.Validador;

type
  { Define a classe da valida��o da ordena��o CRESCENTE. Essa valida��o �      }
  { executada em tempo O(n).                                                   }
  TValidadorOrdenacaoAscendente<T> = class(TValidadorOrdenacao<T>)
  protected
    function estaOrdenado(const esquerda, direita: T): Boolean; override;
  end;

  { Define a classe da valida��o da ordena��o DECRESCENTE. Essa valida��o �    }
  { executada em tempo O(n).                                                   }
  TValidadorOrdenacaoDescendente<T> = class(TValidadorOrdenacao<T>)
  protected
    function estaOrdenado(const esquerda, direita: T): Boolean; override;
  end;

implementation

{ TValidadorOrdenacaoAscendente<T> }

function TValidadorOrdenacaoAscendente<T>.estaOrdenado(const esquerda,
  direita: T): Boolean;
begin
  Result := comparador.Compare(esquerda, direita) <= 0;
end;

{ TValidadorOrdenacaoDescendente<T> }

function TValidadorOrdenacaoDescendente<T>.estaOrdenado(const esquerda,
  direita: T): Boolean;
begin
  Result := comparador.Compare(esquerda, direita) >= 0;
end;

end.
