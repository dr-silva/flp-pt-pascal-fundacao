(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Ordenacoes.Insercao.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Ordena��es da Funda��o: aqui ser�o declaradas as classes que
 *            implementam as ordena��es por inser��o.
 *            Para maiores informa��es sobre os classificadores vistos aqui,
 *            consulte a documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Ordenacoes.Insercao;

interface
uses
  Ordenacoes.Insercao;

type
  { Define a classe da ordena��o por inser��o CRESCENTE. Essa ordena��o �      }
  { executada em tempo O(n^2).                                                 }
  TOrdenacaoPorInsercaoAscendente<T> = class(TOrdenacaoPorInsercao<T>)
  protected
    function eOrdenavel(const i, j: Integer): Boolean; override;
  end;

  { Define a classe da ordena��o por inser��o DECRESCENTE. Essa ordena��o �    }
  { executada em tempo O(n^2).                                                 }
  TOrdenacaoPorInsercaoDescendente<T> = class(TOrdenacaoPorInsercao<T>)
  protected
    function eOrdenavel(const i, j: Integer): Boolean; override;
  end;

implementation

{ TOrdenacaoPorInsercaoAscendente<T> }

function TOrdenacaoPorInsercaoAscendente<T>.eOrdenavel(const i,
  j: Integer): Boolean;
begin
  Result := (comparador.Compare(item[i], item[j]) > 0);
end;

{ TOrdenacaoPorInsercaoDescendente<T> }

function TOrdenacaoPorInsercaoDescendente<T>.eOrdenavel(const i,
  j: Integer): Boolean;
begin
  Result := (comparador.Compare(item[i], item[j]) < 0);
end;

end.
