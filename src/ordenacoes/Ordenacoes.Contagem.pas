(*******************************************************************************
 *
 * Arquivo  : Ordenacoes.Contagem.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o classificador base da ordena��o por contagem. Essa orde-
 *            na��o � executada em tempo O(n).
 *
 ******************************************************************************)
unit Ordenacoes.Contagem;

interface
uses
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes,
  Ordenacoes.Abstrato;

type
  TOrdenacaoPorContagem<T> = class abstract(TInterfacedObject, IOrdenacao<T>)
  private
    fItens   : TArray<T>;
    fTotal   ,
    fMinimo  ,
    fMaximo  : Integer;
    fProjetor: IProjetor<T, Integer>;

    procedure definirMinMax;

    function preparar: Boolean;
    function indexar(const valor: T): Integer;
  protected
    function indexarSaida(const indice: Integer): Integer; virtual; abstract;

    property total: Integer read fTotal;
  public
    constructor Create(const projecao: TFuncaoProjecao<T, Integer>); overload;
    constructor Create(const projetor: IProjetor<T, Integer>);       overload;
    destructor  Destroy; override;

    procedure ordenar(itens: TArray<T>);

    property minimo: Integer read fMinimo write fMinimo;
    property maximo: Integer read fMaximo write fMaximo;
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes;

{ TOrdenacaoPorContagem<T> }

constructor TOrdenacaoPorContagem<T>.Create(
  const projecao: TFuncaoProjecao<T, Integer>);
begin
  if not Assigned(projecao) then
    raise ArgumentoNulo('projecao') at ReturnAddress;

  Create( TFabricaProjetor.criar<T, Integer>(projecao) );
end;

constructor TOrdenacaoPorContagem<T>.Create(
  const projetor: IProjetor<T, Integer>);
begin
  if not Assigned(projetor) then
    raise ArgumentoNulo('projetor') at ReturnAddress;

  fProjetor := projetor;
  fMinimo   := Integer.MaxValue;
  fMaximo   := Integer.MinValue;
end;

procedure TOrdenacaoPorContagem<T>.definirMinMax;
var
  i, valor: Integer;
begin
  fMinimo := Integer.MaxValue;
  fMaximo := Integer.MinValue;

  for i := 0 to total - 1 do
  begin
    valor := fProjetor.projetar(fItens[i]);

    if (valor < fMinimo) then
      fMinimo := valor;

    if (valor > fMaximo) then
      fMaximo := valor;
  end;
end;

destructor TOrdenacaoPorContagem<T>.Destroy;
begin
  fProjetor := nil;

  inherited Destroy;
end;

function TOrdenacaoPorContagem<T>.indexar(const valor: T): Integer;
begin
  Result := fProjetor.projetar(valor) - minimo;
end;

procedure TOrdenacaoPorContagem<T>.ordenar(itens: TArray<T>);
var
  saida    : TArray<T>;
  contagem : array of Integer;
  i, indice,
  diferenca: Integer;
begin
  fItens := itens;
  fTotal := Length(itens);

  if (fTotal <= 0) or (not preparar) then
    Exit;

  diferenca := maximo - minimo;

  SetLength(saida   , total);
  SetLength(contagem, diferenca + 1);

  for i := 0 to diferenca do
    contagem[i] := 0;

  for i := 0 to total - 1 do
  begin
    indice := indexar(itens[i]);

    contagem[indice] := contagem[indice] + 1;
  end;

  contagem[0] := contagem[0] - 1;
  for i := 1 to diferenca do
    contagem[i] := contagem[i] + contagem[i - 1];

  for i := total - 1 downto 0 do
  begin
    indice := indexar(itens[i]);

    saida   [ contagem[indice] ] := itens[i];
    contagem[indice]             := contagem[indice] - 1;
  end;

  for i := 0 to total - 1 do
    fItens[i] := saida[ indexarSaida(i) ];

  fMinimo := Integer.MaxValue;
  fMaximo := Integer.MinValue;
end;

function TOrdenacaoPorContagem<T>.preparar: Boolean;
begin
  if (minimo > maximo) then
    definirMinMax;

  Result := ((maximo - minimo) > 1);
end;

end.
