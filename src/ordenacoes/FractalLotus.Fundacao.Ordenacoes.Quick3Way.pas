(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Ordenacoes.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Ordena��es da Funda��o: aqui ser�o declaradas as classes que
 *            implementam as ordena��es Quick-3way.
 *            Para maiores informa��es sobre os classificadores vistos aqui,
 *            consulte a documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Ordenacoes.Quick3Way;

interface
uses
  Ordenacoes.Quick3Way;

type
  { Define a classe da ordena��o Quick 3-Way CRESCENTE. Essa ordena��o �       }
  { executada em tempo O(n * log_2(n)).                                        }
  TOrdenacaoQuick3WayAscendente<T> = class(TOrdenacaoQuick3Way<T>)
  protected
    function comparar(const esquerda, direita: T): Integer; override;
  end;

  { Define a classe da ordena��o Quick 3-Way DECRESCENTE. Essa ordena��o �     }
  { executada em tempo O(n * log_2(n)).                                        }
  TOrdenacaoQuick3WayDescendente<T> = class(TOrdenacaoQuick3Way<T>)
  protected
    function comparar(const esquerda, direita: T): Integer; override;
  end;

implementation

{ TOrdenacaoQuick3WayAscendente<T> }

function TOrdenacaoQuick3WayAscendente<T>.comparar(const esquerda,
  direita: T): Integer;
begin
  Result := comparador.Compare(esquerda, direita);
end;

{ TOrdenacaoQuick3WayDescendente<T> }

function TOrdenacaoQuick3WayDescendente<T>.comparar(const esquerda,
  direita: T): Integer;
begin
  Result := -comparador.Compare(esquerda, direita);
end;

end.
