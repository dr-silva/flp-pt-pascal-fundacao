(*******************************************************************************
 *
 * Arquivo  : Ordenacoes.Quick.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o classificador base da ordena��o Quick. Essa ordena��o �
 *            executada em tempo O(n * log_2(n)).
 *
 ******************************************************************************)
unit Ordenacoes.Quick;

interface
uses
  Ordenacoes.Abstrato;

type
  TOrdenacaoQuick<T> = class abstract(TOrdenacaoAbstrata<T>)
  private
    function  particionar(const minimo, maximo: Integer): Integer;
    procedure ordenar    (const minimo, maximo: Integer);
  protected
    function eOrdenavel(const i, j: Integer): Boolean; virtual; abstract;
    procedure fazerOrdenacao; override;
  end;

implementation
uses
  FractalLotus.Fundacao.Matematica;

{ TOrdenacaoQuick<T> }

procedure TOrdenacaoQuick<T>.fazerOrdenacao;
begin
  ordenar(0, total - 1);
end;

procedure TOrdenacaoQuick<T>.ordenar(const minimo, maximo: Integer);
var
  ponto: Integer;
begin
  if (minimo < maximo) then
  begin
    ponto := particionar(minimo, maximo);

    ordenar(minimo, ponto - 1);
    ordenar(ponto + 1, maximo);
  end;
end;

function TOrdenacaoQuick<T>.particionar(const minimo, maximo: Integer): Integer;
var
  j: Integer;
begin
  Result := TMatematica.uniforme(maximo - minimo) + minimo; // gera um n�mero ale�torio entre min e max
  trocar(Result, maximo);

  Result := minimo;
  for j := minimo to maximo - 1 do
    if (eOrdenavel(j, maximo)) then
    begin
      trocar(Result, j);
      Result := Result + 1;
    end;

  trocar(Result, maximo);
end;

end.
