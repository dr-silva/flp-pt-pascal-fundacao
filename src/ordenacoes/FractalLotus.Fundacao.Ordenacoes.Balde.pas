(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Ordenacoes.Balde.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Ordena��es da Funda��o: aqui ser�o declaradas as classes que
 *            implementam as ordena��es por balde.
 *            Para maiores informa��es sobre os classificadores vistos aqui,
 *            consulte a documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Ordenacoes.Balde;

interface
uses
  Ordenacoes.Balde;

type
  { Define a classe da ordena��o por balde CRESCENTE. Essa ordena��o �         }
  { executada em tempo O(n).                                                   }
  TOrdenacaoPorBaldeAscendente<T> = class(TOrdenacaoPorBalde<T>)
  protected
    function indexar(const indice: Integer): Integer; override;
  end;

  { Define a classe da ordena��o por balde DECRESCENTE. Essa ordena��o �       }
  { executada em tempo O(n).                                                   }
  TOrdenacaoPorBaldeDescendente<T> = class(TOrdenacaoPorBalde<T>)
  protected
    function indexar(const indice: Integer): Integer; override;
  end;

implementation

{ TOrdenacaoPorBaldeAscendente<T> }

function TOrdenacaoPorBaldeAscendente<T>.indexar(const
  indice: Integer): Integer;
begin
  Result := indice;
end;

{ TOrdenacaoPorBaldeDescendente<T> }

function TOrdenacaoPorBaldeDescendente<T>.indexar(const
  indice: Integer): Integer;
begin
  Result := total - (indice + 1);
end;

end.
