(*******************************************************************************
 *
 * Arquivo  : Ordenacoes.Shell.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o classificador base da ordena��o Shell. Essa ordena��o �
 *            executada em tempo O(n^2).
 *
 ******************************************************************************)
unit Ordenacoes.Shell;

interface
uses
  Ordenacoes.Abstrato;

type
  TOrdenacaoShell<T> = class abstract(TOrdenacaoAbstrata<T>)
  protected
    function eOrdenavel(const i, j: Integer): Boolean; virtual; abstract;
    procedure fazerOrdenacao; override;
  end;

implementation

{ TOrdenacaoShell<T> }

procedure TOrdenacaoShell<T>.fazerOrdenacao;
var
  i, j, lacuna: Integer;
begin
  lacuna := 1;
  while (lacuna < total div 3) do
    lacuna := 3 * lacuna + 1;

  while (lacuna >= 1) do
  begin
    for i := lacuna to total - 1 do
    begin
      j := i;
      while ((j >= lacuna) and eOrdenavel(j - lacuna, j)) do
      begin
        trocar(j - lacuna, j);
        j := j - lacuna;
      end;
    end;

    lacuna := lacuna div 3;
  end;
end;

end.
