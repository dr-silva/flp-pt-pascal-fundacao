(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Ordenacoes.Selecao.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Ordena��es da Funda��o: aqui ser�o declaradas as classes que
 *            implementam as ordena��es por sele��o.
 *            Para maiores informa��es sobre os classificadores vistos aqui,
 *            consulte a documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Ordenacoes.Selecao;

interface
uses
  Ordenacoes.Selecao;

type
  { Define a classe da ordena��o por sele��o CRESCENTE. Essa ordena��o �       }
  { executada em tempo O(n^2).                                                 }
  TOrdenacaoPorSelecaoAscendente<T> = class(TOrdenacaoPorSelecao<T>)
  protected
    function eOrdenavel(const i, j: Integer): Boolean; override;
  end;

  { Define a classe da ordena��o por sele��o DECRESCENTE. Essa ordena��o �     }
  { executada em tempo O(n^2).                                                 }
  TOrdenacaoPorSelecaoDescendente<T> = class(TOrdenacaoPorSelecao<T>)
  protected
    function eOrdenavel(const i, j: Integer): Boolean; override;
  end;

implementation

{ TOrdenacaoPorSelecaoAscendente<T> }

function TOrdenacaoPorSelecaoAscendente<T>.eOrdenavel(const i,
  j: Integer): Boolean;
begin
  Result := (comparador.Compare(item[i], item[j]) > 0);
end;

{ TOrdenacaoPorSelecaoDescendente<T> }

function TOrdenacaoPorSelecaoDescendente<T>.eOrdenavel(const i,
  j: Integer): Boolean;
begin
  Result := (comparador.Compare(item[i], item[j]) < 0);
end;

end.
