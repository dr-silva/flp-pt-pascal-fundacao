(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Ordenacoes.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Ordena��es da Funda��o: aqui ser�o declaradas as classes que
 *            implementam as ordena��es Heap.
 *            Para maiores informa��es sobre os classificadores vistos aqui,
 *            consulte a documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Ordenacoes.Heap;

interface
uses
  Ordenacoes.Heap;

type
  { Define a classe da ordena��o Heap CRESCENTE. Essa ordena��o � executada    }
  { em tempo O(n * log_2(n)).                                                  }
  TOrdenacaoHeapAscendente<T> = class(TOrdenacaoHeap<T>)
  protected
    function eOrdenavel(const i, j: Integer): Boolean; override;
  end;

  { Define a classe da ordena��o Heap DECRESCENTE. Essa ordena��o � executada  }
  { em tempo O(n * log_2(n)).                                                  }
  TOrdenacaoHeapDescendente<T> = class(TOrdenacaoHeap<T>)
  protected
    function eOrdenavel(const i, j: Integer): Boolean; override;
  end;

implementation

{ TOrdenacaoHeapAscendente<T> }

function TOrdenacaoHeapAscendente<T>.eOrdenavel(const i, j: Integer): Boolean;
begin
  Result := (comparador.Compare(item[i], item[j]) > 0);
end;

{ TOrdenacaoHeapDescendente<T> }

function TOrdenacaoHeapDescendente<T>.eOrdenavel(const i, j: Integer): Boolean;
begin
  Result := (comparador.Compare(item[i], item[j]) < 0);
end;

end.
