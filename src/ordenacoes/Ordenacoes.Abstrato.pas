(*******************************************************************************
 *
 * Arquivo  : Ordenacoes.Abstrato.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma classe abstrata b�sica para as classes de ordena��o
 *            com o intuito de minimizar c�digo na implementa��o das classes
 *            finais.
 *
 ******************************************************************************)
unit Ordenacoes.Abstrato;

interface
uses
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

type
  TOrdenacaoAbstrata<T> = class abstract(TInterfacedObject, IOrdenacao<T>)
  private
    fItens     : TArray<T>;
    fTotal     : Integer;
    fComparador: IComparador<T>;

    function obterItem(const indice: Integer): T;
  protected
    procedure trocar(const i, j: Integer);
    procedure fazerOrdenacao; virtual; abstract;

    property total     : Integer        read fTotal      write fTotal;
    property comparador: IComparador<T> read fComparador;

    property item[const indice: Integer]: T read obterItem;
  public
    constructor Create();                                       overload;
    constructor Create(const comparacao: TFuncaoComparacao<T>); overload;
    constructor Create(const comparador: IComparador<T>);       overload;
    destructor  Destroy;                                        override;

    procedure ordenar(itens: TArray<T>);
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes;

{ TOrdenacaoAbstrata<T> }

constructor TOrdenacaoAbstrata<T>.Create;
begin
  Create( TFabricaComparador.criar<T>() );
end;

constructor TOrdenacaoAbstrata<T>.Create(
  const comparacao: TFuncaoComparacao<T>);
begin
  Create( TFabricaComparador.criar<T>(comparacao) );
end;

constructor TOrdenacaoAbstrata<T>.Create(const comparador: IComparador<T>);
begin
  if (comparador = nil) then
    raise ArgumentoNulo('comparador') at ReturnAddress;

  fComparador := comparador;
end;

destructor TOrdenacaoAbstrata<T>.Destroy;
begin
  fComparador := nil;

  inherited Destroy;
end;

function TOrdenacaoAbstrata<T>.obterItem(const indice: Integer): T;
begin
  Result := fItens[indice];
end;

procedure TOrdenacaoAbstrata<T>.ordenar(itens: TArray<T>);
begin
  fItens := itens;
  fTotal := Length(itens);

  if (fTotal > 0) then
    fazerOrdenacao;
end;

procedure TOrdenacaoAbstrata<T>.trocar(const i, j: Integer);
var
  temp: T;
begin
  temp      := fItens[i];
  fItens[i] := fItens[j];
  fItens[j] := temp;
end;

end.
