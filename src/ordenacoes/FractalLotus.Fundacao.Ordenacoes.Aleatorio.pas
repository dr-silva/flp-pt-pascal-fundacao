(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Ordenacoes.Aleatorio.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Ordena��es da Funda��o: aqui ser�o declaradas as classes que
 *            implementam as interfaces IOrdena��o<T> e IValidadorOrdena��o<T>,
 *            declaradas no Cerne da Funda��o.
 *            Para maiores informa��es sobre os classificadores vistos aqui,
 *            consulte a documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Ordenacoes.Aleatorio;

interface
uses
  FractalLotus.Fundacao.Ordenacoes.Balde,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes,
  FractalLotus.Fundacao.Matematica.GeradorCongruencial;

type
  TOrdenacaoAleatoria<T> = class(TOrdenacaoPorBaldeAscendente<T>)
  strict private type
    TProjetorAleatorio = class (TInterfacedObject, IProjetor<T, Double>)
    private
      fGerador: IGeradorCongruencial;
    public
      constructor Create;
      destructor  Destroy; override;

      function projetar(const valor: T): Double;
    end;
  private
    fProjetor: IProjetor<T, Double>;
  public
    constructor Create();
    destructor  Destroy; override;
  end;

implementation
uses
  System.SysUtils,
  FractalLotus.Fundacao.Matematica.FabricaGeradorCongruencial;

{ TOrdenacaoAleatoria<T>.TProjetorAleatorio }

constructor TOrdenacaoAleatoria<T>.TProjetorAleatorio.Create;
begin
  fGerador := TFabricaGeradorCongruencial.criar
  (
    TTipoGeradorCongruencial.tgcLinear
  );
end;

destructor TOrdenacaoAleatoria<T>.TProjetorAleatorio.Destroy;
begin
  fGerador := nil;

  inherited Destroy;
end;

function TOrdenacaoAleatoria<T>.TProjetorAleatorio.projetar(
  const valor: T): Double;
begin
  Result := fGerador.uniforme;
end;

{ TOrdenacaoAleatoria<T> }

constructor TOrdenacaoAleatoria<T>.Create;
begin
  fProjetor := TProjetorAleatorio.Create;

  inherited Create( fProjetor );
end;

destructor TOrdenacaoAleatoria<T>.Destroy;
begin
  fProjetor := nil;

  inherited Destroy;
end;

end.
