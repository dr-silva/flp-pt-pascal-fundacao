(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Ordenacoes.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Ordena��es da Funda��o: aqui ser�o declaradas as classes que
 *            implementam as ordena��es Shell.
 *            Para maiores informa��es sobre os classificadores vistos aqui,
 *            consulte a documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Ordenacoes.Shell;

interface
uses
  Ordenacoes.Shell;

type
  { Define a classe da ordena��o Shell CRESCENTE. Essa ordena��o � executada   }
  { em tempo O(n^2).                                                           }
  TOrdenacaoShellAscendente<T> = class(TOrdenacaoShell<T>)
  protected
    function eOrdenavel(const i, j: Integer): Boolean; override;
  end;

  { Define a classe da ordena��o Shell CRESCENTE. Essa ordena��o � executada   }
  { em tempo O(n^2).                                                           }
  TOrdenacaoShellDescendente<T> = class(TOrdenacaoShell<T>)
  protected
    function eOrdenavel(const i, j: Integer): Boolean; override;
  end;

implementation

{ TOrdenacaoShellAscendente<T> }

function TOrdenacaoShellAscendente<T>.eOrdenavel(const i, j: Integer): Boolean;
begin
  Result := (comparador.Compare(item[i], item[j]) > 0);
end;

{ TOrdenacaoShellDescendente<T> }

function TOrdenacaoShellDescendente<T>.eOrdenavel(const i, j: Integer): Boolean;
begin
  Result := (comparador.Compare(item[i], item[j]) < 0);
end;

end.
