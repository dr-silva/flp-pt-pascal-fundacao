(*******************************************************************************
 *
 * Arquivo  : Ordenacoes.Merge.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o classificador base da ordena��o Merge. Essa ordena��o �
 *            executada em tempo O(n * log_2(n)).
 *
 ******************************************************************************)
unit Ordenacoes.Merge;

interface
uses
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes,
  Ordenacoes.Abstrato;

type
  TOrdenacaoMerge<T> = class abstract(TInterfacedObject, IOrdenacao<T>)
  private
    fItens     ,
    fEsquerda  ,
    fDireita   : TArray<T>;
    fComparador: IComparador<T>;

    function criarEsquerda(const minimo, maximo: Integer): Integer;
    function criarDireita (const minimo, maximo: Integer): Integer;

    function definirEsquerda(const indice, esquerda: Integer): Integer;
    function definirDireita (const indice, direita : Integer): Integer;

    procedure fundir    (const minimo, mediana, maximo: Integer);
    procedure porEmOrdem(const minimo, maximo: Integer);
  protected
    function eOrdenavel(const esquerda, direita: T): Boolean; virtual; abstract;

    property comparador: IComparador<T> read fComparador;
  public
    constructor Create();                                       overload;
    constructor Create(const comparacao: TFuncaoComparacao<T>); overload;
    constructor Create(const comparador: IComparador<T>);       overload;
    destructor  Destroy; override;

    procedure ordenar(itens: TArray<T>);
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes;

{ TOrdenacaoMerge<T> }

constructor TOrdenacaoMerge<T>.Create(const comparacao: TFuncaoComparacao<T>);
begin
  Create( TFabricaComparador.criar<T>(comparacao) );
end;

constructor TOrdenacaoMerge<T>.Create;
begin
  Create( TFabricaComparador.criar<T>() );
end;

constructor TOrdenacaoMerge<T>.Create(const comparador: IComparador<T>);
begin
  if (comparador = nil) then
    raise ArgumentoNulo('comparador') at ReturnAddress;

  fComparador := comparador;
end;

function TOrdenacaoMerge<T>.criarDireita(const minimo,
  maximo: Integer): Integer;
var
  i: Integer;
begin
  Result := maximo - minimo;

  SetLength(fDireita, Result);

  for i := 0 to Result - 1 do
    fDireita[i] := fItens[minimo + i + 1];
end;

function TOrdenacaoMerge<T>.criarEsquerda(const minimo,
  maximo: Integer): Integer;
var
  i: Integer;
begin
  Result := maximo - minimo + 1;

  SetLength(fEsquerda, Result);

  for i := 0 to Result - 1 do
    fEsquerda[i] := fItens[minimo + i];
end;

function TOrdenacaoMerge<T>.definirDireita(const indice,
  direita: Integer): Integer;
begin
  fItens[indice] := fDireita[direita];
  Result         := direita + 1;
end;

function TOrdenacaoMerge<T>.definirEsquerda(const indice,
  esquerda: Integer): Integer;
begin
  fItens[indice] := fEsquerda[esquerda];
  Result         := esquerda + 1;
end;

destructor TOrdenacaoMerge<T>.Destroy;
begin
  fComparador := nil;

  inherited Destroy;
end;

procedure TOrdenacaoMerge<T>.fundir(const minimo, mediana, maximo: Integer);
var
  e, d, i,
  totalEsquerda,
  totalDireita : Integer;
begin
  totalEsquerda := criarEsquerda(minimo, mediana);
  totalDireita  := criarDireita (mediana, maximo);

  e := 0;
  d := 0;

  for i := minimo to maximo do
    if (e >= totalEsquerda) then
      d := definirDireita(i, d)
    else if (d >= totalDireita) then
      e := definirEsquerda(i, e)
    else if (eOrdenavel(fEsquerda[e], fDireita[d])) then
      e := definirEsquerda(i, e)
    else
      d := definirDireita(i, d);
end;

procedure TOrdenacaoMerge<T>.ordenar(itens: TArray<T>);
begin
  fItens := itens;

  porEmOrdem(0, Length(itens) - 1);
end;

procedure TOrdenacaoMerge<T>.porEmOrdem(const minimo, maximo: Integer);
var
  mediana: Integer;
begin
  if (minimo < maximo) then
  begin
    mediana := (minimo + maximo) div 2;

    porEmOrdem(minimo,      mediana);
    porEmOrdem(mediana + 1, maximo );

    fundir(minimo, mediana, maximo);
  end;
end;

end.
