(*******************************************************************************
 *
 * Arquivo  : Ordenacoes.Heap.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o classificador base da ordena��o Heap. Essa ordena��o �
 *            executada em tempo O(n * log_2(n)).
 *
 ******************************************************************************)
unit Ordenacoes.Heap;

interface
uses
  Ordenacoes.Abstrato;

type
  TOrdenacaoHeap<T> = class abstract(TOrdenacaoAbstrata<T>)
  private
    procedure amontar(const i: Integer);
  protected
    function eOrdenavel(const i, j: Integer): Boolean; virtual; abstract;
    procedure fazerOrdenacao; override;
  end;

implementation

{ TOrdenacaoHeap<T> }

procedure TOrdenacaoHeap<T>.amontar(const i: Integer);
var
  direita    ,
  esquerda   ,
  prioritario: Integer;
begin
  esquerda := 2 * (i + 1) - 1;
  direita  := 2 * (i + 1);

  if ((esquerda < total) and eOrdenavel(esquerda, i)) then
    prioritario := esquerda
  else
    prioritario := i;

  if ((direita < total) and eOrdenavel(direita, prioritario)) then
    prioritario := direita;

  if (prioritario <> i) then
  begin
    trocar(i, prioritario);
    amontar(prioritario);
  end;
end;

procedure TOrdenacaoHeap<T>.fazerOrdenacao;
var
  i: Integer;
begin
  // criar heap
  for i := total div 2 downto 0 do
    amontar(i);

  // ordenar array
  for i := total - 1 downto 1 do
  begin
    trocar(0, i);
    total := total - 1;
    amontar(0);
  end;
end;

end.
