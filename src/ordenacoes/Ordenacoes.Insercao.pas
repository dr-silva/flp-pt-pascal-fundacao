(*******************************************************************************
 *
 * Arquivo  : Ordenacoes.Insercao.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o classificador base da ordena��o por inser��o. Essa orde-
 *            na��o � executada em tempo O(n^2).
 *
 ******************************************************************************)
unit Ordenacoes.Insercao;

interface
uses
  Ordenacoes.Abstrato;

type
  TOrdenacaoPorInsercao<T> = class abstract(TOrdenacaoAbstrata<T>)
  protected
    function eOrdenavel(const i, j: Integer): Boolean; virtual; abstract;
    procedure fazerOrdenacao; override;
  end;

implementation

{ TOrdenacaoPorInsercao<T> }

procedure TOrdenacaoPorInsercao<T>.fazerOrdenacao;
var
  i, j: Integer;
begin
  for j := 1 to total - 1 do
  begin
    i := j - 1;
    while ((i >= 0) and eOrdenavel(i, i + 1)) do
    begin
      trocar(i, i + 1);
      i := i - 1;
    end;
  end;
end;

end.
