(*******************************************************************************
 *
 * Arquivo  : Ordenacoes.Validador.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o classificador base da valida��o de ordena��es. Essa vali-
 *            da��o � executada em tempo O(n).
 *
 ******************************************************************************)
unit Ordenacoes.Validador;

interface
uses
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

type
  TValidadorOrdenacao<T> = class abstract(TInterfacedObject, IValidadorOrdenacao<T>)
  private
    fComparador: IComparador<T>;
  protected
    function estaOrdenado(const esquerda, direita: T): Boolean; virtual; abstract;

    property comparador: IComparador<T> read fComparador;
  public
    constructor Create();                                       overload;
    constructor Create(const comparacao: TFuncaoComparacao<T>); overload;
    constructor Create(const comparador: IComparador<T>);       overload;
    destructor  Destroy;                                        override;

    function validar(itens: TArray<T>): Boolean;
  end;


implementation
uses
  System.SysUtils, Recursos.Excecoes;

{ TValidadorOrdenacao<T> }

constructor TValidadorOrdenacao<T>.Create;
begin
  Create( TFabricaComparador.criar<T>() );
end;

constructor TValidadorOrdenacao<T>.Create(
  const comparacao: TFuncaoComparacao<T>);
begin
  Create( TFabricaComparador.criar<T>(comparacao) );
end;

constructor TValidadorOrdenacao<T>.Create(const comparador: IComparador<T>);
begin
  if (comparador = nil) then
    raise ArgumentoNulo('comparador') at ReturnAddress;

  fComparador := comparador;
end;

destructor TValidadorOrdenacao<T>.Destroy;
begin
  fComparador := nil;

  inherited Destroy;
end;

function TValidadorOrdenacao<T>.validar(itens: TArray<T>): Boolean;
var
  i, total: Integer;
begin
  total := Length(itens);
  i     := 1;

  while ((i < total) and estaOrdenado(itens[i - 1], itens[i])) do
    i := i + 1;

  Result := (i = total) or (total = 0);
end;

end.
