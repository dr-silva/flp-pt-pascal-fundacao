(*******************************************************************************
 *
 * Arquivo  : Ordenacoes.Quick3Way.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o classificador base da ordena��o Quick. Essa ordena��o �
 *            executada em tempo O(n * log_2(n)).
 *
 ******************************************************************************)
unit Ordenacoes.Quick3Way;

interface
uses
  Ordenacoes.Abstrato;

type
  TOrdenacaoQuick3Way<T> = class abstract(TOrdenacaoAbstrata<T>)
  private
    procedure ordenar(const minimo, maximo: Integer);
  protected
    function comparar(const esquerda, direita: T): Integer; virtual; abstract;
    procedure fazerOrdenacao; override;
  end;

implementation

{ TOrdenacaoQuick3Way<T> }

procedure TOrdenacaoQuick3Way<T>.fazerOrdenacao;
begin
  ordenar(0, total - 1);
end;

procedure TOrdenacaoQuick3Way<T>.ordenar(const minimo, maximo: Integer);
var
  i,
  menor,
  maior,
  comparacao: Integer;
  valor     : T;
begin
  if (minimo >= maximo) then
    Exit;

  menor := minimo;
  maior := maximo;
  i     := minimo + 1;
  valor := item[minimo];

  while (i <= maior) do
  begin
    comparacao := comparar(item[i], valor);

    if (comparacao < 0) then
    begin
      trocar(menor, i);

      menor := menor + 1;
      i     := i     + 1;
    end
    else if (comparacao > 0) then
    begin
      trocar(i, maior);

      maior := maior - 1;
    end
    else
      i := i + 1;
  end;

  ordenar(minimo, menor - 1);
  ordenar(maior + 1, maximo);
end;

end.
