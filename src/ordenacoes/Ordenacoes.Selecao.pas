(*******************************************************************************
 *
 * Arquivo  : Ordenacoes.Selecao.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o classificador base da ordena��o por sele��o. Essa orde-
 *            na��o � executada em tempo O(n^2).
 *
 ******************************************************************************)
unit Ordenacoes.Selecao;

interface
uses
  Ordenacoes.Abstrato;

type
  TOrdenacaoPorSelecao<T> = class abstract(TOrdenacaoAbstrata<T>)
  protected
    function eOrdenavel(const i, j: Integer): Boolean; virtual; abstract;
    procedure fazerOrdenacao; override;
  end;

implementation

{ TOrdenacaoPorSelecao<T> }

procedure TOrdenacaoPorSelecao<T>.fazerOrdenacao;
var
  i, j, minimo: Integer;
begin
  for i := 0 to total - 1 do
  begin
    minimo := i;

    for j := i + 1 to total - 1 do
      if (eOrdenavel(minimo, j)) then
        minimo := j;

    trocar(i, minimo);
  end;
end;

end.
