(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Ordenacoes.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Ordena��es da Funda��o: aqui ser�o declaradas as classes que
 *            implementam as ordena��es Quick.
 *            Para maiores informa��es sobre os classificadores vistos aqui,
 *            consulte a documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Ordenacoes.Quick;

interface
uses
  Ordenacoes.Quick;

type
  { Define a classe da ordena��o Quick CRESCENTE. Essa ordena��o � executada   }
  { em tempo O(n * log_2(n)).                                                  }
  TOrdenacaoQuickAscendente<T> = class(TOrdenacaoQuick<T>)
  protected
    function eOrdenavel(const i, j: Integer): Boolean; override;
  end;

  { Define a classe da ordena��o Quick DECRESCENTE. Essa ordena��o � executada }
  { em tempo O(n * log_2(n)).                                                  }
  TOrdenacaoQuickDescendente<T> = class(TOrdenacaoQuick<T>)
  protected
    function eOrdenavel(const i, j: Integer): Boolean; override;
  end;

implementation

{ TOrdenacaoQuickAscendente<T> }

function TOrdenacaoQuickAscendente<T>.eOrdenavel(const i, j: Integer): Boolean;
begin
  Result := (comparador.Compare(item[i], item[j]) <= 0);
end;

{ TOrdenacaoQuickDescendente<T> }

function TOrdenacaoQuickDescendente<T>.eOrdenavel(const i, j: Integer): Boolean;
begin
  Result := (comparador.Compare(item[i], item[j]) >= 0);
end;

end.
