(*******************************************************************************
 *
 * Arquivo  : Ordenacoes.Balde.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o classificador base da ordena��o por balde. Essa ordena��o
 *            � executada em tempo O(n).
 *
 ******************************************************************************)
unit Ordenacoes.Balde;

interface
uses
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes,
  Ordenacoes.Abstrato;

type
  TOrdenacaoPorBalde<T> = class abstract(TInterfacedObject, IOrdenacao<T>)
  private type
    PItem = ^TItem;
    TItem = record
    public
      valor   : T;
      projecao: Double;
      ulterior,
      anterior: PItem;
    end;
  private
    fTotal   : Integer;
    fItens   : TArray<T>;
    fBaldes  : array of PItem;
    fProjetor: IProjetor<T, Double>;

    procedure criarItem(const valor: T);
    procedure destruirItem(item: PItem);
    procedure limparBaldes;

    procedure porEmOrdem(const indice: Integer);
    function  transferir(const indice, balde: Integer): Integer;
    function  trocar(esquerda, direita: PItem): PItem;
  protected
    function indexar(const indice: Integer): Integer; virtual; abstract;

    property total: Integer read fTotal;
  public
    constructor Create(const projecao: TFuncaoProjecao<T, Double>); overload;
    constructor Create(const projetor: IProjetor<T, Double>);       overload;
    destructor  Destroy; override;

    procedure ordenar(itens: TArray<T>);
  end;

implementation
uses
  System.SysUtils, System.Math, Recursos.Excecoes;

{ TOrdenacaoPorBalde<T> }

constructor TOrdenacaoPorBalde<T>.Create(
  const projecao: TFuncaoProjecao<T, Double>);
begin
  Create( TFabricaProjetor.criar<T, Double>(projecao) );
end;

constructor TOrdenacaoPorBalde<T>.Create(const projetor: IProjetor<T, Double>);
begin
  if (projetor = nil) then
    raise ArgumentoNulo('projetor') at ReturnAddress;

  fProjetor := projetor;
end;

procedure TOrdenacaoPorBalde<T>.criarItem(const valor: T);
var
  item  ,
  lista : PItem;
  indice: Integer;
begin
  New(item);

  try
    item^.valor    := valor;
    item^.projecao := fProjetor.projetar(valor);
    item^.ulterior := nil;
    item^.anterior := nil;
  except
    FreeMem(item);
    raise;
  end;

  indice := Floor(fTotal * item^.projecao);
  lista  := fBaldes[indice];

  item^.ulterior := lista;
  if (lista <> nil) then
    lista^.anterior := item;

  fBaldes[indice] := item;
end;

destructor TOrdenacaoPorBalde<T>.Destroy;
begin
  if (fBaldes <> nil) then
    limparBaldes;

  inherited Destroy;
end;

procedure TOrdenacaoPorBalde<T>.destruirItem(item: PItem);
var
  temp: PItem;
begin
  while (item <> nil) do
  begin
    temp := item;
    item := item^.ulterior;

    FreeMem(temp);
  end;
end;

procedure TOrdenacaoPorBalde<T>.limparBaldes;
var
  i: Integer;
begin
  for i := 0 to total - 1 do
    destruirItem(fBaldes[i]);

  SetLength(fBaldes, 0);
  fBaldes := nil;
end;

procedure TOrdenacaoPorBalde<T>.porEmOrdem(const indice: Integer);
var
  fixo    ,
  movel   ,
  primeiro: PItem;
begin
  primeiro := fBaldes[indice];
  if (primeiro = nil) then
    Exit;

  fixo := primeiro^.ulterior;
  while (fixo <> nil) do
  begin
    movel := fixo^.anterior;

    while ((movel <> nil) and (movel^.projecao > movel^.ulterior^.projecao)) do
    begin
      movel := trocar(movel, movel^.ulterior);
      movel := movel^.anterior;
    end;

    fixo := fixo^.ulterior;
  end;

  while (primeiro^.anterior <> nil) do
    primeiro := primeiro^.anterior;

  fBaldes[indice] := primeiro;
end;

procedure TOrdenacaoPorBalde<T>.ordenar(itens: TArray<T>);
var
  i, indice: Integer;
begin
  fTotal := Length(itens);
  fItens := itens;

  if (fTotal <= 0) then
    Exit;

  SetLength(fBaldes, total);

  for i := 0 to total - 1 do
    criarItem(itens[i]);

  for i := 0 to total - 1 do
    porEmOrdem(i);

  indice := 0;
  for i := 0 to total - 1 do
    indice := transferir(indice, i);

  limparBaldes;
end;

function TOrdenacaoPorBalde<T>.transferir(const indice,
  balde: Integer): Integer;
var
  item: PItem;
begin
  item   := fBaldes[balde];
  Result := indice;

  while (item <> nil) do
  begin
    fItens[ indexar(Result) ] := item^.valor;

    item   := item^.ulterior;
    Result := Result + 1;
  end;
end;

function TOrdenacaoPorBalde<T>.trocar(esquerda, direita: PItem): PItem;
var
  extremaEsquerda,
  extremaDireita : PItem;
begin
  extremaEsquerda := esquerda^.anterior;
  extremaDireita  := direita ^.ulterior;

  if (extremaEsquerda <> nil) then
    extremaEsquerda^.ulterior := direita;
  if (extremaDireita <> nil) then
    extremaDireita^.anterior := esquerda;

  direita ^.ulterior := esquerda;
  esquerda^.ulterior := extremaDireita;
  esquerda^.anterior := direita;
  direita ^.anterior := extremaEsquerda;

  Result := direita;
end;

end.
