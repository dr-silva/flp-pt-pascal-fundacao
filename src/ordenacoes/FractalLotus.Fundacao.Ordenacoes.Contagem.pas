(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Ordenacoes.Contagem.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Ordena��es da Funda��o: aqui ser�o declaradas as classes que
 *            implementam as ordena��es por contagem.
 *            Para maiores informa��es sobre os classificadores vistos aqui,
 *            consulte a documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Ordenacoes.Contagem;

interface
uses
  Ordenacoes.Contagem;

type
  { Define a classe da ordena��o por contagem CRESCENTE. Essa ordena��o �      }
  { executada em tempo O(n).                                                   }
  TOrdenacaoPorContagemAscendente<T> = class(TOrdenacaoPorContagem<T>)
  protected
    function indexarSaida(const indice: Integer): Integer; override;
  end;

  { Define a classe da ordena��o por contagem DECRESCENTE. Essa ordena��o �    }
  { executada em tempo O(n).                                                   }
  TOrdenacaoPorContagemDescendente<T> = class(TOrdenacaoPorContagem<T>)
  protected
    function indexarSaida(const indice: Integer): Integer; override;
  end;

implementation

{ TOrdenacaoPorContagemAscendente<T> }

function TOrdenacaoPorContagemAscendente<T>.indexarSaida(const
  indice: Integer): Integer;
begin
  Result := indice;
end;

{ TOrdenacaoPorContagemDescendente<T> }

function TOrdenacaoPorContagemDescendente<T>.indexarSaida(const
  indice: Integer): Integer;
begin
  Result := total - (indice + 1);
end;

end.
