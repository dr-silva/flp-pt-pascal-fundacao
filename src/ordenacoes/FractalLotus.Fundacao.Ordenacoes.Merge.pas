(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Ordenacoes.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-18 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Ordena��es da Funda��o: aqui ser�o declaradas as classes que
 *            implementam as ordena��es Merge.
 *            Para maiores informa��es sobre os classificadores vistos aqui,
 *            consulte a documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Ordenacoes.Merge;

interface
uses
  Ordenacoes.Merge;

type
  { Define a classe da ordena��o Merge CRESCENTE. Essa ordena��o � executada   }
  { em tempo O(n * log_2(n)).                                                  }
  TOrdenacaoMergeAscendente<T> = class(TOrdenacaoMerge<T>)
  protected
    function eOrdenavel(const esquerda, direita: T): Boolean; override;
  end;

  { Define a classe da ordena��o Merge DECRESCENTE. Essa ordena��o � executada }
  { em tempo O(n * log_2(n)).                                                  }
  TOrdenacaoMergeDescendente<T> = class(TOrdenacaoMerge<T>)
  protected
    function eOrdenavel(const esquerda, direita: T): Boolean; override;
  end;

implementation

{ TOrdenacaoMergeAscendente<T> }

function TOrdenacaoMergeAscendente<T>.eOrdenavel(const esquerda,
  direita: T): Boolean;
begin
  Result := (comparador.Compare(esquerda, direita) <= 0);
end;

{ TOrdenacaoMergeDescendente<T> }

function TOrdenacaoMergeDescendente<T>.eOrdenavel(const esquerda,
  direita: T): Boolean;
begin
  Result := (comparador.Compare(esquerda, direita) >= 0);
end;

end.
