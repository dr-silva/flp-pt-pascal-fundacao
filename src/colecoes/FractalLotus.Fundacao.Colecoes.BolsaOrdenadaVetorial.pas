(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.BolsaOrdenadaVetorial.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o vetorial e ordenada que implementa a interface
 *            IGuard�vel<T> cujo objetivo � ter uma cole��o em que n�o haja a
 *            necessidade de remover itens dela.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.BolsaOrdenadaVetorial;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes, Colecoes.VetorizacaoOrdenada;

type
  TBolsaOrdenadaVetorial<T> = class
  (
    TVetorizacaoOrdenada<T, Pointer>,
    IEnumeravel<T>,
    IGuardavel <T>
  )
  private
    function obterSomenteLeitura: Boolean; inline;
    function obterPrimeiro      : T;       inline;
    function obterUltimo        : T;       inline;
  public
    function ToString: string;    override;
    function ToArray : TArray<T>;

    function GetEnumerator: TEnumerator<T>;

    procedure guardar(const valor: T);                overload;
    procedure guardar(const valores: TArray<T>);      overload;
    procedure guardar(const valores: IEnumeravel<T>); overload;

    property primeiro      : T       read obterPrimeiro;
    property ultimo        : T       read obterUltimo;
    property somenteLeitura: Boolean read obterSomenteLeitura;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes,
  Extensores.ExtensorPadrao;

{ TBolsaOrdenadaVetorial<T> }

procedure TBolsaOrdenadaVetorial<T>.guardar(const valores: TArray<T>);
var
  i: Integer;
begin
  for i := Low(valores) to High(valores) do
    guardar(valores[i]);
end;

procedure TBolsaOrdenadaVetorial<T>.guardar(const valor: T);
var
  i: Integer;
begin
  if cheio then
    raise ColecaoCheia at ReturnAddress
  else if vazio then
    inserirValor(0, valor, nil)
  else
  begin
    i := indexar(valor);

    if (comparar(valor, i) < 0) then
      inserirValor(i, valor, nil)
    else
      inserirValor(i + 1, valor, nil);
  end;
end;

function TBolsaOrdenadaVetorial<T>.GetEnumerator: TEnumerator<T>;
begin
  Result := TEnumeradorChaves.Create(Self, 0, total);
end;

procedure TBolsaOrdenadaVetorial<T>.guardar(const valores: IEnumeravel<T>);
var
  valor: T;
begin
  for valor in valores do
    guardar(valor);
end;

function TBolsaOrdenadaVetorial<T>.obterPrimeiro: T;
begin
  Result := minimo;
end;

function TBolsaOrdenadaVetorial<T>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TBolsaOrdenadaVetorial<T>.obterUltimo: T;
begin
  Result := maximo;
end;

function TBolsaOrdenadaVetorial<T>.ToArray: TArray<T>;
begin
  Result := TExtensorPadrao.toArray<T>(Self);
end;

function TBolsaOrdenadaVetorial<T>.ToString: string;
begin
  Result := TExtensorPadrao.toString<T>(Self);
end;

end.
