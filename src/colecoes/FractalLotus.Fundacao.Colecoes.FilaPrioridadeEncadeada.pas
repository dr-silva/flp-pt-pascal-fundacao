(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.FilaPrioridadeEncadeada.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: DeFIne uma cole��o de �rvore ligadaque � uma fila cujos elementos
 *            t�m prioridade para se ordenarem nela:
 *              - PRIORIDADE M�NIMA: aquele com prioridade MENOR estar� � frente
 *                                   na fila, � feita uma ordena��o CRESCENTE;
 *              - PRIORIDADE M�XIMA: aquele com prioridade MAIOR estar� � frente
 *                                   na fila, � feita uma ordena��o DECRESCENTE.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.FilaPrioridadeEncadeada;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes, Colecoes.HeapEncadeado;

type
  TFilaPrioridadeEncadeada<T> = class abstract
  (
    THeapEncadeado<T>, IEnfileiravel<T>
  )
  protected
    function obterSomenteLeitura: Boolean; override;
  public
    procedure enfileirar(const valor: T);
    function  desenfileirar: T;
    function  espiar: T;
    procedure limpar;
  end;

  TFilaPrioridadeMinimaEncadeada<T> = class (TFilaPrioridadeEncadeada<T>)
  protected
    function eOrdenavel(comparacao: Integer): Boolean; override;
  end;

  TFilaPrioridadeMaximaEncadeada<T> = class (TFilaPrioridadeEncadeada<T>)
  protected
    function eOrdenavel(comparacao: Integer): Boolean; override;
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes;

{ TFilaPrioridadeEncadeada<T> }

function TFilaPrioridadeEncadeada<T>.desenfileirar: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := excluir(1);
end;

procedure TFilaPrioridadeEncadeada<T>.enfileirar(const valor: T);
begin
  if cheio then
    raise ColecaoCheia at ReturnAddress;

  inserir(valor);
end;

function TFilaPrioridadeEncadeada<T>.espiar: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := buscarNo(1)^.dado;
end;

procedure TFilaPrioridadeEncadeada<T>.limpar;
begin
  excluirTodos;
end;

function TFilaPrioridadeEncadeada<T>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

{ TFilaPrioridadeMinimaEncadeada<T> }

function TFilaPrioridadeMinimaEncadeada<T>.eOrdenavel(
  comparacao: Integer): Boolean;
begin
  Result := comparacao > 0;
end;

{ TFilaPrioridadeMaximaEncadeada<T> }

function TFilaPrioridadeMaximaEncadeada<T>.eOrdenavel(
  comparacao: Integer): Boolean;
begin
  Result := comparacao < 0;
end;

end.
