(*******************************************************************************
 *
 * Arquivo  : Colecoes.ArvoreRubronegra.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma classe b�sica e abstrata para cole��es ordenadas, mas
 *            que n�o necessariamente implementem o IOrden�vel, feitas atrav�s
 *            de �rvore bin�ria.
 *
 ******************************************************************************)
unit Colecoes.ArvoreRubronegra;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.Colecoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

type
  TArvoreRubronegra<TChave, TValor> = class abstract (TInterfacedObject)
  protected type
    PNo = ^TNo;
    TNo = record
      chave    : TChave;
      valor    : TValor;
      cor      : Boolean;
      tamanho  : Integer;
      direita  ,
      esquerda ,
      ancestral: PNo;
    end;

    TEnumerador<T> = class abstract (TEnumerator<T>)
    private
      fColecao   : TArvoreRubronegra<TChave, TValor>;
      fAscendente: Boolean;
      fAtual     ,
      fProximo   ,
      fUltimo    : PNo;
    protected
      function DoMoveNext: Boolean; override;

      property atual: PNo read fAtual;
    public
      constructor Create(const colecao        : TArvoreRubronegra<TChave, TValor>;
                         const ascendente     : Boolean;
                         const proximo, ultimo: PNo);
      destructor  Destroy; override;
    end;

    TEnumeradorChaves = class (TEnumerador<TChave>)
    protected
      function DoGetCurrent: TChave; override;
    end;

    TEnumeradorValores = class (TEnumerador<TValor>)
    protected
      function DoGetCurrent: TValor; override;
    end;

    TEnumeradorPares = class (TEnumerador<TPair<TChave, TValor>>)
    protected
      function DoGetCurrent: TPair<TChave, TValor>; override;
    end;

    TColecaoArborea<T> = class abstract (TInterfacedObject, IEnumeravel<T>)
    private
      fAscendente: Boolean;
      fColecao   : TArvoreRubronegra<TChave, TValor>;
      fPrimeiro  ,
      fUltimo    : PNo;

      function obterTotal         : Integer;
      function obterVazio         : Boolean; inline;
      function obterCheio         : Boolean; inline;
      function obterSomenteLeitura: Boolean; inline;
    protected
      property ascendente: Boolean                           read fAscendente;
      property colecao   : TArvoreRubronegra<TChave, TValor> read fColecao;
      property primeiro  : PNo                               read fPrimeiro;
      property ultimo    : PNo                               read fUltimo;
    public
      constructor Create(const colecao : TArvoreRubronegra<TChave, TValor>;
                         const primeiro,
                               ultimo  : PNo);
      destructor  Destroy; override;

      function ToString: string; override;
      function ToArray : TArray<T>;

      function GetEnumerator: TEnumerator<T>; virtual; abstract;

      property total         : Integer read obterTotal;
      property vazio         : Boolean read obterVazio;
      property cheio         : Boolean read obterCheio;
      property somenteLeitura: Boolean read obterSomenteLeitura;
    end;

    TColecaoChaves = class (TColecaoArborea<TChave>)
    public
      function GetEnumerator: TEnumerator<TChave>; override;
    end;

    TColecaoValores = class (TColecaoArborea<TValor>)
    public
      function GetEnumerator: TEnumerator<TValor>; override;
    end;

    TColecaoPares = class (TColecaoArborea<TPair<TChave, TValor>>)
    public
      function GetEnumerator: TEnumerator<TPair<TChave, TValor>>; override;
    end;
  private
    fRaiz      : PNo;
    fComparador: IComparador<TChave>;
    const VERMELHO = False;
    const PRETO    = True;

    function obterTotal: Integer; inline;
    function obterVazio: Boolean; inline;
    function obterCheio: Boolean; inline;

    function eVermelho(const no: PNo): Boolean; inline;
    function ePreto   (const no: PNo): Boolean; inline;

    procedure girarParaEsquerda(no: PNo);
    procedure girarParaDireita (no: PNo);

    procedure consertarInsercao        (no: PNo);
    function  consertarInsercaoEsquerda(no: PNo): PNo;
    function  consertarInsercaoDireita (no: PNo): PNo;

    procedure consertarExclusao        (no, pai: PNo);
    function  consertarExclusaoEsquerda(no, pai: PNo): PNo;
    function  consertarExclusaoDireita (no, pai: PNo): PNo;
  protected
    function criarNo(const chave: TChave; const valor: TValor): PNo;
    procedure destruirNo(no: PNo);

    function buscarNoMinimo     (no: PNo): PNo;
    function buscarNoMaximo     (no: PNo): PNo;
    function buscarNoPredecessor(no: PNo): PNo;
    function buscarNoSucessor   (no: PNo): PNo;

    function buscarNo    (const chave: TChave): PNo;
    function buscarNoPiso(const chave: TChave): PNo;
    function buscarNoTeto(const chave: TChave): PNo;

    function indexarNo(no: PNo): Integer;
    function desindexarNo(const indice: Integer): PNo;

    procedure inserirNo(no: PNo);
    procedure excluirNo(no: PNo);

    property raiz      : PNo                 read fRaiz       write fRaiz;
    property comparador: IComparador<TChave> read fComparador;
  public
    constructor Create;                                              overload;
    constructor Create(const comparacao: TFuncaoComparacao<TChave>); overload;
    constructor Create(const comparador: IComparador<TChave>);       overload;
    destructor  Destroy;                                             override;

    property total: Integer read obterTotal;
    property vazio: Boolean read obterVazio;
    property cheio: Boolean read obterCheio;
  end;

  TArvoreRubronegraOrdenada<TChave, TValor> = class abstract
  (
    TArvoreRubronegra<TChave, TValor>,
    IOrdenavel<TChave>
  )
  private
    function obterMinimo: TChave; inline;
    function obterMaximo: TChave; inline;
  public
    function piso(const chave: TChave): TChave;
    function teto(const chave: TChave): TChave;

    function indexar   (const chave : TChave ): Integer;
    function desindexar(const indice: Integer): TChave;

    function contar     (const inferior, superior: TChave): Integer;
    function obterChaves(const inferior, superior: TChave): IEnumeravel<TChave>;

    property minimo: TChave read obterMinimo;
    property maximo: TChave read obterMaximo;
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes, Extensores.ExtensorPadrao;

{ TArvoreRubronegra<TChave, TValor> }

function TArvoreRubronegra<TChave, TValor>.buscarNo(const chave: TChave): PNo;
var
  comparacao: Integer;
begin
  Result := fRaiz;

  while (Result <> nil) do
  begin
    comparacao := fComparador.Compare(chave, Result^.chave);

    if (comparacao < 0) then
      Result := Result^.esquerda
    else if (comparacao > 0) then
      Result := Result^.direita
    else
      Break;
  end;
end;

function TArvoreRubronegra<TChave, TValor>.buscarNoMaximo(no: PNo): PNo;
begin
  Result := no;

  if (Result <> nil) then
    while (Result^.direita <> nil) do
      Result := Result^.direita;
end;

function TArvoreRubronegra<TChave, TValor>.buscarNoMinimo(no: PNo): PNo;
begin
  Result := no;

  if (Result <> nil) then
    while (Result^.esquerda <> nil) do
      Result := Result^.esquerda;
end;

function TArvoreRubronegra<TChave, TValor>.buscarNoPiso(
  const chave: TChave): PNo;
var
  no: PNo;
  comparacao: Integer;
begin
  Result := nil;
  no     := fRaiz;

  while (no <> nil) do
  begin
    comparacao := fComparador.Compare(chave, no^.chave);

    if (comparacao < 0) then
      no := no^.esquerda
    else if (comparacao > 0) then
    begin
      Result := no;
      no     := no^.direita;
    end
    else
    begin
      Result := no;
      no     := nil;
    end;
  end;
end;

function TArvoreRubronegra<TChave, TValor>.buscarNoPredecessor(no: PNo): PNo;
begin
  if (no = nil) then
    Result := nil
  else if (no^.esquerda <> nil) then
    Result := buscarNoMaximo(no^.esquerda)
  else
  begin
    Result := no^.ancestral;
    while ((Result <> nil) and (no = Result^.esquerda)) do
    begin
      no     := Result;
      Result := Result^.ancestral;
    end;
  end;
end;

function TArvoreRubronegra<TChave, TValor>.buscarNoSucessor(no: PNo): PNo;
begin
  if (no = nil) then
    Result := nil
  else if (no^.direita <> nil) then
    Result := buscarNoMinimo(no^.direita)
  else
  begin
    Result := no^.ancestral;
    while ((Result <> nil) and (no = Result^.direita)) do
    begin
      no     := Result;
      Result := Result^.ancestral;
    end;
  end;
end;

function TArvoreRubronegra<TChave, TValor>.buscarNoTeto(
  const chave: TChave): PNo;
var
  no: PNo;
  comparacao: Integer;
begin
  Result := nil;
  no     := fRaiz;

  while (no <> nil) do
  begin
    comparacao := fComparador.Compare(chave, no^.chave);

    if (comparacao < 0) then
    begin
      Result := no;
      no     := no^.esquerda;
    end
    else if (comparacao > 0) then
      no := no^.direita
    else
    begin
      Result := no;
      no     := nil;
    end;
  end;
end;

procedure TArvoreRubronegra<TChave, TValor>.consertarExclusao(no, pai: PNo);
var
  neoPai: PNo;
begin
  while ((no <> fRaiz) and ePreto(no)) do
  begin
    if (no = nil) then
      neoPai := pai
    else
      neoPai := no^.ancestral;

    if (no = neoPai^.esquerda) then
      no := consertarExclusaoEsquerda(no, neoPai)
    else
      no := consertarExclusaoDireita(no, neoPai);
  end;

  if (no <> nil) then
    no^.cor := PRETO;
end;

function TArvoreRubronegra<TChave, TValor>.consertarExclusaoDireita(no,
  pai: PNo): PNo;
var
  irmao: PNo;
begin
  irmao := pai^.esquerda;

  if eVermelho(irmao) then
  begin
    irmao^.cor := PRETO;
    pai  ^.cor := VERMELHO;

    girarParaDireita(pai);

    irmao := pai^.esquerda;
  end;

  if ((irmao = nil) or (ePreto(irmao^.esquerda) and ePreto(irmao^.direita))) then
  begin
    if (irmao <> nil) then
      irmao^.cor := VERMELHO;

    Result  := pai;
  end
  else
  begin
    if ePreto(irmao^.esquerda) then
    begin
      if (irmao^.direita <> nil) then
        irmao^.direita^.cor := PRETO;

      irmao^.cor := VERMELHO;

      girarParaEsquerda(irmao);

      irmao := pai^.esquerda;
    end;

    irmao          ^.cor := pai^.cor;
    pai            ^.cor := PRETO;
    irmao^.esquerda^.cor := PRETO;

    girarParaDireita(pai);

    Result := fRaiz;
  end;
end;

function TArvoreRubronegra<TChave, TValor>.consertarExclusaoEsquerda(no,
  pai: PNo): PNo;
var
  irmao: PNo;
begin
  irmao := pai^.direita;

  if eVermelho(irmao) then
  begin
    irmao^.cor := PRETO;
    pai  ^.cor := VERMELHO;

    girarParaEsquerda(pai);

    irmao := pai^.direita;
  end;

  if ((irmao = nil) or (ePreto(irmao^.esquerda) and ePreto(irmao^.direita))) then
  begin
    if (irmao <> nil) then
      irmao^.cor := VERMELHO;

    Result := pai;
  end
  else
  begin
    if ePreto(irmao^.direita) then
    begin
      if (irmao^.esquerda <> nil) then
        irmao^.esquerda^.cor := PRETO;

      irmao^.cor := VERMELHO;

      girarParaDireita(irmao);

      irmao := pai^.direita;
    end;

    irmao         ^.cor := pai^.cor;
    pai           ^.cor := PRETO;
    irmao^.direita^.cor := PRETO;

    girarParaEsquerda(pai);

    Result := fRaiz;
  end;
end;

procedure TArvoreRubronegra<TChave, TValor>.consertarInsercao(no: PNo);
var
  pai,
  avo: PNo;
begin
  pai := no^.ancestral;
  while eVermelho(pai) do
  begin
    avo := pai^.ancestral;
    if (avo = nil) then
      Break;

    if (pai = avo^.esquerda) then
      no := consertarInsercaoEsquerda(no)
    else
      no := consertarInsercaoDireita(no);

    pai := no^.ancestral;
  end;

  fRaiz.cor := PRETO;
end;

function TArvoreRubronegra<TChave, TValor>.consertarInsercaoDireita(
  no: PNo): PNo;
var
  tio: PNo;
begin
  tio := no^.ancestral^.ancestral^.esquerda;
  if eVermelho(tio) then
  begin
    no^.ancestral^           .cor := PRETO;
    tio^                     .cor := PRETO;
    no^.ancestral^.ancestral^.cor := VERMELHO;
    Result                        := no^.ancestral^.ancestral;
  end
  else
  begin
    if (no = no^.ancestral^.esquerda) then
    begin
      no := no^.ancestral;
      girarParaDireita(no);
    end;

    no^.ancestral^           .cor := PRETO;
    no^.ancestral^.ancestral^.cor := VERMELHO;

    girarParaEsquerda(no^.ancestral^.ancestral);

    Result := no;
  end;
end;

function TArvoreRubronegra<TChave, TValor>.consertarInsercaoEsquerda(
  no: PNo): PNo;
var
  tio: PNo;
begin
  tio := no^.ancestral^.ancestral^.direita;
  if eVermelho(tio) then
  begin
    no^.ancestral^           .cor := PRETO;
    tio^                     .cor := PRETO;
    no^.ancestral^.ancestral^.cor := VERMELHO;
    Result                        := no^.ancestral^.ancestral;
  end
  else
  begin
    if (no = no^.ancestral^.direita) then
    begin
      no := no^.ancestral;
      girarParaEsquerda(no);
    end;

    no^.ancestral^           .cor := PRETO;
    no^.ancestral^.ancestral^.cor := VERMELHO;

    girarParaDireita(no^.ancestral^.ancestral);

    Result := no;
  end;
end;

constructor TArvoreRubronegra<TChave, TValor>.Create;
begin
  Create
  (
    TFabricaComparador.criar<TChave>()
  );
end;

constructor TArvoreRubronegra<TChave, TValor>.Create(
  const comparacao: TFuncaoComparacao<TChave>);
begin
  Create
  (
    TFabricaComparador.criar<TChave>(comparacao)
  );
end;

constructor TArvoreRubronegra<TChave, TValor>.Create(
  const comparador: IComparador<TChave>);
begin
  if (comparador = nil) then
    raise ArgumentoNulo('comparador') at ReturnAddress;

  fComparador := comparador;
  fRaiz       := nil;
end;

function TArvoreRubronegra<TChave, TValor>.criarNo(const chave: TChave;
  const valor: TValor): PNo;
begin
  New(Result);

  Result^.chave     := chave;
  Result^.valor     := valor;
  Result^.cor       := VERMELHO;
  Result^.tamanho   := 1;
  Result^.ancestral := nil;
  Result^.esquerda  := nil;
  Result^.direita   := nil;
end;

function TArvoreRubronegra<TChave, TValor>.desindexarNo(
  const indice: Integer): PNo;
var
  i,
  idx: Integer;
begin
  if ((indice < 1) or (indice > total)) then
    Exit(nil);

  idx := indice;
  Result  := fRaiz;

  while (idx > 0) do
  begin
    if (Result^.esquerda <> nil) then
      i := Result^.esquerda^.tamanho + 1
    else
      i := 1;

    if (idx = i) then
      Break
    else if (idx < i) then
      Result := Result^.esquerda
    else
    begin
      idx    := idx - i;
      Result := Result^.direita;
    end;
  end;
end;

destructor TArvoreRubronegra<TChave, TValor>.Destroy;
begin
  destruirNo(fRaiz);

  inherited;
end;

procedure TArvoreRubronegra<TChave, TValor>.destruirNo(no: PNo);
var
  temp: PNo;
begin
  while (no <> nil) do
    if ((no^.esquerda = nil) and (no^.direita = nil)) then
    begin
      temp := no;
      no   := no^.ancestral;
      if (no <> nil) then
      begin
        if (temp = no^.esquerda) then
          no^.esquerda := nil
        else
          no^.direita := nil;
      end;

      temp^.chave := default(TChave);
      temp^.valor := default(TValor);

      FreeMem(temp);
    end
    else if (no^.esquerda <> nil) then
      no := no^.esquerda
    else
      no := no^.direita;
end;

function TArvoreRubronegra<TChave, TValor>.ePreto(const no: PNo): Boolean;
begin
  Result := (no = nil) or (no^.cor = PRETO);
end;

function TArvoreRubronegra<TChave, TValor>.eVermelho(const no: PNo): Boolean;
begin
  Result := (no <> nil) and (no^.cor = VERMELHO);
end;

procedure TArvoreRubronegra<TChave, TValor>.excluirNo(no: PNo);
var
  x, extraido: PNo;
begin
  if (no = nil) then
    Exit;

  if ((no^.esquerda = nil) or (no^.direita = nil)) then
    extraido := no
  else
    extraido := buscarNoSucessor(no);

  if (extraido^.esquerda <> nil) then
    x := extraido^.esquerda
  else
    x := extraido^.direita;

  if (x <> nil) then
    x^.ancestral := extraido^.ancestral;

  if (extraido^.ancestral = nil) then
    fRaiz := x
  else if (extraido = extraido^.ancestral^.esquerda) then
    extraido^.ancestral^.esquerda := x
  else
    extraido^.ancestral^.direita := x;

  if (extraido <> no) then
  begin
    no^.chave   := extraido^.chave;
    no^.valor   := extraido^.valor;
  end;

  repeat
    no^.tamanho := no^.tamanho - 1;
    no          := no^.ancestral;
  until (no = nil);

  if (extraido^.cor = PRETO) then
    consertarExclusao(x, extraido^.ancestral);

  extraido^.ancestral := nil;
  extraido^.esquerda  := nil;
  extraido^.direita   := nil;

  destruirNo(extraido);
end;

procedure TArvoreRubronegra<TChave, TValor>.girarParaDireita(no: PNo);
var
  pivo: PNo;
begin
  if ((no = nil) or (no^.esquerda = nil)) then
    Exit;

  pivo            := no  ^.esquerda;
  no  ^.esquerda  := pivo^.direita;
  pivo^.ancestral := no  ^.ancestral;

  if (pivo^.direita <> nil) then
    pivo^.direita^.ancestral := no;

  if (no^.ancestral = nil) then
    fRaiz := pivo
  else if (no = no^.ancestral^.esquerda) then
    no^.ancestral^.esquerda := pivo
  else
    no^.ancestral^.direita := pivo;

  pivo^.direita   := no;
  no  ^.ancestral := pivo;
  pivo^.tamanho   := no^.tamanho;
  no  ^.tamanho   := 1;

  if (no^.esquerda <> nil) then
    no^.tamanho := no^.tamanho + no^.esquerda^.tamanho;
  if (no^.direita <> nil) then
    no^.tamanho := no^.tamanho + no^.direita^.tamanho;
end;

procedure TArvoreRubronegra<TChave, TValor>.girarParaEsquerda(no: PNo);
var
  pivo: PNo;
begin
  if ((no = nil) or (no^.direita = nil)) then
    Exit;

  pivo            := no  ^.direita;
  no  ^.direita   := pivo^.esquerda;
  pivo^.ancestral := no  ^.ancestral;

  if (pivo^.esquerda <> nil) then
    pivo^.esquerda^.ancestral := no;

  if (no^.ancestral = nil) then
    fRaiz := pivo
  else if (no = no^.ancestral^.esquerda) then
    no^.ancestral^.esquerda := pivo
  else
    no^.ancestral^.direita := pivo;

  pivo^.esquerda  := no;
  no  ^.ancestral := pivo;
  pivo^.tamanho   := no^.tamanho;
  no  ^.tamanho   := 1;

  if (no^.esquerda <> nil) then
    no^.tamanho := no^.tamanho + no^.esquerda^.tamanho;
  if (no^.direita <> nil) then
    no^.tamanho := no^.tamanho + no^.direita^.tamanho;
end;

function TArvoreRubronegra<TChave, TValor>.indexarNo(no: PNo): Integer;
var
  x: PNo;
begin
  if (no = nil) then
    Exit(0);

  if (no^.esquerda = nil) then
    Result := 1
  else
    Result := no^.esquerda.tamanho + 1;

  x := no;
  while (x <> fRaiz) do
  begin
    if (x = x^.ancestral^.direita) then
    begin
      if (x^.ancestral^.esquerda = nil) then
        Result := Result + 1
      else
        Result := Result + x^.ancestral^.esquerda^.tamanho + 1;
    end;

    x := x^.ancestral;
  end;
end;

procedure TArvoreRubronegra<TChave, TValor>.inserirNo(no: PNo);
var
  pai,
  traco: PNo;
begin
  pai   := nil;
  traco := fRaiz;

  while (traco <> nil) do
  begin
    traco^.tamanho := traco^.tamanho + 1;
    pai            := traco;

    if (fComparador.Compare(no^.chave, traco^.chave) < 0) then
      traco := traco^.esquerda
    else
      traco := traco^.direita;
  end;

  no^.ancestral := pai;
  if (pai = nil) then
    fRaiz := no
  else if (fComparador.Compare(no^.chave, pai^.chave) < 0) then
    pai^.esquerda := no
  else
    pai^.direita := no;

  consertarInsercao(no);
end;

function TArvoreRubronegra<TChave, TValor>.obterCheio: Boolean;
begin
  Result := total = Integer.MaxValue;
end;

function TArvoreRubronegra<TChave, TValor>.obterTotal: Integer;
begin
  if (fRaiz = nil) then
    Result := 0
  else
    Result := fRaiz^.tamanho;
end;

function TArvoreRubronegra<TChave, TValor>.obterVazio: Boolean;
begin
  Result := fRaiz = nil;
end;

{ TArvoreRubronegra<TChave, TValor>.TEnumerador<T> }

constructor TArvoreRubronegra<TChave, TValor>.TEnumerador<T>.Create(
  const colecao: TArvoreRubronegra<TChave, TValor>; const ascendente: Boolean;
  const proximo, ultimo: PNo);
begin
  fColecao    := colecao;
  fAscendente := ascendente;
  fProximo    := proximo;
  fUltimo     := ultimo;
end;

destructor TArvoreRubronegra<TChave, TValor>.TEnumerador<T>.Destroy;
begin
  fColecao    := nil;
  fAtual      := nil;
  fProximo    := nil;
  fUltimo     := nil;

  inherited Destroy;
end;

function TArvoreRubronegra<TChave, TValor>.TEnumerador<T>.DoMoveNext: Boolean;
begin
  Result := (fProximo <> nil);

  if Result then
  begin
    fAtual := fProximo;

    if (fProximo = fUltimo) then
      fProximo := nil
    else if fAscendente then
      fProximo := fColecao.buscarNoSucessor(fProximo)
    else
      fProximo := fColecao.buscarNoPredecessor(fProximo);
  end;
end;

{ TArvoreRubronegra<TChave, TValor>.TEnumeradorChaves }

function TArvoreRubronegra<TChave, TValor>.TEnumeradorChaves.DoGetCurrent: TChave;
begin
  Result := atual.chave;
end;

{ TArvoreRubronegra<TChave, TValor>.TEnumeradorValores }

function TArvoreRubronegra<TChave, TValor>.TEnumeradorValores.DoGetCurrent: TValor;
begin
  Result := atual.valor;
end;

{ TArvoreRubronegra<TChave, TValor>.TEnumeradorPares }

function TArvoreRubronegra<TChave, TValor>.TEnumeradorPares.DoGetCurrent: TPair<TChave, TValor>;
begin
  Result.Key   := atual.chave;
  Result.Value := atual.valor;
end;

{ TArvoreRubronegra<TChave, TValor>.TColecaoArborea<T> }

constructor TArvoreRubronegra<TChave, TValor>.TColecaoArborea<T>.Create(
  const colecao: TArvoreRubronegra<TChave, TValor>; const primeiro,
  ultimo: PNo);
begin
  fColecao    := colecao;
  fPrimeiro   := primeiro;
  fUltimo     := ultimo;

  if ((fPrimeiro = nil) or (fUltimo = nil)) then
    fAscendente := True
  else
    fAscendente := colecao.comparador.Compare(primeiro^.chave, ultimo^.chave) <= 0;
end;

destructor TArvoreRubronegra<TChave, TValor>.TColecaoArborea<T>.Destroy;
begin
  fColecao    := nil;
  fPrimeiro   := nil;
  fUltimo     := nil;

  inherited Destroy;
end;

function TArvoreRubronegra<TChave, TValor>.TColecaoArborea<T>.obterCheio: Boolean;
begin
  Result := total = fColecao.total;
end;

function TArvoreRubronegra<TChave, TValor>.TColecaoArborea<T>.obterSomenteLeitura: Boolean;
begin
  Result := True;
end;

function TArvoreRubronegra<TChave, TValor>.TColecaoArborea<T>.obterTotal: Integer;
var
  idxPrimeiro,
  idxUltimo  : Integer;
begin
  idxPrimeiro := fColecao.indexarNo(fPrimeiro);
  idxUltimo   := fColecao.indexarNo(fUltimo);

  if fAscendente then
    Result := idxUltimo - idxPrimeiro + 1
  else
    Result := idxPrimeiro - idxUltimo + 1;
end;

function TArvoreRubronegra<TChave, TValor>.TColecaoArborea<T>.obterVazio: Boolean;
begin
  Result := total = 0;
end;

function TArvoreRubronegra<TChave, TValor>.TColecaoArborea<T>.ToArray: TArray<T>;
begin
  Result := TExtensorPadrao.toArray<T>(Self);
end;

function TArvoreRubronegra<TChave, TValor>.TColecaoArborea<T>.ToString: string;
begin
  Result := TExtensorPadrao.toString<T>(Self);
end;

{ TArvoreRubronegra<TChave, TValor>.TColecaoChaves }

function TArvoreRubronegra<TChave, TValor>.TColecaoChaves.GetEnumerator: TEnumerator<TChave>;
begin
  Result := TEnumeradorChaves.Create(colecao, ascendente, primeiro, ultimo);
end;

{ TArvoreRubronegra<TChave, TValor>.TColecaoValores }

function TArvoreRubronegra<TChave, TValor>.TColecaoValores.GetEnumerator: TEnumerator<TValor>;
begin
  Result := TEnumeradorValores.Create(colecao, ascendente, primeiro, ultimo);
end;

{ TArvoreRubronegra<TChave, TValor>.TColecaoPares }

function TArvoreRubronegra<TChave, TValor>.TColecaoPares.GetEnumerator: TEnumerator<TPair<TChave, TValor>>;
begin
  Result := TEnumeradorPares.Create(colecao, ascendente, primeiro, ultimo);
end;

{ TArvoreRubronegraOrdenada<TChave, TValor> }

function TArvoreRubronegraOrdenada<TChave, TValor>.contar(const inferior,
  superior: TChave): Integer;
var
  menor, maior: PNo;
begin
  if (comparador.Compare(inferior, superior) <= 0) then
  begin
    menor := buscarNoTeto(inferior);
    maior := buscarNoPiso(superior);
  end
  else
  begin
    menor := buscarNoTeto(superior);
    maior := buscarNoPiso(inferior);
  end;

  Result := indexarNo(maior) - indexarNo(menor) + 1;
end;

function TArvoreRubronegraOrdenada<TChave, TValor>.desindexar(
  const indice: Integer): TChave;
var
  no: PNo;
begin
  no := desindexarNo(indice + 1);

  if (no = nil) then
    raise IndiceForaLimites('indice', 0, total - 1) at ReturnAddress;

  Result := no^.chave;
end;

function TArvoreRubronegraOrdenada<TChave, TValor>.indexar(
  const chave: TChave): Integer;
var
  no: PNo;
begin
  no := buscarNoTeto(chave);

  if (no = nil) then
    Result := total
  else
    Result := indexarNo(no) - 1;
end;

function TArvoreRubronegraOrdenada<TChave, TValor>.obterChaves(const inferior,
  superior: TChave): IEnumeravel<TChave>;
var
  menor, maior: PNo;
begin
  if (comparador.Compare(inferior, superior) <= 0) then
  begin
    menor := buscarNoTeto(inferior);
    maior := buscarNoPiso(superior);
  end
  else
  begin
    menor := buscarNoTeto(superior);
    maior := buscarNoPiso(inferior);
  end;

  Result := TColecaoChaves.Create(Self, menor, maior);
end;

function TArvoreRubronegraOrdenada<TChave, TValor>.obterMaximo: TChave;
begin
  if (vazio) then
    raise ColecaoVazia at ReturnAddress;

  Result := buscarNoMaximo(raiz)^.chave;
end;

function TArvoreRubronegraOrdenada<TChave, TValor>.obterMinimo: TChave;
begin
  if (vazio) then
    raise ColecaoVazia at ReturnAddress;

  Result := buscarNoMinimo(raiz)^.chave;
end;

function TArvoreRubronegraOrdenada<TChave, TValor>.piso(
  const chave: TChave): TChave;
var
  no: PNo;
begin
  if (vazio) then
    raise ColecaoVazia at ReturnAddress;

  no := buscarNoPiso(chave);

  if (no = nil) then
    raise EArgumentOutOfRangeException.Create
    (
      'O par�metro "chave" � menor que a chave m�nima'
    );

  Result := no^.chave;
end;

function TArvoreRubronegraOrdenada<TChave, TValor>.teto(
  const chave: TChave): TChave;
var
  no: PNo;
begin
  if (vazio) then
    raise ColecaoVazia at ReturnAddress;

  no := buscarNoTeto(chave);

  if (no = nil) then
    raise EArgumentOutOfRangeException.Create
    (
      'O par�metro "chave" � maior que a chave m�xima'
    );

  Result := no^.chave;
end;

end.
