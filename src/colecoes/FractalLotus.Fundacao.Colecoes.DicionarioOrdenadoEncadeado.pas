(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.DicionarioOrdenadoEncadeado.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o ordenada do tipo �rvore bin�ria que implementa
 *            a interface IDicionariz�vel<TChave, TValor>. � uma estrutura de
 *            dados com pares chave-valor cuja principal fun��o � buscar o valor
 *            que esteja associado a uma determinada chave.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.DicionarioOrdenadoEncadeado;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.Colecoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes,
  Colecoes.ArvoreRubronegra;

type
  TDicionarioOrdenadoEncadeado<TChave, TValor> = class
  (
    TArvoreRubronegraOrdenada<TChave, TValor>,
    IDicionarizavel          <TChave, TValor>,
    IEnumeravel        <TPair<TChave, TValor>>
  )
  private type
    TColecao<T> = class abstract (TInterfacedObject, IEnumeravel<T>)
    private
      fColecao: TDicionarioOrdenadoEncadeado<TChave, TValor>;

      function obterTotal         : Integer; inline;
      function obterVazio         : Boolean; inline;
      function obterCheio         : Boolean; inline;
      function obterSomenteLeitura: Boolean; inline;
    protected
      property colecao: TDicionarioOrdenadoEncadeado<TChave, TValor> read fColecao;
    public
      constructor Create(const dicionario: TDicionarioOrdenadoEncadeado<TChave, TValor>);
      destructor  Destroy; override;

      function ToString: string; override;
      function ToArray : TArray<T>;

      function GetEnumerator: TEnumerator<T>; virtual; abstract;

      property total         : Integer read obterTotal;
      property vazio         : Boolean read obterVazio;
      property cheio         : Boolean read obterCheio;
      property somenteLeitura: Boolean read obterSomenteLeitura;
    end;

    TChaves = class (TColecao<TChave>)
    public
      function GetEnumerator: TEnumerator<TChave>; override;
    end;

    TValores = class (TColecao<TValor>)
    public
      function GetEnumerator: TEnumerator<TValor>; override;
    end;
  private
    fChaves : IEnumeravel<TChave>;
    fValores: IEnumeravel<TValor>;

    function obterTotal         : Integer; inline;
    function obterVazio         : Boolean; inline;
    function obterCheio         : Boolean; inline;
    function obterSomenteLeitura: Boolean; inline;
    function obterChaves        : IEnumeravel<TChave>; inline;
    function obterValores       : IEnumeravel<TValor>; inline;

    function  obterValor(const chave: TChave): TValor;
    procedure definirValor(const chave: TChave; const valor: TValor); inline;
  public
    constructor Create;                                              overload;
    constructor Create(const comparacao: TFuncaoComparacao<TChave>); overload;
    constructor Create(const comparador: IComparador<TChave>);       overload;
    destructor  Destroy;                                             override;

    function ToString: string;                        override;
    function ToArray : TArray<TPair<TChave, TValor>>;

    function GetEnumerator: TEnumerator<TPair<TChave, TValor>>; inline;

    procedure adicionar(const chave: TChave; const valor: TValor);          overload;
    procedure adicionar(const dicionario: IDicionarizavel<TChave, TValor>); overload;

    function  contem (const chave: TChave): Boolean; inline;
    function  remover(const chave: TChave): Boolean;
    procedure limpar;

    function tentarObter(const chave: TChave; out valor: TValor): Boolean;

    property chaves : IEnumeravel<TChave> read obterChaves;
    property valores: IEnumeravel<TValor> read obterValores;

    property valor[const chave: TChave]: TValor read obterValor write definirValor; default;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes,
  Extensores.ExtensorPadrao;

{ TDicionarioOrdenadoEncadeado<TChave, TValor> }

procedure TDicionarioOrdenadoEncadeado<TChave, TValor>.adicionar(
  const dicionario: IDicionarizavel<TChave, TValor>);
var
  par: TPair<TChave, TValor>;
begin
  for par in dicionario do
    adicionar(par.Key, par.Value);
end;

procedure TDicionarioOrdenadoEncadeado<TChave, TValor>.adicionar(
  const chave: TChave; const valor: TValor);
var
  no: PNo;
begin
  no := buscarNo(chave);

  if (no <> nil) then
    no^.valor := valor
  else if cheio then
    raise ColecaoVazia at ReturnAddress
  else
    inserirNo( criarNo(chave, valor) );
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.contem(
  const chave: TChave): Boolean;
begin
  Result := (not vazio) and (buscarNo(chave) <> nil);
end;

constructor TDicionarioOrdenadoEncadeado<TChave, TValor>.Create;
begin
  inherited Create;

  fChaves  := TChaves.Create(Self);
  fValores := TValores.Create(Self);
end;

constructor TDicionarioOrdenadoEncadeado<TChave, TValor>.Create(
  const comparacao: TFuncaoComparacao<TChave>);
begin
  inherited Create(comparacao);

  fChaves  := TChaves.Create(Self);
  fValores := TValores.Create(Self);
end;

constructor TDicionarioOrdenadoEncadeado<TChave, TValor>.Create(
  const comparador: IComparador<TChave>);
begin
  inherited Create(comparador);

  fChaves  := TChaves.Create(Self);
  fValores := TValores.Create(Self);
end;

procedure TDicionarioOrdenadoEncadeado<TChave, TValor>.definirValor(
  const chave: TChave; const valor: TValor);
begin
  adicionar(chave, valor);
end;

destructor TDicionarioOrdenadoEncadeado<TChave, TValor>.Destroy;
begin
  fChaves  := nil;
  fValores := nil;

  inherited Destroy;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.GetEnumerator: TEnumerator<TPair<TChave, TValor>>;
begin
  Result := TEnumeradorPares.Create
            (
              Self, True, buscarNoMinimo(raiz), buscarNoMaximo(raiz)
            );
end;

procedure TDicionarioOrdenadoEncadeado<TChave, TValor>.limpar;
begin
  if not vazio then
  begin
    destruirNo(raiz);
    raiz := nil;
  end;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.obterChaves: IEnumeravel<TChave>;
begin
  Result := fChaves;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.obterCheio: Boolean;
begin
  Result := inherited cheio;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.obterTotal: Integer;
begin
  Result := inherited total;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.obterValor(
  const chave: TChave): TValor;
var
  no: PNo;
begin
  no := buscarNo(chave);

  if (no = nil) then
    raise ChaveDicionario at ReturnAddress;

  Result := no^.valor;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.obterValores: IEnumeravel<TValor>;
begin
  Result := fValores;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.obterVazio: Boolean;
begin
  Result := inherited vazio;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.remover(
  const chave: TChave): Boolean;
var
  no: PNo;
begin
  if vazio then
    no := nil
  else
    no := buscarNo(chave);

  Result := (no <> nil);

  if Result then
    excluirNo(no);
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.tentarObter(
  const chave: TChave; out valor: TValor): Boolean;
var
  no: PNo;
begin
  no     := buscarNo(chave);
  Result := (no <> nil);

  if Result then
    valor := no^.valor;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.ToArray: TArray<TPair<TChave, TValor>>;
begin
  Result := TExtensorPadrao.toArray<TPair<TChave, TValor>>(Self);
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.ToString: string;
begin
  Result := TExtensorPadrao.toString<TPair<TChave, TValor>>(Self);
end;

{ TDicionarioOrdenadoEncadeado<TChave, TValor>.TColecao<T> }

constructor TDicionarioOrdenadoEncadeado<TChave, TValor>.TColecao<T>.Create(
  const dicionario: TDicionarioOrdenadoEncadeado<TChave, TValor>);
begin
  fColecao := dicionario;
end;

destructor TDicionarioOrdenadoEncadeado<TChave, TValor>.TColecao<T>.Destroy;
begin
  fColecao := nil;

  inherited Destroy;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.TColecao<T>.obterCheio: Boolean;
begin
  Result := colecao.cheio;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.TColecao<T>.obterSomenteLeitura: Boolean;
begin
  Result := True;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.TColecao<T>.obterTotal: Integer;
begin
  Result := colecao.total;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.TColecao<T>.obterVazio: Boolean;
begin
  Result := colecao.vazio;
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.TColecao<T>.ToArray: TArray<T>;
begin
  Result := TExtensorPadrao.toArray<T>(Self);
end;

function TDicionarioOrdenadoEncadeado<TChave, TValor>.TColecao<T>.ToString: string;
begin
  Result := TExtensorPadrao.toString<T>(Self);
end;

{ TDicionarioOrdenadoEncadeado<TChave, TValor>.TChaves }

function TDicionarioOrdenadoEncadeado<TChave, TValor>.TChaves.GetEnumerator: TEnumerator<TChave>;
begin
  Result := TEnumeradorChaves.Create
  (
    colecao,
    True,
    colecao.buscarNoMinimo(colecao.raiz),
    colecao.buscarNoMaximo(colecao.raiz)
  );
end;

{ TDicionarioOrdenadoEncadeado<TChave, TValor>.TValores }

function TDicionarioOrdenadoEncadeado<TChave, TValor>.TValores.GetEnumerator: TEnumerator<TValor>;
begin
  Result := TEnumeradorValores.Create
  (
    colecao,
    True,
    colecao.buscarNoMinimo(colecao.raiz),
    colecao.buscarNoMaximo(colecao.raiz)
  );
end;

end.
