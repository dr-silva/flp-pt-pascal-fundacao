(*******************************************************************************
 *
 * Arquivo  : Colecoes.HeapVetorial.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Dene uma classe b�sica e abstrata para cole��es vetoriais que
 *            funcionem como um heap bin�rio.
 *
 ******************************************************************************)
unit Colecoes.HeapVetorial;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.Colecoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

type
  THeapVetorial<T> = class abstract(TInterfacedObject, IEnumeravel<T>)
  private
    fTotal     ,
    fCapacidade: Integer;
    fItens     : array of T;
    fComparador: IComparador<T>;

    function obterTotal: Integer; inline;
    function obterVazio: Boolean; inline;
    function obterCheio: Boolean; inline;
    function obterItem(const indice: Integer): T;
    function ordenar(const esquerda, direita: Integer): Boolean; inline;

    procedure trocar(const i, j: Integer);
    procedure emergir(const indice: Integer);
    procedure imergir(const indice: Integer);
  protected
    function obterSomenteLeitura: Boolean; virtual; abstract;
    function eOrdenavel(comparacao: Integer): Boolean; virtual; abstract;

    procedure inserir(const valor: T);
    function  excluir(const indice: Integer): T;
    procedure excluirTodos;

    property item[const indice: Integer]: T read obterItem;
  public
    constructor Create(const capacidade: Integer);                                         overload;
    constructor Create(const capacidade: Integer; const comparacao: TFuncaoComparacao<T>); overload;
    constructor Create(const capacidade: Integer; const comparador: IComparador<T>);       overload;
    destructor Destroy; override;

    function ToString: string; override;
    function ToArray : TArray<T>;

    function GetEnumerator: TEnumerator<T>;

    property total         : Integer read obterTotal;
    property vazio         : Boolean read obterVazio;
    property cheio         : Boolean read obterCheio;
    property somenteLeitura: Boolean read obterSomenteLeitura;
  end;

  TEnumerador<T> = class(TEnumerator<T>)
  private
    fHeap  : THeapVetorial<T>;
    fIndice: Integer;
  protected
    function DoGetCurrent: T;     override;
    function DoMoveNext: Boolean; override;
  public
    constructor Create(const heap: THeapVetorial<T>);
    destructor  Destroy; override;
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes, Extensores.ExtensorPadrao;

{ THeapVetorial<T> }

constructor THeapVetorial<T>.Create(const capacidade: Integer;
  const comparacao: TFuncaoComparacao<T>);
begin
  Create
  (
    capacidade, TFabricaComparador.criar<T>(comparacao)
  );
end;

constructor THeapVetorial<T>.Create(const capacidade: Integer);
begin
  Create
  (
    capacidade, TFabricaComparador.criar<T>()
  );
end;

constructor THeapVetorial<T>.Create(const capacidade: Integer;
  const comparador: IComparador<T>);
begin
  if (capacidade <= 0) then
    raise ArgumentoNegativo('capacidade') at ReturnAddress;

  if (comparador = nil) then
    raise ArgumentoNulo('comparador') at ReturnAddress;

  fTotal      := 0;
  fCapacidade := capacidade;
  fComparador := comparador;

  SetLength(fItens, capacidade);
end;

destructor THeapVetorial<T>.Destroy;
begin
  fComparador := nil;

  SetLength(fItens, 0);

  inherited Destroy;
end;

procedure THeapVetorial<T>.emergir(const indice: Integer);
var
  i, ancestral: Integer;
begin
  i := indice;
  while (i > 0) do
  begin
    ancestral  := (i + 1) div 2 - 1;

    if (ordenar(ancestral, i)) then
    begin
      trocar(ancestral, i);
      i := ancestral;
    end
    else
      Break;
  end;
end;

function THeapVetorial<T>.ordenar(const esquerda, direita: Integer): Boolean;
begin
  Result := eOrdenavel(fComparador.Compare(fItens[esquerda], fItens[direita]));
end;

function THeapVetorial<T>.excluir(const indice: Integer): T;
begin
  if (vazio) then
    raise ColecaoVazia at ReturnAddress;

  trocar(indice, fTotal - 1);

  Result := fItens[fTotal - 1];
  fTotal := fTotal - 1;

  imergir(indice);
end;

procedure THeapVetorial<T>.excluirTodos;
begin
  fTotal := 0;
end;

function THeapVetorial<T>.GetEnumerator: TEnumerator<T>;
begin
  Result := TEnumerador<T>.Create(Self);
end;

procedure THeapVetorial<T>.imergir(const indice: Integer);
var
  i,
  direita,
  esquerda,
  prioridade: Integer;
begin
  i := indice;

  while (i < fTotal) do
  begin
    esquerda := 2 * (i + 1) - 1;
    direita  := 2 * (i + 1);

    if ((esquerda < fTotal) and ordenar(i, esquerda)) then
      prioridade := esquerda
    else
      prioridade := i;

    if ((direita < fTotal) and ordenar(prioridade, direita)) then
      prioridade := direita;

    if (prioridade <> i) then
    begin
      trocar(i, prioridade);
      i := prioridade;
    end
    else
      Break;
  end;
end;

procedure THeapVetorial<T>.inserir(const valor: T);
begin
  if (cheio) then
    raise ColecaoCheia at ReturnAddress;

  fItens[fTotal] := valor;
  fTotal         := fTotal + 1;

  emergir(fTotal - 1);
end;

function THeapVetorial<T>.obterCheio: Boolean;
begin
  Result := fTotal = fCapacidade;
end;

function THeapVetorial<T>.obterItem(const indice: Integer): T;
begin
  if ((indice < 0) or (indice >= fCapacidade)) then
    raise IndiceForaLimites('indice', 0, fCapacidade - 1) at ReturnAddress;

  Result := fItens[indice];
end;

function THeapVetorial<T>.obterTotal: Integer;
begin
  Result := fTotal;
end;

function THeapVetorial<T>.obterVazio: Boolean;
begin
  Result := fTotal = 0;
end;

function THeapVetorial<T>.ToArray: TArray<T>;
begin
  Result := TExtensorPadrao.toArray<T>(Self);
end;

function THeapVetorial<T>.ToString: string;
begin
  Result := TExtensorPadrao.toString<T>(Self);
end;

procedure THeapVetorial<T>.trocar(const i, j: Integer);
var
  temp: T;
begin
  if (i <> j) then
  begin
    temp      := fItens[i];
    fItens[i] := fItens[j];
    fItens[j] := temp;
  end;
end;

{ TEnumerador<T> }

constructor TEnumerador<T>.Create(const heap: THeapVetorial<T>);
begin
  fHeap   := heap;
  fIndice := -1;
end;

destructor TEnumerador<T>.Destroy;
begin
  fHeap := nil;

  inherited Destroy;
end;

function TEnumerador<T>.DoGetCurrent: T;
begin
  Result := fHeap.fItens[fIndice];
end;

function TEnumerador<T>.DoMoveNext: Boolean;
begin
  fIndice := fIndice + 1;
  Result  := fIndice < fHeap.fTotal;
end;

end.
