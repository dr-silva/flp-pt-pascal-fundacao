(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.ConjuntoHashEncadeado.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o n�o-ordenada do tipo tabela hash com resolu��o
 *            de colis�es por encadeamento que implementa a interfce IDistingu�-
 *            vel<TChave>. Essa cole��o n�o permite que seus elementos sejam du-
 *            plicados e � interpretada como um conjunto no sentido matem�tico.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.ConjuntoHashEncadeado;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes, Colecoes.HashEncadeado;

type
  TConjuntoHashEncadeado<TChave> = class
  (
    THashEncadeado<TChave, Pointer>,
    IEnumeravel  <TChave>,
    IDistinguivel<TChave>
  )
  private
    function obterTotal         : Integer; inline;
    function obterVazio         : Boolean; inline;
    function obterCheio         : Boolean; inline;
    function obterSomenteLeitura: Boolean; inline;
  public
    function ToString: string;         override;
    function ToArray : TArray<TChave>;

    function GetEnumerator: TEnumerator<TChave>; inline;

    function adicionar(chave: TChave): Boolean;
    function remover  (chave: TChave): Boolean;
    function contem   (chave: TChave): Boolean; inline;

    procedure limpar;

    procedure unir          (colecao: IEnumeravel<TChave>);
    procedure excetuar      (colecao: IEnumeravel<TChave>);
    procedure interseccionar(colecao: IEnumeravel<TChave>);

    property somenteLeitura: Boolean read obterSomenteLeitura;
  end;

implementation
uses
  System.SysUtils,
  System.Math,
  Recursos.Excecoes,
  Extensores.ExtensorPadrao;

{ TConjuntoHashEncadeado<TChave> }

function TConjuntoHashEncadeado<TChave>.adicionar(chave: TChave): Boolean;
begin
  Result := not (cheio or contem(chave));

  if Result then
    inserirItem(chave, nil);
end;

function TConjuntoHashEncadeado<TChave>.contem(chave: TChave): Boolean;
begin
  Result := (not vazio) and (buscarItem(chave) <> nil);
end;

procedure TConjuntoHashEncadeado<TChave>.excetuar(colecao: IEnumeravel<TChave>);
var
  item: TChave;
begin
//  if (colecao = nil) then
//    raise EArgumentNilException.CreateFmt(S_ARGUMENTO_NULO, ['colecao']);

  if (colecao <> nil) then
    for item in colecao do
      remover(item);
end;

function TConjuntoHashEncadeado<TChave>.GetEnumerator: TEnumerator<TChave>;
begin
  Result := TEnumeradorChaves.Create(Self);
end;

procedure TConjuntoHashEncadeado<TChave>.interseccionar(
  colecao: IEnumeravel<TChave>);
var
  item  : PItem;
  valor : TChave;
  novos : array of PItem;
  total ,
  indice: Integer;
begin
//  if (colecao = nil) then
//    raise EArgumentNilException.CreateFmt(S_ARGUMENTO_NULO, ['colecao']);

  if (colecao = nil) then
    total := 0
  else
    total := colecao.total;

  SetLength(novos, Min(Self.total, total));
  indice := -1;

  if (colecao <> nil) then
    for valor in colecao do
    begin
      item := buscarItem(valor);
      if (item <> nil) then
      begin
        extrairItem(item);
        Inc(indice);
        novos[indice] := item;
      end;
    end;

  excluirTodos;
  if (indice >= 0) then
    redimensionar(Floor(Log2(indice + 1)) + 1);

  while (indice >= 0) do
  begin
    inserirItem(novos[indice]);
    Dec(indice);
  end;

  SetLength(novos, 0);
end;

procedure TConjuntoHashEncadeado<TChave>.limpar;
begin
  if not vazio then
  begin
    excluirTodos;
    redimensionar(4);
  end;
end;

function TConjuntoHashEncadeado<TChave>.obterCheio: Boolean;
begin
  Result := inherited cheio;
end;

function TConjuntoHashEncadeado<TChave>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TConjuntoHashEncadeado<TChave>.obterTotal: Integer;
begin
  Result := inherited total;
end;

function TConjuntoHashEncadeado<TChave>.obterVazio: Boolean;
begin
  Result := inherited vazio;
end;

function TConjuntoHashEncadeado<TChave>.remover(chave: TChave): Boolean;
var
  item: PItem;
begin
  if vazio then
    item := nil
  else
    item := buscarItem(chave);

  Result := (item <> nil);

  if Result then
    excluirItem(item);
end;

function TConjuntoHashEncadeado<TChave>.ToArray: TArray<TChave>;
begin
  Result := TExtensorPadrao.toArray<TChave>(Self);
end;

function TConjuntoHashEncadeado<TChave>.ToString: string;
begin
  Result := TExtensorPadrao.toString<TChave>(Self);
end;

procedure TConjuntoHashEncadeado<TChave>.unir(colecao: IEnumeravel<TChave>);
var
  item: TChave;
begin
//  if (colecao = nil) then
//    raise EArgumentNilException.CreateFmt(S_ARGUMENTO_NULO, ['colecao']);

  if (colecao <> nil) then
    for item in colecao do
      adicionar(item);
end;

end.
