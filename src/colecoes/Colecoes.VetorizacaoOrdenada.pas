(*******************************************************************************
 *
 * Arquivo  : Colecoes.VetorizacaoOrdenada.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Dene uma classe b�sica e abstrata para as cole��es vetoriais
 *            ordenadas.
 *
 ******************************************************************************)
unit Colecoes.VetorizacaoOrdenada;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.Colecoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

type
  TVetorizacaoOrdenada<TChave, TValor> = class abstract
  (
    TInterfacedObject,
    IOrdenavel<TChave>
  )
  protected type
    TEnumerador<TDado> = class abstract (TEnumerator<TDado>)
    private
      fTotal   ,
      fIndice  ,
      fContador: Integer;
      fColecao : TVetorizacaoOrdenada<TChave, TValor>;
    protected
      function DoMoveNext: Boolean; override;

      property indice : Integer                              read fIndice;
      property colecao: TVetorizacaoOrdenada<TChave, TValor> read fColecao;
    public
      constructor Create(const colecao: TVetorizacaoOrdenada<TChave, TValor>; const primeiro, total: Integer);
      destructor  Destroy; override;
    end;

    TEnumeradorChaves = class (TEnumerador<TChave>)
    protected
      function DoGetCurrent: TChave; override;
    end;

    TEnumeradorValores = class (TEnumerador<TValor>)
    protected
      function DoGetCurrent: TValor; override;
    end;

    TEnumeradorPares = class (TEnumerador<TPair<TChave, TValor>>)
    protected
      function DoGetCurrent: TPair<TChave, TValor>; override;
    end;

    TColecaoVetorial<TDado> = class abstract (TInterfacedObject, IEnumeravel<TDado>)
    private
      fPrimeiro,
      fUltimo  : Integer;
      fColecao : TVetorizacaoOrdenada<TChave, TValor>;

      function obterTotal         : Integer; inline;
      function obterVazio         : Boolean; inline;
      function obterCheio         : Boolean; inline;
      function obterSomenteLeitura: Boolean; inline;
    protected
      property primeiro: Integer                         read fPrimeiro;
      property ultimo  : Integer                         read fUltimo;
      property colecao : TVetorizacaoOrdenada<TChave, TValor> read fColecao;
    public
      constructor Create(const colecao: TVetorizacaoOrdenada<TChave, TValor>; const primeiro, ultimo: Integer);
      destructor  Destroy; override;

      function ToString: string; override;
      function ToArray : TArray<TDado>;

      function GetEnumerator: TEnumerator<TDado>; virtual; abstract;

      property total         : Integer read obterTotal;
      property vazio         : Boolean read obterVazio;
      property cheio         : Boolean read obterCheio;
      property somenteLeitura: Boolean read obterSomenteLeitura;
    end;

    TColecaoChaves = class (TColecaoVetorial<TChave>)
    public
      function GetEnumerator: TEnumerator<TChave>; override;
    end;

    TColecaoValores = class (TColecaoVetorial<TValor>)
    public
      function GetEnumerator: TEnumerator<TValor>; override;
    end;
  private
    fTotal     ,
    fCapacidade: Integer;
    fItens     : array of TPair<TChave, TValor>;
    fComparador: IComparador<TChave>;

    function obterTotal : Integer; inline;
    function obterVazio : Boolean; inline;
    function obterCheio : Boolean; inline;
    function obterMinimo: TChave;  inline;
    function obterMaximo: TChave;  inline;

    function obterChave(const indice: Integer): TChave; inline;
    function obterValor(const indice: Integer): TValor; inline;
    procedure definirValor(const indice: Integer; const valor: TValor); inline;
  protected
    {$region 'Assist�ncia'}
    function comparar(const chave: TChave; const indice: Integer): Integer; inline;
    procedure inserirValor(const indice: Integer; const chave: TChave; const valor: TValor);
    procedure excluirValor(const indice: Integer);
    procedure excluirTodos; inline;
    function contemChave(const chave: TChave): Boolean; inline;

    property capacidade: Integer                  read fCapacidade;
    property chave[const indice: Integer]: TChave read obterChave;
    property valor[const indice: Integer]: TValor read obterValor write definirValor;
    {$endregion}
  public
    {$region 'Construtores'}
    constructor Create(const capacidade: Integer);                                              overload;
    constructor Create(const capacidade: Integer; const comparacao: TFuncaoComparacao<TChave>); overload;
    constructor Create(const capacidade: Integer; const comparador: IComparador<TChave>);       overload;
    destructor  Destroy;                                                                        override;
    {$endregion}

    {$region 'Orden�vel'}
    function piso(const chave: TChave): TChave;
    function teto(const chave: TChave): TChave;

    function indexar   (const chave : TChave ): Integer;
    function desindexar(const indice: Integer): TChave;

    function contar     (const inferior, superior: TChave): Integer;
    function obterChaves(const inferior, superior: TChave): IEnumeravel<TChave>;
    {$endregion}

    {$region 'Propriedades'}
    property total : Integer read obterTotal;
    property vazio : Boolean read obterVazio;
    property cheio : Boolean read obterCheio;

    property minimo: TChave  read obterMinimo;
    property maximo: TChave  read obterMaximo;
    {$endregion}
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes, Extensores.ExtensorPadrao;

{ TVetorizacaoOrdenada<TChave, TValor> }

function TVetorizacaoOrdenada<TChave, TValor>.comparar(const chave: TChave;
  const indice: Integer): Integer;
begin
  Result := fComparador.Compare(chave, obterChave(indice));
end;

function TVetorizacaoOrdenada<TChave, TValor>.contar(const inferior,
  superior: TChave): Integer;
var
  iInf, iSup: Integer;
begin
  iInf := indexar(inferior);
  iSup := indexar(superior);

  if (iSup > 0) and (comparar(superior, iSup) = 0) then
    Dec(iSup);

  Result := iSup - iInf;

  if (Result >= 0) then
    Inc(Result)
  else
    Dec(Result);
end;

constructor TVetorizacaoOrdenada<TChave, TValor>.Create(const capacidade: Integer);
begin
  Create
  (
    capacidade,
    TFabricaComparador.criar<TChave>()
  );
end;

constructor TVetorizacaoOrdenada<TChave, TValor>.Create(const capacidade: Integer;
  const comparacao: TFuncaoComparacao<TChave>);
begin
  Create
  (
    capacidade,
    TFabricaComparador.criar<TChave>(comparacao)
  );
end;

function TVetorizacaoOrdenada<TChave, TValor>.contemChave(
  const chave: TChave): Boolean;
begin
  Result := (not vazio) and (comparar(chave, indexar(chave)) = 0);
end;

constructor TVetorizacaoOrdenada<TChave, TValor>.Create(const capacidade: Integer;
  const comparador: IComparador<TChave>);
begin
  if (capacidade <= 0) then
    raise ArgumentoNegativo('capacidade') at ReturnAddress;

  if (comparador = nil) then
    raise ArgumentoNulo('comparador') at ReturnAddress;

  fTotal      := 0;
  fCapacidade := capacidade;
  fComparador := comparador;

  SetLength(fItens, capacidade);
end;

procedure TVetorizacaoOrdenada<TChave, TValor>.definirValor(
  const indice: Integer; const valor: TValor);
begin
  fItens[indice].Value := valor;
end;

function TVetorizacaoOrdenada<TChave, TValor>.desindexar(
  const indice: Integer): TChave;
begin
  if ((indice < 0) or (indice >= fTotal)) then
    raise IndiceForaLimites('indice', 0, fTotal - 1) at ReturnAddress;
    
  Result := obterChave(indice);
end;

destructor TVetorizacaoOrdenada<TChave, TValor>.Destroy;
begin
  fComparador := nil;

  SetLength(fItens, 0);
  
  inherited Destroy;
end;

procedure TVetorizacaoOrdenada<TChave, TValor>.excluirTodos;
begin
  fTotal := 0;
end;

procedure TVetorizacaoOrdenada<TChave, TValor>.excluirValor(const indice: Integer);
var
  i: Integer;
begin
  for i := indice + 1 to fTotal - 1 do
  begin
    fItens[i - 1].Key   := fItens[i].Key;
    fItens[i - 1].Value := fItens[i].Value;
  end;

  Dec(fTotal);
end;

function TVetorizacaoOrdenada<TChave, TValor>.indexar(const chave: TChave): Integer;
var
  minimo, mediana,
  maximo, comparacao: Integer;
begin
  minimo := 0;
  maximo := fTotal - 1;
  Result := -1;

  while ((minimo < maximo) and (Result < 0)) do
  begin
    mediana    := (minimo + maximo) div 2;
    comparacao := comparar(chave, mediana);

    if (comparacao < 0) then
      maximo := mediana - 1
    else if (comparacao > 0) then
      minimo := mediana + 1
    else
      Result := mediana;
  end;

  if (Result < 0) then
    Result := minimo;
end;

procedure TVetorizacaoOrdenada<TChave, TValor>.inserirValor(const indice: Integer;
  const chave: TChave; const valor: TValor);
var
  i: Integer;
begin
  for i := fTotal - 1 downto indice do
  begin
    fItens[i + 1].Key   := fItens[i].Key;
    fItens[i + 1].Value := fItens[i].Value;
  end;

  fItens[indice].Key   := chave;
  fItens[indice].Value := valor;

  Inc(fTotal);
end;

function TVetorizacaoOrdenada<TChave, TValor>.obterChave(
  const indice: Integer): TChave;
begin
  Result := fItens[indice].Key;
end;

function TVetorizacaoOrdenada<TChave, TValor>.obterChaves(const inferior,
  superior: TChave): IEnumeravel<TChave>;
begin

end;

function TVetorizacaoOrdenada<TChave, TValor>.obterCheio: Boolean;
begin
  Result := fTotal = fCapacidade;
end;

function TVetorizacaoOrdenada<TChave, TValor>.obterValor(const indice: Integer): TValor;
begin
  Result := fItens[indice].Value;
end;

function TVetorizacaoOrdenada<TChave, TValor>.obterMaximo: TChave;
begin
  Result := desindexar(fTotal - 1);
end;

function TVetorizacaoOrdenada<TChave, TValor>.obterMinimo: TChave;
begin
  Result := desindexar(0);
end;

function TVetorizacaoOrdenada<TChave, TValor>.obterTotal: Integer;
begin
  Result := fTotal;
end;

function TVetorizacaoOrdenada<TChave, TValor>.obterVazio: Boolean;
begin
  Result := fTotal = 0;
end;

function TVetorizacaoOrdenada<TChave, TValor>.piso(const chave: TChave): TChave;
var
  i: Integer;
begin
  i := indexar(chave);

  if (comparar(chave, i) < 0) then
    i := i - 1;

  Result := desindexar(i);
end;

function TVetorizacaoOrdenada<TChave, TValor>.teto(const chave: TChave): TChave;
var
  i: Integer;
begin
  i := indexar(chave);

  Result := desindexar(i);
end;

{ TVetorizacaoOrdenada<TChave, TValor>.TEnumerador<TDado> }

constructor TVetorizacaoOrdenada<TChave, TValor>.TEnumerador<TDado>.Create(
  const colecao: TVetorizacaoOrdenada<TChave, TValor>; const primeiro,
  total: Integer);
begin
  fColecao := colecao;

  if (total >= 0) then
  begin
    fTotal    := +total;
    fContador := +1;
  end
  else
  begin
    fTotal    := -total;
    fContador := -1;
  end;

  fIndice := primeiro - fContador;
end;

destructor TVetorizacaoOrdenada<TChave, TValor>.TEnumerador<TDado>.Destroy;
begin
  fColecao := nil;

  inherited Destroy;
end;

function TVetorizacaoOrdenada<TChave, TValor>.TEnumerador<TDado>.DoMoveNext: Boolean;
begin
  Result := fTotal > 0;

  if (Result) then
  begin
    fIndice := fIndice + fContador;
    fTotal  := fTotal - 1;
  end;
end;

{ TVetorizacaoOrdenada<TChave, TValor>.TEnumeradorChaves }

function TVetorizacaoOrdenada<TChave, TValor>.TEnumeradorChaves.DoGetCurrent: TChave;
begin
  Result := fColecao.chave[indice]
end;

{ TVetorizacaoOrdenada<TChave, TValor>.TEnumeradorValores }

function TVetorizacaoOrdenada<TChave, TValor>.TEnumeradorValores.DoGetCurrent: TValor;
begin
  Result := colecao.valor[indice]
end;

{ TVetorizacaoOrdenada<TChave, TValor>.TEnumeradorPadrao }

function TVetorizacaoOrdenada<TChave, TValor>.TEnumeradorPares.DoGetCurrent: TPair<TChave, TValor>;
begin
  Result.Key   := colecao.chave[indice];
  Result.Value := colecao.valor[indice];
end;

{ TVetorizacaoOrdenada<TChave, TValoe>.TColecaoVetorial<TDado> }

constructor TVetorizacaoOrdenada<TChave, TValor>.TColecaoVetorial<TDado>.Create(
  const colecao: TVetorizacaoOrdenada<TChave, TValor>; const primeiro,
  ultimo: Integer);
begin
  fPrimeiro := primeiro;
  fUltimo   := ultimo;
  fColecao  := colecao;
end;

destructor TVetorizacaoOrdenada<TChave, TValor>.TColecaoVetorial<TDado>.Destroy;
begin
  fColecao := nil;

  inherited Destroy;
end;

function TVetorizacaoOrdenada<TChave, TValor>.TColecaoVetorial<TDado>.obterCheio: Boolean;
begin
  Result := total = fColecao.total;
end;

function TVetorizacaoOrdenada<TChave, TValor>.TColecaoVetorial<TDado>.obterSomenteLeitura: Boolean;
begin
  Result := True;
end;

function TVetorizacaoOrdenada<TChave, TValor>.TColecaoVetorial<TDado>.obterTotal: Integer;
begin
  if (fUltimo < 0) or (fPrimeiro < 0) then
    Result := 0
  else
    Result := Abs(fUltimo - fPrimeiro) + 1;
end;

function TVetorizacaoOrdenada<TChave, TValor>.TColecaoVetorial<TDado>.obterVazio: Boolean;
begin
  Result := total = 0;
end;

function TVetorizacaoOrdenada<TChave, TValor>.TColecaoVetorial<TDado>.ToArray: TArray<TDado>;
begin
  Result := TExtensorPadrao.toArray<TDado>(Self);
end;

function TVetorizacaoOrdenada<TChave, TValor>.TColecaoVetorial<TDado>.ToString: string;
begin
  Result := TExtensorPadrao.toString<TDado>(Self);
end;

{ TVetorizacaoOrdenada<TChave, TValor>.TColecaoChaves }

function TVetorizacaoOrdenada<TChave, TValor>.TColecaoChaves.GetEnumerator: TEnumerator<TChave>;
begin
  Result := TEnumeradorChaves.Create(colecao, primeiro, Abs(total));
end;

{ TVetorizacaoOrdenada<TChave, TValor>.TColecaoValores }

function TVetorizacaoOrdenada<TChave, TValor>.TColecaoValores.GetEnumerator: TEnumerator<TValor>;
begin
  Result := TEnumeradorValores.Create(colecao, primeiro, Abs(total));
end;

end.
