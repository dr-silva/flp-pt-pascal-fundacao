(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.FilaVetorial.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o vetorial que implementa a interface IEnfileir�-
 *            vel<T>, assim, h� uma cole��o que se baseia no princ�pio primeiro
 *            a entrar - primeiro a sair (First In, First Out - FIFO).
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.FilaVetorial;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes, Colecoes.Vetorizacao;

type
  TFilaVetorial<T> = class (TVetorizacao<T>, IEnfileiravel<T>)
  private
    fInicio,
    fFim   : Integer;
  protected
    function obterTotal         : Integer; override;
    function obterVazio         : Boolean; override;
    function obterCheio         : Boolean; override;
    function obterSomenteLeitura: Boolean; override;
  public
    constructor Create(const capacidade: Integer);

    function GetEnumerator: TEnumerator<T>; override;

    procedure enfileirar(const valor: T);
    function  desenfileirar: T;
    function  espiar: T;
    procedure limpar;
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes;

{ TFilaVetorial<T> }

constructor TFilaVetorial<T>.Create(const capacidade: Integer);
begin
  inherited Create(capacidade, capacidade + 1);

  fInicio := 0;
  fFim    := 0;
end;

function TFilaVetorial<T>.desenfileirar: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := item[fInicio];

  if (fInicio = capacidade) then
    fInicio := 0
  else
    Inc(fInicio);
end;

procedure TFilaVetorial<T>.enfileirar(const valor: T);
begin
  if cheio then
    raise ColecaoCheia at ReturnAddress;

  item[fFim] := valor;

  if (fFim = capacidade) then
    fFim := 0
  else
    Inc(fFim);
end;

function TFilaVetorial<T>.espiar: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := item[fInicio];
end;

function TFilaVetorial<T>.GetEnumerator: TEnumerator<T>;
begin
  Result := Colecoes.Vetorizacao.TEnumerador<T>.Create(Self, fInicio - 1, total);
end;

procedure TFilaVetorial<T>.limpar;
begin
  fInicio := 0;
  fFim    := 0;
end;

function TFilaVetorial<T>.obterCheio: Boolean;
begin
  Result := (fInicio = fFim + 1) or ((fFim = capacidade) and (fInicio = 0));
end;

function TFilaVetorial<T>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TFilaVetorial<T>.obterTotal: Integer;
begin
  if (fFim >= fInicio) then
    Result := fFim - fInicio
  else
    Result := capacidade + fFim - fInicio + 1;
end;

function TFilaVetorial<T>.obterVazio: Boolean;
begin
  Result := fInicio = fFim;
end;

end.
