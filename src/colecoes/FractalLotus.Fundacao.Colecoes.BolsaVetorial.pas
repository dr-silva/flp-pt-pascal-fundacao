(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.BolsaVetorial.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o vetorial em que n�o haja a necessidade de remo-
 *            ��o de itens.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.BolsaVetorial;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes, Colecoes.Vetorizacao;

type
  TBolsaVetorial<T> = class (TVetorizacao<T>, IGuardavel<T>)
  private
    fTotal: Integer;

    function obterPrimeiro: T;
    function obterUltimo  : T;
  protected
    function obterTotal         : Integer; override;
    function obterVazio         : Boolean; override;
    function obterCheio         : Boolean; override;
    function obterSomenteLeitura: Boolean; override;
  public
    constructor Create(const capacidade: Integer);

    function GetEnumerator: TEnumerator<T>; override;

    procedure guardar(const valor: T);                overload;
    procedure guardar(const valores: TArray<T>);      overload;
    procedure guardar(const valores: IEnumeravel<T>); overload;

    property primeiro: T read obterPrimeiro;
    property ultimo  : T read obterUltimo;
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes;

{ TBolsaVetorial<T> }

constructor TBolsaVetorial<T>.Create(const capacidade: Integer);
begin
  inherited Create(capacidade);

  fTotal := 0;
end;

function TBolsaVetorial<T>.GetEnumerator: TEnumerator<T>;
begin
  Result := Colecoes.Vetorizacao.TEnumerador<T>.Create(Self, -1, total);
end;

procedure TBolsaVetorial<T>.guardar(const valores: IEnumeravel<T>);
var
  valor: T;
begin
  for valor in valores do
    guardar(valor);
end;

procedure TBolsaVetorial<T>.guardar(const valores: TArray<T>);
var
  i: Integer;
begin
  for i := Low(valores) to High(valores) do
    guardar(valores[i]);
end;

procedure TBolsaVetorial<T>.guardar(const valor: T);
begin
  if cheio then
    raise ColecaoCheia at ReturnAddress;

  item[fTotal] := valor;
  Inc(fTotal);
end;

function TBolsaVetorial<T>.obterCheio: Boolean;
begin
  Result := fTotal = capacidade;
end;

function TBolsaVetorial<T>.obterPrimeiro: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := item[0];
end;

function TBolsaVetorial<T>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TBolsaVetorial<T>.obterTotal: Integer;
begin
  Result := fTotal;
end;

function TBolsaVetorial<T>.obterUltimo: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := item[fTotal - 1];
end;

function TBolsaVetorial<T>.obterVazio: Boolean;
begin
  Result := fTotal = 0;
end;

end.
