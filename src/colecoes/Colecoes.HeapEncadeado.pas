(*******************************************************************************
 *
 * Arquivo  : Colecoes.HeapEncadeado.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Dene uma classe b�sica e abstrata para cole��es encadeadas que
 *            funcionem como um heap bin�rio.
 *
 ******************************************************************************)
unit Colecoes.HeapEncadeado;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.Colecoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

type
  THeapEncadeado<T> = class abstract(TInterfacedObject, IEnumeravel<T>)
  protected type
    PNo = ^TNo;
    TNo = record
      dado     : T;
      direita  ,
      esquerda ,
      ancestral: PNo;
    end;

    TEnumerador = class(TEnumerator<T>)
    private
      fAtual  ,
      fProximo: PNo;

      procedure avancar;
    protected
      function DoGetCurrent: T;     override;
      function DoMoveNext: Boolean; override;
    public
      constructor Create(const proximo: PNo);
      destructor  Destroy; override;
    end;
  private
    fComparador: IComparador<T>;
    fPrimeiro  ,
    fUltimo    : PNo;
    fTotal     : Integer;

    function obterTotal: Integer; inline;
    function obterVazio: Boolean; inline;
    function obterCheio: Boolean; inline;

    function ordenar(const e, d: PNo): Boolean; inline;
    procedure trocar(a, b: PNo);
    procedure emergir(no: PNo);
    procedure imergir(no: PNo);

    function criarNo(const valor: T): PNo;
    procedure destruirNo(no: PNo);
  protected
    function obterSomenteLeitura: Boolean; virtual; abstract;
    function eOrdenavel(comparacao: Integer): Boolean; virtual; abstract;

    function buscarNo(const indice: Integer): PNo;
    procedure inserir(const valor: T);
    function  excluir(const indice: Integer): T;
    procedure excluirTodos;
  public
    constructor Create();                                       overload;
    constructor Create(const comparacao: TFuncaoComparacao<T>); overload;
    constructor Create(const comparador: IComparador<T>);       overload;
    destructor  Destroy;                                        override;

    function ToString: string; override;
    function ToArray : TArray<T>;

    function GetEnumerator: TEnumerator<T>;

    property total         : Integer read obterTotal;
    property vazio         : Boolean read obterVazio;
    property cheio         : Boolean read obterCheio;
    property somenteLeitura: Boolean read obterSomenteLeitura;
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes, Extensores.ExtensorPadrao;

{ THeapEncadeado<T> }

function THeapEncadeado<T>.buscarNo(const indice: Integer): PNo;
begin
  if (indice <= 1) then
    Result := fPrimeiro
  else
  begin
    Result := buscarNo(indice div 2);

    if (indice mod 2 = 0) then
      Result := Result^.esquerda
    else
      Result := Result^.direita;
  end;
end;

constructor THeapEncadeado<T>.Create;
begin
  Create(TFabricaComparador.criar<T>());
end;

constructor THeapEncadeado<T>.Create(const comparacao: TFuncaoComparacao<T>);
begin
  Create(TFabricaComparador.criar<T>(comparacao));
end;

constructor THeapEncadeado<T>.Create(const comparador: IComparador<T>);
begin
  if (comparador = nil) then
    raise ArgumentoNulo('comparador') at ReturnAddress;

  fComparador := comparador;
  fPrimeiro   := nil;
  fUltimo     := nil;
end;

function THeapEncadeado<T>.criarNo(const valor: T): PNo;
begin
  New(Result);

  Result^.dado      := valor;
  Result^.ancestral := nil;
  Result^.esquerda  := nil;
  Result^.direita   := nil;
end;

destructor THeapEncadeado<T>.Destroy;
begin
  excluirTodos;
  fComparador := nil;

  inherited Destroy;
end;

procedure THeapEncadeado<T>.destruirNo(no: PNo);
begin
  if (no = nil) then
    Exit;

  if (no^.esquerda <> nil) then
    destruirNo(no^.esquerda);

  if (no^.direita <> nil) then
    destruirNo(no^.direita);

  no^.dado := default(T);

  FreeMem(no);
end;

procedure THeapEncadeado<T>.emergir(no: PNo);
begin
  while (no <> fPrimeiro) do
    if (ordenar(no^.ancestral, no)) then
    begin
      trocar(no^.ancestral, no);
      no := no^.ancestral;
    end
    else
      Break;
end;

function THeapEncadeado<T>.excluir(const indice: Integer): T;
var
  no: PNo;
begin
  if (vazio) then
    raise ColecaoVazia at ReturnAddress;

  no     := buscarNo(indice);
  Result := no^.dado;

  trocar(no, fUltimo);
  if (fUltimo^.ancestral <> nil) then
  begin
    if (fUltimo^.ancestral^.esquerda = fUltimo) then
      fUltimo^.ancestral^.esquerda := nil
    else
      fUltimo^.ancestral^.direita := nil;
  end
  else
    fPrimeiro := nil;

  destruirNo(fUltimo);

  fTotal  := fTotal - 1;
  fUltimo := buscarNo(fTotal);

  imergir(no);
end;

procedure THeapEncadeado<T>.excluirTodos;
begin
  if (fPrimeiro <> nil) then
    destruirNo(fPrimeiro);

  fPrimeiro := nil;
  fUltimo   := nil;
  fTotal    := 0;
end;

function THeapEncadeado<T>.GetEnumerator: TEnumerator<T>;
var
  proximo: PNo;
begin
  proximo := fPrimeiro;

  if (proximo <> nil) then
    while (proximo^.esquerda <> nil) do
      proximo := proximo^.esquerda;

  Result := TEnumerador.Create(proximo);
end;

procedure THeapEncadeado<T>.imergir(no: PNo);
var
  prioritario: PNo;
begin
  while (no <> fUltimo) do
  begin
    if ((no^.esquerda <> nil) and ordenar(no, no^.esquerda)) then
      prioritario := no^.esquerda
    else
      prioritario := no;

    if ((no^.direita <> nil) and ordenar(prioritario, no^.direita)) then
      prioritario := no^.direita;

    if (prioritario <> no) then
    begin
      trocar(prioritario, no);
      no := prioritario;
    end
    else
      Break;
  end;
end;

procedure THeapEncadeado<T>.inserir(const valor: T);
var
  no ,
  pai: PNo;
begin
  if (cheio) then
    raise ColecaoCheia at ReturnAddress;

  no      := criarNo(valor);
  fUltimo := no;
  fTotal  := fTotal + 1;

  if (fPrimeiro = nil) then
    fPrimeiro := no
  else
  begin
    pai := buscarNo(fTotal div 2);
    if (fTotal mod 2 = 0) then
      pai^.esquerda := no
    else
      pai^.direita := no;

    no^.ancestral := pai;

    emergir(no);
  end;
end;

function THeapEncadeado<T>.obterCheio: Boolean;
begin
  Result := fTotal = Integer.MaxValue;
end;

function THeapEncadeado<T>.obterTotal: Integer;
begin
  Result := fTotal;
end;

function THeapEncadeado<T>.obterVazio: Boolean;
begin
  Result := fPrimeiro = nil;
end;

function THeapEncadeado<T>.ordenar(const e, d: PNo): Boolean;
begin
  Result := eOrdenavel(fComparador.Compare(e^.dado, d^.dado))
end;

function THeapEncadeado<T>.ToArray: TArray<T>;
begin
  Result := TExtensorPadrao.toArray<T>(Self);
end;

function THeapEncadeado<T>.ToString: string;
begin
  Result := TExtensorPadrao.toString<T>(Self);
end;

procedure THeapEncadeado<T>.trocar(a, b: PNo);
var
  temp: T;
begin
  if (a <> b) then
  begin
    temp    := a^.dado;
    a^.dado := b^.dado;
    b^.dado := temp;
  end;
end;

{ THeapEncadeado<T>.TEnumerador }

procedure THeapEncadeado<T>.TEnumerador.avancar;
begin
  fProximo := fProximo^.ancestral;

  if (fProximo <> nil) then
    if ((fProximo^.esquerda = fAtual) and (fProximo^.direita <> nil)) then
    begin
      fProximo := fProximo^.direita;
      while (fProximo^.esquerda <> nil) do
        fProximo := fProximo^.esquerda;
    end;
end;

constructor THeapEncadeado<T>.TEnumerador.Create(const proximo: PNo);
begin
  fAtual   := nil;
  fProximo := proximo;
end;

destructor THeapEncadeado<T>.TEnumerador.Destroy;
begin
  fAtual   := nil;
  fProximo := nil;

  inherited Destroy;
end;

function THeapEncadeado<T>.TEnumerador.DoGetCurrent: T;
begin
  Result := fAtual^.dado;
end;

function THeapEncadeado<T>.TEnumerador.DoMoveNext: Boolean;
begin
  Result := fProximo <> nil;

  if (Result) then
  begin
    fAtual := fProximo;
    avancar;
  end;
end;

end.
