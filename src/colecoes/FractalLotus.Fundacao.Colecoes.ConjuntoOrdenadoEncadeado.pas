(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.ConjuntoOrdenadoEncadeado.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o ordenada de �rvore bin�ria que implementa a in-
 *            terface IDistingu�vel<TChave>. Essa cole��o n�o permite que seus
 *            elementos sejam duplicados e � interpretada como um conjunto no
 *            sentido matem�tico.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.ConjuntoOrdenadoEncadeado;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes, Colecoes.ArvoreRubronegra;

type
  TConjuntoOrdenadoEncadeado<TChave> = class
  (
    TArvoreRubronegraOrdenada<TChave, Pointer>,
    IEnumeravel              <TChave>,
    IDistinguivel            <TChave>
  )
  private
    function obterTotal         : Integer; inline;
    function obterVazio         : Boolean; inline;
    function obterCheio         : Boolean; inline;
    function obterSomenteLeitura: Boolean; inline;
  public
    function ToString: string;         override;
    function ToArray : TArray<TChave>; inline;

    function GetEnumerator: TEnumerator<TChave>; inline;

    function adicionar(chave: TChave): Boolean;
    function remover  (chave: TChave): Boolean;
    function contem   (chave: TChave): Boolean; inline;

    procedure limpar;

    procedure unir          (colecao: IEnumeravel<TChave>);
    procedure excetuar      (colecao: IEnumeravel<TChave>);
    procedure interseccionar(colecao: IEnumeravel<TChave>);

    property somenteLeitura: Boolean read obterSomenteLeitura;
  end;

implementation
uses
  System.SysUtils,
  System.Math,
  Recursos.Excecoes,
  Extensores.ExtensorPadrao;

{ TConjuntoOrdenadoEncadeado<TChave> }

function TConjuntoOrdenadoEncadeado<TChave>.adicionar(chave: TChave): Boolean;
begin
  Result := not contem(chave);

  if Result then
    inserirNo( criarNo(chave, nil) );
end;

function TConjuntoOrdenadoEncadeado<TChave>.contem(chave: TChave): Boolean;
begin
  Result := (not vazio) and (buscarNo(chave) <> nil);
end;

procedure TConjuntoOrdenadoEncadeado<TChave>.excetuar(
  colecao: IEnumeravel<TChave>);
var
  item: TChave;
begin
//  if (colecao = nil) then
//    raise EArgumentNilException.CreateFmt(S_ARGUMENTO_NULO, ['colecao']);

  if Assigned(colecao) then
    for item in colecao do
      remover(item);
end;

function TConjuntoOrdenadoEncadeado<TChave>.GetEnumerator: TEnumerator<TChave>;
begin
  Result := TEnumeradorChaves.Create
  (
    Self, True, buscarNoMinimo(raiz), buscarNoMaximo(raiz)
  );
end;

procedure TConjuntoOrdenadoEncadeado<TChave>.interseccionar(
  colecao: IEnumeravel<TChave>);
var
  valor : TChave;
  novos : array of TChave;
  total ,
  indice: Integer;
begin
//  if (colecao = nil) then
//    raise EArgumentNilException.CreateFmt(S_ARGUMENTO_NULO, ['colecao']);

  if Assigned(colecao) then
    total := colecao.total
  else
    total := 0;

  SetLength(novos, Min(Self.total, total));
  indice := -1;

  if Assigned(colecao) then
    for valor in colecao do
      if contem(valor) then
      begin
        Inc(indice);
        novos[indice] := valor;
      end;

  limpar;

  while (indice >= 0) do
  begin
    inserirNo( criarNo(novos[indice], nil) );
    Dec(indice);
  end;

  SetLength(novos, 0);
end;

procedure TConjuntoOrdenadoEncadeado<TChave>.limpar;
begin
  if (raiz <> nil) then
  begin
    destruirNo(raiz);
    raiz := nil;
  end;
end;

function TConjuntoOrdenadoEncadeado<TChave>.obterCheio: Boolean;
begin
  Result := inherited cheio;
end;

function TConjuntoOrdenadoEncadeado<TChave>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TConjuntoOrdenadoEncadeado<TChave>.obterTotal: Integer;
begin
  Result := inherited total;
end;

function TConjuntoOrdenadoEncadeado<TChave>.obterVazio: Boolean;
begin
  Result := inherited vazio;
end;

function TConjuntoOrdenadoEncadeado<TChave>.remover(chave: TChave): Boolean;
var
  no: PNo;
begin
  if vazio then
    no := nil
  else
    no := buscarNo(chave);

  Result := (no <> nil);

  if Result then
    excluirNo(no);
end;

function TConjuntoOrdenadoEncadeado<TChave>.ToArray: TArray<TChave>;
begin
  Result := TExtensorPadrao.toArray<TChave>(Self);
end;

function TConjuntoOrdenadoEncadeado<TChave>.ToString: string;
begin
  Result := TExtensorPadrao.toString<TChave>(Self);
end;

procedure TConjuntoOrdenadoEncadeado<TChave>.unir(colecao: IEnumeravel<TChave>);
var
  item: TChave;
begin
//  if (colecao = nil) then
//    raise EArgumentNilException.CreateFmt(S_ARGUMENTO_NULO, ['colecao']);

  if Assigned(colecao) then
    for item in colecao do
      adicionar(item);
end;

end.
