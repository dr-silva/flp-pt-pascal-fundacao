(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.PilhaVetorial.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o vetorial que implementa a interface IEmpilh�-
 *            vel<T>, assim, h� uma cole��o que se baseia no princ�pio �ltimo a
 *            entrar, primeiro a sair (Last In - First Out, LIFO).
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.PilhaVetorial;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes, Colecoes.Vetorizacao;

type
  TPilhaVetorial<T> = class (TVetorizacao<T>, IEmpilhavel<T>)
  private
    fTopo: Integer;
  protected
    function obterTotal         : Integer; override;
    function obterVazio         : Boolean; override;
    function obterCheio         : Boolean; override;
    function obterSomenteLeitura: Boolean; override;
  public
    constructor Create(const capacidade: Integer);

    function GetEnumerator: TEnumerator<T>; override;

    procedure empilhar(const valor: T);
    function  desempilhar: T;
    function  espiar: T;
    procedure limpar;
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes;

{ TPilhaVetorial<T> }

constructor TPilhaVetorial<T>.Create(const capacidade: Integer);
begin
  inherited Create(capacidade);

  fTopo := -1;
end;

function TPilhaVetorial<T>.desempilhar: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := item[fTopo];
  Dec(fTopo);
end;

procedure TPilhaVetorial<T>.empilhar(const valor: T);
begin
  if (cheio) then
    raise ColecaoCheia at ReturnAddress;

  Inc(fTopo);
  item[fTopo] := valor;
end;

function TPilhaVetorial<T>.espiar: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := item[fTopo];
end;

function TPilhaVetorial<T>.GetEnumerator: TEnumerator<T>;
begin
  Result := Colecoes.Vetorizacao.TEnumerador<T>.Create(Self, -1, total);
end;

procedure TPilhaVetorial<T>.limpar;
begin
  fTopo := -1;
end;

function TPilhaVetorial<T>.obterCheio: Boolean;
begin
  Result := fTopo + 1 = capacidade;
end;

function TPilhaVetorial<T>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TPilhaVetorial<T>.obterTotal: Integer;
begin
  Result := fTopo + 1;
end;

function TPilhaVetorial<T>.obterVazio: Boolean;
begin
  Result := fTopo = -1;
end;

end.
