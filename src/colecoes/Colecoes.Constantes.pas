unit Colecoes.Constantes;

interface

const
  primos: array [4..31] of Integer = (
            13, // �ndice = 04
            31, // �ndice = 05
            61, // �ndice = 06
           127, // �ndice = 07
           251, // �ndice = 08
           509, // �ndice = 09
          1021, // �ndice = 10
          2039, // �ndice = 11
          4093, // �ndice = 12
          8191, // �ndice = 13
         16381, // �ndice = 14
         32749, // �ndice = 15
         65521, // �ndice = 16
        131071, // �ndice = 17
        262139, // �ndice = 18
        524287, // �ndice = 19
       1048573, // �ndice = 20
       2097143, // �ndice = 21
       4194301, // �ndice = 22
       8388593, // �ndice = 23
      16777213, // �ndice = 24
      33554393, // �ndice = 25
      67108859, // �ndice = 26
     134217689, // �ndice = 27
     268435399, // �ndice = 28
     536870909, // �ndice = 29
    1073741789, // �ndice = 30
    2147483647  // �ndice = 31
  );

implementation

end.
