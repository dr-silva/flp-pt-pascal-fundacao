(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.ConjuntoOrdenadoVetorial.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o ordenada vetorial que implementa a interface
 *            IDistingu�vel<TChave>. Esta cole��o n�o permiteque seus elementos
 *            sejam duplicados e � interpretada como um conjunto no sentido ma-
 *            tem�tico.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.ConjuntoOrdenadoVetorial;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes, Colecoes.VetorizacaoOrdenada;

type
  TConjuntoOrdenadoVetorial<TChave> = class
  (
    TVetorizacaoOrdenada<TChave, Pointer>,
    IEnumeravel         <TChave>,
    IDistinguivel       <TChave>
  )
  protected
    function obterTotal         : Integer; inline;
    function obterVazio         : Boolean; inline;
    function obterCheio         : Boolean; inline;
    function obterSomenteLeitura: Boolean; inline;
  public
    function ToString: string;         override;
    function ToArray : TArray<TChave>; inline;

    function GetEnumerator: TEnumerator<TChave>;

    function adicionar(chave: TChave): Boolean;
    function remover  (chave: TChave): Boolean;
    function contem   (chave: TChave): Boolean; inline;

    procedure limpar; inline;

    procedure unir          (colecao: IEnumeravel<TChave>);
    procedure excetuar      (colecao: IEnumeravel<TChave>);
    procedure interseccionar(colecao: IEnumeravel<TChave>);
  end;

implementation
uses
  System.SysUtils,
  System.Math,
  Recursos.Excecoes,
  Extensores.ExtensorPadrao;

{ TConjuntoOrdenadoVetorial<TChave> }

function TConjuntoOrdenadoVetorial<TChave>.adicionar(chave: TChave): Boolean;
var
  indice,
  comparacao: Integer;
begin
  if cheio then
    Result := False
  else if vazio then
  begin
    inserirValor(0, chave, nil);
    Result := True;
  end
  else
  begin
    indice     := indexar (chave);
    comparacao := comparar(chave, indice);

    if (comparacao < 0) then
      inserirValor(indice, chave, nil)
    else if (comparacao > 0) then
      inserirValor(indice + 1, chave, nil);

    Result := comparacao <> 0;
  end;
end;

function TConjuntoOrdenadoVetorial<TChave>.contem(chave: TChave): Boolean;
begin
  Result := contemChave(chave);
end;

procedure TConjuntoOrdenadoVetorial<TChave>.excetuar(
  colecao: IEnumeravel<TChave>);
var
  item: TChave;
begin
//  if (colecao = nil) then
//    raise EArgumentNilException.CreateFmt(S_ARGUMENTO_NULO, ['colecao']);

  if (colecao <> nil) then
    for item in colecao do
      remover(item);
end;

function TConjuntoOrdenadoVetorial<TChave>.GetEnumerator: TEnumerator<TChave>;
begin
  Result := TEnumeradorChaves.Create(Self, 0, total);
end;

procedure TConjuntoOrdenadoVetorial<TChave>.interseccionar(
  colecao: IEnumeravel<TChave>);
var
  valor : TChave;
  novos : array of TChave;
  total ,
  indice: Integer;
begin
//  if (colecao = nil) then
//    raise EArgumentNilException.CreateFmt(S_ARGUMENTO_NULO, ['colecao']);

  if Assigned(colecao) then
    total := colecao.total
  else
    total := 0;

  SetLength(novos, Min(Self.total, total));
  indice := -1;

  if Assigned(colecao) then
    for valor in colecao do
      if contemChave(valor) then
      begin
        Inc(indice);
        novos[indice] := valor;
      end;

  excluirTodos;

  while (indice >= 0) do
  begin
    adicionar(novos[indice]);
    Dec(indice);
  end;

  SetLength(novos, 0);
end;

procedure TConjuntoOrdenadoVetorial<TChave>.limpar;
begin
  excluirTodos;
end;

function TConjuntoOrdenadoVetorial<TChave>.obterCheio: Boolean;
begin
  Result := inherited cheio;
end;

function TConjuntoOrdenadoVetorial<TChave>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TConjuntoOrdenadoVetorial<TChave>.obterTotal: Integer;
begin
  Result := inherited total;
end;

function TConjuntoOrdenadoVetorial<TChave>.obterVazio: Boolean;
begin
  Result := inherited vazio;
end;

function TConjuntoOrdenadoVetorial<TChave>.remover(chave: TChave): Boolean;
var
  indice: Integer;
begin
  if vazio then
    Result := False
  else
  begin
    indice := indexar (chave);
    Result := comparar(chave, indice) = 0;

    if Result then
      excluirValor(indice);
  end;
end;

function TConjuntoOrdenadoVetorial<TChave>.ToArray: TArray<TChave>;
begin
  Result := TExtensorPadrao.toArray<TChave>(Self);
end;

function TConjuntoOrdenadoVetorial<TChave>.ToString: string;
begin
  Result := TExtensorPadrao.toString<TChave>(Self);
end;

procedure TConjuntoOrdenadoVetorial<TChave>.unir(colecao: IEnumeravel<TChave>);
var
  item: TChave;
begin
//  if (colecao = nil) then
//    raise EArgumentNilException.CreateFmt(S_ARGUMENTO_NULO, ['colecao']);

  if (colecao <> nil) then
    for item in colecao do
      adicionar(item);
end;

end.
