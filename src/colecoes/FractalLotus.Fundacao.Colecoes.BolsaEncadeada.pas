(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.BolsaEncadeada.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o de lista ligada em que n�o haja necessidade de
 *            remo��o de itens.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.BolsaEncadeada;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes, Colecoes.Encadeamento;

type
  TBolsaEncadeada<T> = class (TEncadeamento<T>, IGuardavel<T>)
  private
    fPrimeiro,
    fUltimo  : TBolsaEncadeada<T>.PItem;

    function obterPrimeiro: T; inline;
    function obterUltimo  : T; inline;
  protected
    function obterSomenteLeitura: Boolean; override;
  public
    constructor Create;
    destructor  Destroy; override;

    function GetEnumerator: TEnumerator<T>; override;

    procedure guardar(const valor: T);                overload;
    procedure guardar(const valores: TArray<T>);      overload;
    procedure guardar(const valores: IEnumeravel<T>); overload;

    property primeiro: T read obterPrimeiro;
    property ultimo  : T read obterUltimo;
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes;

{ TBolsaEncadeada<T> }

constructor TBolsaEncadeada<T>.Create;
begin
  fPrimeiro := nil;
  fUltimo   := nil;
end;

destructor TBolsaEncadeada<T>.Destroy;
begin
  if (fPrimeiro <> nil) then
    destruirItem(fPrimeiro);

  fPrimeiro := nil;
  fUltimo   := nil;

  inherited Destroy;
end;

function TBolsaEncadeada<T>.GetEnumerator: TEnumerator<T>;
begin
  Result := TBolsaEncadeada<T>.TEnumerador.Create(fPrimeiro);
end;

procedure TBolsaEncadeada<T>.guardar(const valor: T);
var
  item: PItem;
begin
  if cheio then
    raise ColecaoCheia at ReturnAddress;

  item := criarItem(valor);

  if (fUltimo <> nil) then
    fUltimo^.proximo := item;

  fUltimo := item;

  if (fPrimeiro = nil) then
    fPrimeiro := item;
end;

procedure TBolsaEncadeada<T>.guardar(const valores: TArray<T>);
var
  i: Integer;
begin
  for i := Low(valores) to High(valores) do
    guardar(valores[i]);
end;

procedure TBolsaEncadeada<T>.guardar(const valores: IEnumeravel<T>);
var
  valor: T;
begin
  for valor in valores do
    guardar(valor);
end;

function TBolsaEncadeada<T>.obterPrimeiro: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := fPrimeiro^.valor;
end;

function TBolsaEncadeada<T>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TBolsaEncadeada<T>.obterUltimo: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := fUltimo^.valor;
end;

end.
