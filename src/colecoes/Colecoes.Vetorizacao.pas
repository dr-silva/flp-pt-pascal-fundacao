(*******************************************************************************
 *
 * Arquivo  : Colecoes.Vetorizacao.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Dene uma classe b�sica e abstrata para as cole��es vetoriais
 *            n�o-ordenadas
 *
 ******************************************************************************)
unit Colecoes.Vetorizacao;

interface
uses
  System.Generics.Collections, FractalLotus.Fundacao.Cerne.Colecoes;

type
  TVetorizacao<T> = class abstract (TInterfacedObject, IEnumeravel<T>)
  private
    fCapacidade: Integer;
    fItens     : array of T;

    function obterItem(const indice: Integer): T;
    procedure definirItem(const indice: Integer; const valor: T);
  protected
    function obterTotal         : Integer; virtual; abstract;
    function obterVazio         : Boolean; virtual; abstract;
    function obterCheio         : Boolean; virtual; abstract;
    function obterSomenteLeitura: Boolean; virtual; abstract;

    property capacidade: Integer read fCapacidade;
    property item[const indice: Integer]: T read obterItem write definirItem;
  public
    constructor Create(const capacidade: Integer);              overload;
    constructor Create(const capacidade, comprimento: Integer); overload;
    destructor  Destroy;                                        override;

    function ToString: string; override;
    function ToArray : TArray<T>;

    function GetEnumerator: TEnumerator<T>; virtual; abstract;

    property total         : Integer read obterTotal;
    property vazio         : Boolean read obterVazio;
    property cheio         : Boolean read obterCheio;
    property somenteLeitura: Boolean read obterSomenteLeitura;
  end;

  TEnumerador<T> = class(TEnumerator<T>)
  private
    fTotal  ,
    fIndice : Integer;
    fColecao: TVetorizacao<T>;
  protected
    function DoGetCurrent: T; override;
    function DoMoveNext: Boolean; override;
  public
    constructor Create(const colecao: TVetorizacao<T>; const indice, total: Integer);
    destructor  Destroy; override;
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes, Extensores.ExtensorPadrao;

{ TVetorizacao<T> }

constructor TVetorizacao<T>.Create(const capacidade: Integer);
begin
  Create(capacidade, capacidade);
end;

constructor TVetorizacao<T>.Create(const capacidade, comprimento: Integer);
begin
  if (comprimento <= 0) then
    raise ArgumentoNegativo('comprimento') at ReturnAddress;
  if (comprimento < capacidade) then
    raise ArgumentoNegativo('capacidade') at ReturnAddress;

  fCapacidade := capacidade;

  SetLength(fItens, comprimento);
end;

procedure TVetorizacao<T>.definirItem(const indice: Integer; const valor: T);
begin
  if ((indice < 0) or (indice >= fCapacidade)) then
    raise IndiceForaLimites('indice', 0, fCapacidade - 1) at ReturnAddress;

  fItens[indice] := valor;
end;

destructor TVetorizacao<T>.Destroy;
begin
  SetLength(fItens, 0);

  inherited Destroy;
end;

function TVetorizacao<T>.obterItem(const indice: Integer): T;
begin
  if ((indice < 0) or (indice >= fCapacidade)) then
    raise IndiceForaLimites('indice', 0, fCapacidade - 1) at ReturnAddress;

  Result := fItens[indice];
end;

function TVetorizacao<T>.ToArray: TArray<T>;
begin
  Result := TExtensorPadrao.toArray<T>(Self);
end;

function TVetorizacao<T>.ToString: string;
begin
  Result := TExtensorPadrao.toString<T>(Self);
end;

{ TEnumerador<T> }

constructor TEnumerador<T>.Create(
  const colecao: TVetorizacao<T>; const indice, total: Integer);
begin
  fTotal   := total;
  fIndice  := indice;
  fColecao := colecao;
end;

destructor TEnumerador<T>.Destroy;
begin
  fColecao := nil;

  inherited Destroy;
end;

function TEnumerador<T>.DoGetCurrent: T;
begin
  Result := fColecao.item[fIndice];
end;

function TEnumerador<T>.DoMoveNext: Boolean;
begin
  Result := fTotal > 0;

  if (Result) then
  begin
    Dec(fTotal);
    Inc(fIndice);
    if (fIndice >= fColecao.capacidade) then
      fIndice := 0;
  end;
end;

end.
