(*******************************************************************************
 *
 * Arquivo  : Colecoes.HashSondado.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma classe b�sica e abstrata que servir� de base para cole-
 *            ��es n�o-ordenadas que funcionem atrav�s de tabela hash com reso-
 *            lu��o de colis�es por sondagem linear (conhecido como linear pro-
 *            bing).
 *
 ******************************************************************************)
unit Colecoes.HashSondado;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.Colecoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

type
  THashSondado<TChave, TValor> = class abstract (TInterfacedObject)
  private const
    MAX_TOTAL = 3 * (1 shl 29);
  protected type
    {$region 'Item'}
    PItem = ^TItem;
    TItem = record
      chave: TChave;
      valor: TValor;
      hash : Integer;
    end;

    TItemArray = array of PItem;
    {$endregion}

    {$region 'Enumeradores'}
    TEnumerador<T> = class abstract (TEnumerator<T>)
    private
      fColecao: THashSondado<TChave, TValor>;
      fAtual  : PItem;
      fTotal  ,
      fIndice : Integer;
    protected
      function DoMoveNext: Boolean; override;

      property colecao: THashSondado<TChave, TValor> read fColecao;
      property atual  : PItem                        read fAtual;
      property total  : Integer                      read fTotal;
      property indice : Integer                      read fIndice;
    public
      constructor Create(const colecao: THashSondado<TChave, TValor>);
      destructor  Destroy; override;
    end;

    TEnumeradorChaves = class (TEnumerador<TChave>)
    protected
      function DoGetCurrent: TChave; override;
    end;

    TEnumeradorValores = class (TEnumerador<TValor>)
    protected
      function DoGetCurrent: TValor; override;
    end;

    TEnumeradorPares = class (TEnumerador<TPair<TChave, TValor>>)
    protected
      function DoGetCurrent: TPair<TChave, TValor>; override;
    end;
    {$endregion}

    {$region 'Cole��es'}
    TColecaoHash<T> = class abstract (TInterfacedObject, IEnumeravel<T>)
    private
      fColecao: THashSondado<TChave, TValor>;

      function obterTotal         : Integer; inline;
      function obterVazio         : Boolean; inline;
      function obterCheio         : Boolean; inline;
      function obterSomenteLeitura: Boolean; inline;
    protected
      property colecao: THashSondado<TChave, TValor> read fColecao;
    public
      constructor Create(const colecao: THashSondado<TChave, TValor>);
      destructor  Destroy; override;

      function ToString: string; override;
      function ToArray : TArray<T>;

      function GetEnumerator: TEnumerator<T>; virtual; abstract;

      property total         : Integer read obterTotal;
      property vazio         : Boolean read obterVazio;
      property cheio         : Boolean read obterCheio;
      property somenteLeitura: Boolean read obterSomenteLeitura;
    end;

    TColecaoChaves = class (TColecaoHash<TChave>)
    public
      function GetEnumerator: TEnumerator<TChave>; override;
    end;

    TColecaoValores = class (TColecaoHash<TValor>)
    public
      function GetEnumerator: TEnumerator<TValor>; override;
    end;
    {$endregion}
  private
    fTotal       ,
    fPrimo       ,
    fCapacidade  : Integer;
    fLgCapacidade: Byte;
    fExcluido    : PItem;
    fItens       : TItemArray;
    fComparador  : IComparadorIgualdade<TChave>;

    function obterTotal: Integer; inline;
    function obterVazio: Boolean; inline;
    function obterCheio: Boolean; inline;

    function criarItem(const chave: TChave; const valor: TValor): PItem;
    procedure destruirItem(item: PItem);

    function calcularHash(const chave, indice: Integer): Integer;
  private
    fProjetor: IProjetor<TChave, string>;
  protected
    procedure redimensionar(const dimensao: Byte);

    function buscarItem(const chave: TChave): PItem;

    procedure inserirItem(const chave: TChave; const valor: TValor); overload;
    procedure inserirItem(item: PItem); overload;
    procedure extrairItem(item: PItem);
    procedure excluirItem(item: PItem);
    procedure excluirTodos;

    property primo       : Integer                      read fPrimo;
    property capacidade  : Integer                      read fCapacidade;
    property lgCapacidade: Byte                         read fLgCapacidade;
    property excluido    : PItem                        read fExcluido;
    property comparador  : IComparadorIgualdade<TChave> read fComparador;
  public
    constructor Create;                                                 overload;
    constructor Create(const igualdade: TFuncaoIgualdade<TChave>);      overload;
    constructor Create(const hash: TFuncaoHash<TChave>);                overload;
    constructor Create(const igualdade: TFuncaoIgualdade<TChave>;
                       const hash: TFuncaoHash<TChave>);                overload;
    constructor Create(const comparador: IComparadorIgualdade<TChave>); overload;

    constructor Create(const capacidade: Integer);                      overload;
    constructor Create(const capacidade: Integer;
                       const igualdade: TFuncaoIgualdade<TChave>);      overload;
    constructor Create(const capacidade: Integer;
                       const hash: TFuncaoHash<TChave>);                overload;
    constructor Create(const capacidade: Integer;
                       const igualdade: TFuncaoIgualdade<TChave>;
                       const hash: TFuncaoHash<TChave>);                overload;
    constructor Create(const capacidade: Integer;
                       const comparador: IComparadorIgualdade<TChave>); overload;

    destructor  Destroy;                                                override;

    property total: Integer read obterTotal;
    property vazio: Boolean read obterVazio;
    property cheio: Boolean read obterCheio;
    property projetor: IProjetor<TChave, string> read fProjetor;
  end;

implementation
uses
  System.Math,
  System.SysUtils,
  Colecoes.Constantes,
  Recursos.Excecoes,
  Extensores.ExtensorPadrao;

{ THashSondado<TChave, TValor> }

function THashSondado<TChave, TValor>.buscarItem(const chave: TChave): PItem;
var
  novaChave, indice, hash: Integer;
begin
  novaChave := comparador.GetHashCode(chave);
  indice    := 0;
  Result    := nil;
  hash      := calcularHash(novaChave, indice);

  while ((fItens[hash] <> nil) and (indice < capacidade)) do
  begin
    if ((fItens[hash] <> excluido) and comparador.Equals(chave, fItens[hash]^.chave)) then
    begin
      Result := fItens[hash];
      Break;
    end;

    Inc(indice);
    hash := calcularHash(novaChave, indice);
  end;
end;

function THashSondado<TChave, TValor>.calcularHash(const chave,
  indice: Integer): Integer;
var
  novaChave, fator, parcela: Integer;
begin
  novaChave := chave and $7FFFFFFF;
  parcela   := novaChave mod capacidade;
  fator     := 1 + (novaChave mod primo);

  if (fator mod 2 = 0) then
    Inc(fator);

  Result := (parcela + indice * fator) mod capacidade;
end;

constructor THashSondado<TChave, TValor>.Create(
  const igualdade: TFuncaoIgualdade<TChave>; const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>(igualdade, hash)
  );
end;

constructor THashSondado<TChave, TValor>.Create(
  const comparador: IComparadorIgualdade<TChave>);
begin
  if (comparador = nil) then
    raise ArgumentoNulo('comparador') at ReturnAddress;

  fComparador := comparador;
  fExcluido   := criarItem(default(TChave), default(TValor));
  fTotal      := 0;
  fProjetor   := TFabricaProjetor.criarString<TChave>();

  redimensionar(4);
end;

constructor THashSondado<TChave, TValor>.Create;
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>()
  );
end;

constructor THashSondado<TChave, TValor>.Create(
  const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>(hash)
  );
end;

constructor THashSondado<TChave, TValor>.Create(
  const igualdade: TFuncaoIgualdade<TChave>);
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>(igualdade)
  );
end;

function THashSondado<TChave, TValor>.criarItem(const chave: TChave;
  const valor: TValor): PItem;
begin
  New(Result);

  Result^.chave := chave;
  Result^.valor := valor;
  Result^.hash  := -1;
end;

destructor THashSondado<TChave, TValor>.Destroy;
begin
  excluirTodos;
  destruirItem(fExcluido);

  fComparador := nil;
  fProjetor   := nil;

  inherited Destroy;
end;

procedure THashSondado<TChave, TValor>.destruirItem(item: PItem);
begin
  if (item <> nil) then
  begin
    item^.chave := default(TChave);
    item^.valor := default(TValor);

    FreeMem(item);
    item := nil;
  end;
end;

procedure THashSondado<TChave, TValor>.excluirItem(item: PItem);
begin
  extrairItem (item);
  destruirItem(item);

  if (total < capacidade div 8) then
    redimensionar(lgCapacidade - 1);
end;

procedure THashSondado<TChave, TValor>.excluirTodos;
var
  i: Integer;
begin
  for i := 0 to capacidade - 1 do
  begin
    if (fItens[i] <> nil) and (fItens[i] <> fExcluido) then
      destruirItem(fItens[i]);

    fItens[i] := nil;
  end;

  fTotal := 0;
end;

procedure THashSondado<TChave, TValor>.extrairItem(item: PItem);
begin
  if (item = nil) or (item = fExcluido) then
    Exit;

  fItens[item^.hash] := excluido;
  item^.hash         := -1;

  Dec(fTotal);
end;

procedure THashSondado<TChave, TValor>.inserirItem(item: PItem);
var
  novaChave, indice, hash: Integer;
begin
  if (item = nil) or (item = fExcluido) then
    Exit;

  novaChave := comparador.GetHashCode(item^.chave);
  indice    := 0;

  repeat
    hash := calcularHash(novaChave, indice);
    if (fItens[hash] = nil) or (fItens[hash] = fExcluido) then
    begin
      item^.hash   := hash;
      fItens[hash] := item;

      Inc(fTotal);
      Break;
    end;

    Inc(indice);
  until (indice >= capacidade);
end;

procedure THashSondado<TChave, TValor>.inserirItem(const chave: TChave;
  const valor: TValor);
begin
  if (total > 3 * capacidade div 4) then
    redimensionar(lgCapacidade + 1);

  inserirItem( criarItem(chave, valor) );
end;

function THashSondado<TChave, TValor>.obterCheio: Boolean;
begin
  Result := fTotal = MAX_TOTAL;
end;

function THashSondado<TChave, TValor>.obterTotal: Integer;
begin
  Result := fTotal;
end;

function THashSondado<TChave, TValor>.obterVazio: Boolean;
begin
  Result := fTotal = 0;
end;

procedure THashSondado<TChave, TValor>.redimensionar(const dimensao: Byte);
var
  temp, item: PItem;
  antigos: TItemArray;
  capacidadeAntiga, valor, i: Integer;
begin
  if (dimensao < 4) then
    valor := 4
  else if (dimensao > 31) then
    valor := 31
  else
    valor := dimensao;

  if (valor = lgCapacidade) then
    Exit;

  antigos          := fItens;
  capacidadeAntiga := capacidade;
  fItens           := nil;
  fLgCapacidade    := valor;
  fCapacidade      := 1 shl valor; // 2 ^ valor
  fPrimo           := primos[valor];

  SetLength(fItens, capacidade);

  if (total = 0) then
    Exit;

  fTotal := 0;
  for i := 0 to capacidadeAntiga - 1 do
    if ((antigos[i] <> nil) and (antigos[i] <> excluido)) then
      inserirItem(antigos[i]);

  SetLength(antigos, 0);
end;

constructor THashSondado<TChave, TValor>.Create(const capacidade: Integer);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>()
  );
end;

constructor THashSondado<TChave, TValor>.Create(const capacidade: Integer;
  const igualdade: TFuncaoIgualdade<TChave>);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>(igualdade)
  );
end;

constructor THashSondado<TChave, TValor>.Create(const capacidade: Integer;
  const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>(hash)
  );
end;

constructor THashSondado<TChave, TValor>.Create(const capacidade: Integer;
  const igualdade: TFuncaoIgualdade<TChave>; const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>(igualdade, hash)
  );
end;

constructor THashSondado<TChave, TValor>.Create(const capacidade: Integer;
  const comparador: IComparadorIgualdade<TChave>);
begin
  if (comparador = nil) then
    raise ArgumentoNulo('comparador') at ReturnAddress;
  if (capacidade <= 0) then
    raise ArgumentoNegativo('capacidade') at ReturnAddress;

  fComparador := comparador;
  fExcluido   := criarItem(default(TChave), default(TValor));
  fTotal      := 0;
  fProjetor   := TFabricaProjetor.criarString<TChave>();

  redimensionar( System.Math.Ceil(System.Math.Log2(4 * capacidade / 3)) );
end;

{ THashSondado<TChave, TValor>.TEnumerador<T> }

constructor THashSondado<TChave, TValor>.TEnumerador<T>.Create(
  const colecao: THashSondado<TChave, TValor>);
begin
  fColecao := colecao;
  fAtual   := nil;
  fTotal   := colecao.total;
  fIndice  := -1;
end;

destructor THashSondado<TChave, TValor>.TEnumerador<T>.Destroy;
begin
  fColecao := nil;
  fAtual   := nil;

  inherited Destroy;
end;

function THashSondado<TChave, TValor>.TEnumerador<T>.DoMoveNext: Boolean;
begin
  Result := (total > 0);

  if Result then
  begin
    repeat
      Inc(fIndice);
    until (fColecao.fItens[fIndice] <> nil) and
          (fColecao.fItens[fIndice] <> fColecao.fExcluido);

    fAtual := fColecao.fItens[fIndice];
    Dec(fTotal);
  end;
end;

{ THashSondado<TChave, TValor>.TEnumeradorChaves }

function THashSondado<TChave, TValor>.TEnumeradorChaves.DoGetCurrent: TChave;
begin
  Result := atual^.chave;
end;

{ THashSondado<TChave, TValor>.TEnumeradorValores }

function THashSondado<TChave, TValor>.TEnumeradorValores.DoGetCurrent: TValor;
begin
  Result := atual^.valor;
end;

{ THashSondado<TChave, TValor>.TEnumeradorPares }

function THashSondado<TChave, TValor>.TEnumeradorPares.DoGetCurrent: TPair<TChave, TValor>;
begin
  Result.Key   := atual^.chave;
  Result.Value := atual^.valor;
end;

{ THashSondado<TChave, TValor>.TColecaoHash<T> }

constructor THashSondado<TChave, TValor>.TColecaoHash<T>.Create(
  const colecao: THashSondado<TChave, TValor>);
begin
  fColecao := colecao;
end;

destructor THashSondado<TChave, TValor>.TColecaoHash<T>.Destroy;
begin
  fColecao := nil;

  inherited Destroy;
end;

function THashSondado<TChave, TValor>.TColecaoHash<T>.obterCheio: Boolean;
begin
  Result := colecao.cheio;
end;

function THashSondado<TChave, TValor>.TColecaoHash<T>.obterSomenteLeitura: Boolean;
begin
  Result := True;
end;

function THashSondado<TChave, TValor>.TColecaoHash<T>.obterTotal: Integer;
begin
  Result := colecao.total;
end;

function THashSondado<TChave, TValor>.TColecaoHash<T>.obterVazio: Boolean;
begin
  Result := colecao.vazio;
end;

function THashSondado<TChave, TValor>.TColecaoHash<T>.ToArray: TArray<T>;
begin
  Result := TExtensorPadrao.toArray<T>(Self);
end;

function THashSondado<TChave, TValor>.TColecaoHash<T>.ToString: string;
begin
  Result := TExtensorPadrao.toString<T>(Self);
end;

{ THashSondado<TChave, TValor>.TColecaoChaves }

function THashSondado<TChave, TValor>.TColecaoChaves.GetEnumerator: TEnumerator<TChave>;
begin
  Result := TEnumeradorChaves.Create(colecao);
end;

{ THashSondado<TChave, TValor>.TColecaoValores }

function THashSondado<TChave, TValor>.TColecaoValores.GetEnumerator: TEnumerator<TValor>;
begin
  Result := TEnumeradorValores.Create(colecao);
end;

end.
