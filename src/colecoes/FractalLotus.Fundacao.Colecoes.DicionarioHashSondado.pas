(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.DicionarioHashSondado.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o n�o-ordenada do tipo tabela hash com resolu��o
 *            de colis�es por sondagem linear que implementa a interface IDicio-
 *            nariz�vel<TChave, TValor>. � uma estrutura de dados de pares cha-
 *            ve-valor cuja principal fun��o � buscar o valor que esteja associ-
 *            ado a uma determinada chave.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.DicionarioHashSondado;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes,
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes,
  Colecoes.HashSondado;

type
  TDicionarioHashSondado<TChave, TValor> = class
  (
    THashSondado   <TChave, TValor>,
    IDicionarizavel<TChave, TValor>,
    IEnumeravel<TPair<TChave, TValor>>
  )
  private
    fChaves : IEnumeravel<TChave>;
    fValores: IEnumeravel<TValor>;

    function obterTotal         : Integer; inline;
    function obterVazio         : Boolean; inline;
    function obterCheio         : Boolean; inline;
    function obterSomenteLeitura: Boolean; inline;

    function  obterValor(const chave: TChave): TValor;
    procedure definirValor(const chave: TChave; const valor: TValor); inline;

    function  obterChaves : IEnumeravel<TChave>; inline;
    function  obterValores: IEnumeravel<TValor>; inline;
  public
    constructor Create;                                                 overload;
    constructor Create(const igualdade: TFuncaoIgualdade<TChave>);      overload;
    constructor Create(const hash     : TFuncaoHash<TChave>);           overload;
    constructor Create(const igualdade: TFuncaoIgualdade<TChave>;
                       const hash     : TFuncaoHash<TChave>);           overload;
    constructor Create(const comparador: IComparadorIgualdade<TChave>); overload;

    constructor Create(const capacidade: Integer);                      overload;
    constructor Create(const capacidade: Integer;
                       const igualdade: TFuncaoIgualdade<TChave>);      overload;
    constructor Create(const capacidade: Integer;
                       const hash: TFuncaoHash<TChave>);                overload;
    constructor Create(const capacidade: Integer;
                       const igualdade: TFuncaoIgualdade<TChave>;
                       const hash: TFuncaoHash<TChave>);                overload;
    constructor Create(const capacidade: Integer;
                       const comparador: IComparadorIgualdade<TChave>); overload;

    destructor  Destroy;                                                override;

    function ToString: string;                        override;
    function ToArray : TArray<TPair<TChave, TValor>>;

    function GetEnumerator: TEnumerator<TPair<TChave, TValor>>;

    procedure adicionar(const chave: TChave; const valor: TValor);          overload;
    procedure adicionar(const dicionario: IDicionarizavel<TChave, TValor>); overload;

    function  contem (const chave: TChave): Boolean; inline;
    function  remover(const chave: TChave): Boolean;
    procedure limpar;

    function tentarObter(const chave: TChave; out valor: TValor): Boolean;

    property somenteLeitura: Boolean             read obterSomenteLeitura;
    property chaves        : IEnumeravel<TChave> read obterChaves;
    property valores       : IEnumeravel<TValor> read obterValores;
    property valor[const chave: TChave]: TValor  read obterValor write definirValor; default;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes,
  Extensores.ExtensorPadrao;

{ TDicionarioHashSondado<TChave, TValor> }

procedure TDicionarioHashSondado<TChave, TValor>.adicionar(
  const dicionario: IDicionarizavel<TChave, TValor>);
var
  par: TPair<TChave, TValor>;
begin
  for par in dicionario do
    adicionar(par.Key, par.Value);
end;

procedure TDicionarioHashSondado<TChave, TValor>.adicionar(const chave: TChave;
  const valor: TValor);
var
  item: PItem;
begin
  item := buscarItem(chave);

  if (item <> nil) then
    item^.valor := valor
  else if cheio then
    raise ColecaoCheia at ReturnAddress
  else
    inserirItem(chave, valor);
end;

function TDicionarioHashSondado<TChave, TValor>.contem(
  const chave: TChave): Boolean;
begin
  Result := (not vazio) and (buscarItem(chave) <> nil);
end;

constructor TDicionarioHashSondado<TChave, TValor>.Create;
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>()
  );
end;

constructor TDicionarioHashSondado<TChave, TValor>.Create(
  const comparador: IComparadorIgualdade<TChave>);
begin
  inherited Create(comparador);

  fChaves  := TColecaoChaves .Create(Self);
  fValores := TColecaoValores.Create(Self);
end;

constructor TDicionarioHashSondado<TChave, TValor>.Create(
  const igualdade: TFuncaoIgualdade<TChave>);
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>(igualdade)
  );
end;

constructor TDicionarioHashSondado<TChave, TValor>.Create(
  const igualdade: TFuncaoIgualdade<TChave>; const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>(igualdade, hash)
  );
end;

constructor TDicionarioHashSondado<TChave, TValor>.Create(
  const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>(hash)
  );
end;

procedure TDicionarioHashSondado<TChave, TValor>.definirValor(
  const chave: TChave; const valor: TValor);
begin
  adicionar(chave, valor);
end;

destructor TDicionarioHashSondado<TChave, TValor>.Destroy;
begin
  fChaves  := nil;
  fValores := nil;

  inherited Destroy;
end;

function TDicionarioHashSondado<TChave, TValor>.GetEnumerator: TEnumerator<TPair<TChave, TValor>>;
begin
  Result := TEnumeradorPares.Create(Self);
end;

procedure TDicionarioHashSondado<TChave, TValor>.limpar;
begin
  if not vazio then
  begin
    excluirTodos;
    redimensionar(4);
  end;
end;

function TDicionarioHashSondado<TChave, TValor>.obterChaves: IEnumeravel<TChave>;
begin
  Result := fChaves;
end;

function TDicionarioHashSondado<TChave, TValor>.obterCheio: Boolean;
begin
  Result := inherited cheio;
end;

function TDicionarioHashSondado<TChave, TValor>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TDicionarioHashSondado<TChave, TValor>.obterTotal: Integer;
begin
  Result := inherited total;
end;

function TDicionarioHashSondado<TChave, TValor>.obterValor(
  const chave: TChave): TValor;
var
  item: PItem;
begin
  item := buscarItem(chave);

  if (item = nil) then
    raise ChaveDicionario at ReturnAddress;

  Result := item^.valor;
end;

function TDicionarioHashSondado<TChave, TValor>.obterValores: IEnumeravel<TValor>;
begin
  Result := fValores;
end;

function TDicionarioHashSondado<TChave, TValor>.obterVazio: Boolean;
begin
  Result := inherited vazio;
end;

function TDicionarioHashSondado<TChave, TValor>.remover(
  const chave: TChave): Boolean;
var
  item: PItem;
begin
  if vazio then
    item := nil
  else
    item := buscarItem(chave);

  Result := (item <> nil);
  if Result then
    excluirItem(item);
end;

function TDicionarioHashSondado<TChave, TValor>.tentarObter(const chave: TChave;
  out valor: TValor): Boolean;
var
  item: PItem;
begin
  item   := buscarItem(chave);
  Result := (item <> nil);

  if Result then
    valor := item^.valor;
end;

function TDicionarioHashSondado<TChave, TValor>.ToArray: TArray<TPair<TChave, TValor>>;
begin
  Result := TExtensorPadrao.toArray<TPair<TChave, TValor>>(Self);
end;

function TDicionarioHashSondado<TChave, TValor>.ToString: string;
begin
  Result := TExtensorPadrao.toString<TPair<TChave, TValor>>(Self);
end;

constructor TDicionarioHashSondado<TChave, TValor>.Create(
  const capacidade: Integer);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>()
  );
end;

constructor TDicionarioHashSondado<TChave, TValor>.Create(
  const capacidade: Integer; const igualdade: TFuncaoIgualdade<TChave>);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>(igualdade)
  );
end;

constructor TDicionarioHashSondado<TChave, TValor>.Create(
  const capacidade: Integer; const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>(hash)
  );
end;

constructor TDicionarioHashSondado<TChave, TValor>.Create(
  const capacidade: Integer; const igualdade: TFuncaoIgualdade<TChave>;
  const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>(igualdade, hash)
  );
end;

constructor TDicionarioHashSondado<TChave, TValor>.Create(
  const capacidade: Integer; const comparador: IComparadorIgualdade<TChave>);
begin
  inherited Create(capacidade, comparador);

  fChaves  := TColecaoChaves .Create(Self);
  fValores := TColecaoValores.Create(Self);
end;

end.
