(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.DicionarioOrdenadoVetorial.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o ordenada e vetorial que implementa a interface
 *            IDicionariz�vel<TChave, TValor>. � uma estrutura de dados de pares
 *            chave-valor cuja principal fun��o � buscar o valor que esteja as-
 *            sociado a uma determinada chave.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.DicionarioOrdenadoVetorial;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.Colecoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes,
  Colecoes.VetorizacaoOrdenada;

type
  TDicionarioOrdenadoVetorial<TChave, TValor> = class
  (
    TVetorizacaoOrdenada<TChave, TValor>,
    IDicionarizavel     <TChave, TValor>,
    IEnumeravel   <TPair<TChave, TValor>>
  )
  private type
    TColecao<T> = class abstract (TInterfacedObject, IEnumeravel<T>)
    private
      fColecao: TDicionarioOrdenadoVetorial<TChave, TValor>;

      function obterTotal         : Integer; inline;
      function obterVazio         : Boolean; inline;
      function obterCheio         : Boolean; inline;
      function obterSomenteLeitura: Boolean; inline;
    protected
      property colecao: TDicionarioOrdenadoVetorial<TChave, TValor> read fColecao;
    public
      constructor Create(const dicionario: TDicionarioOrdenadoVetorial<TChave, TValor>);
      destructor  Destroy; override;

      function ToString: string; override;
      function ToArray : TArray<T>;

      function GetEnumerator: TEnumerator<T>; virtual; abstract;

      property total         : Integer read obterTotal;
      property vazio         : Boolean read obterVazio;
      property cheio         : Boolean read obterCheio;
      property somenteLeitura: Boolean read obterSomenteLeitura;
    end;

    TChaves = class (TColecao<TChave>)
    public
      function GetEnumerator: TEnumerator<TChave>; override;
    end;

    TValores = class (TColecao<TValor>)
    public
      function GetEnumerator: TEnumerator<TValor>; override;
    end;
  private
    fChaves : IEnumeravel<TChave>;
    fValores: IEnumeravel<TValor>;

    function obterTotal         : Integer; inline;
    function obterVazio         : Boolean; inline;
    function obterCheio         : Boolean; inline;
    function obterSomenteLeitura: Boolean; inline;
    function obterChaves        : IEnumeravel<TChave>; inline;
    function obterValores       : IEnumeravel<TValor>; inline;

    function  obterValor(const chave: TChave): TValor;
    procedure definirValor(const chave: TChave; const valor: TValor); inline;
  public
    constructor Create(const capacidade: Integer);                                              overload;
    constructor Create(const capacidade: Integer; const comparacao: TFuncaoComparacao<TChave>); overload;
    constructor Create(const capacidade: Integer; const comparador: IComparador<TChave>);       overload;
    destructor  Destroy;                                                                        override;

    function ToString: string;                        override;
    function ToArray : TArray<TPair<TChave, TValor>>;

    function GetEnumerator: TEnumerator<TPair<TChave, TValor>>;

    procedure adicionar(const chave: TChave; const valor: TValor);          overload;
    procedure adicionar(const dicionario: IDicionarizavel<TChave, TValor>); overload;

    function  contem (const chave: TChave): Boolean; inline;
    function  remover(const chave: TChave): Boolean;
    procedure limpar; inline;

    function tentarObter(const chave: TChave; out valor: TValor): Boolean;

    property chaves : IEnumeravel<TChave> read obterChaves;
    property valores: IEnumeravel<TValor> read obterValores;
    property valor[const chave: TChave]: TValor read obterValor write definirValor; default;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes,
  Extensores.ExtensorPadrao;

{ TDicionarioOrdenadoVetorial<TChave, TValor> }

procedure TDicionarioOrdenadoVetorial<TChave, TValor>.adicionar(
  const dicionario: IDicionarizavel<TChave, TValor>);
var
  par: TPair<TChave, TValor>;
begin
  for par in dicionario do
    adicionar(par.Key, par.Value);
end;

procedure TDicionarioOrdenadoVetorial<TChave, TValor>.adicionar(
  const chave: TChave; const valor: TValor);
var
  indice    ,
  comparacao: Integer;
begin
  if vazio then
    inserirValor(0, chave, valor)
  else
  begin
    indice     := indexar(chave);
    comparacao := comparar(chave, indice);

    if (comparacao = 0) then
      inherited valor[indice] := valor
    else
    begin
      if cheio then
        raise ColecaoCheia at ReturnAddress;

      if (comparacao < 0) then
        inserirValor(indice, chave, valor)
      else
        inserirValor(indice + 1, chave, valor);
    end;
  end;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.contem(
  const chave: TChave): Boolean;
begin
  Result := contemChave(chave);
end;

constructor TDicionarioOrdenadoVetorial<TChave, TValor>.Create(
  const capacidade: Integer);
begin
  inherited Create(capacidade);

  fChaves  := TChaves.Create(Self);
  fValores := TValores.Create(Self);
end;

constructor TDicionarioOrdenadoVetorial<TChave, TValor>.Create(
  const capacidade: Integer; const comparacao: TFuncaoComparacao<TChave>);
begin
  inherited Create(capacidade, comparacao);

  fChaves  := TChaves.Create(Self);
  fValores := TValores.Create(Self);
end;

constructor TDicionarioOrdenadoVetorial<TChave, TValor>.Create(
  const capacidade: Integer; const comparador: IComparador<TChave>);
begin
  inherited Create(capacidade, comparador);

  fChaves  := TChaves.Create(Self);
  fValores := TValores.Create(Self);
end;

procedure TDicionarioOrdenadoVetorial<TChave, TValor>.definirValor(
  const chave: TChave; const valor: TValor);
begin
  adicionar(chave, valor);
end;

destructor TDicionarioOrdenadoVetorial<TChave, TValor>.Destroy;
begin

  inherited;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.GetEnumerator: TEnumerator<TPair<TChave, TValor>>;
begin
  Result := TEnumeradorPares.Create(Self, 0, total);
end;

procedure TDicionarioOrdenadoVetorial<TChave, TValor>.limpar;
begin
  excluirTodos;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.obterChaves: IEnumeravel<TChave>;
begin
  Result := fChaves;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.obterCheio: Boolean;
begin
  Result := inherited cheio;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.obterTotal: Integer;
begin
  Result := inherited total;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.obterValor(
  const chave: TChave): TValor;
var
  indice: Integer;
begin
  indice := indexar(chave);

  if (comparar(chave, indice) <> 0) then
    raise ChaveDicionario at ReturnAddress;

  Result := inherited valor[indice];
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.obterValores: IEnumeravel<TValor>;
begin
  Result := fValores;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.obterVazio: Boolean;
begin
  Result := inherited vazio;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.remover(
  const chave: TChave): Boolean;
var
  indice: Integer;
begin
  if not vazio then
  begin
    indice := indexar (chave);
    Result := comparar(chave, indice) = 0;

    if Result then
      excluirValor(indice);
  end
  else
    Result := False;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.tentarObter(
  const chave: TChave; out valor: TValor): Boolean;
var
  indice: Integer;
begin
  indice := indexar (chave);
  Result := comparar(chave, indice) = 0;

  if Result then
    valor := inherited valor[indice];
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.ToArray: TArray<TPair<TChave, TValor>>;
begin
  Result := TExtensorPadrao.toArray<TPair<TChave, TValor>>(Self);
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.ToString: string;
begin
  Result := TExtensorPadrao.toString<TPair<TChave, TValor>>(Self);
end;

{ TDicionarioOrdenadoVetorial<TChave, TValor>.TColecao<T> }

constructor TDicionarioOrdenadoVetorial<TChave, TValor>.TColecao<T>.Create(
  const dicionario: TDicionarioOrdenadoVetorial<TChave, TValor>);
begin
  fColecao := dicionario;
end;

destructor TDicionarioOrdenadoVetorial<TChave, TValor>.TColecao<T>.Destroy;
begin
  fColecao := nil;

  inherited Destroy;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.TColecao<T>.obterCheio: Boolean;
begin
  Result := colecao.cheio;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.TColecao<T>.obterSomenteLeitura: Boolean;
begin
  Result := True;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.TColecao<T>.obterTotal: Integer;
begin
  Result := colecao.total;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.TColecao<T>.obterVazio: Boolean;
begin
  Result := colecao.vazio;
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.TColecao<T>.ToArray: TArray<T>;
begin
  Result := TExtensorPadrao.toArray<T>(Self);
end;

function TDicionarioOrdenadoVetorial<TChave, TValor>.TColecao<T>.ToString: string;
begin
  Result := TExtensorPadrao.toString<T>(Self);
end;

{ TDicionarioOrdenadoVetorial<TChave, TValor>.TChaves }

function TDicionarioOrdenadoVetorial<TChave, TValor>.TChaves.GetEnumerator: TEnumerator<TChave>;
begin
  Result := TEnumeradorChaves.Create(colecao, 0, colecao.total);
end;

{ TDicionarioOrdenadoVetorial<TChave, TValor>.TValores }

function TDicionarioOrdenadoVetorial<TChave, TValor>.TValores.GetEnumerator: TEnumerator<TValor>;
begin
  Result := TEnumeradorValores.Create(colecao, 0, colecao.total);
end;

end.
