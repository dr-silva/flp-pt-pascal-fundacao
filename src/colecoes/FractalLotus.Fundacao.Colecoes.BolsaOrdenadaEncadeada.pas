(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.BolsaOrdenadaEncadeada.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o ordenada de �rvore bin�ria que implementa a in-
 *            terface IGuard�vel<T> cujo objetivo � ter uma cole��o em que n�o
 *            haja a necessidade de remover itens.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.BolsaOrdenadaEncadeada;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes, Colecoes.ArvoreRubronegra;

type
  TBolsaOrdenadaEncadeada<T> = class
  (
    TArvoreRubronegraOrdenada<T, Pointer>,
    IEnumeravel<T>,
    IGuardavel <T>
  )
  private
    function obterPrimeiro      : T;       inline;
    function obterUltimo        : T;       inline;
    function obterSomenteLeitura: Boolean; inline;
  public
    function ToString: string; override;
    function ToArray : TArray<T>;

    function GetEnumerator: TEnumerator<T>;

    procedure guardar(const valor: T);                overload;
    procedure guardar(const valores: TArray<T>);      overload;
    procedure guardar(const valores: IEnumeravel<T>); overload;

    property primeiro      : T       read obterPrimeiro;
    property ultimo        : T       read obterUltimo;
    property somenteLeitura: Boolean read obterSomenteLeitura;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes,
  Extensores.ExtensorPadrao;

{ TBolsaOrdenadaEncadeada<T> }

procedure TBolsaOrdenadaEncadeada<T>.guardar(const valores: TArray<T>);
var
  i: Integer;
begin
  for i := Low(valores) to High(valores) do
    guardar(valores[i]);
end;

procedure TBolsaOrdenadaEncadeada<T>.guardar(const valor: T);
begin
  if cheio then
    raise ColecaoCheia at ReturnAddress;

  inserirNo( criarNo(valor, nil) );
end;

function TBolsaOrdenadaEncadeada<T>.GetEnumerator: TEnumerator<T>;
var
  primeiro, ultimo: PNo;
begin
  primeiro := buscarNoMinimo(raiz);
  ultimo   := buscarNoMaximo(raiz);
  Result   := TEnumeradorChaves.Create(Self, True, primeiro, ultimo);
end;

procedure TBolsaOrdenadaEncadeada<T>.guardar(const valores: IEnumeravel<T>);
var
  valor: T;
begin
  for valor in valores do
    guardar(valor);
end;

function TBolsaOrdenadaEncadeada<T>.obterPrimeiro: T;
begin
  Result := minimo;
end;

function TBolsaOrdenadaEncadeada<T>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TBolsaOrdenadaEncadeada<T>.obterUltimo: T;
begin
  Result := maximo;
end;

function TBolsaOrdenadaEncadeada<T>.ToArray: TArray<T>;
begin
  Result := TExtensorPadrao.toArray<T>(Self);
end;

function TBolsaOrdenadaEncadeada<T>.ToString: string;
begin
  Result := TExtensorPadrao.toString<T>(Self);
end;

end.
