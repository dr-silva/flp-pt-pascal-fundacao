(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.FilaPrioridadeVetorial.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o vetorial que � uma fila cujos elementos t�m
 *            prioridade para se ordenarem nela:
 *              - PRIORIDADE M�NIMA: aquele com prioridade MENOR estar� � frente
 *                                   na fila, � feita uma ordena��o CRESCENTE;
 *              - PRIORIDADE M�XIMA: aquele com prioridade MAIOR estar� � frente
 *                                   na fila, � feita uma ordena��o DECRESCENTE.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.FilaPrioridadeVetorial;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes, Colecoes.HeapVetorial;

type
  TFilaPrioridadeVetorial<T> = class abstract
  (
    THeapVetorial<T>, IEnfileiravel<T>
  )
  protected
    function obterSomenteLeitura: Boolean; override;
  public
    procedure enfileirar(const valor: T);
    function  desenfileirar: T;
    function  espiar: T;
    procedure limpar;
  end;

  TFilaPrioridadeMinimaVetorial<T> = class (TFilaPrioridadeVetorial<T>)
  protected
    function eOrdenavel(comparacao: Integer): Boolean; override;
  end;

  TFilaPrioridadeMaximaVetorial<T> = class (TFilaPrioridadeVetorial<T>)
  protected
    function eOrdenavel(comparacao: Integer): Boolean; override;
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes;

{ TFilaPrioridadeVetorial<T> }

function TFilaPrioridadeVetorial<T>.desenfileirar: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := excluir(0);
end;

procedure TFilaPrioridadeVetorial<T>.enfileirar(const valor: T);
begin
  if cheio then
    raise ColecaoCheia at ReturnAddress;

  inserir(valor);
end;

function TFilaPrioridadeVetorial<T>.espiar: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := item[0];
end;

procedure TFilaPrioridadeVetorial<T>.limpar;
begin
  if not vazio then
    excluirTodos;
end;

function TFilaPrioridadeVetorial<T>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

{ TFilaPrioridadeMinimaVetorial<T> }

function TFilaPrioridadeMinimaVetorial<T>.eOrdenavel(
  comparacao: Integer): Boolean;
begin
  Result := comparacao > 0;
end;

{ TFilaPrioridadeMaximaVetorial<T> }

function TFilaPrioridadeMaximaVetorial<T>.eOrdenavel(
  comparacao: Integer): Boolean;
begin
  Result := comparacao < 0;
end;

end.
