(*******************************************************************************
 *
 * Arquivo  : Colecoes.Encadeamento.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Dene uma classe b�sica e abstrata para as cole��es n�o-ordenadas
 *            feitas com listas ligadas.
 *
 ******************************************************************************)
unit Colecoes.Encadeamento;

interface
uses
  System.Generics.Collections, FractalLotus.Fundacao.Cerne.Colecoes;

type
  TEncadeamento<T> = class abstract(TInterfacedObject, IEnumeravel<T>)
  protected type
    PItem = ^TItem;
    TItem = record
      valor  : T;
      proximo: PItem;
    end;

    TEnumerador = class(TEnumerator<T>)
    private
      fAtual  ,
      fProximo: PItem;
    protected
      function DoGetCurrent: T;     override;
      function DoMoveNext: Boolean; override;
    public
      constructor Create(const proximo: PItem);
      destructor  Destroy; override;
    end;
  private
    fTotal: Integer;

    function obterTotal: Integer; inline;
    function obterVazio: Boolean; inline;
    function obterCheio: Boolean; inline;
  protected
    function obterSomenteLeitura: Boolean; virtual; abstract;

    function criarItem(const valor: T): PItem;
    procedure destruirItem(item: PItem);
  public
    function ToString: string; override;
    function ToArray : TArray<T>;

    function GetEnumerator: TEnumerator<T>; virtual; abstract;

    property total         : Integer read obterTotal;
    property vazio         : Boolean read obterVazio;
    property cheio         : Boolean read obterCheio;
    property somenteLeitura: Boolean read obterSomenteLeitura;
  end;

implementation
uses
  System.SysUtils, Extensores.ExtensorPadrao;

{ TEncadeamento<T> }

function TEncadeamento<T>.criarItem(const valor: T): PItem;
begin
  New(Result);

  Result^.valor   := valor;
  Result^.proximo := nil;
  fTotal          := fTotal + 1;
end;

procedure TEncadeamento<T>.destruirItem(item: PItem);
var
  temp: PItem;
begin
  while (item <> nil) do
  begin
    temp   := item;
    item   := item^.proximo;
    fTotal := fTotal - 1;

    temp.valor   := default(T);
    temp.proximo := nil;

    FreeMem(temp);
  end;
end;

function TEncadeamento<T>.obterCheio: Boolean;
begin
  Result := fTotal = Integer.MaxValue;
end;

function TEncadeamento<T>.obterTotal: Integer;
begin
  Result := fTotal;
end;

function TEncadeamento<T>.obterVazio: Boolean;
begin
  Result := fTotal = 0;
end;

function TEncadeamento<T>.ToArray: TArray<T>;
begin
  Result := TExtensorPadrao.toArray<T>(Self);
end;

function TEncadeamento<T>.ToString: string;
begin
  Result := TExtensorPadrao.toString<T>(Self);
end;

{ TEncadeamento<T>.TEnumerador }

constructor TEncadeamento<T>.TEnumerador.Create(const proximo: PItem);
begin
  fAtual   := nil;
  fProximo := proximo;
end;

destructor TEncadeamento<T>.TEnumerador.Destroy;
begin
  fAtual   := nil;
  fProximo := nil;

  inherited Destroy;
end;

function TEncadeamento<T>.TEnumerador.DoGetCurrent: T;
begin
  Result := fAtual^.valor;
end;

function TEncadeamento<T>.TEnumerador.DoMoveNext: Boolean;
begin
  Result := fProximo <> nil;

  if (Result) then
  begin
    fAtual   := fProximo;
    fProximo := fProximo^.proximo;
  end;
end;

end.
