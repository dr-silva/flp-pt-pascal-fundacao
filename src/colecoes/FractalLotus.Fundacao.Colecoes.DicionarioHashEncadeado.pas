(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.DicionarioHashEncadeado.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o n�o-ordenada do tipo tabela hash com resolu��o
 *            de colis�es por encadeamento que implementa a interface IDiciona-
 *            riz�vel<TChave, TValor>. � uma estrutura de dados de pares chave-
 *            -valor cuja principal fun��o � buscar o valor que esteja associado
 *            a uma determinada chave.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.DicionarioHashEncadeado;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes,
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes,
  Colecoes.HashEncadeado;

type
  TDicionarioHashEncadeado<TChave, TValor> = class
  (
    THashEncadeado <TChave, TValor>,
    IDicionarizavel<TChave, TValor>,
    IEnumeravel<TPair<TChave, TValor>>
  )
  private
    fChaves : IEnumeravel<TChave>;
    fValores: IEnumeravel<TValor>;

    function obterTotal         : Integer; inline;
    function obterVazio         : Boolean; inline;
    function obterCheio         : Boolean; inline;
    function obterSomenteLeitura: Boolean; inline;

    function  obterValor(const chave: TChave): TValor;
    procedure definirValor(const chave: TChave; const valor: TValor); inline;

    function  obterChaves : IEnumeravel<TChave>; inline;
    function  obterValores: IEnumeravel<TValor>; inline;
  public
    constructor Create;                                                 overload;
    constructor Create(const igualdade: TFuncaoIgualdade<TChave>);      overload;
    constructor Create(const hash     : TFuncaoHash<TChave>);           overload;
    constructor Create(const igualdade: TFuncaoIgualdade<TChave>;
                       const hash     : TFuncaoHash<TChave>);           overload;
    constructor Create(const comparador: IComparadorIgualdade<TChave>); overload;

    constructor Create(const capacidade: Integer);                      overload;
    constructor Create(const capacidade: Integer;
                       const igualdade: TFuncaoIgualdade<TChave>);      overload;
    constructor Create(const capacidade: Integer;
                       const hash: TFuncaoHash<TChave>);                overload;
    constructor Create(const capacidade: Integer;
                       const igualdade: TFuncaoIgualdade<TChave>;
                       const hash: TFuncaoHash<TChave>);                overload;
    constructor Create(const capacidade: Integer;
                       const comparador: IComparadorIgualdade<TChave>); overload;

    destructor  Destroy;                                                override;

    function ToString: string;                        override;
    function ToArray : TArray<TPair<TChave, TValor>>;

    function GetEnumerator: TEnumerator<TPair<TChave, TValor>>;

    procedure adicionar(const chave: TChave; const valor: TValor);          overload;
    procedure adicionar(const dicionario: IDicionarizavel<TChave, TValor>); overload;

    function  contem (const chave: TChave): Boolean; inline;
    function  remover(const chave: TChave): Boolean;
    procedure limpar;

    function tentarObter(const chave: TChave; out valor: TValor): Boolean;

    property somenteLeitura: Boolean             read obterSomenteLeitura;
    property chaves        : IEnumeravel<TChave> read obterChaves;
    property valores       : IEnumeravel<TValor> read obterValores;
    property valor[const chave: TChave]: TValor  read obterValor write definirValor; default;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes,
  Extensores.ExtensorPadrao;

{ TDicionarioHashEncadeado<TChave, TValor> }

procedure TDicionarioHashEncadeado<TChave, TValor>.adicionar(
  const dicionario: IDicionarizavel<TChave, TValor>);
var
  par: TPair<TChave, TValor>;
begin
  for par in dicionario do
    adicionar(par.Key, par.Value);
end;

procedure TDicionarioHashEncadeado<TChave, TValor>.adicionar(
  const chave: TChave; const valor: TValor);
var
  item: PItem;
begin
  item := buscarItem(chave);
  if (item = nil) then
  begin
    if cheio then
    raise ColecaoCheia at ReturnAddress;

    inserirItem(chave, valor);
  end
  else
    item^.valor := valor;
end;

function TDicionarioHashEncadeado<TChave, TValor>.contem(
  const chave: TChave): Boolean;
begin
  Result := (not vazio) and (buscarItem(chave) <> nil);
end;

constructor TDicionarioHashEncadeado<TChave, TValor>.Create;
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>()
  );
end;

constructor TDicionarioHashEncadeado<TChave, TValor>.Create(
  const comparador: IComparadorIgualdade<TChave>);
begin
  inherited Create(comparador);

  fChaves  := TColecaoChaves .Create(Self);
  fValores := TColecaoValores.Create(Self);
end;

constructor TDicionarioHashEncadeado<TChave, TValor>.Create(
  const igualdade: TFuncaoIgualdade<TChave>);
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>(igualdade)
  );
end;

constructor TDicionarioHashEncadeado<TChave, TValor>.Create(
  const igualdade: TFuncaoIgualdade<TChave>; const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>(igualdade, hash)
  );
end;

constructor TDicionarioHashEncadeado<TChave, TValor>.Create(
  const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>(hash)
  );
end;

procedure TDicionarioHashEncadeado<TChave, TValor>.definirValor(
  const chave: TChave; const valor: TValor);
begin
  adicionar(chave, valor);
end;

destructor TDicionarioHashEncadeado<TChave, TValor>.Destroy;
begin
  fChaves  := nil;
  fValores := nil;

  inherited Destroy;
end;

function TDicionarioHashEncadeado<TChave, TValor>.GetEnumerator: TEnumerator<TPair<TChave, TValor>>;
begin
  Result := TEnumeradorPares.Create(Self);
end;

procedure TDicionarioHashEncadeado<TChave, TValor>.limpar;
begin
  if (not vazio) then
  begin
    excluirTodos;
    redimensionar(4);
  end;
end;

function TDicionarioHashEncadeado<TChave, TValor>.obterChaves: IEnumeravel<TChave>;
begin
  Result := fChaves;
end;

function TDicionarioHashEncadeado<TChave, TValor>.obterCheio: Boolean;
begin
  Result := inherited cheio;
end;

function TDicionarioHashEncadeado<TChave, TValor>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TDicionarioHashEncadeado<TChave, TValor>.obterTotal: Integer;
begin
  Result := inherited total;
end;

function TDicionarioHashEncadeado<TChave, TValor>.obterValor(
  const chave: TChave): TValor;
var
  item: PItem;
begin
  item := buscarItem(chave);

  if (item = nil) then
    raise ChaveDicionario at ReturnAddress;

  Result := item^.valor;
end;

function TDicionarioHashEncadeado<TChave, TValor>.obterValores: IEnumeravel<TValor>;
begin
  Result := fValores;
end;

function TDicionarioHashEncadeado<TChave, TValor>.obterVazio: Boolean;
begin
  Result := inherited vazio;
end;

function TDicionarioHashEncadeado<TChave, TValor>.remover(
  const chave: TChave): Boolean;
var
  item: PItem;
begin
  if vazio then
    item := nil
  else
    item := buscarItem(chave);

  Result := (item <> nil);
  if Result then
    excluirItem(item);
end;

function TDicionarioHashEncadeado<TChave, TValor>.tentarObter(
  const chave: TChave; out valor: TValor): Boolean;
var
  item: PItem;
begin
  item   := buscarItem(chave);
  Result := (item <> nil);

  if Result then
    valor := item^.valor;
end;

function TDicionarioHashEncadeado<TChave, TValor>.ToArray: TArray<TPair<TChave, TValor>>;
begin
  Result := TExtensorPadrao.toArray<TPair<TChave, TValor>>(Self);
end;

function TDicionarioHashEncadeado<TChave, TValor>.ToString: string;
begin
  Result := TExtensorPadrao.toString<TPair<TChave, TValor>>(Self);
end;

constructor TDicionarioHashEncadeado<TChave, TValor>.Create(
  const capacidade: Integer);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>()
  );
end;

constructor TDicionarioHashEncadeado<TChave, TValor>.Create(
  const capacidade: Integer; const igualdade: TFuncaoIgualdade<TChave>);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>(igualdade)
  );
end;

constructor TDicionarioHashEncadeado<TChave, TValor>.Create(
  const capacidade: Integer; const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>(hash)
  );
end;

constructor TDicionarioHashEncadeado<TChave, TValor>.Create(
  const capacidade: Integer; const igualdade: TFuncaoIgualdade<TChave>;
  const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>(igualdade, hash)
  );
end;

constructor TDicionarioHashEncadeado<TChave, TValor>.Create(
  const capacidade: Integer; const comparador: IComparadorIgualdade<TChave>);
begin
  inherited Create(capacidade, comparador);

  fChaves  := TColecaoChaves .Create(Self);
  fValores := TColecaoValores.Create(Self);
end;

end.
