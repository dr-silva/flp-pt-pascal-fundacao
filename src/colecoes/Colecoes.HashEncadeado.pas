(*******************************************************************************
 *
 * Arquivo  : Colecoes.HashEncadeado.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma classe abstrata e b�sica que servir� de base para cole-
 *            ��es n�o-ordenadas que funcionem atrav�s de uma tabela hash com
 *            resolu��o de colis�es por encadeament
 *
 ******************************************************************************)
unit Colecoes.HashEncadeado;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.Colecoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes,
  FractalLotus.Fundacao.Matematica.GeradorCongruencial;

type
  THashEncadeado<TChave, TValor> = class abstract (TInterfacedObject)
  protected type
    {$region 'Item'}
    PItem = ^TItem;
    TItem = record
      hash    : Integer;
      chave   : TChave;
      valor   : TValor;
      anterior,
      ulterior: PItem;
    end;

    TItemArray = array of PItem;
    {$endregion}

    {$region 'Enumeradores'}
    TEnumerador<T> = class abstract (TEnumerator<T>)
    private
      fColecao: THashEncadeado<TChave, TValor>;
      fAtual  : PItem;
      fTotal  ,
      fIndice : Integer;
    protected
      function DoMoveNext: Boolean; override;

      property colecao: THashEncadeado<TChave, TValor> read fColecao;
      property atual  : PItem                          read fAtual;
      property total  : Integer                        read fTotal;
      property indice : Integer                        read fIndice;
    public
      constructor Create(const colecao: THashEncadeado<TChave, TValor>);
      destructor  Destroy; override;
    end;

    TEnumeradorChaves = class (TEnumerador<TChave>)
    protected
      function DoGetCurrent: TChave; override;
    end;

    TEnumeradorValores = class (TEnumerador<TValor>)
    protected
      function DoGetCurrent: TValor; override;
    end;

    TEnumeradorPares = class (TEnumerador<TPair<TChave, TValor>>)
    protected
      function DoGetCurrent: TPair<TChave, TValor>; override;
    end;
    {$endregion}

    {$region 'Cole��es'}
    TColecaoHash<T> = class abstract (TInterfacedObject, IEnumeravel<T>)
    private
      fColecao: THashEncadeado<TChave, TValor>;

      function obterTotal         : Integer; inline;
      function obterVazio         : Boolean; inline;
      function obterCheio         : Boolean; inline;
      function obterSomenteLeitura: Boolean; inline;
    protected
      property colecao: THashEncadeado<TChave, TValor> read fColecao;
    public
      constructor Create(const colecao: THashEncadeado<TChave, TValor>);
      destructor  Destroy; override;

      function ToString: string; override;
      function ToArray : TArray<T>;

      function GetEnumerator: TEnumerator<T>; virtual; abstract;

      property total         : Integer read obterTotal;
      property vazio         : Boolean read obterVazio;
      property cheio         : Boolean read obterCheio;
      property somenteLeitura: Boolean read obterSomenteLeitura;
    end;

    TColecaoChaves = class (TColecaoHash<TChave>)
    public
      function GetEnumerator: TEnumerator<TChave>; override;
    end;

    TColecaoValores = class (TColecaoHash<TValor>)
    public
      function GetEnumerator: TEnumerator<TValor>; override;
    end;
    {$endregion}
  private
    fTotal       ,
    fFator       ,
    fPrimo       ,
    fParcela     ,
    fCapacidade  : Integer;
    fLgCapacidade: Byte;
    fItens       : TItemArray;
    fComparador  : IComparadorIgualdade<TChave>;
    fAleatorio   : IGeradorCongruencial;

    function obterTotal: Integer; inline;
    function obterVazio: Boolean; inline;
    function obterCheio: Boolean; inline;

    function criarItem(const chave: TChave; const valor: TValor): PItem;
    procedure destruirItem(item: PItem);

    function calcularHash(const chave: TChave): Integer;
  private
    fProjetor: IProjetor<TChave, string>;
  protected
    procedure redimensionar(const dimensao: Byte);

    function buscarItem(const chave: TChave): PItem;

    procedure inserirItem(const chave: TChave; const valor: TValor); overload;
    procedure inserirItem(item: PItem); overload;
    procedure extrairItem(item: PItem);
    procedure excluirItem(item: PItem);
    procedure excluirTodos;

    property fator       : Integer                      read fFator;
    property primo       : Integer                      read fPrimo;
    property parcela     : Integer                      read fParcela;
    property capacidade  : Integer                      read fCapacidade;
    property lgCapacidade: Byte                         read fLgCapacidade;
    property comparador  : IComparadorIgualdade<TChave> read fComparador;
  public
    constructor Create;                                                 overload;
    constructor Create(const igualdade: TFuncaoIgualdade<TChave>);      overload;
    constructor Create(const hash: TFuncaoHash<TChave>);                overload;
    constructor Create(const igualdade: TFuncaoIgualdade<TChave>;
                       const hash: TFuncaoHash<TChave>);                overload;
    constructor Create(const comparador: IComparadorIgualdade<TChave>); overload;

    constructor Create(const capacidade: Integer);                      overload;
    constructor Create(const capacidade: Integer;
                       const igualdade: TFuncaoIgualdade<TChave>);      overload;
    constructor Create(const capacidade: Integer;
                       const hash: TFuncaoHash<TChave>);                overload;
    constructor Create(const capacidade: Integer;
                       const igualdade: TFuncaoIgualdade<TChave>;
                       const hash: TFuncaoHash<TChave>);                overload;
    constructor Create(const capacidade: Integer;
                       const comparador: IComparadorIgualdade<TChave>); overload;

    destructor  Destroy;                                                override;

    property total: Integer read obterTotal;
    property vazio: Boolean read obterVazio;
    property cheio: Boolean read obterCheio;
    property projetor: IProjetor<TChave, string> read fProjetor;
  end;

implementation
uses
  System.Math,
  System.SysUtils,
  Colecoes.Constantes,
  Recursos.Excecoes,
  Extensores.ExtensorPadrao,
  FractalLotus.Fundacao.Matematica.FabricaGeradorCongruencial;

{ THashEncadeado<TChave, TValor> }

function THashEncadeado<TChave, TValor>.buscarItem(const chave: TChave): PItem;
var
  indice: Integer;
begin
  indice := calcularHash(chave);
  Result := fItens[indice];

  while (Result <> nil) do
    if comparador.Equals(chave, Result^.chave) then
      Break
    else
      Result := Result^.ulterior;
end;

function THashEncadeado<TChave, TValor>.calcularHash(
  const chave: TChave): Integer;
var
  valor: Integer;
begin
  valor  := fComparador.GetHashCode(chave) and $7FFFFFFF;
  Result := ((fFator * valor + fParcela) mod fPrimo) mod fCapacidade;

  if (Result < 0) then
    Result := -Result;
end;

constructor THashEncadeado<TChave, TValor>.Create(
  const igualdade: TFuncaoIgualdade<TChave>; const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>(igualdade, hash)
  );
end;

constructor THashEncadeado<TChave, TValor>.Create(
  const comparador: IComparadorIgualdade<TChave>);
begin
  if (comparador = nil) then
    raise ArgumentoNulo('comparador') at ReturnAddress;

  fComparador := comparador;
  fProjetor   := TFabricaProjetor.criarString<TChave>();
  fAleatorio  := TFabricaGeradorCongruencial.criar(TTipoGeradorCongruencial.tgcLinear);

  redimensionar(4);
end;

constructor THashEncadeado<TChave, TValor>.Create;
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>()
  );
end;

constructor THashEncadeado<TChave, TValor>.Create(
  const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>(hash)
  );
end;

constructor THashEncadeado<TChave, TValor>.Create(
  const igualdade: TFuncaoIgualdade<TChave>);
begin
  Create
  (
    TFabricaComparadorIgualdade.criar<TChave>(igualdade)
  );
end;

function THashEncadeado<TChave, TValor>.criarItem(const chave: TChave;
  const valor: TValor): PItem;
begin
  New(Result);

  Result^.hash     := -1;
  Result^.chave    := chave;
  Result^.valor    := valor;
  Result^.anterior := nil;
  Result^.ulterior := nil;
end;

destructor THashEncadeado<TChave, TValor>.Destroy;
begin
  excluirTodos;
  fComparador := nil;
  fProjetor   := nil;

  inherited Destroy;
end;

procedure THashEncadeado<TChave, TValor>.destruirItem(item: PItem);
var
  temp: PItem;
begin
  while (item <> nil) do
  begin
    temp := item;
    item := item^.ulterior;

    temp^.chave := default(TChave);
    temp^.valor := default(TValor);

    FreeMem(temp);
  end;
end;

procedure THashEncadeado<TChave, TValor>.excluirItem(item: PItem);
begin
  if (item = nil) then
    Exit;

  extrairItem (item);
  destruirItem(item);

  if (total < capacidade div 8) then
    redimensionar(lgCapacidade - 1);
end;

procedure THashEncadeado<TChave, TValor>.excluirTodos;
var
  i: Integer;
begin
  if (total = 0) then
    Exit;

  for i := 0 to capacidade - 1 do
  begin
    if (fItens[i] <> nil) then
      destruirItem(fItens[i]);

    fItens[i] := nil;
  end;

  fTotal := 0;
end;

procedure THashEncadeado<TChave, TValor>.extrairItem(item: PItem);
begin
  if (item = nil) then
    Exit;

  if (item^.anterior <> nil) then
    item^.anterior^.ulterior := item^.ulterior;
  if (item^.ulterior <> nil) then
    item^.ulterior^.anterior := item^.anterior;
  if (fItens[item^.hash] = item) then
    fItens[item^.hash] := item^.ulterior;

  item^.anterior := nil;
  item^.ulterior := nil;

  Dec(fTotal);
end;

procedure THashEncadeado<TChave, TValor>.inserirItem(item: PItem);
begin
  if (item = nil) then
    Exit;

  item^.hash     := calcularHash(item^.chave);
  item^.anterior := nil;
  item^.ulterior := fItens[item^.hash];

  if (fItens[item^.hash] <> nil) then
    fItens[item^.hash]^.anterior := item;

  fItens[item^.hash] := item;

  Inc(fTotal);
end;

procedure THashEncadeado<TChave, TValor>.inserirItem(const chave: TChave;
  const valor: TValor);
begin
  if (total > 3 * capacidade) then
    redimensionar(lgCapacidade + 1);

  inserirItem( criarItem(chave, valor) );
end;

function THashEncadeado<TChave, TValor>.obterCheio: Boolean;
begin
  Result := fTotal = Integer.MaxValue;
end;

function THashEncadeado<TChave, TValor>.obterTotal: Integer;
begin
  Result := fTotal;
end;

function THashEncadeado<TChave, TValor>.obterVazio: Boolean;
begin
  Result := fTotal = 0;
end;

procedure THashEncadeado<TChave, TValor>.redimensionar(const dimensao: Byte);
var
  temp, item: PItem;
  antigos: TItemArray;
  capacidadeAntiga, valor, i: Integer;
begin
  if (dimensao < 4) then
    valor := 4
  else if (dimensao > 31) then
    valor := 31
  else
    valor := dimensao;

  if (valor = lgCapacidade) then
    Exit;

  antigos          := fItens;
  capacidadeAntiga := capacidade;
  fItens           := nil;
  fLgCapacidade    := valor;
  fCapacidade      := 1 shl valor; // 2 ^ valor
  fPrimo           := primos[valor + 1];
  fFator           := fAleatorio.uniforme(capacidade - 1) + 1;
  fParcela         := fAleatorio.uniforme(capacidade);

  SetLength(fItens, capacidade);

  if (total = 0) then
    Exit;

  fTotal := 0;
  for i := 0 to capacidadeAntiga - 1 do
  begin
    item := antigos[i];

    while (item <> nil) do
    begin
      temp := item;
      item := item^.ulterior;

      inserirItem(temp);
    end;
  end;

  SetLength(antigos, 0);
end;

constructor THashEncadeado<TChave, TValor>.Create(const capacidade: Integer);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>()
  );
end;

constructor THashEncadeado<TChave, TValor>.Create(const capacidade: Integer;
  const igualdade: TFuncaoIgualdade<TChave>);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>(igualdade)
  );
end;

constructor THashEncadeado<TChave, TValor>.Create(const capacidade: Integer;
  const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>(hash)
  );
end;

constructor THashEncadeado<TChave, TValor>.Create(const capacidade: Integer;
  const igualdade: TFuncaoIgualdade<TChave>; const hash: TFuncaoHash<TChave>);
begin
  Create
  (
    capacidade, TFabricaComparadorIgualdade.criar<TChave>(igualdade, hash)
  );
end;

constructor THashEncadeado<TChave, TValor>.Create(const capacidade: Integer;
  const comparador: IComparadorIgualdade<TChave>);
begin
  if (comparador = nil) then
    raise ArgumentoNulo('comparador') at ReturnAddress;
  if (capacidade <= 0) then
    raise ArgumentoNegativo('capacidade') at ReturnAddress;

  fComparador := comparador;
  fProjetor   := TFabricaProjetor.criarString<TChave>();
  fAleatorio  := TFabricaGeradorCongruencial.criar(TTipoGeradorCongruencial.tgcLinear);

  redimensionar( Ceil(Log2(capacidade / 3)) );
end;

{ THashEncadeado<TChave, TValor>.TEnumerador<T> }

constructor THashEncadeado<TChave, TValor>.TEnumerador<T>.Create(const
  colecao: THashEncadeado<TChave, TValor>);
begin
  fColecao := colecao;
  fAtual   := nil;
  fTotal   := colecao.total;
  fIndice  := -1;
end;

destructor THashEncadeado<TChave, TValor>.TEnumerador<T>.Destroy;
begin
  fColecao := nil;
  fAtual   := nil;

  inherited Destroy;
end;

function THashEncadeado<TChave, TValor>.TEnumerador<T>.DoMoveNext: Boolean;
begin
  Result := (fTotal > 0);

  if Result then
  begin
    repeat
      if (fAtual = nil) then
      begin
        Inc(fIndice);
        fAtual := fColecao.fItens[fIndice];
      end
      else
        fAtual := fAtual^.ulterior;
    until (fAtual <> nil);
    Dec(fTotal);
  end;
end;

{ THashEncadeado<TChave, TValor>.TEnumeradorChaves }

function THashEncadeado<TChave, TValor>.TEnumeradorChaves.DoGetCurrent: TChave;
begin
  Result := atual^.chave;
end;

{ THashEncadeado<TChave, TValor>.TEnumeradorValores }

function THashEncadeado<TChave, TValor>.TEnumeradorValores.DoGetCurrent: TValor;
begin
  Result := atual^.valor;
end;

{ THashEncadeado<TChave, TValor>.TEnumeradorPares }

function THashEncadeado<TChave, TValor>.TEnumeradorPares.DoGetCurrent: TPair<TChave, TValor>;
begin
  Result.Key   := atual^.chave;
  Result.Value := atual^.valor;
end;

{ THashEncadeado<TChave, TValor>.TColecaoHash<T> }

constructor THashEncadeado<TChave, TValor>.TColecaoHash<T>.Create(
  const colecao: THashEncadeado<TChave, TValor>);
begin
  fColecao := colecao;
end;

destructor THashEncadeado<TChave, TValor>.TColecaoHash<T>.Destroy;
begin
  fColecao := nil;

  inherited Destroy;
end;

function THashEncadeado<TChave, TValor>.TColecaoHash<T>.obterCheio: Boolean;
begin
  Result := colecao.cheio;
end;

function THashEncadeado<TChave, TValor>.TColecaoHash<T>.obterSomenteLeitura: Boolean;
begin
  Result := True;
end;

function THashEncadeado<TChave, TValor>.TColecaoHash<T>.obterTotal: Integer;
begin
  Result := colecao.total;
end;

function THashEncadeado<TChave, TValor>.TColecaoHash<T>.obterVazio: Boolean;
begin
  Result := colecao.vazio;
end;

function THashEncadeado<TChave, TValor>.TColecaoHash<T>.ToArray: TArray<T>;
begin
  Result := TExtensorPadrao.toArray<T>(Self);
end;

function THashEncadeado<TChave, TValor>.TColecaoHash<T>.ToString: string;
begin
  Result := TExtensorPadrao.toString<T>(Self);
end;

{ THashEncadeado<TChave, TValor>.TColecaoChaves }

function THashEncadeado<TChave, TValor>.TColecaoChaves.GetEnumerator: TEnumerator<TChave>;
begin
  Result := TEnumeradorChaves.Create(colecao);
end;

{ THashEncadeado<TChave, TValor>.TColecaoValores }

function THashEncadeado<TChave, TValor>.TColecaoValores.GetEnumerator: TEnumerator<TValor>;
begin
  Result := TEnumeradorValores.Create(colecao);
end;

end.
