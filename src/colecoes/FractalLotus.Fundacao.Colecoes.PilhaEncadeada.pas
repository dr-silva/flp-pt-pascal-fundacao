(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.PilhaEncadeada.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o de lista ligada que implementa a interface IEm-
 *            pilh�vel<T>, assim, � uma cole��o que se baseia no princ�pio �lti-
 *            mo a entrar - primeiro a sair (Last In - First Out, LIFO).
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.PilhaEncadeada;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes, Colecoes.Encadeamento;

type
  TPilhaEncadeada<T> = class (TEncadeamento<T>, IEmpilhavel<T>)
  private
    fPrimeiro: TPilhaEncadeada<T>.PItem;
  protected
    function obterSomenteLeitura: Boolean; override;
  public
    constructor Create;
    destructor  Destroy; override;

    function GetEnumerator: TEnumerator<T>; override;

    procedure empilhar(const valor: T);
    function  desempilhar: T;
    function  espiar: T;
    procedure limpar;
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes;

{ TPilhaEncadeada<T> }

constructor TPilhaEncadeada<T>.Create;
begin
  fPrimeiro := nil;
end;

function TPilhaEncadeada<T>.desempilhar: T;
var
  item: PItem;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  item          := fPrimeiro;
  fPrimeiro     := item^.proximo;
  item^.proximo := nil;
  Result        := item^.valor;

  destruirItem(item);
end;

destructor TPilhaEncadeada<T>.Destroy;
begin
  if (fPrimeiro <> nil) then
    destruirItem(fPrimeiro);

  inherited Destroy;
end;

procedure TPilhaEncadeada<T>.empilhar(const valor: T);
var
  item: PItem;
begin
  if cheio then
    raise ColecaoCheia at ReturnAddress;

  item          := criarItem(valor);
  item^.proximo := fPrimeiro;
  fPrimeiro     := item;
end;

function TPilhaEncadeada<T>.espiar: T;
var
  item: PItem;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := fPrimeiro^.valor;
end;

function TPilhaEncadeada<T>.GetEnumerator: TEnumerator<T>;
begin
  Result := TPilhaEncadeada<T>.TEnumerador.Create(fPrimeiro);
end;

procedure TPilhaEncadeada<T>.limpar;
begin
  if (not vazio) then
  begin
    destruirItem(fPrimeiro);
    fPrimeiro := nil;
  end;
end;

function TPilhaEncadeada<T>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

end.
