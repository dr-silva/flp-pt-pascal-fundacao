(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.FilaEncadeada.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o de lista ligada que implementa a interface IEn-
 *            fileir�vel<T>, assim, � uma cole��o que se baseia no princ�pio
 *            primeiro a entrar - �ltimo a sair (First In, First Out - FIFO).
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.FilaEncadeada;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes, Colecoes.Encadeamento;

type
  TFilaEncadeada<T> = class (TEncadeamento<T>, IEnfileiravel<T>)
  private
    fPrimeiro,
    fUltimo  : TFilaEncadeada<T>.PItem;
  protected
    function obterSomenteLeitura: Boolean; override;
  public
    constructor Create;
    destructor  Destroy; override;

    function GetEnumerator: TEnumerator<T>; override;

    procedure enfileirar(const valor: T);
    function  desenfileirar: T;
    function  espiar: T;
    procedure limpar;
  end;

implementation
uses
  System.SysUtils, Recursos.Excecoes;

{ TFilaEncadeada<T> }

constructor TFilaEncadeada<T>.Create;
begin
  fPrimeiro := nil;
  fUltimo   := nil;
end;

function TFilaEncadeada<T>.desenfileirar: T;
var
  item: PItem;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  item          := fPrimeiro;
  fPrimeiro     := item^.proximo;
  Result        := item^.valor;
  item^.proximo := nil;

  destruirItem(item);
end;

destructor TFilaEncadeada<T>.Destroy;
begin
  if (fPrimeiro <> nil) then
    destruirItem(fPrimeiro);

  fPrimeiro := nil;
  fUltimo   := nil;

  inherited Destroy;
end;

procedure TFilaEncadeada<T>.enfileirar(const valor: T);
var
  item: PItem;
begin
  if cheio then
    raise ColecaoCheia at ReturnAddress;

  item := criarItem(valor);

  if (fUltimo <> nil) then
    fUltimo^.proximo := item;

  fUltimo := item;

  if (fPrimeiro = nil) then
    fPrimeiro := item;
end;

function TFilaEncadeada<T>.espiar: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := fPrimeiro^.valor;
end;

function TFilaEncadeada<T>.GetEnumerator: TEnumerator<T>;
begin
  Result := TFilaEncadeada<T>.TEnumerador.Create(fPrimeiro);
end;

procedure TFilaEncadeada<T>.limpar;
begin
  if (fPrimeiro <> nil) then
  begin
    destruirItem(fPrimeiro);
    fPrimeiro := nil;
  end;
end;

function TFilaEncadeada<T>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

end.
