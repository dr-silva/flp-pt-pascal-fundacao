(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.ListaEncadeada.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Dene uma cole��o de �rvore bin�ria que implementa a interface
 *            IList�vel<T>, uma cole��o que acessa seus elementos atrav�s de
 *            �ndices.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.ListaEncadeada;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes,
  Colecoes.ArvoreRubronegra;

type
  TListaEncadeada<T> = class
  (
    TArvoreRubronegra<Integer, T>,
    IEnumeravel<T>,
    IListavel  <T>
  )
  private
    fProximo: Integer;

    function obterPrimeiro      : T;       inline;
    function obterUltimo        : T;       inline;
    function obterTotal         : Integer; inline;
    function obterVazio         : Boolean; inline;
    function obterCheio         : Boolean; inline;
    function obterSomenteLeitura: Boolean; inline;

    function obterItem(const indice: Integer): T;
    procedure definirItem(const indice: Integer; const valor: T); inline;
  public
    constructor Create;
    destructor  Destroy; override;

    function ToString: string; override;
    function ToArray : TArray<T>;

    function GetEnumerator: TEnumerator<T>;

    function adicionar(const valor: T): Integer;                overload;
    function adicionar(const valores: TArray<T>): Integer;      overload;
    function adicionar(const valores: IEnumeravel<T>): Integer; overload;

    procedure inserir(const indice: Integer; const valor: T);                overload;
    procedure inserir(const indice: Integer; const valores: TArray<T>);      overload;
    procedure inserir(const indice: Integer; const valores: IEnumeravel<T>); overload;

    function  contem (const indice: Integer): Boolean;
    function  remover(const indice: Integer): Boolean;
    procedure limpar;

    function obterItens(const inferior, superior: Integer): IEnumeravel<T>;

    property somenteLeitura: Boolean read obterSomenteLeitura;
    property primeiro      : T       read obterPrimeiro;
    property ultimo        : T       read obterUltimo;

    property item[const indice: Integer]: T read obterItem write definirItem; default;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes,
  Extensores.ExtensorPadrao,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

{ TListaEncadeada<T> }

function TListaEncadeada<T>.adicionar(const valores: TArray<T>): Integer;
var
  k, i: Integer;
begin
  Result := -1;

  for k := Low(valores) to High(valores) do
  begin
    i := adicionar(valores[k]);

    if (i < 0) then
      Break;
    if (Result < 0) then
      Result := i;
  end;
end;

function TListaEncadeada<T>.adicionar(const valor: T): Integer;
begin
  Result := -1;

  if not cheio then
  begin
    while contem(fProximo) do
    begin
      Inc(fProximo);
      if (fProximo < 0) then
        fProximo := 0;
    end;

    Result := fProximo;
    inserirNo( criarNo(Result, valor) );
  end;
end;

function TListaEncadeada<T>.adicionar(const valores: IEnumeravel<T>): Integer;
var
  v: T;
  i: Integer;
begin
  Result := -1;

  for v in valores do
  begin
    i := adicionar(v);

    if (i < 0) then
      Break;
    if (Result < 0) then
      Result := i;
  end;
end;

function TListaEncadeada<T>.contem(const indice: Integer): Boolean;
begin
  Result := (indice >= 0) and (buscarNo(indice) <> nil);
end;

constructor TListaEncadeada<T>.Create;
begin
  inherited Create( TFabricaComparador.criar<Integer>() );

  fProximo := 0;
end;

procedure TListaEncadeada<T>.definirItem(const indice: Integer; const valor: T);
begin
  inserir(indice, valor);
end;

destructor TListaEncadeada<T>.Destroy;
begin
  inherited Destroy;
end;

function TListaEncadeada<T>.GetEnumerator: TEnumerator<T>;
var
  primeiro, ultimo: PNo;
begin
  primeiro := buscarNoMinimo(raiz);
  ultimo   := buscarNoMaximo(raiz);
  Result   := TEnumeradorValores.Create(Self, True, primeiro, ultimo);
end;

procedure TListaEncadeada<T>.inserir(const indice: Integer;
  const valores: TArray<T>);
var
  i: Integer;
begin
  for i := indice + Low(valores) to indice + High(valores) do
  begin
    if (i < 0) then
      Break;

    inserir(indice, valores[i - indice]);
  end;
end;

procedure TListaEncadeada<T>.inserir(const indice: Integer;
  const valores: IEnumeravel<T>);
var
  v: T;
  i: Integer;
begin
  i := indice;

  for v in valores do
  begin
    if (i < 0) then
      Break;

    inserir(indice, v);
    Inc(i);
  end;
end;

procedure TListaEncadeada<T>.inserir(const indice: Integer; const valor: T);
var
  no: PNo;
begin
  if (indice < 0) then
    raise IndiceForaLimites('indice', 0, Integer.MaxValue) at ReturnAddress;

  no := buscarNo(indice);
  if (no = nil) then
    inserirNo( criarNo(indice, valor) )
  else
    no^.valor := valor;
end;

procedure TListaEncadeada<T>.limpar;
begin
  if not vazio then
  begin
    destruirNo(raiz);
    raiz := nil;
  end;
  fProximo := 0;
end;

function TListaEncadeada<T>.obterCheio: Boolean;
begin
  Result := inherited cheio;
end;

function TListaEncadeada<T>.obterItem(const indice: Integer): T;
var
  no: PNo;
begin
  if (indice < 0) then
    no := nil
  else
    no := buscarNo(indice);

  if (no <> nil) then
    Result := no^.valor
  else
    raise ChaveDicionario at ReturnAddress;
end;

function TListaEncadeada<T>.obterItens(const inferior,
  superior: Integer): IEnumeravel<T>;
var
  primeiro, ultimo: PNo;
begin
  if (inferior < 0) then
    raise IndiceForaLimites('inferior', 0, Integer.MaxValue) at ReturnAddress;

  if (superior < 0) then
    raise IndiceForaLimites('superior', 0, Integer.MaxValue) at ReturnAddress;

  if (inferior <= superior) then
  begin
    primeiro := buscarNoTeto(inferior);
    ultimo   := buscarNoPiso(superior);
  end
  else
  begin
    primeiro := buscarNoTeto(superior);
    ultimo   := buscarNoPiso(inferior);
  end;

  Result := TColecaoValores.Create(Self, primeiro, ultimo);
end;

function TListaEncadeada<T>.obterPrimeiro: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := buscarNoMinimo(raiz)^.valor;
end;

function TListaEncadeada<T>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TListaEncadeada<T>.obterTotal: Integer;
begin
  Result := inherited total;
end;

function TListaEncadeada<T>.obterUltimo: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := buscarNoMaximo(raiz)^.valor;
end;

function TListaEncadeada<T>.obterVazio: Boolean;
begin
  Result := inherited vazio;
end;

function TListaEncadeada<T>.remover(const indice: Integer): Boolean;
var
  no: PNo;
begin
  if ((indice < 0) or vazio) then
    no := nil
  else
    no := buscarNo(indice);

  Result := (no <> nil);
  if Result then
    excluirNo(no);
end;

function TListaEncadeada<T>.ToArray: TArray<T>;
begin
  Result := TExtensorPadrao.toArray<T>(Self);
end;

function TListaEncadeada<T>.ToString: string;
begin
  Result := TExtensorPadrao.toString<T>(Self);
end;

end.
