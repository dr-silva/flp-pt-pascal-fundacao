(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Colecoes.ListaVetorial.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define uma cole��o vetorial que implementa a nterface IList�vel<T>,
 *            uma cole��o que acessa seus elementos atrav�s de �ndices.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Colecoes.ListaVetorial;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Cerne.Colecoes, Colecoes.VetorizacaoOrdenada;

type
  TListaVetorial<T> = class
  (
    TVetorizacaoOrdenada<Integer, T>,
    IEnumeravel<T>,
    IListavel  <T>
  )
  private
    fProximo: Integer;

    function obterPrimeiro      : T;       inline;
    function obterUltimo        : T;       inline;
    function obterSomenteLeitura: Boolean; inline;

    function  obterItem  (const indice: Integer): T;
    procedure definirItem(const indice: Integer; const valor: T); inline;

    procedure avancar; inline;
  public
    constructor Create(const capacidade: Integer);

    function ToString: string;    override;
    function ToArray : TArray<T>;

    function GetEnumerator: TEnumerator<T>;

    function adicionar(const valor: T): Integer;                overload;
    function adicionar(const valores: TArray<T>): Integer;      overload;
    function adicionar(const valores: IEnumeravel<T>): Integer; overload;

    procedure inserir(const indice: Integer; const valor: T);                overload;
    procedure inserir(const indice: Integer; const valores: TArray<T>);      overload;
    procedure inserir(const indice: Integer; const valores: IEnumeravel<T>); overload;

    function  contem (const indice: Integer): Boolean; inline;
    function  remover(const indice: Integer): Boolean;
    procedure limpar; inline;

    function obterItens(const inferior, superior: Integer): IEnumeravel<T>;

    property primeiro      : T       read obterPrimeiro;
    property ultimo        : T       read obterUltimo;
    property somenteLeitura: Boolean read obterSomenteLeitura;

    property item[const indice: Integer]: T read obterItem write definirItem; default;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes,
  Extensores.ExtensorPadrao,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

{ TListaVetorial<T> }

function TListaVetorial<T>.adicionar(const valor: T): Integer;
begin
  if cheio then
    Result := -1
  else
  begin
    while (contemChave(fProximo)) do
      avancar;

    inserir(fProximo, valor);
    Result := fProximo;
    avancar;
  end;
end;

function TListaVetorial<T>.adicionar(const valores: IEnumeravel<T>): Integer;
var
  v: T;
  i: Integer;
begin
  Result := -1;

  for v in valores do
  begin
    i := adicionar(v);

    if (i < 0) then
      Break;
    if (Result < 0) then
      Result := i;
  end;
end;

procedure TListaVetorial<T>.avancar;
begin
  Inc(fProximo);
  if (fProximo >= capacidade) then
    fProximo := 0;
end;

function TListaVetorial<T>.adicionar(const valores: TArray<T>): Integer;
var
  k, i: Integer;
begin
  Result := -1;

  for k := Low(valores) to High(valores) do
  begin
    i := adicionar(valores[k]);

    if (i < 0) then
      Break;
    if (Result < 0) then
      Result := i;
  end;
end;

function TListaVetorial<T>.contem(const indice: Integer): Boolean;
begin
  Result := contemChave(indice);
end;

constructor TListaVetorial<T>.Create(const capacidade: Integer);
begin
  inherited Create
  (
    capacidade,
    TFabricaComparador.criar<Integer>()
  );

  fProximo := 0;
end;

procedure TListaVetorial<T>.definirItem(const indice: Integer; const valor: T);
begin
  inserir(indice, valor);
end;

function TListaVetorial<T>.GetEnumerator: TEnumerator<T>;
begin
  Result := TEnumeradorValores.Create(Self, 0, total);
end;

procedure TListaVetorial<T>.inserir(const indice: Integer;
  const valores: TArray<T>);
var
  i: Integer;
begin
  for i := indice + Low(valores) to indice + High(valores) do
  begin
    if (i >= capacidade) then
      Break;

    inserir(indice, valores[i - indice]);
  end;
end;

procedure TListaVetorial<T>.inserir(const indice: Integer; const valor: T);
var
  comparacao,
  indiceReal: Integer;
begin
  if (indice < 0) or (indice >= capacidade) then
    raise IndiceForaLimites('indice', 0, capacidade - 1) at ReturnAddress
  else if vazio then
    inserirValor(0, indice, valor)
  else
  begin
    indiceReal := indexar(indice);
    comparacao := comparar(indice, indiceReal);

    if (comparacao < 0) then
      inserirValor(indiceReal, indice, valor)
    else if (comparacao > 0) then
      inserirValor(indiceReal + 1, indice, valor)
    else
      Self.valor[indiceReal] := valor;
  end;
end;

procedure TListaVetorial<T>.inserir(const indice: Integer;
  const valores: IEnumeravel<T>);
var
  v: T;
  i: Integer;
begin
  i := indice;

  for v in valores do
  begin
    if (i >= capacidade) then
      Break;

    inserir(indice, v);
    Inc(i);
  end;
end;

procedure TListaVetorial<T>.limpar;
begin
  excluirTodos;
  fProximo := 0;
end;

function TListaVetorial<T>.obterItem(const indice: Integer): T;
var
  idxReal: Integer;
begin
  if (indice < 0) or (indice >= capacidade) then
    raise ChaveDicionario at ReturnAddress;

  idxReal := indexar(indice);

  if (comparar(indice, idxReal) <> 0) then
    raise ChaveDicionario at ReturnAddress;

  Result := valor[idxReal];
end;

function TListaVetorial<T>.obterItens(const inferior,
  superior: Integer): IEnumeravel<T>;
var
  primeiro, ultimo: Integer;
begin
  if (inferior < 0) or (inferior >= capacidade) then
    raise IndiceForaLimites('inferior', 0, capacidade - 1) at ReturnAddress;
  if (superior < 0) or (superior >= capacidade) then
    raise IndiceForaLimites('superior', 0, capacidade - 1) at ReturnAddress;

  if (inferior <= superior) then
  begin
    primeiro := indexar(inferior);
    ultimo   := indexar(superior) - 1;
  end
  else
  begin
    primeiro := indexar(superior);
    ultimo   := indexar(inferior) - 1;
  end;

  Result := TColecaoValores.Create(Self, primeiro, ultimo);
end;

function TListaVetorial<T>.obterPrimeiro: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := valor[0];
end;

function TListaVetorial<T>.obterSomenteLeitura: Boolean;
begin
  Result := False;
end;

function TListaVetorial<T>.obterUltimo: T;
begin
  if vazio then
    raise ColecaoVazia at ReturnAddress;

  Result := valor[total - 1];
end;

function TListaVetorial<T>.remover(const indice: Integer): Boolean;
var
  idxReal: Integer;
begin
  if (indice < 0) and (indice >= capacidade) then
    Exit(False);

  idxReal := indexar (indice);
  Result  := comparar(indice, idxReal) = 0;

  if Result then
    excluirValor(idxReal);
end;

function TListaVetorial<T>.ToArray: TArray<T>;
begin
  Result := TExtensorPadrao.toArray<T>(Self);
end;

function TListaVetorial<T>.ToString: string;
begin
  Result := TExtensorPadrao.toString<T>(Self);
end;

end.
