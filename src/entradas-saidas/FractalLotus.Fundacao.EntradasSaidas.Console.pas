(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.EntradasSaidas.Console.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-03-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Classe de utilidade para escrever e ler do console.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.EntradasSaidas.Console;

interface
uses
  System.SysUtils;

type
  TConsole = class sealed
  private
    class function lerTodos<T>(const leitor: TFunc<T>): TArray<T>;
    class function ExceptionToString(e: Exception; const separador: string): string;
  public
    constructor Create;

    {$region 'Leitura Padr�o'}
    class function lerBoolean: Boolean; static;

    class function lerInt8 : ShortInt; static; inline;
    class function lerInt16: SmallInt; static; inline;
    class function lerInt32: Integer;  static; inline;
    class function lerInt64: Int64;    static; inline;

    class function lerUInt8 : Byte;     static; inline;
    class function lerUInt16: Word;     static; inline;
    class function lerUInt32: Cardinal; static; inline;
    class function lerUInt64: UInt64;   static; inline;

    class function lerSingle   : Single;   static; inline;
    class function lerDouble   : Double;   static; inline;
    class function lerQuadruple: Extended; static; inline;

    class function lerChar  : Char;   static; inline;
    class function lerString: string; static; inline;
    class function lerLinha : string; static; inline;
    class function lerTudo  : string; static;

    (*********************************************************)

    class function lerTodosBooleans: TArray<Boolean>; static;

    class function lerTodosInt8s : TArray<ShortInt>; static;
    class function lerTodosInt16s: TArray<SmallInt>; static;
    class function lerTodosInt32s: TArray<Integer>;  static;
    class function lerTodosInt64s: TArray<Int64>;    static;

    class function lerTodosUInt8s : TArray<Byte>;     static;
    class function lerTodosUInt16s: TArray<Word>;     static;
    class function lerTodosUInt32s: TArray<Cardinal>; static;
    class function lerTodosUInt64s: TArray<UInt64>;   static;

    class function lerTodosSingles   : TArray<Single>;   static;
    class function lerTodosDoubles   : TArray<Double>;   static;
    class function lerTodosQuadruples: TArray<Extended>; static;

    class function lerTodosChars  : TArray<Char>;   static;
    class function lerTodosStrings: TArray<string>; static;
    class function lerTodasLinhas : TArray<string>; static;
    {$endregion}

    {$region 'Escritura Padr�o'}
    class procedure escrever(const valor: Boolean); overload; static; inline;

    class procedure escrever(const valor: ShortInt); overload; static; inline;
    class procedure escrever(const valor: SmallInt); overload; static; inline;
    class procedure escrever(const valor: Integer);  overload; static; inline;
    class procedure escrever(const valor: Int64);    overload; static; inline;

    class procedure escrever(const valor: Byte);     overload; static; inline;
    class procedure escrever(const valor: Word);     overload; static; inline;
    class procedure escrever(const valor: Cardinal); overload; static; inline;
    class procedure escrever(const valor: UInt64);   overload; static; inline;

    class procedure escrever(const valor: Single);   overload; static; inline;
    class procedure escrever(const valor: Double);   overload; static; inline;
    class procedure escrever(const valor: Extended); overload; static; inline;

    class procedure escrever(const valor: Char);                                      overload; static; inline;
    class procedure escrever(const valor: array of Char; const inicio: Integer);      overload; static;
    class procedure escrever(const valor: array of Char; const inicio, fim: Integer); overload; static;
    class procedure escrever(const valor: string);                                    overload; static; inline;
    class procedure escrever(const formato: string; args: array of const);            overload; static;

    (**************************************************************************)

    class procedure escreverLinha; overload; static; inline;

    class procedure escreverLinha(const valor: Boolean); overload; static; inline;

    class procedure escreverLinha(const valor: ShortInt); overload; static; inline;
    class procedure escreverLinha(const valor: SmallInt); overload; static; inline;
    class procedure escreverLinha(const valor: Integer);  overload; static; inline;
    class procedure escreverLinha(const valor: Int64);    overload; static; inline;

    class procedure escreverLinha(const valor: Byte);     overload; static; inline;
    class procedure escreverLinha(const valor: Word);     overload; static; inline;
    class procedure escreverLinha(const valor: Cardinal); overload; static; inline;
    class procedure escreverLinha(const valor: UInt64);   overload; static; inline;

    class procedure escreverLinha(const valor: Single);   overload; static; inline;
    class procedure escreverLinha(const valor: Double);   overload; static; inline;
    class procedure escreverLinha(const valor: Extended); overload; static; inline;

    class procedure escreverLinha(const valor: Char);                                      overload; static; inline;
    class procedure escreverLinha(const valor: array of Char; const inicio: Integer);      overload; static;
    class procedure escreverLinha(const valor: array of Char; const inicio, fim: Integer); overload; static;
    class procedure escreverLinha(const valor: string);                                    overload; static; inline;
    class procedure escreverLinha(const formato: string; args: array of const);            overload; static;
    {$endregion}

    {$region 'Escritura Erro'}
    class procedure escreverErro(const valor: Boolean); overload; static; inline;

    class procedure escreverErro(const valor: ShortInt); overload; static; inline;
    class procedure escreverErro(const valor: SmallInt); overload; static; inline;
    class procedure escreverErro(const valor: Integer);  overload; static; inline;
    class procedure escreverErro(const valor: Int64);    overload; static; inline;

    class procedure escreverErro(const valor: Byte);     overload; static; inline;
    class procedure escreverErro(const valor: Word);     overload; static; inline;
    class procedure escreverErro(const valor: Cardinal); overload; static; inline;
    class procedure escreverErro(const valor: UInt64);   overload; static; inline;

    class procedure escreverErro(const valor: Single);   overload; static; inline;
    class procedure escreverErro(const valor: Double);   overload; static; inline;
    class procedure escreverErro(const valor: Extended); overload; static; inline;

    class procedure escreverErro(const valor: Char);                                      overload; static; inline;
    class procedure escreverErro(const valor: array of Char; const inicio: Integer);      overload; static;
    class procedure escreverErro(const valor: array of Char; const inicio, fim: Integer); overload; static;
    class procedure escreverErro(const valor: string);                                    overload; static; inline;
    class procedure escreverErro(const formato: string; args: array of const);            overload; static;

    class procedure escreverErro(e: Exception); overload; static; inline;

    (**************************************************************************)

    class procedure escreverErroLinha; overload; static; inline;

    class procedure escreverErroLinha(const valor: Boolean); overload; static; inline;

    class procedure escreverErroLinha(const valor: ShortInt); overload; static; inline;
    class procedure escreverErroLinha(const valor: SmallInt); overload; static; inline;
    class procedure escreverErroLinha(const valor: Integer);  overload; static; inline;
    class procedure escreverErroLinha(const valor: Int64);    overload; static; inline;

    class procedure escreverErroLinha(const valor: Byte);     overload; static; inline;
    class procedure escreverErroLinha(const valor: Word);     overload; static; inline;
    class procedure escreverErroLinha(const valor: Cardinal); overload; static; inline;
    class procedure escreverErroLinha(const valor: UInt64);   overload; static; inline;

    class procedure escreverErroLinha(const valor: Single);   overload; static; inline;
    class procedure escreverErroLinha(const valor: Double);   overload; static; inline;
    class procedure escreverErroLinha(const valor: Extended); overload; static; inline;

    class procedure escreverErroLinha(const valor: Char);                                      overload; static; inline;
    class procedure escreverErroLinha(const valor: array of Char; const inicio: Integer);      overload; static;
    class procedure escreverErroLinha(const valor: array of Char; const inicio, fim: Integer); overload; static;
    class procedure escreverErroLinha(const valor: string);                                    overload; static; inline;
    class procedure escreverErroLinha(const formato: string; args: array of const);            overload; static;

    class procedure escreverErroLinha(e: Exception); overload; static; inline;
    {$endregion}

    {$region 'Sat�lites'}
    class procedure esperar;                      overload; static;
    class procedure esperar(const texto: string); overload; static; inline;

    class procedure esperarPor(const valor: Char);                      overload; static;
    class procedure esperarPor(const valor: Char; const texto: string); overload; static; inline;
    {$endregion}
  end;

implementation
uses
  System.Generics.Collections, Winapi.Windows;

{ TConsole }

class procedure TConsole.escrever(const valor: Extended);
begin
  Write(Output, valor);
end;

class procedure TConsole.escrever(const valor: Char);
begin
  Write(Output, valor);
end;

class procedure TConsole.escrever(const valor: Single);
begin
  Write(Output, valor);
end;

class procedure TConsole.escrever(const valor: Double);
begin
  Write(Output, valor);
end;

class procedure TConsole.escrever(const valor: string);
begin
  Write(Output, valor);
end;

constructor TConsole.Create;
begin
  raise Exception.Create('TConsole n�o pode ser criado.');
end;

class procedure TConsole.escrever(const formato: string; args: array of const);
begin
  escrever(string.Format(formato, args));
end;

class procedure TConsole.escrever(const valor: array of Char; const inicio: Integer);
begin
  escrever(valor, inicio, Length(valor) - 1);
end;

class procedure TConsole.escrever(const valor: array of Char; const inicio,
  fim: Integer);
begin
  escrever(string.Create(valor, inicio, fim - inicio + 1));
end;

class procedure TConsole.escrever(const valor: UInt64);
begin
  Write(Output, valor);
end;

class procedure TConsole.escrever(const valor: SmallInt);
begin
  Write(Output, valor);
end;

class procedure TConsole.escrever(const valor: Integer);
begin
  Write(Output, valor);
end;

class procedure TConsole.escrever(const valor: Boolean);
begin
  escrever(BoolToStr(valor, True));
end;

class procedure TConsole.escrever(const valor: ShortInt);
begin
  Write(Output, valor);
end;

class procedure TConsole.escrever(const valor: Word);
begin
  Write(Output, valor);
end;

class procedure TConsole.escrever(const valor: Cardinal);
begin
  Write(Output, valor);
end;

class procedure TConsole.escrever(const valor: Int64);
begin
  Write(Output, valor);
end;

class procedure TConsole.escrever(const valor: Byte);
begin
  Write(Output, valor);
end;

class procedure TConsole.escreverErro(const valor: Extended);
begin
  Write(ErrOutput, valor);
end;

class procedure TConsole.escreverErro(const valor: Char);
begin
  Write(ErrOutput, valor);
end;

class procedure TConsole.escreverErro(const valor: Single);
begin
  Write(ErrOutput, valor);
end;

class procedure TConsole.escreverErro(const valor: Double);
begin
  Write(ErrOutput, valor);
end;

class procedure TConsole.escreverErro(const formato: string;
  args: array of const);
begin
  escreverErro(string.Format(formato, args));
end;

class procedure TConsole.escreverErro(e: Exception);
begin
  escreverErro(ExceptionToString(e, ' -> '));
end;

class procedure TConsole.escreverErro(const valor: array of Char;
  const inicio: Integer);
begin
  escreverErro(valor, inicio, High(valor));
end;

class procedure TConsole.escreverErro(const valor: array of Char; const inicio,
  fim: Integer);
begin
  escreverErro(string.Create(valor, inicio, fim - inicio + 1));
end;

class procedure TConsole.escreverErro(const valor: string);
begin
  Write(ErrOutput, valor);
end;

class procedure TConsole.escreverErro(const valor: SmallInt);
begin
  Write(ErrOutput, valor);
end;

class procedure TConsole.escreverErro(const valor: Integer);
begin
  Write(ErrOutput, valor);
end;

class procedure TConsole.escreverErro(const valor: Boolean);
begin
  escreverErro(BoolToStr(valor, True));
end;

class procedure TConsole.escreverErro(const valor: ShortInt);
begin
  Write(ErrOutput, valor);
end;

class procedure TConsole.escreverErro(const valor: Int64);
begin
  Write(ErrOutput, valor);
end;

class procedure TConsole.escreverErro(const valor: Cardinal);
begin
  Write(ErrOutput, valor);
end;

class procedure TConsole.escreverErro(const valor: UInt64);
begin
  Write(ErrOutput, valor);
end;

class procedure TConsole.escreverErro(const valor: Byte);
begin
  Write(ErrOutput, valor);
end;

class procedure TConsole.escreverErro(const valor: Word);
begin
  Write(ErrOutput, valor);
end;

class procedure TConsole.escreverErroLinha(const valor: Byte);
begin
  Writeln(ErrOutput, valor);
end;

class procedure TConsole.escreverErroLinha(const valor: Int64);
begin
  Writeln(ErrOutput, valor);
end;

class procedure TConsole.escreverErroLinha(const valor: Cardinal);
begin
  Writeln(ErrOutput, valor);
end;

class procedure TConsole.escreverErroLinha(const valor: Word);
begin
  Writeln(ErrOutput, valor);
end;

class procedure TConsole.escreverErroLinha(const valor: Integer);
begin
  Writeln(ErrOutput, valor);
end;

class procedure TConsole.escreverErroLinha(const valor: Boolean);
begin
  escreverErroLinha(BoolToStr(valor, True));
end;

class procedure TConsole.escreverErroLinha;
begin
  Writeln(ErrOutput);
end;

class procedure TConsole.escreverErroLinha(const valor: SmallInt);
begin
  Writeln(ErrOutput, valor);
end;

class procedure TConsole.escreverErroLinha(const valor: ShortInt);
begin
  Writeln(ErrOutput, valor);
end;

class procedure TConsole.escreverErroLinha(const valor: UInt64);
begin
  Writeln(ErrOutput, valor);
end;

class procedure TConsole.escreverErroLinha(const valor: string);
begin
  Writeln(ErrOutput, valor);
end;

class procedure TConsole.escreverErroLinha(e: Exception);
begin
  escreverErroLinha(ExceptionToString(e, sLineBreak));
end;

class procedure TConsole.escreverErroLinha(const valor: array of Char;
  const inicio: Integer);
begin
  escreverErroLinha(valor, inicio, Length(valor) - 1);
end;

class procedure TConsole.escreverErroLinha(const valor: array of Char;
  const inicio, fim: Integer);
begin
  escreverErroLinha(string.Create(valor, inicio, fim - inicio + 1));
end;

class procedure TConsole.escreverErroLinha(const formato: string;
  args: array of const);
begin
  escreverErroLinha(string.Format(formato, args));
end;

class procedure TConsole.escreverErroLinha(const valor: Double);
begin
  Writeln(ErrOutput, valor);
end;

class procedure TConsole.escreverErroLinha(const valor: Single);
begin
  Writeln(ErrOutput, valor);
end;

class procedure TConsole.escreverErroLinha(const valor: Char);
begin
  Writeln(ErrOutput, valor);
end;

class procedure TConsole.escreverErroLinha(const valor: Extended);
begin
  Writeln(ErrOutput, valor);
end;

class procedure TConsole.escreverLinha(const valor: Word);
begin
  Writeln(Output, valor);
end;

class procedure TConsole.escreverLinha(const valor: Cardinal);
begin
  Writeln(Output, valor);
end;

class procedure TConsole.escreverLinha(const valor: Byte);
begin
  Writeln(Output, valor);
end;

class procedure TConsole.escreverLinha(const valor: Integer);
begin
  Writeln(Output, valor);
end;

class procedure TConsole.escreverLinha(const valor: Int64);
begin
  Writeln(Output, valor);
end;

class procedure TConsole.escreverLinha(const valor: Extended);
begin
  Writeln(Output, valor);
end;

class procedure TConsole.escreverLinha(const valor: Char);
begin
  Writeln(Output, valor);
end;

class procedure TConsole.escreverLinha(const valor: Double);
begin
  Writeln(Output, valor);
end;

class procedure TConsole.escreverLinha(const valor: UInt64);
begin
  Writeln(Output, valor);
end;

class procedure TConsole.escreverLinha(const valor: Single);
begin
  Writeln(Output, valor);
end;

class procedure TConsole.escreverLinha(const valor: array of Char; const inicio,
  fim: Integer);
begin
  escreverLinha(string.Create(valor, inicio, fim - inicio + 1));
end;

class procedure TConsole.escreverLinha(const valor: array of Char;
  const inicio: Integer);
begin
  escreverLinha(valor, inicio, Length(valor) - 1);
end;

class procedure TConsole.escreverLinha(const formato: string;
  args: array of const);
begin
  escreverLinha(string.Format(formato, args));
end;

class procedure TConsole.escreverLinha(const valor: string);
begin
  Writeln(Output, valor);
end;

class procedure TConsole.escreverLinha(const valor: Boolean);
begin
  escreverLinha(BoolToStr(valor, True));
end;

class procedure TConsole.escreverLinha(const valor: ShortInt);
begin
  Writeln(Output, valor);
end;

class procedure TConsole.escreverLinha(const valor: SmallInt);
begin
  Writeln(Output, valor);
end;

class procedure TConsole.escreverLinha;
begin
  Writeln(Output);
end;

class procedure TConsole.esperar(const texto: string);
begin
  escrever(texto);

  esperar;
end;

class procedure TConsole.esperar;
var
  entrada : THandle;
  registro: TInputRecord;
  eventos : Cardinal;
begin
  entrada := GetStdHandle(STD_INPUT_HANDLE);

  while (ReadConsoleInput(entrada, registro, 1, eventos)) do
  begin
    if ((registro.EventType = KEY_EVENT) and
        (TKeyEventRecord(registro.Event).bKeyDown)) then
      Break;

    Sleep(100);
  end;
end;

class procedure TConsole.esperarPor(const valor: Char; const texto: string);
begin
  escrever(texto);

  esperarPor(valor);
end;

class function TConsole.ExceptionToString(e: Exception;
  const separador: string): string;
begin
  Result := string.Empty;

  while (e <> nil) do
  begin
    if (not string.IsNullOrEmpty(Result)) then
      Result := Result + separador;

    Result := Result + e.ClassName + ': ' + e.Message;

    e := e.InnerException;
  end;
end;

class procedure TConsole.esperarPor(const valor: Char);
var
  entrada : THandle;
  registro: TInputRecord;
  eventos : Cardinal;
begin
  entrada := GetStdHandle(STD_INPUT_HANDLE);

  while (ReadConsoleInput(entrada, registro, 1, eventos)) do
  begin
    if ((registro.EventType = KEY_EVENT)           and
        (TKeyEventRecord(registro.Event).bKeyDown) and
        (TKeyEventRecord(registro.Event).UnicodeChar = valor)) then
      Break;

    Sleep(100);
  end;
end;

class function TConsole.lerBoolean: Boolean;
var
  valor : string;
  numero: Integer;
begin
  Read(Input, valor);

  Result := (SameText(valor, DefaultTrueBoolStr, loInvariantLocale)) or
            (TryStrToInt(valor, numero) and (numero <> 0));
end;

class function TConsole.lerChar: Char;
begin
  Read(Input, Result);
end;

class function TConsole.lerDouble: Double;
begin
  Read(Input, Result);
end;

class function TConsole.lerInt16: SmallInt;
begin
  Read(Input, Result);
end;

class function TConsole.lerInt32: Integer;
begin
  Read(Input, Result);
end;

class function TConsole.lerInt64: Int64;
begin
  Read(Input, Result);
end;

class function TConsole.lerInt8: ShortInt;
begin
  Read(Input, Result);
end;

class function TConsole.lerLinha: string;
begin
  Readln(Input, Result);
end;

class function TConsole.lerQuadruple: Extended;
begin
  Read(Input, Result);
end;

class function TConsole.lerSingle: Single;
begin
  Read(Input, Result);
end;

class function TConsole.lerString: string;
begin
  Read(Input, Result);
end;

class function TConsole.lerTodasLinhas: TArray<string>;
var
  linha: string;
begin
  with TQueue<string>.Create do
    try
      while (not Eof(Input)) do
      begin
        Readln(Input, linha);

        Enqueue(linha);
      end;

      Result := ToArray;
    finally
      Free;
    end;
end;

class function TConsole.lerTodos<T>(const leitor: TFunc<T>): TArray<T>;
var
  valor: T;
begin
  with TQueue<T>.Create do
    try
      while (not Eof(Input)) do
        try
          valor := leitor;

          Enqueue(valor);
        except
          // ignorar erro
        end;

      Result := ToArray;
    finally
      Free;
    end;
end;

class function TConsole.lerTodosBooleans: TArray<Boolean>;
begin
  Result := lerTodos<Boolean>(lerBoolean);
end;

class function TConsole.lerTodosChars: TArray<Char>;
begin
  Result := lerTodos<Char>(lerChar);
end;

class function TConsole.lerTodosDoubles: TArray<Double>;
begin
  Result := lerTodos<Double>(lerDouble);
end;

class function TConsole.lerTodosInt16s: TArray<SmallInt>;
begin
  Result := lerTodos<SmallInt>(lerInt16);
end;

class function TConsole.lerTodosInt32s: TArray<Integer>;
begin
  Result := lerTodos<Integer>(lerInt32);
end;

class function TConsole.lerTodosInt64s: TArray<Int64>;
begin
  Result := lerTodos<Int64>(lerInt64);
end;

class function TConsole.lerTodosInt8s: TArray<ShortInt>;
begin
  Result := lerTodos<ShortInt>(lerInt8);
end;

class function TConsole.lerTodosQuadruples: TArray<Extended>;
begin
  Result := lerTodos<Extended>(lerQuadruple);
end;

class function TConsole.lerTodosSingles: TArray<Single>;
begin
  Result := lerTodos<Single>(lerSingle);
end;

class function TConsole.lerTodosStrings: TArray<string>;
begin
  Result := lerTodos<string>(lerString);
end;

class function TConsole.lerTodosUInt16s: TArray<Word>;
begin
  Result := lerTodos<Word>(lerUInt16);
end;

class function TConsole.lerTodosUInt32s: TArray<Cardinal>;
begin
  Result := lerTodos<Cardinal>(lerUInt32);
end;

class function TConsole.lerTodosUInt64s: TArray<UInt64>;
begin
  Result := lerTodos<UInt64>(lerUInt64);
end;

class function TConsole.lerTodosUInt8s: TArray<Byte>;
begin
  Result := lerTodos<Byte>(lerUInt8);
end;

class function TConsole.lerTudo: string;
begin
  with TStringBuilder.Create do
    try
      while (not Eof(Input)) do
      begin
        Readln(Input, Result);

        Append(Result);
      end;

      Result := ToString;
    finally
      Free;
    end;
end;

class function TConsole.lerUInt16: Word;
begin
  Read(Input, Result);
end;

class function TConsole.lerUInt32: Cardinal;
begin
  Read(Input, Result);
end;

class function TConsole.lerUInt64: UInt64;
begin
  Read(Input, Result);
end;

class function TConsole.lerUInt8: Byte;
begin
  Read(Input, Result);
end;

end.
