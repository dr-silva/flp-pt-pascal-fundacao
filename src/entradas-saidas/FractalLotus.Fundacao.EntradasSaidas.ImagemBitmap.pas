(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.EntradasSaidas.ImagemBitmap.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-06 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Classe para gera��o de imagens matriciais.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.EntradasSaidas.ImagemBitmap;

interface
uses
  System.Classes, System.UITypes, FMX.Graphics;

type
  TAlphaColor = System.UITypes.TAlphaColor;

  TImagemBitmap = class
  strict private
    {$region 'Atributos'}
    fDados   : TBitmapData;
    fBitmap  : TBitmap;
    fMapeado ,
    fSuperior: Boolean;
    {$endregion}

    {$region 'Assistentes'}
    function lerAltura : Integer;
    function lerLargura: Integer;
    function lerPixel(const linha, coluna: Integer): TAlphaColor;
    procedure escreverPixel(const linha, coluna: Integer; const cor: TAlphaColor);
    procedure escreverBitmap(const bitmap: TBitmap);
    procedure salvarImagem(const stream: TStream; const str: string);
    procedure assignTo(destino : TPersistent);
    procedure rotar(const esquerda: Boolean);

    procedure mapear;    inline;
    procedure desmapear; inline;

    function linhaPorOrigem(const linha: Integer): Integer; inline;
    {$endregion}
  public
    {$region 'Construtores & Destruidores'}
    constructor Create(const altura, largura: Integer); overload;
    constructor Create(const arquivo: string);          overload;
    constructor Create(const stream: TStream);          overload;
    destructor  Destroy;                                override;
    {$endregion}

    {$region 'Interface'}
    procedure definir(const linha, coluna: Integer; const    r, g, b: Single); overload; inline;
    procedure definir(const linha, coluna: Integer; const a, r, g, b: Single); overload;

    procedure originarDoSuperiorEsquerdo; inline;
    procedure originarDoInferiorEsquerdo; inline;

    procedure negativar;

    procedure espelharHorizontalmente;
    procedure espelharVerticalmente;

    procedure rotarParaEsquerda; inline;
    procedure rotarParaDireita;  inline;

    function subimagem(const linhaInicial, colunaInicial, linhaFinal, colunaFinal: Integer): TImagemBitmap;

    procedure salvar(const arquivo: string);                  overload;
    procedure salvar(stream: TStream; const formato: string); overload;
    {$endregion}

    {$region 'Propriedades'}
    property bitmap : TBitmap read fBitmap;
    property altura : Integer read lerAltura;
    property largura: Integer read lerLargura;

    property pixel[const linha, coluna: Integer]: TAlphaColor read lerPixel write escreverPixel; default;
    {$endregion}
  end;

implementation
uses
  System.SysUtils, System.Math, FMX.Types, FMX.Utils, FMX.Surfaces;

{ TImagemBitmap }

constructor TImagemBitmap.Create(const altura, largura: Integer);
begin
  fBitmap   := nil;
  fSuperior := True;

  escreverBitmap(TBitmap.Create(largura, altura));
end;

procedure TImagemBitmap.assignTo(destino: TPersistent);
var
  linha: Integer;
begin
  if ((destino is TBitmapSurface) and fMapeado) then
  begin
    TBitmapSurface(destino).SetSize(largura, altura, fBitmap.PixelFormat);

    for linha := 0 to altura - 1 do
      System.Move
      (
        fDados.GetScanline(linha)^,
        TBitmapSurface(destino).Scanline[linha]^,
        fDados.BytesPerLine
      );
  end;
end;

constructor TImagemBitmap.Create(const stream: TStream);
begin
  fBitmap   := nil;
  fSuperior := True;

  escreverBitmap(TBitmap.CreateFromStream(stream));
end;

procedure TImagemBitmap.definir(const linha, coluna: Integer; const r, g,
  b: Single);
begin
  definir(linha, coluna, 1, r, g, b);
end;

procedure TImagemBitmap.definir(const linha, coluna: Integer; const a, r, g,
  b: Single);
var
  cor : TAlphaColor;
  pCor: PAlphaColorRec;
begin
  if ((a < 0) or (a > 1)) then
    raise EArgumentOutOfRangeException.Create('Error Message');
  if ((r < 0) or (r > 1)) then
    raise EArgumentOutOfRangeException.Create('Error Message');
  if ((g < 0) or (g > 1)) then
    raise EArgumentOutOfRangeException.Create('Error Message');
  if ((b < 0) or (b > 1)) then
    raise EArgumentOutOfRangeException.Create('Error Message');

  pCor := @cor;

  pCor^.A := Byte(Floor(Min(255, a * 256)));
  pCor^.R := Byte(Floor(Min(255, r * 256)));
  pCor^.G := Byte(Floor(Min(255, g * 256)));
  pCor^.B := Byte(Floor(Min(255, b * 256)));

  pixel[linha, coluna] := cor;
end;

procedure TImagemBitmap.desmapear;
begin
  if fMapeado then
    fBitmap.Unmap(fDados);

  fMapeado := False;
end;

constructor TImagemBitmap.Create(const arquivo: string);
begin
  fBitmap   := nil;
  fSuperior := True;

  escreverBitmap(TBitmap.CreateFromFile(arquivo));
end;

destructor TImagemBitmap.Destroy;
begin
  desmapear;
  fBitmap.Free;
  inherited Destroy;
end;

procedure TImagemBitmap.escreverBitmap(const bitmap: TBitmap);
var
  temp: TBitmap;
begin
  if (bitmap = nil) then
    Exit;

  desmapear;
  temp    := fBitmap;
  fBitmap := bitmap;

  if (temp <> nil) then
    temp.Free;

  mapear;
end;

procedure TImagemBitmap.escreverPixel(const linha, coluna: Integer;
  const cor: TAlphaColor);
begin
  if ((linha < 0) or (linha >= fBitmap.Height)) then
    raise EArgumentOutOfRangeException.Create('Error Message');
  if ((coluna < 0) or (coluna >= fBitmap.Width)) then
    raise EArgumentOutOfRangeException.Create('Error Message');

  if fMapeado then
    fDados.SetPixel(coluna, linhaPorOrigem(linha), cor);
end;

procedure TImagemBitmap.espelharHorizontalmente;
var
  temp  : TAlphaColor;
  linha ,
  coluna: Integer;
begin
  if fMapeado then
    for linha := 0 to altura - 1 do
      for coluna := 0 to (largura - 1) div 2 do
      begin
        temp := PAlphaColorArray(fDados.Data)[linha * (fDados.Pitch div 4) + largura - 1 - coluna];
        PAlphaColorArray(fDados.Data)[linha * (fDados.Pitch div 4) + largura - 1 - coluna] := PAlphaColorArray(fDados.Data)[linha * (fDados.Pitch div 4) + coluna];
        PAlphaColorArray(fDados.Data)[linha * (fDados.Pitch div 4) + coluna] := temp;
      end;
end;

procedure TImagemBitmap.espelharVerticalmente;
var
  temp : PAlphaColorArray;
  linha: Integer;
begin
  if fMapeado then
  begin
    GetMem(temp, largura * 4);
    try
      for linha := 0 to (altura - 1) div 2 do
      begin
        System.Move(PAlphaColorArray(fDados.Data)[(altura - 1 - linha) * (fDados.Pitch div 4)], temp[0], fDados.Pitch);
        System.Move(PAlphaColorArray(fDados.Data)[linha * (fDados.Pitch div 4)], PAlphaColorArray(fDados.Data)[(altura - 1 - linha) * (fDados.Pitch div 4)], fDados.Pitch);
        System.Move(temp[0], PAlphaColorArray(fDados.Data)[linha * (fDados.Pitch div 4)], fDados.Pitch);
      end;
    finally
      FreeMem(temp, largura * 4);
    end;
  end;
end;

function TImagemBitmap.lerAltura: Integer;
begin
  Result := fBitmap.Height;
end;

function TImagemBitmap.lerLargura: Integer;
begin
  Result := fBitmap.Width;
end;

function TImagemBitmap.lerPixel(const linha, coluna: Integer): TAlphaColor;
begin
  if ((linha < 0) or (linha >= fBitmap.Height)) then
    raise EArgumentOutOfRangeException.Create('Error Message');
  if ((coluna < 0) or (coluna >= fBitmap.Width)) then
    raise EArgumentOutOfRangeException.Create('Error Message');

  if fMapeado then
    Result := fDados.GetPixel(coluna, linhaPorOrigem(linha))
  else
    Result := 0;
end;

function TImagemBitmap.linhaPorOrigem(const linha: Integer): Integer;
begin
  if (fSuperior) then
    Result := linha
  else
    Result := altura - linha - 1;
end;

procedure TImagemBitmap.mapear;
begin
  fMapeado := fBitmap.Map(TMapAccess.ReadWrite, fDados);
end;

procedure TImagemBitmap.negativar;
var
  linha ,
  coluna: Integer;
begin
  if fMapeado then
    for linha := 0 to altura - 1 do
      for coluna := 0 to largura - 1 do
        with TAlphaColorRec(fDados.GetPixelAddr(coluna, linha)^) do
        begin
          R := 255 - R;
          G := 255 - G;
          B := 255 - B;
        end;
end;

procedure TImagemBitmap.originarDoInferiorEsquerdo;
begin
  fSuperior := False;
end;

procedure TImagemBitmap.originarDoSuperiorEsquerdo;
begin
  fSuperior := True;
end;

procedure TImagemBitmap.rotar(const esquerda: Boolean);
var
  dados : TBitmapData;
  temp  ,
  bytes ,
  linha ,
  coluna: Integer;
  bitmap: TBitmap;
begin
  if not fMapeado then
    Exit;

  bytes  := fBitmap.BytesPerPixel;
  bitmap := TBitmap.Create(altura, largura);

  if bitmap.Map(TMapAccess.Write, dados) then
    try
      for linha := 0 to altura - 1 do
      begin
        if esquerda then
          temp := linha
        else
          temp := altura - linha - 1;

        for coluna := 0 to largura - 1 do
          System.Move
          (
            fDados.GetPixelAddr(coluna, linha)^,
            dados .GetPixelAddr(temp, coluna)^,
            bytes
          );
      end;
    finally
      bitmap.Unmap(dados);

      escreverBitmap(bitmap);
    end;
end;

procedure TImagemBitmap.rotarParaDireita;
begin
  rotar(False);
end;

procedure TImagemBitmap.rotarParaEsquerda;
begin
  rotar(True);
end;

procedure TImagemBitmap.salvar(stream: TStream; const formato: string);
begin
  salvarImagem(stream, formato);
end;

procedure TImagemBitmap.salvarImagem(const stream: TStream; const str: string);
var
  salvo  : Boolean;
  surface: TBitmapSurface;
begin
  surface := TBitmapSurface.Create;
  try
    if fMapeado then
    begin
      assignTo(surface);

      if (stream <> nil) then
        salvo := TBitmapCodecManager.SaveToStream(stream, surface, str)
      else
        salvo := TBitmapCodecManager.SaveToFile(str, surface);

      if not salvo then
        raise EBitmapSavingFailed.Create('Saving bitmap failed.');
    end;
  finally
    surface.Free;
  end;
end;

procedure TImagemBitmap.salvar(const arquivo: string);
begin
  salvarImagem(nil, arquivo);
end;

function TImagemBitmap.subimagem(const linhaInicial, colunaInicial, linhaFinal,
  colunaFinal: Integer): TImagemBitmap;
var
  y,
  bytes     ,
  linha     ,
  coluna    ,
  altura    ,
  largura   : Integer;
begin
  if ((linhaInicial < 0) or (linhaInicial >= Self.altura)) then
    raise EArgumentOutOfRangeException.Create('Error Message');
  if ((colunaInicial < 0) or (colunaInicial >= Self.altura)) then
    raise EArgumentOutOfRangeException.Create('Error Message');
  if ((linhaFinal < 0) or (linhaFinal >= Self.largura)) then
    raise EArgumentOutOfRangeException.Create('Error Message');
  if ((colunaFinal < 0) or (colunaFinal >= Self.largura)) then
    raise EArgumentOutOfRangeException.Create('Error Message');

  bytes      := fBitmap.BytesPerPixel;
  linha      := Min(linhaPorOrigem(linhaInicial), linhaPorOrigem(linhaFinal));
  coluna     := Min(colunaInicial, colunaFinal);
  altura     := Abs(linhaFinal - linhaInicial)   + 1;
  largura    := Abs(colunaFinal - colunaInicial) + 1;
  Result     := TImagemBitmap.Create(altura, largura);

  if (Self.fMapeado and Result.fMapeado) then
    for y := 0 to altura - 1 do
      System.Move
      (
        Self  .fDados.GetPixelAddr(coluna, y + linha)^,
        Result.fDados.GetPixelAddr(0, y)^,
        largura * bytes
      );
end;

end.
