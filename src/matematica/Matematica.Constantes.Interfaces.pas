(*******************************************************************************
 *
 * Arquivo  : Matematica.Constantes.Interfaces.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-26 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Declara uma inst�ncia simples de um comparador (de igualdade ou
 *            n�o) instanciado pela classe TMatem�tica.
 *
 ******************************************************************************)
unit Matematica.Constantes.Interfaces;

interface

type
  PInstanciaSimples = ^TInstanciaSimples;
  TInstanciaSimples = record
    tabelaInfo: Pointer;
    refCount  : Integer;
    epsilon   : Extended;
  end;

  {$region 'Interface'}
  function SemQueryInterface(instancia: Pointer; const IID: TGUID; out Obj): HResult; stdcall;
  function MemAddref        (instancia: Pointer): Integer; stdcall;
  function MemRelease       (instancia: Pointer): Integer; stdcall;

  function instanciar(tabelaInfo: Pointer; const epsilon: Extended): Pointer;
  {$endregion}

implementation

{$region 'Interface'}
function SemQueryInterface(instancia: Pointer; const IID: TGUID; out Obj): HResult; stdcall;
begin
  Result := E_NOINTERFACE;
end;

function MemAddref(instancia: Pointer): Integer; stdcall;
begin
  Result := AtomicIncrement(PInstanciaSimples(instancia)^.refCount);
end;

function MemRelease(instancia: Pointer): Integer; stdcall;
begin
  Result := AtomicDecrement(PInstanciaSimples(instancia)^.refCount);

  if (Result = 0) then
    FreeMem(instancia);
end;

function instanciar(tabelaInfo: Pointer; const epsilon: Extended): Pointer;
var
  instancia: PInstanciaSimples;
begin
  GetMem(instancia, SizeOf(instancia^));

  instancia^.tabelaInfo := tabelaInfo;
  instancia^.refCount   := 0;
  instancia^.epsilon    := epsilon;

  Result := instancia;
end;
{$endregion}

end.
