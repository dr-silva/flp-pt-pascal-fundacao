(*******************************************************************************
 *
 * Arquivo  : Matematica.Geradores.GCLBorlandPascal.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-28 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o gerador congruencial linear com as especifica��es do
 *            Borland Pascal.
 *
 ******************************************************************************)
unit Matematica.Geradores.GCLBorlandPascal;

interface
uses
  FractalLotus.Fundacao.Matematica.GeradorCongruencial;

type
  TGCLBorlandPascal = class (TGeradorCongruencial)
  private
    fValor: Cardinal;

    function proximo: Cardinal; inline;
  public
    constructor Create(const semente: Cardinal); override;

    function uniforme()                     : Double;  overload; override;
    function uniforme(const limite: Integer): Integer; overload; override;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes;

{ TGCLBorlandPascal }

constructor TGCLBorlandPascal.Create(const semente: Cardinal);
begin
  inherited Create(semente);

  fValor := semente;
end;

function TGCLBorlandPascal.proximo: Cardinal;
begin
  fValor := fValor * $08088405 + 1;
  Result := fValor;
end;

function TGCLBorlandPascal.uniforme(const limite: Integer): Integer;
var
  temp: Cardinal;
begin
  if (limite <= 0) then
    raise ArgumentoNegativo('limite') at ReturnAddress;

  temp   := proximo;
  Result := (UInt64(UInt32(limite)) * UInt64(temp)) shr 32;
end;

function TGCLBorlandPascal.uniforme: Double;
var
  temp: Cardinal;
begin
  temp   := proximo;
  Result := Int64(temp) * (1.0 / $10000 / $10000); // 2^-32
end;

end.
