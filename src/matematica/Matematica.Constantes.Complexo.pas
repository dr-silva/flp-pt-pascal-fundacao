(*******************************************************************************
 *
 * Arquivo  : Matematica.Constantes.Complexo.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-09-13 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Guarda as inst�ncias dos protocolos de fun��es usados pelo tipo
 *            de dado Complexo.
 *
 ******************************************************************************)
unit Matematica.Constantes.Complexo;

interface
uses
  System.TypInfo,
  Cerne.Constantes.Interfaces,
  Matematica.Constantes.Interfaces,
  FractalLotus.Fundacao.Matematica.Complexo;

{$region 'Procuradores'}
function IgualarComplexo             (Inst: Pointer; const Left, Right: TComplexo): Boolean;
function ObterHashCodeComplexo       (Inst: Pointer; const Value      : TComplexo): Integer;
function ReduzirAdicaoComplexo       (Inst: Pointer; const valor, item: TComplexo): TComplexo;
function ReduzirMultiplicacaoComplexo(Inst: Pointer; const valor, item: TComplexo): TComplexo;
function ProjetarStringComplexo      (Inst: Pointer; const valor      : TComplexo): string;
{$endregion}

{$region 'Instancia��o'}
function InstanciarComparadorIgualdadeComplexo(const epsilon: Extended): Pointer;
function InstanciarRedutorAdicaoComplexo       : Pointer;
function InstanciarRedutorMultiplicacaoComplexo: Pointer;
function InstanciarProjetorStringComplexo      : Pointer;
{$endregion}

implementation

const
  {$region 'Tabelas Info'}
  TabelaInfoIgualdadeComplexo: array [0..4] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @IgualarComplexo,
    @ObterHashCodeComplexo
  );
  TabelaInfoReducaoAdicaoComplexo: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReduzirAdicaoComplexo
  );
  TabelaInfoReducaoMultiplicacaoComplexo: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReduzirMultiplicacaoComplexo
  );
  TabelaInfoProjecaoStringComplexo: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjetarStringComplexo
  );
  {$endregion}

  {$region 'Inst�ncias'}
  InstanciaReducaoAdicaoComplexo       : Pointer = @TabelaInfoReducaoAdicaoComplexo;
  InstanciaReducaoMultiplicacaoComplexo: Pointer = @TabelaInfoReducaoMultiplicacaoComplexo;
  InstanciaProjecaoStringComplexo      : Pointer = @TabelaInfoProjecaoStringComplexo;
  {$endregion}

{$region 'Procuradores'}
function IgualarComplexo(Inst: Pointer; const Left, Right: TComplexo): Boolean;
begin
  Result := TComplexo.dist(Left, Right) < PInstanciaSimples(Inst)^.epsilon;
end;

function ObterHashCodeComplexo(Inst: Pointer; const Value: TComplexo): Integer;
begin
  Result := Value.hashCode;
end;

function ReduzirAdicaoComplexo(Inst: Pointer; const valor, item: TComplexo): TComplexo;
begin
  Result := valor + item;
end;

function ReduzirMultiplicacaoComplexo(Inst: Pointer; const valor, item: TComplexo): TComplexo;
begin
  Result := valor * item;
end;

function ProjetarStringComplexo(Inst: Pointer; const valor: TComplexo): string;
begin
  Result := valor.toString;
end;
{$endregion}

{$region 'Instancia��o'}
function InstanciarComparadorIgualdadeComplexo(const epsilon: Extended): Pointer;
begin
  Result := instanciar(@TabelaInfoIgualdadeComplexo, epsilon);
end;

function InstanciarRedutorAdicaoComplexo: Pointer;
begin
  Result := @InstanciaReducaoAdicaoComplexo;
end;

function InstanciarRedutorMultiplicacaoComplexo: Pointer;
begin
  Result := @InstanciaReducaoMultiplicacaoComplexo;
end;

function InstanciarProjetorStringComplexo: Pointer;
begin
  Result := @InstanciaProjecaoStringComplexo;
end;
{$endregion}

end.
