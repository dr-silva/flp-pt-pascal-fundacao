(*******************************************************************************
 *
 * Arquivo  : Matematica.Geradores.GCLBorlandC.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-28 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o gerador congruencial linear com as especifica��es do
 *            Borland C.
 *
 ******************************************************************************)
unit Matematica.Geradores.GCLBorlandC;

interface
uses
  FractalLotus.Fundacao.Matematica.GeradorCongruencial;

type
  TGCLBorlandC = class (TGeradorCongruencial)
  private
    fValor: Cardinal;

    function proximo: Cardinal; inline;
  public
    constructor Create(const semente: Cardinal); override;

    function uniforme()                     : Double;  overload; override;
    function uniforme(const limite: Integer): Integer; overload; override;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes;

{ TGCLBorlandC }

constructor TGCLBorlandC.Create(const semente: Cardinal);
begin
  inherited Create(semente);

  fValor := semente;
end;

function TGCLBorlandC.proximo: Cardinal;
begin
  fValor := (fValor * $015A4E35 + 1);
  Result := (fValor and $7FFFFFFF);
end;

function TGCLBorlandC.uniforme: Double;
begin
  Result := proximo / $80000000;
end;

function TGCLBorlandC.uniforme(const limite: Integer): Integer;
begin
  if (limite <= 0) then
    raise ArgumentoNegativo('limite') at ReturnAddress;

  Result := proximo mod UInt32(limite);
end;

end.
