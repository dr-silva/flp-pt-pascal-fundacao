(*******************************************************************************
 *
 * Arquivo  : Matematica.Geradores.GCLOpenVms.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-26 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o gerador congruencial linear com as especifica��es do
 *            Open VMS, um sistema operacional.
 *
 ******************************************************************************)
unit Matematica.Geradores.GCLOpenVms;

interface
uses
  FractalLotus.Fundacao.Matematica.GeradorCongruencial;

type
  TGCLOpenVms = class (TGeradorCongruencial)
  private
    fValor: Cardinal;

    function proximo: Cardinal; inline;
  public
    constructor Create(const semente: Cardinal); override;

    function uniforme()                     : Double;  overload; override;
    function uniforme(const limite: Integer): Integer; overload; override;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes;

{ TGCLOpenVms }

constructor TGCLOpenVms.Create(const semente: Cardinal);
begin
  inherited Create(semente);

  fValor := semente;
end;

function TGCLOpenVms.proximo: Cardinal;
begin
  fValor := fValor * $10DCD + 1;
  Result := fValor;
end;

function TGCLOpenVms.uniforme(const limite: Integer): Integer;
begin
  if (limite <= 0) then
    raise ArgumentoNegativo('limite') at ReturnAddress;

  Result := proximo mod UInt32(limite);
end;

function TGCLOpenVms.uniforme: Double;
begin
  Result := proximo * (1.0 / $10000 / $10000); // 2^-32
end;

end.
