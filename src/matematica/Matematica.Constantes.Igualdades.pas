unit Matematica.Constantes.Igualdades;

interface
uses
  System.TypInfo,
  Matematica.Constantes.Interfaces;

{$region 'Procuradores'}
function IgualarComp    (Inst: Pointer; const Left, Right: Comp    ): Boolean;
function IgualarSingle  (Inst: Pointer; const Left, Right: Single  ): Boolean;
function IgualarDouble  (Inst: Pointer; const Left, Right: Double  ): Boolean;
function IgualarExtended(Inst: Pointer; const Left, Right: Extended): Boolean;
function IgualarCurrency(Inst: Pointer; const Left, Right: Currency): Boolean;

function ObterHashCodeComp    (Inst: Pointer; const Value: Comp    ): Integer;
function ObterHashCodeSingle  (Inst: Pointer; const Value: Single  ): Integer;
function ObterHashCodeDouble  (Inst: Pointer; const Value: Double  ): Integer;
function ObterHashCodeExtended(Inst: Pointer; const Value: Extended): Integer;
function ObterHashCodeCurrency(Inst: Pointer; const Value: Currency): Integer;
{$endregion}

{$region 'Instanciação'}
function InstanciarIgualdade(tipo: TFloatType; epsilon: Extended): Pointer; inline;
function SelecionarIgualdade(info: PTypeInfo;  epsilon: Extended): Pointer; inline;
{$endregion}

const
  {$region 'Tabelas Info'}
  TabelaInfoIgualdadeComp: array [0..4] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @IgualarComp,
    @ObterHashCodeComp
  );

  TabelaInfoIgualdadeSingle: array [0..4] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @IgualarSingle,
    @ObterHashCodeSingle
  );

  TabelaInfoIgualdadeDouble: array [0..4] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @IgualarDouble,
    @ObterHashCodeDouble
  );

  TabelaInfoIgualdadeExtended: array [0..4] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @IgualarExtended,
    @ObterHashCodeExtended
  );

  TabelaInfoIgualdadeCurrency: array [0..4] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @IgualarCurrency,
    @ObterHashCodeCurrency
  );
  {$endregion}

implementation
uses
  System.Math, System.Hash;

const
{$IF (SizeOf(Extended) > 10) and (Defined(CPUX86) or Defined(CPUX64))}
  // Intel FPU case. GetHashValue needs actual Extended value bytes.
  BYTESIZEOFEXTENDED = 10;
{$ELSE}
  BYTESIZEOFEXTENDED = SizeOf(Extended);
{$ENDIF}

{$region 'Procuradores'}
function IgualarComp(Inst: Pointer; const Left, Right: Comp): Boolean;
begin
  Result := Abs(Right - Left) < PInstanciaSimples(Inst)^.epsilon;
end;

function IgualarSingle(Inst: Pointer; const Left, Right: Single): Boolean;
begin
  Result := Abs(Right - Left) < PInstanciaSimples(Inst)^.epsilon;
end;

function IgualarDouble(Inst: Pointer; const Left, Right: Double): Boolean;
begin
  Result := Abs(Right - Left) < PInstanciaSimples(Inst)^.epsilon;
end;

function IgualarExtended(Inst: Pointer; const Left, Right: Extended): Boolean;
begin
  Result := Abs(Right - Left) < PInstanciaSimples(Inst)^.epsilon;
end;

function IgualarCurrency(Inst: Pointer; const Left, Right: Currency): Boolean;
begin
  Result := Abs(Right - Left) < PInstanciaSimples(Inst)^.epsilon;
end;

function ObterHashCodeComp(Inst: Pointer; const Value: Comp): Integer;
var
  m: Extended;
  e: Integer;
begin
  Frexp(Value, m, e);
  if m = 0 then
    m := Abs(m);
  Result := THashBobJenkins.GetHashValue(m, BYTESIZEOFEXTENDED, 0);
  Result := THashBobJenkins.GetHashValue(e, SizeOf(e), Result);
end;

function ObterHashCodeSingle(Inst: Pointer; const Value: Single): Integer;
var
  m: Extended;
  e: Integer;
begin
  Frexp(Value, m, e);
  if m = 0 then
    m := Abs(m);
  Result := THashBobJenkins.GetHashValue(m, BYTESIZEOFEXTENDED, 0);
  Result := THashBobJenkins.GetHashValue(e, SizeOf(e), Result);
end;

function ObterHashCodeDouble(Inst: Pointer; const Value: Double): Integer;
var
  m: Extended;
  e: Integer;
begin
  Frexp(Value, m, e);
  if m = 0 then
    m := Abs(m);
  Result := THashBobJenkins.GetHashValue(m, BYTESIZEOFEXTENDED, 0);
  Result := THashBobJenkins.GetHashValue(e, SizeOf(e), Result);
end;

function ObterHashCodeExtended(Inst: Pointer; const Value: Extended): Integer;
var
  m: Extended;
  e: Integer;
begin
  Frexp(Value, m, e);
  if m = 0 then
    m := Abs(m);
  Result := THashBobJenkins.GetHashValue(m, BYTESIZEOFEXTENDED, 0);
  Result := THashBobJenkins.GetHashValue(e, SizeOf(e), Result);
end;

function ObterHashCodeCurrency(Inst: Pointer; const Value: Currency): Integer;
var
  m: Extended;
  e: Integer;
begin
  Frexp(Value, m, e);
  if m = 0 then
    m := Abs(m);
  Result := THashBobJenkins.GetHashValue(m, BYTESIZEOFEXTENDED, 0);
  Result := THashBobJenkins.GetHashValue(e, SizeOf(e), Result);
end;
{$endregion}

{$region 'Instanciação'}
function InstanciarIgualdade(tipo: TFloatType; epsilon: Extended): Pointer;
begin
  case tipo of
    ftSingle  : Result := instanciar(@TabelaInfoIgualdadeSingle,   epsilon);
    ftDouble  : Result := instanciar(@TabelaInfoIgualdadeDouble,   epsilon);
    ftExtended: Result := instanciar(@TabelaInfoIgualdadeExtended, epsilon);
    ftComp    : Result := instanciar(@TabelaInfoIgualdadeComp,     epsilon);
    ftCurr    : Result := instanciar(@TabelaInfoIgualdadeCurrency, epsilon);
    else        Result := nil;
  end;
end;

function SelecionarIgualdade(info: PTypeInfo;  epsilon: Extended): Pointer;
begin
  if ((info <> nil) and (info^.Kind = tkFloat)) then
    Result := InstanciarIgualdade(GetTypeData(info)^.FloatType, epsilon)
  else
    Result := nil;
end;
{$endregion}

end.
