(*******************************************************************************
 *
 * Arquivo  : Matematica.Geradores.GCLGLibC.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-25 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o gerador congruencial linear com as especifica��es do
 *            GNU C Library.
 *
 ******************************************************************************)
unit Matematica.Geradores.GCLGLibC;

interface
uses
  FractalLotus.Fundacao.Matematica.GeradorCongruencial;

type
  TGCLGLibC = class (TGeradorCongruencial)
  private
    fValor: Cardinal;

    function proximo: Cardinal; inline;
  public
    constructor Create(const semente: Cardinal); override;

    function uniforme()                     : Double;  overload; override;
    function uniforme(const limite: Integer): Integer; overload; override;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes;

{ TGCLGLibC }

constructor TGCLGLibC.Create(const semente: Cardinal);
begin
  inherited Create(semente);

  fValor := semente;
end;

function TGCLGLibC.proximo: Cardinal;
begin
  fValor := (fValor * $41C64E6D + $3039) and $7FFFFFFF;
  Result := fValor;
end;

function TGCLGLibC.uniforme(const limite: Integer): Integer;
begin
  if (limite <= 0) then
    raise ArgumentoNegativo('limite') at ReturnAddress;

  Result := proximo mod UInt32(limite);
end;

function TGCLGLibC.uniforme: Double;
begin
  Result := proximo / $80000000;
end;

end.
