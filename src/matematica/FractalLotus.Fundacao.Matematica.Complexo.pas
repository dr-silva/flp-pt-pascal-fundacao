(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Matematica.Complexo.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-09-13 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o tipo de dado que representa um n�mero complexo, um corpo
 *            escrito na forma a + bi, para a, b que pertencem aos reais.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Matematica.Complexo;

interface
uses
  System.SysUtils,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

type
  TComplexo = record
  private
    {$region 'Constantes'}
    class var fZERO,
              fNaN : TComplexo;
    {$endregion}
  public
    {$region 'Atributos'}
    re,
    im: Double;
    {$endregion}
  public
    {$region 'construtores'}
    constructor Create(const re, im: Double);

    class function FormaPolar(const raio, theta: Double): TComplexo; static; inline;
    {$endregion}

    {$region 'Propriedades & M�todos'}
    function modulo     : Double;
    function argumento  : Double;
    function conjugado  : TComplexo;
    function hashCode   : Integer;
    function toString   : string;    overload;
    function eReal      : Boolean;
    function eImaginario: Boolean;

    function toString(const settings: TFormatSettings): string; overload;
    function toString(const format: TFloatFormat; const precision, digits: Integer): string overload;
    function toString(const format: TFloatFormat; const precision, digits: Integer; const settings: TFormatSettings): string overload;
    {$endregion}

    {$region 'Operadores de Convers�o'}
    class operator Implicit(const valor: Double ): TComplexo; overload;
    class operator Implicit(const valor: Integer): TComplexo; overload;
    class operator Explicit(const valor: Double ): TComplexo; overload;
    class operator Explicit(const valor: Integer): TComplexo; overload;
    {$endregion}

    {$region 'Operadores Compara��o'}
    class operator Equal   (const esquerda, direita: TComplexo): Boolean;
    class operator NotEqual(const esquerda, direita: TComplexo): Boolean;
    {$endregion}

    {$region 'Operadores Un�rios'}
    class operator Negative(const valor: TComplexo): TComplexo;
    class operator Positive(const valor: TComplexo): TComplexo;
    {$endregion}

    {$region 'Operadores Bin�rios'}
    class operator Add(const esquerda: Double;    const direita: TComplexo): TComplexo;
    class operator Add(const esquerda: Integer;   const direita: TComplexo): TComplexo;
    class operator Add(const esquerda: TComplexo; const direita: Double   ): TComplexo;
    class operator Add(const esquerda: TComplexo; const direita: Integer  ): TComplexo;
    class operator Add(const esquerda: TComplexo; const direita: TComplexo): TComplexo;

    class operator Subtract(const esquerda: Double;    const direita: TComplexo): TComplexo;
    class operator Subtract(const esquerda: Integer;   const direita: TComplexo): TComplexo;
    class operator Subtract(const esquerda: TComplexo; const direita: Double   ): TComplexo;
    class operator Subtract(const esquerda: TComplexo; const direita: Integer  ): TComplexo;
    class operator Subtract(const esquerda: TComplexo; const direita: TComplexo): TComplexo;

    class operator Multiply(const esquerda: Double;    const direita: TComplexo): TComplexo;
    class operator Multiply(const esquerda: Integer;   const direita: TComplexo): TComplexo;
    class operator Multiply(const esquerda: TComplexo; const direita: Double   ): TComplexo;
    class operator Multiply(const esquerda: TComplexo; const direita: Integer  ): TComplexo;
    class operator Multiply(const esquerda: TComplexo; const direita: TComplexo): TComplexo;

    class operator Divide(const esquerda: Double;    const direita: TComplexo): TComplexo;
    class operator Divide(const esquerda: Integer;   const direita: TComplexo): TComplexo;
    class operator Divide(const esquerda: TComplexo; const direita: Double   ): TComplexo;
    class operator Divide(const esquerda: TComplexo; const direita: Integer  ): TComplexo;
    class operator Divide(const esquerda: TComplexo; const direita: TComplexo): TComplexo;
    {$endregion}

    {$region 'Fun��es'}
    class function eZero(const valor: TComplexo; const epsilon: Double): Boolean; overload; static; inline;
    class function eZero(const valor: TComplexo): Boolean;                        overload; static;
    class function eNaN (const valor: TComplexo): Boolean;                                  static;

    class function abs (const valor: TComplexo): Double;             static; inline;
    class function arg (const valor: TComplexo): Double;             static; inline;
    class function dist(const esquerda, direita: TComplexo): Double; static; inline;

    class function conj(const valor: TComplexo): TComplexo;                    static; inline;
    class function exp (const valor: TComplexo): TComplexo;          overload; static; inline;
    class function ln  (const valor: TComplexo): TComplexo;                    static; inline;
    class function sen (const valor: TComplexo): TComplexo;                    static;
    class function cos (const valor: TComplexo): TComplexo;                    static;
    class function tan (const valor: TComplexo): TComplexo;                    static; inline;
    class function csc (const valor: TComplexo): TComplexo;                    static; inline;
    class function sec (const valor: TComplexo): TComplexo;                    static; inline;
    class function cot (const valor: TComplexo): TComplexo;                    static; inline;
    class function senh(const valor: TComplexo): TComplexo;                    static; inline;
    class function cosh(const valor: TComplexo): TComplexo;                    static; inline;
    class function tanh(const valor: TComplexo): TComplexo;                    static; inline;
    class function csch(const valor: TComplexo): TComplexo;                    static; inline;
    class function sech(const valor: TComplexo): TComplexo;                    static; inline;
    class function coth(const valor: TComplexo): TComplexo;                    static; inline;
    class function exp (const base, expoente: TComplexo): TComplexo; overload; static; inline;
    class function log (const base, valor   : TComplexo): TComplexo;           static; inline;
    {$endregion}

    {$region 'Protocolos de Fun��es'}
    class function comparadorIgualdade                       : IComparadorIgualdade<TComplexo>; overload; static;
    class function comparadorIgualdade(const epsilon: Double): IComparadorIgualdade<TComplexo>; overload; static;
    class function redutorSoma                               : IRedutor<TComplexo, TComplexo>;            static;
    class function redutorMultiplicacao                      : IRedutor<TComplexo, TComplexo>;            static;
    class function projetorString                            : IProjetor<TComplexo, string>;              static;
    {$endregion}

    {$region 'Constantes'}
    class property ZERO: TComplexo read fZERO;
    class property NaN : TComplexo read fNaN;
    {$endregion}
  end;

implementation
uses
  System.Math,
  Matematica.Constantes.Complexo,
  Matematica.Constantes.Igualdades,
  FractalLotus.Fundacao.Matematica;

const
  COMPLEXO_ZERO: TComplexo = (re: 0.0; im: 0.0);
  COMPLEXO_NaN : TComplexo = (re: 0.0 / 0.0; im: 0.0 / 0.0);

{ TComplexo }

class function TComplexo.abs(const valor: TComplexo): Double;
begin
  Result := valor.modulo;
end;

class operator TComplexo.Add(const esquerda: TComplexo;
  const direita: Integer): TComplexo;
begin
  Result.re := esquerda.re + direita;
  Result.im := esquerda.im;
end;

class operator TComplexo.Add(const esquerda, direita: TComplexo): TComplexo;
begin
  Result.re := esquerda.re + direita.re;
  Result.im := esquerda.im + direita.im;
end;

class operator TComplexo.Add(const esquerda: Double;
  const direita: TComplexo): TComplexo;
begin
  Result.re := esquerda + direita.re;
  Result.im :=            direita.im;
end;

class operator TComplexo.Add(const esquerda: Integer;
  const direita: TComplexo): TComplexo;
begin
  Result.re := esquerda + direita.re;
  Result.im :=            direita.im;
end;

class operator TComplexo.Add(const esquerda: TComplexo;
  const direita: Double): TComplexo;
begin
  Result.re := esquerda.re + direita;
  Result.im := esquerda.im;
end;

class function TComplexo.arg(const valor: TComplexo): Double;
begin
  Result := valor.argumento;
end;

function TComplexo.argumento: Double;
begin
  Result := System.Math.ArcTan2(im, re);
end;

class function TComplexo.comparadorIgualdade: IComparadorIgualdade<TComplexo>;
begin
  Result := comparadorIgualdade(TMatematica.EPSILON);
end;

class function TComplexo.comparadorIgualdade(
  const epsilon: Double): IComparadorIgualdade<TComplexo>;
var
  valor: Double;
begin
  if (epsilon <= 0) then
    valor := TMatematica.EPSILON
  else
    valor := epsilon;

  Result := IComparadorIgualdade<TComplexo>(InstanciarComparadorIgualdadeComplexo(valor));
end;

class function TComplexo.conj(const valor: TComplexo): TComplexo;
begin
  Result := valor.conjugado;
end;

function TComplexo.conjugado: TComplexo;
begin
  Result.re :=  re;
  Result.im := -im;
end;

class function TComplexo.cos(const valor: TComplexo): TComplexo;
begin
  Result.re := + System.Cos(valor.re) * System.Math.Cosh(valor.im);
  Result.im := - System.Sin(valor.re) * System.Math.Sinh(valor.im);
end;

class function TComplexo.cosh(const valor: TComplexo): TComplexo;
begin
  Result := (exp(valor) + exp(-valor)) / 2;
end;

class function TComplexo.cot(const valor: TComplexo): TComplexo;
begin
  Result := cos(valor) / sen(valor);
end;

class function TComplexo.coth(const valor: TComplexo): TComplexo;
begin
  Result := cosh(valor) / senh(valor);
end;

constructor TComplexo.Create(const re, im: Double);
begin
  Self.re := re;
  Self.im := im;
end;

class function TComplexo.csc(const valor: TComplexo): TComplexo;
begin
  Result := 1.0 / sen(valor);
end;

class function TComplexo.csch(const valor: TComplexo): TComplexo;
begin
  Result := 1 / senh(valor);
end;

class function TComplexo.dist(const esquerda, direita: TComplexo): Double;
begin
  Result := (esquerda - direita).modulo;
end;

class operator TComplexo.Divide(const esquerda: TComplexo;
  const direita: Double): TComplexo;
begin
  if System.Math.IsZero(direita) then
    Exit(COMPLEXO_NaN);

  Result.re := esquerda.Re / direita;
  Result.im := esquerda.Im / direita;
end;

class operator TComplexo.Divide(const esquerda: TComplexo;
  const direita: Integer): TComplexo;
begin
  if (direita = 0) then
    Exit(COMPLEXO_NaN);

  Result.re := esquerda.Re / direita;
  Result.im := esquerda.Im / direita;
end;

class operator TComplexo.Divide(const esquerda: Integer;
  const direita: TComplexo): TComplexo;
var
  alfa: Double;
  conj: TComplexo;
begin
  if eZero(direita) then
    Exit(COMPLEXO_NaN);

  alfa := esquerda / System.Math.Power(direita.modulo, 2);
  conj := direita.conjugado;

  Result.re := alfa * conj.re;
  Result.im := alfa * conj.im;
end;

class operator TComplexo.Divide(const esquerda: Double;
  const direita: TComplexo): TComplexo;
var
  alfa: Double;
  conj: TComplexo;
begin
  if eZero(direita) then
    Exit(COMPLEXO_NaN);

  alfa := esquerda / System.Math.Power(direita.modulo, 2);
  conj := direita.conjugado;

  Result.re := alfa * conj.re;
  Result.im := alfa * conj.im;
end;

class operator TComplexo.Divide(const esquerda, direita: TComplexo): TComplexo;
var
  // rEsq, rDir, tEsq, tDir: Double;
  raio, theta: Double;
begin
  if eZero(direita) then
    Exit(COMPLEXO_NaN);

  raio      := esquerda.modulo    / direita.modulo;
  theta     := esquerda.argumento - direita.argumento;
  Result.re := raio * System.Cos(theta);
  Result.im := raio * System.Sin(theta);
{
  rEsq := esquerda.modulo;
  tEsq := esquerda.argumento;
  rDir := direita .modulo;
  tDir := direita .argumento;

  Result.Re := (rEsq / rDir) * System.Cos(tEsq - tDir);
  Result.Im := (rEsq / rDir) * System.Sin(tEsq - tDir);
}
end;

function TComplexo.eImaginario: Boolean;
var
  reType: TFloatSpecial;
begin
  reType := re.SpecialType;
  Result := (reType = fsZero) or (reType = fsNZero);
end;

class function TComplexo.eNaN(const valor: TComplexo): Boolean;
var
  reType,
  imType: TFloatSpecial;
begin
  reType := valor.re.SpecialType;
  imType := valor.im.SpecialType;
  Result := (reType = fsNaN) or (reType = fsInf) or (reType = fsNInf) or
            (imType = fsNaN) or (imType = fsInf) or (imType = fsNInf);
end;

class operator TComplexo.Equal(const esquerda, direita: TComplexo): Boolean;
begin
  Result := (esquerda.re = direita.re) and (esquerda.im = direita.im);
end;

function TComplexo.eReal: Boolean;
var
  imType: TFloatSpecial;
begin
  imType := im.SpecialType;
  Result := (imType = fsZero) or (imType = fsNZero);
end;

class function TComplexo.exp(const base, expoente: TComplexo): TComplexo;
begin
  Result := exp(expoente * ln(base));
end;

class function TComplexo.exp(const valor: TComplexo): TComplexo;
begin
  Result.re := System.Exp(valor.re) * System.Cos(valor.im);
  Result.im := System.Exp(valor.re) * System.Sin(valor.im);
end;

class operator TComplexo.Explicit(const valor: Integer): TComplexo;
begin
  Result.re := valor;
  Result.im := 0;
end;

class operator TComplexo.Explicit(const valor: Double): TComplexo;
begin
  Result.re := valor;
  Result.im := 0;
end;

class function TComplexo.eZero(const valor: TComplexo): Boolean;
var
  floatType: TFloatSpecial;
begin
  floatType := valor.modulo.SpecialType;
  Result    := (floatType = fsZero) or (floatType = fsNZero);
end;

class function TComplexo.eZero(const valor: TComplexo;
  const epsilon: Double): Boolean;
begin
  Result := valor.modulo < epsilon;
end;

class function TComplexo.FormaPolar(const raio, theta: Double): TComplexo;
begin
  Result.re := raio * System.Cos(theta);
  Result.im := raio * System.Sin(theta);
end;

function TComplexo.hashCode: Integer;
begin
  Result := ObterHashCodeDouble(nil, re) xor ObterHashCodeDouble(nil, im);
end;

class operator TComplexo.Implicit(const valor: Double): TComplexo;
begin
  Result.re := valor;
  Result.im := 0;
end;

class operator TComplexo.Implicit(const valor: Integer): TComplexo;
begin
  Result.re := valor;
  Result.im := 0;
end;

class function TComplexo.ln(const valor: TComplexo): TComplexo;
begin
  Result.re := System.Ln(valor.modulo);
  Result.im := valor.argumento;
end;

class function TComplexo.log(const base, valor: TComplexo): TComplexo;
begin
  Result := ln(valor) / ln(base);
end;

function TComplexo.modulo: Double;
begin
  Result := System.Math.Hypot(re, im);
end;

class operator TComplexo.Multiply(const esquerda: Double;
  const direita: TComplexo): TComplexo;
begin
  Result.re := esquerda * direita.re;
  Result.im := esquerda * direita.im;
end;

class operator TComplexo.Multiply(const esquerda,
  direita: TComplexo): TComplexo;
begin
  Result.re := esquerda.re * direita.re - esquerda.im * direita .im;
  Result.im := esquerda.re * direita.im + direita .re * esquerda.im;
end;

class operator TComplexo.Multiply(const esquerda: TComplexo;
  const direita: Integer): TComplexo;
begin
  Result.re := esquerda.re * direita;
  Result.im := esquerda.im * direita;
end;

class operator TComplexo.Multiply(const esquerda: TComplexo;
  const direita: Double): TComplexo;
begin
  Result.re := esquerda.re * direita;
  Result.im := esquerda.im * direita;
end;

class operator TComplexo.Multiply(const esquerda: Integer;
  const direita: TComplexo): TComplexo;
begin
  Result.re := esquerda * direita.re;
  Result.im := esquerda * direita.im;
end;

class operator TComplexo.Negative(const valor: TComplexo): TComplexo;
begin
  Result.re := -valor.re;
  Result.im := -valor.im;
end;

class operator TComplexo.NotEqual(const esquerda, direita: TComplexo): Boolean;
begin
  Result := (esquerda.re <> direita.re) or (esquerda.im <> direita.im);
end;

class operator TComplexo.Positive(const valor: TComplexo): TComplexo;
begin
  Result.re := valor.re;
  Result.im := valor.im;
end;

class function TComplexo.projetorString: IProjetor<TComplexo, string>;
begin
  Result := IProjetor<TComplexo, string>(InstanciarProjetorStringComplexo);
end;

class function TComplexo.redutorMultiplicacao: IRedutor<TComplexo, TComplexo>;
begin
  Result := IRedutor<TComplexo, TComplexo>(InstanciarRedutorMultiplicacaoComplexo);
end;

class function TComplexo.redutorSoma: IRedutor<TComplexo, TComplexo>;
begin
  Result := IRedutor<TComplexo, TComplexo>(InstanciarRedutorAdicaoComplexo);
end;

class function TComplexo.sec(const valor: TComplexo): TComplexo;
begin
  Result := 1.0 / cos(valor);
end;

class function TComplexo.sech(const valor: TComplexo): TComplexo;
begin
  Result := 1 / cosh(valor);
end;

class function TComplexo.sen(const valor: TComplexo): TComplexo;
begin
  Result.re := System.Sin(valor.re) * System.Math.Cosh(valor.im);
  Result.im := System.Cos(valor.re) * System.Math.Sinh(valor.im);
end;

class function TComplexo.senh(const valor: TComplexo): TComplexo;
begin
  Result := (exp(valor) - exp(-valor)) / 2;
end;

class operator TComplexo.Subtract(const esquerda: TComplexo;
  const direita: Double): TComplexo;
begin
  Result.re := esquerda.re - direita;
  Result.im := esquerda.im;
end;

class operator TComplexo.Subtract(const esquerda: TComplexo;
  const direita: Integer): TComplexo;
begin
  Result.re := esquerda.re - direita;
  Result.im := esquerda.im;
end;

class operator TComplexo.Subtract(const esquerda,
  direita: TComplexo): TComplexo;
begin
  Result.re := esquerda.re - direita.re;
  Result.im := esquerda.im - direita.im;
end;

class operator TComplexo.Subtract(const esquerda: Integer;
  const direita: TComplexo): TComplexo;
begin
  Result.re := esquerda - direita.re;
  Result.im :=          - direita.im;
end;

class operator TComplexo.Subtract(const esquerda: Double;
  const direita: TComplexo): TComplexo;
begin
  Result.re := esquerda - direita.re;
  Result.im :=          - direita.im;
end;

class function TComplexo.tan(const valor: TComplexo): TComplexo;
begin
  Result := sen(valor) / cos(valor);
end;

class function TComplexo.tanh(const valor: TComplexo): TComplexo;
begin
  Result := senh(valor) / cosh(valor);
end;

function TComplexo.toString(const format: TFloatFormat; const precision,
  digits: Integer; const settings: TFormatSettings): string;
var
  reStr,
  imStr: string;
begin
  reStr :=     re .ToString(format, precision, digits, settings);
  imStr := Abs(im).ToString(format, precision, digits, settings);

  if eReal then
    Result := reStr
  else if eImaginario then
    Result := im.ToString(format, precision, digits, settings) + 'i'
  else if (im > 0) then
    Result := reStr + ' + ' + imStr + 'i'
  else
    Result := reStr + ' - ' + imStr + 'i';
end;

function TComplexo.toString(const format: TFloatFormat; const precision,
  digits: Integer): string;
begin
  Result := toString(format, precision, digits, FormatSettings);
end;

function TComplexo.toString(const settings: TFormatSettings): string;
begin
  if eZero(Self) then
    Result := '0'
  else if eReal then
    Result := re.ToString(settings)
  else if eImaginario then
    Result := im.ToString(settings) + 'i'
  else if (im > 0) then
    Result := re.ToString(settings) + ' + ' + im.ToString(settings) + 'i'
  else
    Result := re.ToString(settings) + ' - ' + Abs(im).ToString(settings) + 'i'
end;

function TComplexo.toString: string;
begin
  Result := toString(FormatSettings);
end;

initialization
  TComplexo.fZERO := COMPLEXO_ZERO;
  TComplexo.fNaN  := COMPLEXO_NaN;

end.
