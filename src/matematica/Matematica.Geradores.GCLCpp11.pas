(*******************************************************************************
 *
 * Arquivo  : Matematica.Geradores.GCLCpp11.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-00-25 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o gerador congruencial linear com as especifica��es do
 *            C++ aprovadas pela International Organization for Standardization
 *            em 12 de Agosto de 2011.
 *
 ******************************************************************************)
unit Matematica.Geradores.GCLCpp11;

interface
uses
  FractalLotus.Fundacao.Matematica.GeradorCongruencial;

type
  TGCLCpp11 = class (TGeradorCongruencial)
  private
    fValor: Cardinal;

    function proximo: Cardinal; inline;
  public
    constructor Create(const semente: Cardinal); override;

    function uniforme()                     : Double;  overload; override;
    function uniforme(const limite: Integer): Integer; overload; override;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes;

{ TGCLCpp11 }

constructor TGCLCpp11.Create(const semente: Cardinal);
begin
  inherited Create(semente);

  fValor := semente;
end;

function TGCLCpp11.proximo: Cardinal;
begin
  fValor := (fValor * $BC8F) and $7FFFFFFF;
  Result := fValor;
end;

function TGCLCpp11.uniforme(const limite: Integer): Integer;
begin
  if (limite <= 0) then
    raise ArgumentoNegativo('limite') at ReturnAddress;

  Result := proximo mod UInt32(limite);
end;

function TGCLCpp11.uniforme: Double;
begin
  Result := proximo / $80000000;
end;

end.
