(*******************************************************************************
 *
 * Arquivo  : Matematica.Constantes.Comparadores.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-26 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Monta a inst�ncia simples de um comparador para os tipos de pon-
 *            tos-flutuantes instanciado pela classe TMatem�tica.
 *
 ******************************************************************************)
unit Matematica.Constantes.Comparadores;

interface
uses
  System.TypInfo,
  Matematica.Constantes.Interfaces;

{$region 'Procuradores'}
function CompararComp    (Inst: Pointer; const Left, Right: Comp    ): Integer;
function CompararSingle  (Inst: Pointer; const Left, Right: Single  ): Integer;
function CompararDouble  (Inst: Pointer; const Left, Right: Double  ): Integer;
function CompararExtended(Inst: Pointer; const Left, Right: Extended): Integer;
function CompararCurrency(Inst: Pointer; const Left, Right: Currency): Integer;
{$endregion}

{$region 'Instancia��o'}
function InstanciarComparador(tipo: TFloatType; epsilon: Extended): Pointer; inline;
function SelecionarComparador(info: PTypeInfo;  epsilon: Extended): Pointer; inline;
{$endregion}

const
  {$region 'Tabelas Info'}
  TabelaInfoComparadorComp: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @CompararComp
  );

  TabelaInfoComparadorSingle: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @CompararSingle
  );

  TabelaInfoComparadorDouble: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @CompararDouble
  );

  TabelaInfoComparadorExtended: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @CompararExtended
  );

  TabelaInfoComparadorCurrency: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @CompararCurrency
  );
  {$endregion}

implementation

{$region 'Procuradores'}
function CompararComp(Inst: Pointer; const Left, Right: Comp): Integer;
var
  epsilon  ,
  diferenca: Comp;
begin
  epsilon   := PInstanciaSimples(Inst)^.epsilon;
  diferenca := Right - Left;

  if (diferenca <= -epsilon) then
    Result := -1
  else if (diferenca >= epsilon) then
    Result := +1
  else
    Result := 0;
end;

function CompararSingle(Inst: Pointer; const Left, Right: Single): Integer;
var
  epsilon  ,
  diferenca: Single;
begin
  epsilon   := PInstanciaSimples(Inst)^.epsilon;
  diferenca := Right - Left;

  if (diferenca <= -epsilon) then
    Result := -1
  else if (diferenca >= epsilon) then
    Result := +1
  else
    Result := 0;
end;

function CompararDouble(Inst: Pointer; const Left, Right: Double): Integer;
var
  epsilon  ,
  diferenca: Double;
begin
  epsilon   := PInstanciaSimples(Inst)^.epsilon;
  diferenca := Right - Left;

  if (diferenca <= -epsilon) then
    Result := -1
  else if (diferenca >= epsilon) then
    Result := +1
  else
    Result := 0;
end;

function CompararExtended(Inst: Pointer; const Left, Right: Extended): Integer;
var
  epsilon  ,
  diferenca: Extended;
begin
  epsilon   := PInstanciaSimples(Inst)^.epsilon;
  diferenca := Right - Left;

  if (diferenca <= -epsilon) then
    Result := -1
  else if (diferenca >= epsilon) then
    Result := +1
  else
    Result := 0;
end;

function CompararCurrency(Inst: Pointer; const Left, Right: Currency): Integer;
var
  epsilon  ,
  diferenca: Currency;
begin
  epsilon   := PInstanciaSimples(Inst)^.epsilon;
  diferenca := Right - Left;

  if (diferenca <= -epsilon) then
    Result := -1
  else if (diferenca >= epsilon) then
    Result := +1
  else
    Result := 0;
end;
{$endregion}

{$region 'Instancia��o'}
function InstanciarComparador(tipo: TFloatType; epsilon: Extended): Pointer;
begin
  case tipo of
    ftSingle  : Result := instanciar(@TabelaInfoComparadorSingle,   epsilon);
    ftDouble  : Result := instanciar(@TabelaInfoComparadorDouble,   epsilon);
    ftExtended: Result := instanciar(@TabelaInfoComparadorExtended, epsilon);
    ftComp    : Result := instanciar(@TabelaInfoComparadorComp,     epsilon);
    ftCurr    : Result := instanciar(@TabelaInfoComparadorCurrency, epsilon);
    else        Result := nil;
  end;
end;

function SelecionarComparador(info: PTypeInfo; epsilon: Extended): Pointer;
begin
  if ((info <> nil) and (info^.Kind = tkFloat)) then
    Result := InstanciarComparador(GetTypeData(info)^.FloatType, epsilon)
  else
    Result := nil;
end;
{$endregion}

end.
