(*******************************************************************************
 *
 * Arquivo  : Matematica.Geradores.GCLMMIX.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-28 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o gerador congruencial linear com as especifica��es do
 *            MMIX desenhado por Donald Knuth.
 *
 ******************************************************************************)
unit Matematica.Geradores.GCLMMIX;

interface
uses
  FractalLotus.Fundacao.Matematica.GeradorCongruencial;

type
  TGCLMMIX = class (TGeradorCongruencial)
  private const
    DIVISOR   : Extended = 1.0 / $10000 / $10000 / $10000 / $10000; // 2^-64
    MULTIPLIER: UInt64   = $5851F42D4C957F2D; // 6.364.136.223.846.793.005
    INCREMENT : UInt64   = $14057B7EF767814F; // 1.442.695.040.888.963.407
  private
    fValor: UInt64;

    function proximo: UInt64; inline;
  public
    constructor Create(const semente: Cardinal); override;

    function uniforme()                     : Double;  overload; override;
    function uniforme(const limite: Integer): Integer; overload; override;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes;

{ TGCLMMIX }

constructor TGCLMMIX.Create(const semente: Cardinal);
begin
  inherited Create(semente);

  fValor := semente;
end;

function TGCLMMIX.proximo: UInt64;
begin
  fValor := fValor * MULTIPLIER + INCREMENT;
  Result := fValor;
end;

function TGCLMMIX.uniforme(const limite: Integer): Integer;
begin
  if (limite <= 0) then
    raise ArgumentoNegativo('limite') at ReturnAddress;

  Result := proximo mod UInt64(limite);
end;

function TGCLMMIX.uniforme: Double;
begin
  Result := proximo * DIVISOR;
end;

end.
