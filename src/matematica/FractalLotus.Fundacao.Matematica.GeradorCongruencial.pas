(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Matematica.GeradorCongruencial.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o protocolo dum gerador de n�meros pseudo-aleat�rios.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Matematica.GeradorCongruencial;

interface

type
  IGeradorCongruencial = interface
    ['{49BE5B53-99DE-4E69-B729-95CF4C15B9B2}']

    function obterSemente: Cardinal;

    function uniforme                       : Double;  overload;
    function uniforme(const limite: Integer): Integer; overload;

    property semente: Cardinal read obterSemente;
  end;

  TGeradorCongruencial = class abstract(TInterfacedObject, IGeradorCongruencial)
  strict private
    fSemente: Cardinal;
  private
    function obterSemente: Cardinal; inline;
  public
    constructor Create(const semente: Cardinal); virtual;

    function uniforme                       : Double;  overload; virtual; abstract;
    function uniforme(const limite: Integer): Integer; overload; virtual; abstract;

    property semente: Cardinal read obterSemente;
  end;

  TGeradorCongruencialClass = class of TGeradorCongruencial;

implementation

{ TGeradorCongruencial }

constructor TGeradorCongruencial.Create(const semente: Cardinal);
begin
  fSemente := semente;
end;

function TGeradorCongruencial.obterSemente: Cardinal;
begin
  Result := fSemente;
end;

end.
