(*******************************************************************************
 *
 * Arquivo  : Matematica.Geradores.GCLPosix.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-26 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o gerador congruencial linear com as especifica��es do
 *            POSIX (Portable Operating System Interface), uma fam�lia de
 *            padr�es especificados pela IEEE Computer Society.
 *
 ******************************************************************************)
unit Matematica.Geradores.GCLPosix;

interface
uses
  FractalLotus.Fundacao.Matematica.GeradorCongruencial;

type
  TGCLPosix = class (TGeradorCongruencial)
  private
    fValor: UInt64;

    function proximo: Cardinal; inline;
  public
    constructor Create(const semente: Cardinal); override;

    function uniforme()                     : Double;  overload; override;
    function uniforme(const limite: Integer): Integer; overload; override;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes;

{ TGCLPosix }

constructor TGCLPosix.Create(const semente: Cardinal);
begin
  inherited Create(semente);

  fValor := semente;
end;

function TGCLPosix.proximo: Cardinal;
begin
  fValor := (fValor * UInt64($5DEECE66D) + 11) and $FFFFFFFFFFFF;
  Result := (fValor shr 16);
end;

function TGCLPosix.uniforme(const limite: Integer): Integer;
begin
  if (limite <= 0) then
    raise ArgumentoNegativo('limite') at ReturnAddress;

  Result := proximo mod Cardinal(limite);
end;

function TGCLPosix.uniforme: Double;
begin
  Result := proximo / $10000 / $10000; // 2^32
end;

end.
