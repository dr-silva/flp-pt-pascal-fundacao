(*******************************************************************************
 *
 * Arquivo  : Matematica.Geradores.GCLAppleCarbon.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-26 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o gerador congruencial linear com as especifica��es do
 *            Apple Carbon desenvolvido pela Apple para o Mac OS.
 *
 ******************************************************************************)
unit Matematica.Geradores.GCLAppleCarbon;

interface
uses
  FractalLotus.Fundacao.Matematica.GeradorCongruencial;

type
  TGCLAppleCarbon = class (TGeradorCongruencial)
  private
    fValor: Cardinal;

    function proximo: Cardinal; inline;
  public
    constructor Create(const semente: Cardinal); override;

    function uniforme()                     : Double;  overload; override;
    function uniforme(const limite: Integer): Integer; overload; override;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes;

{ TGCLAppleCarbon }

constructor TGCLAppleCarbon.Create(const semente: Cardinal);
begin
  inherited Create(semente);

  fValor := semente;
end;

function TGCLAppleCarbon.proximo: Cardinal;
begin
  fValor := (fValor * $41A7) and $7FFFFFFF;
  Result := fValor;
end;

function TGCLAppleCarbon.uniforme(const limite: Integer): Integer;
begin
  if (limite <= 0) then
    raise ArgumentoNegativo('limite') at ReturnAddress;

  Result := proximo mod UInt32(limite)
end;

function TGCLAppleCarbon.uniforme: Double;
begin
  Result := proximo / $80000000; // 2^31
end;

end.
