(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Matematica.FabricaGeradorCongruencial.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Classe de suporte para registrar e instanciar implementa��es do
 *            gerador congruencial.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Matematica.FabricaGeradorCongruencial;

interface
uses
  System.Generics.Collections,
  FractalLotus.Fundacao.Matematica.GeradorCongruencial;

type
  TTipoGeradorCongruencial = (tgcLinear, tgcNaoLinear);

  TFabricaGeradorCongruencial = class
  private
    {$region 'Atributos'}
    fRegistros: TDictionary<string, TGeradorCongruencialClass>;
    {$endregion}

    {$region 'M�todos'}
    procedure registrar(const chave: string; const classe: TGeradorCongruencialClass); overload;

    function  obter (const chave: string)                 : TGeradorCongruencialClass; inline;
    function  listar                                      : TList<TGeradorCongruencialClass>; overload; inline;
    function  listar(const tipo: TTipoGeradorCongruencial): TList<TGeradorCongruencialClass>; overload; inline;
    {$endregion}

    {$region 'Singleton'}
    constructor Create;

    class var fInstancia: TFabricaGeradorCongruencial;
    {$endregion}
  public
    destructor Destroy; override;

    class procedure registrar(const nome: string; const tipo: TTipoGeradorCongruencial; const classe: TGeradorCongruencialClass); overload; static;

    class function criar                                                                                   : IGeradorCongruencial; overload;
    class function criar(const semente: Cardinal                                                          ): IGeradorCongruencial; overload;
    class function criar(const tipo: TTipoGeradorCongruencial                                             ): IGeradorCongruencial; overload;
    class function criar(const tipo: TTipoGeradorCongruencial; const semente: Cardinal                    ): IGeradorCongruencial; overload;
    class function criar(const nome: string; const tipo: TTipoGeradorCongruencial                         ): IGeradorCongruencial; overload;
    class function criar(const nome: string; const tipo: TTipoGeradorCongruencial; const semente: Cardinal): IGeradorCongruencial; overload;
  end;

implementation
uses
  System.Classes,
  System.SysUtils,
  FractalLotus.Fundacao.Matematica,
  // GERADORES PARA SEREM REGISTRADOS //
  Matematica.Geradores.GCLAppleCarbon,
  Matematica.Geradores.GCLBorlandC,
  Matematica.Geradores.GCLBorlandPascal,
  Matematica.Geradores.GCLCpp11,
  Matematica.Geradores.GCLGLibC,
  Matematica.Geradores.GCLMMIX,
  Matematica.Geradores.GCLNewLib,
  Matematica.Geradores.GCLNumericalRecipes,
  Matematica.Geradores.GCLOpenVms,
  Matematica.Geradores.GCLPosix,
  Matematica.Geradores.GCLRtlUniform;

type
  TTipoGeradorCongruencialHelper = record helper for TTipoGeradorCongruencial
  public
    function toString: string; inline;
  end;

{ TTipoGeradorCongruencialHelper }

function TTipoGeradorCongruencialHelper.toString: string;
begin
  case Self of
    tgcLinear   : Result := 'linear';
    tgcNaoLinear: Result := 'nao-linear';
  end;
end;

{ TFabricaGeradorCongruencial }

constructor TFabricaGeradorCongruencial.Create;
begin
  fRegistros := TDictionary<string, TGeradorCongruencialClass>.Create;
end;

class function TFabricaGeradorCongruencial.criar(
  const semente: Cardinal): IGeradorCongruencial;
var
  lista: TList<TGeradorCongruencialClass>;
begin
  TMonitor.Enter(fInstancia);
  try
    lista := fInstancia.listar;
    try
      if (lista.Count > 0) then
        Result := lista[ TMatematica.uniforme(lista.Count) ].Create(semente)
      else
        Result := nil;
    finally
      lista.Free;
    end;
  finally
    TMonitor.Exit(fInstancia);
  end;
end;

class function TFabricaGeradorCongruencial.criar: IGeradorCongruencial;
begin
  Result := criar(TThread.GetTickCount);
end;

class function TFabricaGeradorCongruencial.criar(
  const tipo: TTipoGeradorCongruencial;
  const semente: Cardinal): IGeradorCongruencial;
var
  lista: TList<TGeradorCongruencialClass>;
begin
  TMonitor.Enter(fInstancia);
  try
    lista := fInstancia.listar(tipo);
    try
      if (lista.Count > 0) then
        Result := lista[ TMatematica.uniforme(lista.Count) ].Create(semente)
      else
        Result := nil;
    finally
      lista.Free;
    end;
  finally
    TMonitor.Exit(fInstancia);
  end;
end;

class function TFabricaGeradorCongruencial.criar(
  const tipo: TTipoGeradorCongruencial): IGeradorCongruencial;
begin
  Result := criar(tipo, TThread.GetTickCount);
end;

destructor TFabricaGeradorCongruencial.Destroy;
begin
  fRegistros.Free;

  inherited Destroy;
end;

function TFabricaGeradorCongruencial.listar: TList<TGeradorCongruencialClass>;
begin
  Result := TList<TGeradorCongruencialClass>.Create(fRegistros.Values);
end;

function TFabricaGeradorCongruencial.listar(
  const tipo: TTipoGeradorCongruencial): TList<TGeradorCongruencialClass>;
var
  str: string;
  par: TPair<string, TGeradorCongruencialClass>;
begin
  str    := tipo.toString;
  Result := TList<TGeradorCongruencialClass>.Create;

  for par in fRegistros do
    if par.Key.StartsWith(str) then
      Result.Add(par.Value);
end;

function TFabricaGeradorCongruencial.obter(
  const chave: string): TGeradorCongruencialClass;
begin
  if not fRegistros.TryGetValue(chave, Result) then
    Result := nil;
end;

procedure TFabricaGeradorCongruencial.registrar(const chave: string;
  const classe: TGeradorCongruencialClass);
begin
  if fRegistros.ContainsKey(chave) then
    fRegistros[chave] := classe
  else
    fRegistros.Add(chave, classe);
end;

class procedure TFabricaGeradorCongruencial.registrar(const nome: string;
  const tipo: TTipoGeradorCongruencial; const classe: TGeradorCongruencialClass);
begin
  TMonitor.Enter(fInstancia);
  try
    fInstancia.registrar(Format('%s-%s', [tipo.toString, nome]), classe);
  finally
    TMonitor.Exit(fInstancia);
  end;
end;

class function TFabricaGeradorCongruencial.criar(const nome: string;
  const tipo: TTipoGeradorCongruencial): IGeradorCongruencial;
begin
  Result := criar(nome, tipo, TThread.GetTickCount);
end;

class function TFabricaGeradorCongruencial.criar(const nome: string;
  const tipo: TTipoGeradorCongruencial; const semente: Cardinal): IGeradorCongruencial;
var
  classe: TGeradorCongruencialClass;
begin
  TMonitor.Enter(fInstancia);
  try
    classe := fInstancia.obter(Format('%s-%s', [tipo.toString, nome]));

    if (classe <> nil) then
      Result := classe.Create(semente)
    else
      Result := nil;
  finally
    TMonitor.Exit(fInstancia);
  end;
end;

initialization
  TFabricaGeradorCongruencial.fInstancia := TFabricaGeradorCongruencial.Create;
  TFabricaGeradorCongruencial.registrar('apple-carbon',      tgcLinear, TGCLAppleCarbon);
  TFabricaGeradorCongruencial.registrar('borland-c',         tgcLinear, TGCLBorlandC);
  TFabricaGeradorCongruencial.registrar('borland-pascal',    tgcLinear, TGCLBorlandPascal);
  TFabricaGeradorCongruencial.registrar('cpp11',             tgcLinear, TGCLCpp11);
  TFabricaGeradorCongruencial.registrar('glibc',             tgcLinear, TGCLGLibC);
  TFabricaGeradorCongruencial.registrar('mmix',              tgcLinear, TGCLMMIX);
  TFabricaGeradorCongruencial.registrar('new-lib',           tgcLinear, TGCLNewLib);
  TFabricaGeradorCongruencial.registrar('numerical-recipes', tgcLinear, TGCLNumericalRecipes);
  TFabricaGeradorCongruencial.registrar('open-vms',          tgcLinear, TGCLOpenVms);
  TFabricaGeradorCongruencial.registrar('posix',             tgcLinear, TGCLPosix);
  TFabricaGeradorCongruencial.registrar('rtl-uniform',       tgcLinear, TGCLRtlUniform);

finalization
  TFabricaGeradorCongruencial.fInstancia.Free;

end.
