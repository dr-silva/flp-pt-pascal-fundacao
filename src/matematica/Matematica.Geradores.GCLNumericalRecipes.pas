(*******************************************************************************
 *
 * Arquivo  : Matematica.Geradores.GCLNumericalRecipes.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-24 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o gerador congruencial linear com as especifica��es do
 *            Numerical Recipes.
 *
 ******************************************************************************)
unit Matematica.Geradores.GCLNumericalRecipes;

interface
uses
  FractalLotus.Fundacao.Matematica.GeradorCongruencial;

type
  TGCLNumericalRecipes = class (TGeradorCongruencial)
  private
    fValor: Cardinal;

    function proximo: Cardinal; inline;
  public
    constructor Create(const semente: Cardinal); override;

    function uniforme()                     : Double;  overload; override;
    function uniforme(const limite: Integer): Integer; overload; override;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes;

{ TGCLNumericalRecipes }

constructor TGCLNumericalRecipes.Create(const semente: Cardinal);
begin
  inherited Create(semente);

  fValor := semente;
end;

function TGCLNumericalRecipes.proximo: Cardinal;
begin
  fValor := fValor * $19660D + $3C6EF35F;
  Result := fValor;
end;

function TGCLNumericalRecipes.uniforme(const limite: Integer): Integer;
begin
  if (limite <= 0) then
    raise ArgumentoNegativo('limite') at ReturnAddress;

  Result := proximo mod UInt32(limite);
end;

function TGCLNumericalRecipes.uniforme: Double;
begin
  Result := proximo * ((1.0 / $10000) / $10000); // 2^-32
end;

end.
