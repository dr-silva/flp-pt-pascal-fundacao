(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Matematica.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-07-27 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Classe base para se trabalhar com os tipos de dados definidos nes-
 *            te pacote.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Matematica;

interface
uses
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

type
  TMatematica = class
  public const
    EPSILON = 1E-7;
  private
    class var fOriginal: Cardinal;
    class var fSemente : Cardinal;

    class function proximo: Cardinal; inline;

    class function criarComparador<T>(epsilon: Extended): IComparador<T>;
    class function criarIgualdade <T>(epsilon: Extended): IComparadorIgualdade<T>;
  public
    class function uniforme(const limite: Integer): Integer; overload; static;
    class function uniforme                       : Double;  overload; static;

    class function criarComparadorComp    (const epsilon: Comp     = 0): IComparador<Comp>;     static; inline;
    class function criarComparadorSingle  (const epsilon: Single   = 0): IComparador<Single>;   static; inline;
    class function criarComparadorDouble  (const epsilon: Double   = 0): IComparador<Double>;   static; inline;
    class function criarComparadorExtended(const epsilon: Extended = 0): IComparador<Extended>; static; inline;
    class function criarComparadorCurrency(const epsilon: Currency = 0): IComparador<Currency>; static; inline;

    class function criarComparadorIgualdadeComp    (const epsilon: Comp     = 0): IComparadorIgualdade<Comp>;     static; inline;
    class function criarComparadorIgualdadeSingle  (const epsilon: Single   = 0): IComparadorIgualdade<Single>;   static; inline;
    class function criarComparadorIgualdadeDouble  (const epsilon: Double   = 0): IComparadorIgualdade<Double>;   static; inline;
    class function criarComparadorIgualdadeExtended(const epsilon: Extended = 0): IComparadorIgualdade<Extended>; static; inline;
    class function criarComparadorIgualdadeCurrency(const epsilon: Currency = 0): IComparadorIgualdade<Currency>; static; inline;

    class property semente: Cardinal read fOriginal;
  end;

implementation
uses
  System.Classes,
  System.TypInfo,
  System.SysUtils,
  Recursos.Excecoes,
  Matematica.Constantes.Interfaces,
  Matematica.Constantes.Igualdades,
  Matematica.Constantes.Comparadores;

{ TMatematica }

class function TMatematica.criarComparador<T>(
  epsilon: Extended): IComparador<T>;
begin
  if (epsilon <= 0) then
    epsilon := Self.EPSILON;

  Result := IComparador<T>(SelecionarComparador(TypeInfo(T), epsilon));
end;

class function TMatematica.criarComparadorComp(
  const epsilon: Comp): IComparador<Comp>;
begin
  Result := criarComparador<Comp>(epsilon);
end;

class function TMatematica.criarComparadorCurrency(
  const epsilon: Currency): IComparador<Currency>;
begin
  Result := criarComparador<Currency>(epsilon);
end;

class function TMatematica.criarComparadorDouble(
  const epsilon: Double): IComparador<Double>;
begin
  Result := criarComparador<Double>(epsilon);
end;

class function TMatematica.criarComparadorExtended(
  const epsilon: Extended): IComparador<Extended>;
begin
  Result := criarComparador<Extended>(epsilon);
end;

class function TMatematica.criarComparadorIgualdadeComp(
  const epsilon: Comp): IComparadorIgualdade<Comp>;
begin
  Result := criarIgualdade<Comp>(epsilon);
end;

class function TMatematica.criarComparadorIgualdadeCurrency(
  const epsilon: Currency): IComparadorIgualdade<Currency>;
begin
  Result := criarIgualdade<Currency>(epsilon);
end;

class function TMatematica.criarComparadorIgualdadeDouble(
  const epsilon: Double): IComparadorIgualdade<Double>;
begin
  Result := criarIgualdade<Double>(epsilon);
end;

class function TMatematica.criarComparadorIgualdadeExtended(
  const epsilon: Extended): IComparadorIgualdade<Extended>;
begin
  Result := criarIgualdade<Extended>(epsilon);
end;

class function TMatematica.criarComparadorIgualdadeSingle(
  const epsilon: Single): IComparadorIgualdade<Single>;
begin
  Result := criarIgualdade<Single>(epsilon);
end;

class function TMatematica.criarComparadorSingle(
  const epsilon: Single): IComparador<Single>;
begin
  Result := criarComparador<Single>(epsilon);
end;

class function TMatematica.criarIgualdade<T>(
  epsilon: Extended): IComparadorIgualdade<T>;
begin
  if (epsilon <= 0) then
    epsilon := Self.EPSILON;

  Result := IComparadorIgualdade<T>(SelecionarIgualdade(TypeInfo(T), epsilon));
end;

class function TMatematica.proximo: Cardinal;
begin
  fSemente := (fSemente * 1048573 + 11) shr 32;
  Result   := fSemente;
end;

class function TMatematica.uniforme(const limite: Integer): Integer;
begin
  if (limite <= 0) then
    raise ArgumentoNegativo('limite') at ReturnAddress;

  Result := proximo mod UInt32(limite);
end;

class function TMatematica.uniforme: Double;
begin
  Result := (proximo / $10000) / $10000;
end;

initialization
  TMatematica.fOriginal := TThread.GetTickCount;
  TMatematica.fSemente  := TMatematica.fOriginal;

end.
