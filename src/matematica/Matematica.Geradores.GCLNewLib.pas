(*******************************************************************************
 *
 * Arquivo  : Matematica.Geradores.GCLNewLib.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-08-26 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Define o gerador congruencial linear com as especifica��es do
 *            New Lib, uma biblioteca padr�o para o C.
 *
 ******************************************************************************)
unit Matematica.Geradores.GCLNewLib;

interface
uses
  FractalLotus.Fundacao.Matematica.GeradorCongruencial;

type
  TGCLNewLib = class (TGeradorCongruencial)
  private
    fValor: UInt64;

    function proximo: Cardinal; inline;
  public
    constructor Create(const semente: Cardinal); override;

    function uniforme()                     : Double;  overload; override;
    function uniforme(const limite: Integer): Integer; overload; override;
  end;

implementation
uses
  System.SysUtils,
  Recursos.Excecoes;

{ TGCLNewLib }

constructor TGCLNewLib.Create(const semente: Cardinal);
begin
  inherited Create(semente);

  fValor := semente;
end;

function TGCLNewLib.proximo: Cardinal;
begin
  fValor := fValor * UInt64($5851F42D4C957F2D) + 1;
  Result := (fValor shr 32);
end;

function TGCLNewLib.uniforme(const limite: Integer): Integer;
begin
  if (limite <= 0) then
    raise ArgumentoNegativo('limite') at ReturnAddress;

  Result := proximo mod Cardinal(limite);
end;

function TGCLNewLib.uniforme: Double;
begin
  Result := proximo * (1.0 / $10000 / $10000); // 2^-32
end;

end.
