(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Cerne.Funcoes.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-08 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Cerne da Funda��o: aqui ser�o declaradas as fun��es procuradoras
 *            e somente elas. A unit FractalLotus.Fundacao.Cerne.pas est� sendo
 *            quebrada em v�rios arquivos para melhorar a legibilidade e a ma-
 *            nuten��o, os arquivos s�o:
 *              - FractalLotus.Fundacao.Cerne.Funcoes.pas
 *              - FractalLotus.Fundacao.Cerne.Colecoes.pas
 *              - FractalLotus.Fundacao.Cerne.ProtocolosFuncoes.pas
 *              - FractalLotus.Fundacao.Cerne.Estruturas.pas
 *            Para maiores informa��es sobre esses classificadores consulte a
 *            documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Cerne.Funcoes;

interface

type
  {$region 'Comparativas'}
  { Define uma fun��o an�nima para verificar a igualdade entre dois valores do }
  { tipo gen�rico T. Espera-se desta fun��o que ela contenha as mesmas propri- }
  { edades da igualdade matem�tica, ou seja, ela deve ser reflexiva, simetrica }
  { e transitiva.                                                              }
  TFuncaoIgualdade<T> = reference to function
  (
    const esquerda, direita: T
  ): Boolean;

  { Define um tipo de fun��o an�nima usada para criar uma rela��o de ordem em  }
  { um tipo gen�rico T. O retorno da fun��o � um n�mero inteiro que pode ter a }
  { seguinte interpreta��o, para a, b: T:                                      }
  {   - FuncaoComparacao(a, b) = 0 se, e somente se, a = b;                    }
  {   - FuncaoComparacao(a, b) < 0 se, e somente se, a < b;                    }
  {   - FuncaoComparacao(a, b) > 0 se, e somente se, a > b.                    }
  TFuncaoComparacao<T> = reference to function
  (
    const esquerda, direita: T
  ): Integer;

  { Define um tipo de fun��o an�nima sobre um tipo gen�rico T que deve validar }
  { se um valor de entrada respeita a um conjunto de crit�rios.                }
  TFuncaoPredicado<T> = reference to function(const valor: T): Boolean;
  {$endregion}

  {$region 'Redutivas'}
  { Define uma fun��o an�nima usada como uma fun��o agregadora que ser� apli-  }
  { cada �lguma cole��o. O tipo gen�rico TAcumulador deve corresponder ao re-  }
  { sultado agregado e o tipo gen�rico TFonte, ao tipo do elemento da cole��o. }
  TFuncaoReducao<TAcumulador, TFonte> = reference to function
  (
    const valor: TAcumulador; const item: TFonte
  ): TAcumulador;

  { Define uma fun��o an�nima usada como uma fun��o agregadora que ser� apli-  }
  { cada �lguma cole��o. O tipo gen�rico TFonte deve corresponder tanto ao     }
  { resultado agregado quanto ao tipo do elemento da cole��o.                  }
  TFuncaoReducaoSimples<TFonte> = reference to function
  (
    const valor, item: TFonte
  ): TFonte;
  {$endregion}

  {$region 'Projetivas'}
  { Define um tipo de fun��o an�nima usada para dar uma perspectiva diferente, }
  { TResult, ao valor do tipo gen�rico T.                                      }
  TFuncaoProjecao<T, TResult> = reference to function(const valor: T): TResult;

  { Define uma fun��o projetiva cujo valor resultante seja o mesmo de entrada. }
  TFuncaoIdentidade<T> = reference to function(const valor: T): T;

  { Define uma fun��o projetiva cujo resultado seja um inteiro que represente  }
  { o valor de entrada.                                                        }
  TFuncaoHash<T> = reference to function(const valor: T): Integer;

  { Define uma fun��o projetiva cujo resultado seja um string que represente   }
  { o valor de entrada.                                                        }
  TFuncaoString<T> = reference to function(const valor: T): string;
  {$endregion}

  {$region 'Gen�ricas'}
  { Define uma fun��o an�nima, sem par�metros de entrada e que retorne um va-  }
  { lor do tipo gen�rico TResult.                                              }
  TFuncao<TResult> = reference to function: TResult;

  { Define uma fun��o an�nima, com um par�metro de entrada do tipo T e que     }
  { retorne um valor do tipo gen�rico TResult.                                 }
  TFuncao1<T, TResult> = reference to function (const arg: T): TResult;

  { Define uma fun��o an�nima, com entradas T1 e T2 e que retorne um valor do  }
  { tipo TResult.                                                              }
  TFuncao2<T1, T2, TResult> = reference to function
  (
    const arg1: T1; const arg2: T2
  ): TResult;

  { Define uma fun��o an�nima, com entradas T1, T2 e T3 e que retorne um valor }
  { do tipo TResult.                                                           }
  TFuncao3<T1, T2, T3, TResult> = reference to function
  (
    const arg1: T1; const arg2: T2; const arg3: T3
  ): TResult;

  { Define uma fun��o an�nima, com entradas T1, T2, T3 e T4 e que retorne um   }
  { valor do tipo TResult.                                                     }
  TFuncao4<T1, T2, T3, T4, TResult> = reference to function
  (
    const arg1: T1; const arg2: T2; const arg3: T3; const arg4: T4
  ): TResult;

  { Define uma fun��o an�nima, com entradas T1, T2, T3, T4 e T5 e que retorne  }
  { um valor do tipo TResult.                                                  }
  TFuncao5<T1, T2, T3, T4, T5, TResult> = reference to function
  (
    const arg1: T1; const arg2: T2; const arg3: T3;
    const arg4: T4; const arg5: T5
  ): TResult;

  { Define uma fun��o an�nima, com entradas T1, T2, T3, T4, T5 e T6 e que      }
  { retorne um valor do tipo TResult.                                          }
  TFuncao6<T1, T2, T3, T4, T5, T6, TResult> = reference to function
  (
    const arg1: T1; const arg2: T2; const arg3: T3;
    const arg4: T4; const arg5: T5; const arg6: T6
  ): TResult;

  { Define uma fun��o an�nima, com entradas T1, T2, T3, T4, T5, T6 e T7 e que  }
  { retorne um valor do tipo TResult.                                          }
  TFuncao7<T1, T2, T3, T4, T5, T6, T7, TResult> = reference to function
  (
    const arg1: T1; const arg2: T2; const arg3: T3; const arg4: T4;
    const arg5: T5; const arg6: T6; const arg7: T7
  ): TResult;

  { Define uma fun��o an�nima, com entradas T1, T2, T3, T4, T5, T6, T7 e T8 e  }
  { que retorne um valor do tipo TResult.                                      }
  TFuncao8<T1, T2, T3, T4, T5, T6, T7, T8, TResult> = reference to function
  (
    const arg1: T1; const arg2: T2; const arg3: T3; const arg4: T4;
    const arg5: T5; const arg6: T6; const arg7: T7; const arg8: T8
  ): TResult;

  { Define uma fun��o an�nima, com entradas T1, T2, T3, T4, T5, T6, T7, T8 e   }
  { T9 e que retorne um valor do tipo TResult.                                 }
  TFuncao9<T1, T2, T3, T4, T5, T6, T7, T8, T9, TResult> = reference to function
  (
    const arg1: T1; const arg2: T2; const arg3: T3;
    const arg4: T4; const arg5: T5; const arg6: T6;
    const arg7: T7; const arg8: T8; const arg9: T9
  ): TResult;
  {$endregion}

  {$region 'Precedimentos'}
  { Define um procedimento an�nimo sem par�metros de entrada.                  }
  TProcedimento = reference to procedure;

  { Define um procedimento an�nimo com parametro de entrada T.                 }
  TProcedimento1<T> = reference to procedure (const arg: T);

  { Define um procedimento an�nimo com parametros de entrada T1 e T2.          }
  TProcedimento2<T1, T2> = reference to procedure (const arg1: T1; const arg2: T2);

  { Define um procedimento an�nimo com parametros de entrada T1, T2 e T3.      }
  TProcedimento3<T1, T2, T3> = reference to procedure
  (
    const arg1: T1; const arg2: T2; const arg3: T3
  );

  { Define um procedimento an�nimo com parametros de entrada T1, T2, T3 e T4.  }
  TProcedimento4<T1, T2, T3, T4> = reference to procedure
  (
    const arg1: T1; const arg2: T2; const arg3: T3; const arg4: T4
  );

  { Define um procedimento an�nimo com parametros de entrada T1, T2, T3, T4 e  }
  { T5.                                                                        }
  TProcedimento5<T1, T2, T3, T4, T5> = reference to procedure
  (
    const arg1: T1; const arg2: T2; const arg3: T3; const arg4: T4; const arg5: T5
  );

  { Define um procedimento an�nimo com parametros de entrada T1, T2, T3, T4,   }
  { T5 e T6.                                                                   }
  TProcedimento6<T1, T2, T3, T4, T5, T6> = reference to procedure
  (
    const arg1: T1; const arg2: T2; const arg3: T3;
    const arg4: T4; const arg5: T5; const arg6: T6
  );

  { Define um procedimento an�nimo com parametros de entrada T1, T2, T3, T4,   }
  { T5, T6 e T7.                                                               }
  TProcedimento7<T1, T2, T3, T4, T5, T6, T7> = reference to procedure
  (
    const arg1: T1; const arg2: T2; const arg3: T3; const arg4: T4;
    const arg5: T5; const arg6: T6; const arg7: T7
  );

  { Define um procedimento an�nimo com parametros de entrada T1, T2, T3, T4,   }
  { T5, T6, T7 e T8.                                                           }
  TProcedimento8<T1, T2, T3, T4, T5, T6, T7, T8> = reference to procedure
  (
    const arg1: T1; const arg2: T2; const arg3: T3; const arg4: T4;
    const arg5: T5; const arg6: T6; const arg7: T7; const arg8: T8
  );

  { Define um procedimento an�nimo com parametros de entrada T1, T2, T3, T4,   }
  { T5, T6, T7, T8 e T9.                                                       }
  TProcedimento9<T1, T2, T3, T4, T5, T6, T7, T8, T9> = reference to procedure
  (
    const arg1: T1; const arg2: T2; const arg3: T3;
    const arg4: T4; const arg5: T5; const arg6: T6;
    const arg7: T7; const arg8: T8; const arg9: T9
  );
  {$endregion}

implementation

end.
