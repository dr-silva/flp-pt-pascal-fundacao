(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Cerne.Colecoes.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-08 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Cerne da Funda��o: aqui ser�o declaradas as estruturas b�sicas de
 *            projetos da Fractal Lotus. A unit FractalLotus.Fundacao.Cerne.pas
 *            est� sendo quebrada em v�rios arquivos para melhorar a legibilida-
 *            de e a manuten��o, os arquivos s�o:
 *              - FractalLotus.Fundacao.Cerne.Funcoes.pas
 *              - FractalLotus.Fundacao.Cerne.Colecoes.pas
 *              - FractalLotus.Fundacao.Cerne.ProtocolosFuncoes.pas
 *              - FractalLotus.Fundacao.Cerne.Estruturas.pas
 *            Para maiores informa��es sobre esses classificadores consulte a
 *            documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Cerne.Estruturas;

interface

type
  { Esta � uma estrutura para armazenar as informa��es dum intervalo de tempo. }
  TPeriodo = record
  private
    fDias         : Integer;
    fHoras        ,
    fMinutos      ,
    fSegundos     : ShortInt;
    fMilissegundos: SmallInt;

    {$region 'Assistentes'}
    class procedure periodizar(const valor: Int64; var periodo: TPeriodo); static;
    class function valorar(const periodo: TPeriodo): Int64; static;
    class function anel(const valor, modulo: Integer; var adicional: Integer): Integer; static;
    {$endregion}
  public
    {$region 'Construtures'}
    constructor Create
    (
      const dias: Integer;
      const horas, minutos, segundos, milissegundos: ShortInt
    ); overload;
    constructor Create(const inicio, fim: TDateTime); overload;
    {$endregion}

    {$region 'Consultas'}
    function eIgual(const valor: TPeriodo): Boolean;    overload; inline;
    function compararA(const valor: TPeriodo): Integer;           inline;
    function obterHash: Integer;                        overload; inline;
    function toString: string;                          overload; inline;
    {$endregion}

    {$region 'Assistentes'}
    class function eIgual(const esquerda, direita: TPeriodo): Boolean;        overload; static; inline;
    class function comparar(const esquerda, direita: TPeriodo): Integer;                static;
    class function obterHash(const valor: TPeriodo): Integer;                 overload; static; inline;
    class function toString(const valor: TPeriodo): string                    overload; static;
    class function parse(const str: string): TPeriodo;                                  static;
    class function tryParse(const str: string; out valor: TPeriodo): Boolean;           static;
    {$endregion}

    {$region 'Operadores'}
    class operator Equal             (a, b: TPeriodo): Boolean;
    class operator NotEqual          (a, b: TPeriodo): Boolean;
    class operator GreaterThan       (a, b: TPeriodo): Boolean;
    class operator LessThan          (a, b: TPeriodo): Boolean;
    class operator GreaterThanOrEqual(a, b: TPeriodo): Boolean;
    class operator LessThanOrEqual   (a, b: TPeriodo): Boolean;

    class operator Positive(a: TPeriodo): TPeriodo;
    class operator Negative(a: TPeriodo): TPeriodo;

    class operator Add     (a, b: TPeriodo): TPeriodo;
    class operator Subtract(a, b: TPeriodo): TPeriodo;
    {$endregion}

    {$region 'Propriedades'}
    property dias         : Integer  read fDias;
    property horas        : ShortInt read fHoras;
    property minutos      : ShortInt read fMinutos;
    property segundos     : ShortInt read fSegundos;
    property milissegundos: SmallInt read fMilissegundos;
    {$endregion}
  end;

  { Esta classe tem o objetivo de contar o tempo decorrido entre o "agora" e a }
  { inicializa��o, que pode ser o momento de sua instancia��o ou a �ltima vez  }
  { que foi reiniciado.                                                        }
  TTemporizador = class
  private
    fInicio: TDateTime;
  public
    constructor Create;

    procedure reiniciar;
    function decorrer                    : TPeriodo; overload; inline;
    function decorrer(reiniciar: Boolean): TPeriodo; overload; inline;
  end;

implementation
uses
  System.SysUtils, System.DateUtils,
  Recursos.Excecoes;

{ TPeriodo }

class operator TPeriodo.Add(a, b: TPeriodo): TPeriodo;
begin
  Result := TPeriodo.Create
  (
    a.dias          + b.dias,
    a.horas         + b.horas,
    a.minutos       + b.minutos,
    a.segundos      + b.segundos,
    a.milissegundos + b.milissegundos
  );
end;

class function TPeriodo.anel(const valor, modulo: Integer;
  var adicional: Integer): Integer;
var
  eNegativo: Boolean;
begin
  eNegativo := (valor < 0);

  if (eNegativo) then
    Result := -valor
  else
    Result := valor;

  adicional := Result div modulo;
  Result    := Result mod modulo;

  if (eNegativo) then
  begin
    Result    := modulo - Result;
    adicional := -(adicional + 1);
  end;
end;

class function TPeriodo.comparar(const esquerda, direita: TPeriodo): Integer;
begin
  if (esquerda < direita) then
    Result := -1
  else if (esquerda > direita) then
    Result := 1
  else
    Result := 0;
end;

function TPeriodo.compararA(const valor: TPeriodo): Integer;
begin
  Result := comparar(Self, valor);
end;

constructor TPeriodo.Create(const dias: Integer; const horas, minutos, segundos,
  milissegundos: ShortInt);
var
  adicional: Integer;
begin
  adicional      := 0;
  fMilissegundos := anel(milissegundos, MSecsPerSec, adicional);
  fSegundos      := anel(segundos + adicional, SecsPerMin, adicional);
  fMinutos       := anel(minutos  + adicional, MinsPerHour, adicional);
  fHoras         := anel(horas    + adicional, HoursPerDay, adicional);
  fDias          := dias + adicional;
end;

constructor TPeriodo.Create(const inicio, fim: TDateTime);
begin
  periodizar(MilliSecondsBetween(fim, inicio), Self);
end;

class function TPeriodo.eIgual(const esquerda, direita: TPeriodo): Boolean;
begin
  Result := (esquerda = direita);
end;

function TPeriodo.eIgual(const valor: TPeriodo): Boolean;
begin
  Result := eIgual(Self, valor);
end;

class operator TPeriodo.Equal(a, b: TPeriodo): Boolean;
begin
  Result := (a.dias          = b.dias)     and
            (a.horas         = b.horas)    and
            (a.minutos       = b.minutos)  and
            (a.segundos      = b.segundos) and
            (a.milissegundos = b.milissegundos);
end;

class operator TPeriodo.GreaterThan(a, b: TPeriodo): Boolean;
begin
  Result :=
  (a.dias > b.dias) or
  ((a.dias = b.dias) and (a.horas > b.horas)) or
  ((a.dias = b.dias) and (a.horas = b.horas) and (a.minutos > b.minutos)) or
  ((a.dias = b.dias) and (a.horas = b.horas) and (a.minutos = b.minutos) and
   (a.segundos > b.segundos)) or
  ((a.dias = b.dias) and (a.horas = b.horas) and (a.minutos = b.minutos) and
   (a.segundos = b.segundos) and (a.milissegundos > b.milissegundos));
end;

class operator TPeriodo.GreaterThanOrEqual(a, b: TPeriodo): Boolean;
begin
  Result :=
  (a.dias > b.dias) or
  ((a.dias = b.dias) and (a.horas > b.horas)) or
  ((a.dias = b.dias) and (a.horas = b.horas) and (a.minutos > b.minutos)) or
  ((a.dias = b.dias) and (a.horas = b.horas) and (a.minutos = b.minutos) and
   (a.segundos > b.segundos)) or
  ((a.dias = b.dias) and (a.horas = b.horas) and (a.minutos = b.minutos) and
   (a.segundos = b.segundos) and (a.milissegundos >= b.milissegundos));
end;

class operator TPeriodo.LessThan(a, b: TPeriodo): Boolean;
begin
  Result := (b > a);
end;

class operator TPeriodo.LessThanOrEqual(a, b: TPeriodo): Boolean;
begin
  Result := (b >= a);
end;

class operator TPeriodo.Negative(a: TPeriodo): TPeriodo;
begin
  Result := TPeriodo.Create
  (
    -a.dias,
    -a.horas,
    -a.minutos,
    -a.segundos,
    -a.milissegundos
  );
end;

class operator TPeriodo.NotEqual(a, b: TPeriodo): Boolean;
begin
  Result := not (a = b);
end;

function TPeriodo.obterHash: Integer;
begin
  Result := valorar(Self);
end;

class function TPeriodo.obterHash(const valor: TPeriodo): Integer;
begin
  Result := valorar(valor);
end;

class function TPeriodo.parse(const str: string): TPeriodo;
begin
  if (not tryParse(str, Result)) then
    raise PeriodoInvalido('str') at ReturnAddress;
end;

class procedure TPeriodo.periodizar(const valor: Int64; var periodo: TPeriodo);
var
  adicional: Integer;
begin
  periodo.fMilissegundos := anel(valor, MSecsPerSec, adicional);
  periodo.fSegundos      := anel(adicional, SecsPerMin, adicional);
  periodo.fMinutos       := anel(adicional, MinsPerHour, adicional);
  periodo.fHoras         := anel(adicional, HoursPerDay, adicional);
  periodo.fDias          := adicional;
end;

class operator TPeriodo.Positive(a: TPeriodo): TPeriodo;
begin
  Result := a;
end;

class operator TPeriodo.Subtract(a, b: TPeriodo): TPeriodo;
begin
  Result := TPeriodo.Create
  (
    a.dias          - b.dias,
    a.horas         - b.horas,
    a.minutos       - b.minutos,
    a.segundos      - b.segundos,
    a.milissegundos - b.milissegundos
  );
end;

function TPeriodo.toString: string;
begin
  Result := toString(Self);
end;

class function TPeriodo.toString(const valor: TPeriodo): string;
begin
  Result := string.Format
  (
    '%d:%.2d:%.2d:%.2d.%.3d',
    [
      valor.dias,
      valor.horas,
      valor.minutos,
      valor.segundos,
      valor.milissegundos
    ]
  );
end;

class function TPeriodo.tryParse(const str: string;
  out valor: TPeriodo): Boolean;
var
  valores: TArray<string>;
begin
  Result := False;
  try
    valores := str.Split([':', ':', ':', '.']);

    if (Length(valores) = 5) then
    begin
      valor := TPeriodo.Create
      (
        StrToInt(valores[0]),
        StrToInt(valores[1]),
        StrToInt(valores[2]),
        StrToInt(valores[3]),
        StrToInt(valores[4])
      );

      Result := True;
    end;
  except
    Result := False;
  end;
end;

class function TPeriodo.valorar(const periodo: TPeriodo): Int64;
begin
  Result :=  periodo.milissegundos +
            (periodo.segundos * MSecsPerSec) +
            (periodo.minutos  * SecsPerMin  * MSecsPerSec) +
            (periodo.horas    * MinsPerHour * SecsPerMin  * MSecsPerSec) +
            (periodo.dias     * HoursPerDay * MinsPerHour * SecsPerMin * MSecsPerSec);
end;

{ TTemporizador }

constructor TTemporizador.Create;
begin
  reiniciar;
end;

function TTemporizador.decorrer: TPeriodo;
begin
  Result := TPeriodo.Create(fInicio, Now);
end;

function TTemporizador.decorrer(reiniciar: Boolean): TPeriodo;
begin
  Result := TPeriodo.Create(fInicio, Now);
  if (reiniciar) then
    Self.reiniciar;
end;

procedure TTemporizador.reiniciar;
begin
  fInicio := Now;
end;

end.
