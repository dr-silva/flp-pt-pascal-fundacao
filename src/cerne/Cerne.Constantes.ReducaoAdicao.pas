(*******************************************************************************
 *
 * Arquivo  : Cerne.Constantes.ReducaoAdicao.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-08 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Guarda as inst�ncias do protocolo IRedutor, quando este redutor
 *            for do tipo aditivo (operador +) para os tipos primitivos do Del-
 *            phi (Object Pascal).
 *
 ******************************************************************************)
unit Cerne.Constantes.ReducaoAdicao;

interface
uses
  System.TypInfo, Cerne.Constantes.Interfaces;

{$region 'Procuradores'}
function ReducaoAdicaoShortInt(instancia: Pointer; const valor, item: ShortInt): ShortInt;
function ReducaoAdicaoByte    (instancia: Pointer; const valor, item: Byte    ): Byte;
function ReducaoAdicaoSmallInt(instancia: Pointer; const valor, item: SmallInt): SmallInt;
function ReducaoAdicaoWord    (instancia: Pointer; const valor, item: Word    ): Word;
function ReducaoAdicaoInteger (instancia: Pointer; const valor, item: Integer ): Integer;
function ReducaoAdicaoCardinal(instancia: Pointer; const valor, item: Cardinal): Cardinal;
function ReducaoAdicaoInt64   (instancia: Pointer; const valor, item: Int64   ): Int64;
function ReducaoAdicaoUInt64  (instancia: Pointer; const valor, item: UInt64  ): UInt64;

function ReducaoAdicaoSingle  (instancia: Pointer; const valor, item: Single  ): Single;
function ReducaoAdicaoDouble  (instancia: Pointer; const valor, item: Double  ): Double;
function ReducaoAdicaoExtended(instancia: Pointer; const valor, item: Extended): Extended;
function ReducaoAdicaoComp    (instancia: Pointer; const valor, item: Comp    ): Comp;
function ReducaoAdicaoCurrency(instancia: Pointer; const valor, item: Currency): Currency;

function ReducaoAdicaoString       (instancia: Pointer; const valor, item: String       ): string;
function ReducaoAdicaoAnsiString   (instancia: Pointer; const valor, item: AnsiString   ): AnsiString;
function ReducaoAdicaoWideString   (instancia: Pointer; const valor, item: WideString   ): WideString;
function ReducaoAdicaoUnicodeString(instancia: Pointer; const valor, item: UnicodeString): UnicodeString;
{$endregion}

{$region 'Instancia��o'}
function SelecionarReducaoAdicaoInteger (info: PTypeInfo): Pointer; inline;
function SelecionarReducaoAdicaoInt64   (info: PTypeInfo): Pointer; inline;
function SelecionarReducaoAdicaoFloat   (info: PTypeInfo): Pointer; inline;
function LocalizarInstanciaReducaoAdicao(info: PTypeInfo; tamanho: Integer): Pointer; inline;
{$endregion}

const
  {$region 'Tabelas Info'}
  TabelaInfoReducaoAdicaoShortInt: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoShortInt
  );
  TabelaInfoReducaoAdicaoByte: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoByte
  );
  TabelaInfoReducaoAdicaoSmallInt: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoSmallInt
  );
  TabelaInfoReducaoAdicaoWord: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoWord
  );
  TabelaInfoReducaoAdicaoInteger: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoInteger
  );
  TabelaInfoReducaoAdicaoCardinal: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoCardinal
  );
  TabelaInfoReducaoAdicaoInt64: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoInt64
  );
  TabelaInfoReducaoAdicaoUInt64: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoUInt64
  );

  TabelaInfoReducaoAdicaoSingle: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoSingle
  );
  TabelaInfoReducaoAdicaoDouble: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoDouble
  );
  TabelaInfoReducaoAdicaoExtended: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoExtended
  );
  TabelaInfoReducaoAdicaoComp: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoComp
  );
  TabelaInfoReducaoAdicaoCurrency: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoCurrency
  );

  TabelaInfoReducaoAdicaoString: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoString
  );
  TabelaInfoReducaoAdicaoAnsiString: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoAnsiString
  );
  TabelaInfoReducaoAdicaoWideString: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoWideString
  );
  TabelaInfoReducaoAdicaoUnicodeString: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoAdicaoUnicodeString
  );
  {$endregion}

  {$region 'Inst�ncias'}
  InstanciaReducaoAdicaoShortInt: Pointer = @TabelaInfoReducaoAdicaoShortInt;
  InstanciaReducaoAdicaoByte    : Pointer = @TabelaInfoReducaoAdicaoByte;
  InstanciaReducaoAdicaoSmallInt: Pointer = @TabelaInfoReducaoAdicaoSmallInt;
  InstanciaReducaoAdicaoWord    : Pointer = @TabelaInfoReducaoAdicaoWord;
  InstanciaReducaoAdicaoInteger : Pointer = @TabelaInfoReducaoAdicaoInteger;
  InstanciaReducaoAdicaoCardinal: Pointer = @TabelaInfoReducaoAdicaoCardinal;
  InstanciaReducaoAdicaoInt64   : Pointer = @TabelaInfoReducaoAdicaoInt64;
  InstanciaReducaoAdicaoUInt64  : Pointer = @TabelaInfoReducaoAdicaoUInt64;

  InstanciaReducaoAdicaoSingle  : Pointer = @TabelaInfoReducaoAdicaoSingle;
  InstanciaReducaoAdicaoDouble  : Pointer = @TabelaInfoReducaoAdicaoDouble;
  InstanciaReducaoAdicaoExtended: Pointer = @TabelaInfoReducaoAdicaoExtended;
  InstanciaReducaoAdicaoComp    : Pointer = @TabelaInfoReducaoAdicaoComp;
  InstanciaReducaoAdicaoCurrency: Pointer = @TabelaInfoReducaoAdicaoCurrency;

  InstanciaReducaoAdicaoString       : Pointer = @TabelaInfoReducaoAdicaoString;
  InstanciaReducaoAdicaoAnsiString   : Pointer = @TabelaInfoReducaoAdicaoAnsiString;
  InstanciaReducaoAdicaoWideString   : Pointer = @TabelaInfoReducaoAdicaoWideString;
  InstanciaReducaoAdicaoUnicodeString: Pointer = @TabelaInfoReducaoAdicaoUnicodeString;
  {$endregion}

implementation

{$region 'Procuradores'}
function ReducaoAdicaoShortInt(instancia: Pointer; const valor, item: ShortInt): ShortInt;
begin
  Result := valor + item;
end;

function ReducaoAdicaoByte(instancia: Pointer; const valor, item: Byte): Byte;
begin
  Result := valor + item;
end;

function ReducaoAdicaoSmallInt(instancia: Pointer; const valor, item: SmallInt): SmallInt;
begin
  Result := valor + item;
end;

function ReducaoAdicaoWord(instancia: Pointer; const valor, item: Word): Word;
begin
  Result := valor + item;
end;

function ReducaoAdicaoInteger(instancia: Pointer; const valor, item: Integer): Integer;
begin
  Result := valor + item;
end;

function ReducaoAdicaoCardinal(instancia: Pointer; const valor, item: Cardinal): Cardinal;
begin
  Result := valor + item;
end;

function ReducaoAdicaoInt64(instancia: Pointer; const valor, item: Int64): Int64;
begin
  Result := valor + item;
end;

function ReducaoAdicaoUInt64(instancia: Pointer; const valor, item: UInt64): UInt64;
begin
  Result := valor + item;
end;

function ReducaoAdicaoSingle(instancia: Pointer; const valor, item: Single): Single;
begin
  Result := valor + item;
end;

function ReducaoAdicaoDouble(instancia: Pointer; const valor, item: Double): Double;
begin
  Result := valor + item;
end;

function ReducaoAdicaoExtended(instancia: Pointer; const valor, item: Extended): Extended;
begin
  Result := valor + item;
end;

function ReducaoAdicaoComp(instancia: Pointer; const valor, item: Comp): Comp;
begin
  Result := valor + item;
end;

function ReducaoAdicaoCurrency(instancia: Pointer; const valor, item: Currency): Currency;
begin
  Result := valor + item;
end;

function ReducaoAdicaoString(instancia: Pointer; const valor, item: String): string;
begin
  Result := valor + item;
end;

function ReducaoAdicaoAnsiString(instancia: Pointer; const valor, item: AnsiString): AnsiString;
begin
  Result := valor + item;
end;

function ReducaoAdicaoWideString(instancia: Pointer; const valor, item: WideString): WideString;
begin
  Result := valor + item;
end;

function ReducaoAdicaoUnicodeString(instancia: Pointer; const valor, item: UnicodeString): UnicodeString;
begin
  Result := valor + item;
end;
{$endregion}

{$region 'Instancia��o'}
function SelecionarReducaoAdicaoInteger(info: PTypeInfo ): Pointer;
begin
  case GetTypeData(info)^.OrdType of
    otSByte: Result := @InstanciaReducaoAdicaoShortInt;
    otUByte: Result := @InstanciaReducaoAdicaoByte;
    otSWord: Result := @InstanciaReducaoAdicaoSmallInt;
    otUWord: Result := @InstanciaReducaoAdicaoWord;
    otSLong: Result := @InstanciaReducaoAdicaoInteger;
    otULong: Result := @InstanciaReducaoAdicaoCardinal;
    else     Result := nil;
  end;
end;

function SelecionarReducaoAdicaoInt64(info: PTypeInfo ): Pointer;
var
  data: PTypeData;
begin
  data := GetTypeData(info);

  if (data^.MaxInt64Value > data^.MinInt64Value) then
    Result := @InstanciaReducaoAdicaoInt64
  else
    Result := @InstanciaReducaoAdicaoUInt64;
end;

function SelecionarReducaoAdicaoFloat(info: PTypeInfo ): Pointer;
begin
  case GetTypeData(info)^.FloatType of
    ftSingle  : Result := @InstanciaReducaoAdicaoSingle;
    ftDouble  : Result := @InstanciaReducaoAdicaoDouble;
    ftExtended: Result := @InstanciaReducaoAdicaoExtended;
    ftComp    : Result := @InstanciaReducaoAdicaoComp;
    ftCurr    : Result := @InstanciaReducaoAdicaoCurrency;
    else        Result := nil;
  end;
end;

function LocalizarInstanciaReducaoAdicao(info: PTypeInfo; tamanho: Integer): Pointer;
begin
  if (info <> nil) then
    case info^.Kind of
      tkChar       ,
      tkWChar      ,
      tkInteger    ,
      tkEnumeration: Result := SelecionarReducaoAdicaoInteger(info);
      tkInt64      : Result := SelecionarReducaoAdicaoInt64  (info);
      tkFloat      : Result := SelecionarReducaoAdicaoFloat  (info);

      tkString     : Result := @InstanciaReducaoAdicaoString;
      tkLString    : Result := @InstanciaReducaoAdicaoAnsiString;
      tkWString    : Result := @InstanciaReducaoAdicaoWideString;
      tkUString    : Result := @InstanciaReducaoAdicaoUnicodeString;
      else           Result := nil;
    end
  else
    Result := nil;
end;
{$endregion}

end.
