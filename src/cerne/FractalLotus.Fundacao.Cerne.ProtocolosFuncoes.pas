(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Cerne.ProtocolosFuncoes.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-08 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Cerne da Funda��o: aqui ser�o declaradas os protocolos das fun��es
 *            procuradoras, que s�o as fun��es an�nimas. A unit
 *            FractalLotus.Fundacao.Cerne.pas est� sendo quebrada em v�rios ar-
 *            quivos para melhorar a legibilidade e a manuten��o, os arquivos
 *            s�o:
 *              - FractalLotus.Fundacao.Cerne.Funcoes.pas
 *              - FractalLotus.Fundacao.Cerne.Colecoes.pas
 *              - FractalLotus.Fundacao.Cerne.ProtocolosFuncoes.pas
 *              - FractalLotus.Fundacao.Cerne.Estruturas.pas
 *            Para maiores informa��es sobre esses classificadores consulte a
 *            documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

interface
uses
  System.Generics.Defaults, FractalLotus.Fundacao.Cerne.Funcoes;

type
  {$region 'Igualdade'}
  { Define um protocolo que deve ser implementado para classes que necessitem  }
  { de suporte � compara��o de igualde e/ou precisem do c�digo hash.           }
  IComparadorIgualdade<T> = interface(IEqualityComparer<T>)
    ['{70E83EE6-FFD5-4D6A-9E6A-E69A898B2FE0}']
  end;

  { Classe para criar instancias da interface IComparadorIgualdade.            }
  TFabricaComparadorIgualdade = class sealed
  public
    class function criar<T>: IComparadorIgualdade<T>; overload; static;
    class function criar<T>
    (
      const igualdade: TFuncaoIgualdade<T>
    ): IComparadorIgualdade<T>; overload; static;
    class function criar<T>
    (
      const hash: TFuncaoHash<T>
    ): IComparadorIgualdade<T>; overload; static;
    class function criar<T>
    (
      const igualdade: TFuncaoIgualdade<T>; const hash: TFuncaoHash<T>
    ): IComparadorIgualdade<T>; overload; static;
  end;
  {$endregion}

  {$region 'Compara��o'}
  { Define um protocolo que deve ser implementado para classes que necessitem  }
  { de uma rela��o de ordem.                                                   }
  IComparador<T> = interface(IComparer<T>)
    ['{6F80C1EF-E127-4F27-8EF6-4341B6CB4DE7}']
  end;

  { Classe para criar instancias da interface IComparador.                     }
  TFabricaComparador = class sealed
  public
    class function criar<T>: IComparador<T>; overload; static;
    class function criar<T>
    (
      comparacao: TFuncaoComparacao<T>
    ): IComparador<T>; overload; static;
  end;
  {$endregion}

  {$region 'Predicado'}
  { Define uma interface que deve ser implementada quando for necess�rio algum }
  { um predicado para uma classe.                                              }
  IPredicado<T> = interface
    ['{B07400B4-FA0F-4506-9987-E64616469217}']

    function validar(const valor: T): Boolean;
  end;

  { Classe para criar instancias da interface IPredicado.                      }
  TFabricaPredicado = class sealed
  public
    class function criar<T>: IPredicado<T>; overload; static;
    class function criar<T>
    (
      const predicado: TFuncaoPredicado<T>
    ): IPredicado<T>; overload; static;
  end;
  {$endregion}

  {$region 'Redu��o'}
  { Define uma interface que deve ser implementada quando for preciso fazer    }
  { uma agrega��o dalguma cole��o do tipo gen�rico TFonte. O tipo gen�rico     }
  { TAcumulador representa o tipo de retorno da agrega��o.                     }
  IRedutor<TAcumulador, TFonte> = interface
    ['{BD32FA9F-AEFB-4696-8A2D-50D6EDB4ABEB}']

    function reduzir(const valor: TAcumulador; const item: TFonte): TAcumulador;
  end;

  { Classe para criar instancias da interface IRedutor.                        }
  TFabricaRedutor = class sealed
  public
    class function criar<TAcumulador, TFonte>
    (
      const reducao: TFuncaoReducao<TAcumulador, TFonte>
    ): IRedutor<TAcumulador,TFonte>; overload; static;
    class function criar<TFonte>
    (
      const reducao: TFuncaoReducaoSimples<TFonte>
    ): IRedutor<TFonte, TFonte>; overload; static;

    class function criarSoma         <TFonte>: IRedutor<TFonte, TFonte>; static;
    class function criarMultiplicacao<TFonte>: IRedutor<TFonte, TFonte>; static;
  end;
  {$endregion}

  {$region 'Proje��o'}
  { Define um protocolo que deve ser implementado para classes que queiram     }
  { realizar algum tipo de "convers�o" feita do tipo gen�rico T para o tipo    }
  { gen�rico TResult.                                                          }
  IProjetor<T, TResult> = interface
    ['{09AA923A-0CEB-482B-9EE9-06B869F58221}']

    function projetar(const valor: T): TResult;
  end;

  { Classe para criar instancias da interface IProjetor.                       }
  TFabricaProjetor = class sealed
  public
    class function criar<T>: IProjetor<T, T>; overload; static;
    class function criar<T, TResult>
    (
      const projecao: TFuncaoProjecao<T, TResult>
    ): IProjetor<T, TResult>; overload; static;

    class function criarString<T>: IProjetor<T, string>; overload; static;
    class function criarString<T>
    (
      const funcao: TFuncaoString<T>
    ): IProjetor<T, string>; overload; static;
  end;
  {$endregion}

  {$region 'Correla��o'}
  { Define uma interface que deve ser implementada para classes que precisem   }
  { estabelecer um uma rela��o entre dois valores de tipos gen�ricos distintos }
  { (TEsquerda e TDireita). Caso esta rela��o entre os dois valores se confir- }
  { me, eles servir�o de base para criar um valor resultante.                  }
  ICorrelacionamento<TEsquerda, TDireita, TResult> = interface
    ['{75DCB452-DA3F-41E3-B6FF-2021E0D8B573}']

    function obterCorrelacao: TResult;
    function correlacionar
    (
      const esquerda: TEsquerda; const direita: TDireita
    ): Boolean;

    property correlacao: TResult read obterCorrelacao;
  end;

  { Classe para criar instancias da interface ICorrelacionamento.              }
  TFabricaCorrelacionamento = class sealed
  public
    class function criar<TFonte, TChave, TResult>
    (
      const projecao  : TFuncaoProjecao<TFonte, TChave>;
      const resultante: TFuncao2<TFonte, TFonte, TResult>
    ): ICorrelacionamento<TFonte, TFonte, TResult>; overload; static;
    class function criar<TFonte, TChave, TResult>
    (
      const projecao  : TFuncaoProjecao<TFonte, TChave>;
      const resultante: TFuncao2<TFonte, TFonte, TResult>;
      const igualdade : TFuncaoIgualdade<TChave>
    ): ICorrelacionamento<TFonte, TFonte, TResult>; overload; static;
    class function criar<TFonte, TChave, TResult>
    (
      const projecao  : TFuncaoProjecao<TFonte, TChave>;
      const resultante: TFuncao2<TFonte, TFonte, TResult>;
      const comparador: IComparadorIgualdade<TChave>
    ): ICorrelacionamento<TFonte, TFonte, TResult>; overload; static;

    class function criar<TEsquerda, TDireita, TChave, TResult>
    (
      const projecaoEsquerda: TFuncaoProjecao<TEsquerda, TChave>;
      const projecaoDireita : TFuncaoProjecao<TDireita, TChave>;
      const resultante      : TFuncao2<TEsquerda, TDireita, TResult>
    ): ICorrelacionamento<TEsquerda, TDireita, TResult>; overload; static;
    class function criar<TEsquerda, TDireita, TChave, TResult>
    (
      const projecaoEsquerda: TFuncaoProjecao<TEsquerda, TChave>;
      const projecaoDireita : TFuncaoProjecao<TDireita, TChave>;
      const resultante      : TFuncao2<TEsquerda, TDireita, TResult>;
      const igualdade       : TFuncaoIgualdade<TChave>
    ): ICorrelacionamento<TEsquerda, TDireita, TResult>; overload; static;
    class function criar<TEsquerda, TDireita, TChave, TResult>
    (
      const projecaoEsquerda: TFuncaoProjecao<TEsquerda, TChave>;
      const projecaoDireita : TFuncaoProjecao<TDireita, TChave>;
      const resultante      : TFuncao2<TEsquerda, TDireita, TResult>;
      const comparador      : IComparadorIgualdade<TChave>
    ): ICorrelacionamento<TEsquerda, TDireita, TResult>; overload; static;
  end;
  {$endregion}

  {$region 'Ordena��o'}
  { Define um protocolo que deve ser implementado por classes que queiram re-  }
  { organizar um array.                                                        }
  IOrdenacao<T> = interface
    ['{4C5D33B8-DD84-46AF-B4C2-EEDBD41C548C}']

    procedure ordenar(itens: TArray<T>);
  end;

  { Define um protocolo que deve ser implementado por classes que queiram ave- }
  { riguar se um array est� em uma determinada ordem.                          }
  IValidadorOrdenacao<T> = interface
    ['{5D9EC52C-A3C3-4787-AA4B-378C01562910}']

    function validar(itens: TArray<T>): Boolean;
  end;
  {$endregion}

implementation
  uses
    System.TypInfo,
    Cerne.Procuradores,
    Cerne.Constantes.Interfaces,
    Cerne.Constantes.Predicado,
    Cerne.Constantes.ReducaoAdicao,
    Cerne.Constantes.ReducaoMultiplicacao,
    Cerne.Constantes.ProjecaoString;

{ TFabricaComparadorIgualdade }

class function TFabricaComparadorIgualdade.criar<T>(
  const igualdade: TFuncaoIgualdade<T>): IComparadorIgualdade<T>;
begin
  Result := TComparadorIgualdade<T>.Create(igualdade, nil);
end;

class function TFabricaComparadorIgualdade.criar<T>: IComparadorIgualdade<T>;
begin
  Result := IComparadorIgualdade<T>
  (
    _LookupVtableInfo(giEqualityComparer, TypeInfo(T), SizeOf(T))
  );
end;

class function TFabricaComparadorIgualdade.criar<T>(
  const igualdade: TFuncaoIgualdade<T>;
  const hash: TFuncaoHash<T>): IComparadorIgualdade<T>;
begin
  Result := TComparadorIgualdade<T>.Create(igualdade, hash);
end;

class function TFabricaComparadorIgualdade.criar<T>(
  const hash: TFuncaoHash<T>): IComparadorIgualdade<T>;
begin
  Result := TComparadorIgualdade<T>.Create(nil, hash);
end;

{ TFabricaComparador }

class function TFabricaComparador.criar<T>: IComparador<T>;
begin
  Result := IComparador<T>
  (
    _LookupVtableInfo(giComparer, TypeInfo(T), SizeOf(T))
  );
end;

class function TFabricaComparador.criar<T>(
  comparacao: TFuncaoComparacao<T>): IComparador<T>;
begin
  Result := TComparador<T>.Create(comparacao);
end;

{ TFabricaPredicado }

class function TFabricaPredicado.criar<T>: IPredicado<T>;
begin
  Result := IPredicado<T>(LocalizarInstanciaPredicado(TypeInfo(T), SizeOf(T)));
end;

class function TFabricaPredicado.criar<T>(
  const predicado: TFuncaoPredicado<T>): IPredicado<T>;
begin
  Result := TPredicado<T>.Create(predicado);
end;

{ TFabricaRedutor }

class function TFabricaRedutor.criar<TAcumulador, TFonte>(
  const reducao: TFuncaoReducao<TAcumulador, TFonte>): IRedutor<TAcumulador, TFonte>;
begin
  Result := TRedutor<TAcumulador, TFonte>.Create(reducao);
end;

class function TFabricaRedutor.criar<TFonte>(
  const reducao: TFuncaoReducaoSimples<TFonte>): IRedutor<TFonte, TFonte>;
var
  novaReducao: ^TFuncaoReducao<TFonte, TFonte>;
begin
  novaReducao := @reducao;

  Result := TRedutor<TFonte, TFonte>.Create(novaReducao^);
end;

class function TFabricaRedutor.criarMultiplicacao<TFonte>: IRedutor<TFonte, TFonte>;
begin
  Result := IRedutor<TFonte, TFonte>
  (
    LocalizarInstanciaReducaoMultiplicacao(TypeInfo(TFonte), SizeOf(TFonte))
  );
end;

class function TFabricaRedutor.criarSoma<TFonte>: IRedutor<TFonte, TFonte>;
begin
  Result := IRedutor<TFonte, TFonte>
  (
    LocalizarInstanciaReducaoAdicao(TypeInfo(TFonte), SizeOf(TFonte))
  );
end;

{ TFabricaProjetor }

class function TFabricaProjetor.criar<T, TResult>(
  const projecao: TFuncaoProjecao<T, TResult>): IProjetor<T, TResult>;
begin
  Result := TProjetor<T, TResult>.Create(projecao);
end;

class function TFabricaProjetor.criar<T>: IProjetor<T, T>;
begin
  Result := TProjetorIdentidade<T>.Create;
end;

class function TFabricaProjetor.criarString<T>(
  const funcao: TFuncaoString<T>): IProjetor<T, string>;
var
  projecao: ^TFuncaoProjecao<T, string>;
begin
  projecao := @funcao;

  Result := TProjetor<T, string>.Create(projecao^);
end;

class function TFabricaProjetor.criarString<T>: IProjetor<T, string>;
begin
  Result := IProjetor<T, string>
  (
    LocalizarInstanciaProjecaoString(TypeInfo(T), SizeOf(T))
  );
end;

{ TFabricaCorrelacionamento }

class function TFabricaCorrelacionamento.criar<TEsquerda, TDireita, TChave, TResult>(
  const projecaoEsquerda: TFuncaoProjecao<TEsquerda, TChave>;
  const projecaoDireita: TFuncaoProjecao<TDireita, TChave>;
  const resultante: TFuncao2<TEsquerda, TDireita, TResult>;
  const comparador: IComparadorIgualdade<TChave>): ICorrelacionamento<TEsquerda, TDireita, TResult>;
begin
  Result := TCorrelacionamento<TEsquerda, TDireita, TChave, TResult>.Create
  (
    resultante, projecaoEsquerda, projecaoDireita, comparador
  );
end;

class function TFabricaCorrelacionamento.criar<TEsquerda, TDireita, TChave, TResult>(
  const projecaoEsquerda: TFuncaoProjecao<TEsquerda, TChave>;
  const projecaoDireita: TFuncaoProjecao<TDireita, TChave>;
  const resultante: TFuncao2<TEsquerda, TDireita, TResult>;
  const igualdade: TFuncaoIgualdade<TChave>): ICorrelacionamento<TEsquerda, TDireita, TResult>;
begin
  Result := TCorrelacionamento<TEsquerda, TDireita, TChave, TResult>.Create
  (
    resultante,
    projecaoEsquerda,
    projecaoDireita,
    TFabricaComparadorIgualdade.criar<TChave>(igualdade)
  );
end;

class function TFabricaCorrelacionamento.criar<TEsquerda, TDireita, TChave, TResult>(
  const projecaoEsquerda: TFuncaoProjecao<TEsquerda, TChave>;
  const projecaoDireita: TFuncaoProjecao<TDireita, TChave>;
  const resultante: TFuncao2<TEsquerda, TDireita, TResult>): ICorrelacionamento<TEsquerda, TDireita, TResult>;
begin
  Result := TCorrelacionamento<TEsquerda, TDireita, TChave, TResult>.Create
  (
    resultante,
    projecaoEsquerda,
    projecaoDireita,
    TFabricaComparadorIgualdade.criar<TChave>()
  );
end;

class function TFabricaCorrelacionamento.criar<TFonte, TChave, TResult>(
  const projecao: TFuncaoProjecao<TFonte, TChave>;
  const resultante: TFuncao2<TFonte, TFonte, TResult>;
  const comparador: IComparadorIgualdade<TChave>): ICorrelacionamento<TFonte, TFonte, TResult>;
begin
  Result := TCorrelacionamento<TFonte, TFonte, TChave, TResult>.Create
  (
    resultante, projecao, projecao, comparador
  );
end;

class function TFabricaCorrelacionamento.criar<TFonte, TChave, TResult>(
  const projecao: TFuncaoProjecao<TFonte, TChave>;
  const resultante: TFuncao2<TFonte, TFonte, TResult>;
  const igualdade: TFuncaoIgualdade<TChave>): ICorrelacionamento<TFonte, TFonte, TResult>;
begin
  Result := TCorrelacionamento<TFonte, TFonte, TChave, TResult>.Create
  (
    resultante,
    projecao,
    projecao,
    TFabricaComparadorIgualdade.criar<TChave>(igualdade)
  );
end;

class function TFabricaCorrelacionamento.criar<TFonte, TChave, TResult>(
  const projecao: TFuncaoProjecao<TFonte, TChave>;
  const resultante: TFuncao2<TFonte, TFonte, TResult>): ICorrelacionamento<TFonte, TFonte, TResult>;
begin
  Result := TCorrelacionamento<TFonte, TFonte, TChave, TResult>.Create
  (
    resultante,
    projecao,
    projecao,
    TFabricaComparadorIgualdade.criar<TChave>()
  );
end;

end.
