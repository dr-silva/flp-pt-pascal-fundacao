(*******************************************************************************
 *
 * Arquivo  : Cerne.Procuradores.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-08 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Guarda os procuradores (classes que implementam interfaces para
 *            uma fun��o espec�fica) do Cerne da Funda��o.
 *
 ******************************************************************************)
unit Cerne.Procuradores;

interface
uses
  FractalLotus.Fundacao.Cerne.Funcoes,
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes;

type
  TComparadorIgualdade<T> = class(TInterfacedObject, IComparadorIgualdade<T>)
  private
    fPadrao   : IComparadorIgualdade<T>;
    fIgualdade: TFuncaoIgualdade<T>;
    fHash     : TFuncaoHash<T>;
  public
    constructor Create
    (
      const igualdade: TFuncaoIgualdade<T>; const hash: TFuncaoHash<T>
    );

    function Equals(const esquerda, direita: T): Boolean; reintroduce;
    function GetHashCode(const valor: T): Integer;        reintroduce;
  end;

  TComparador<T> = class(TInterfacedObject, IComparador<T>)
  private
    fComparacao: TFuncaoComparacao<T>;
  public
    constructor Create(const comparacao: TFuncaoComparacao<T>);

    function Compare(const esquerda, direita: T): Integer;
  end;

  TPredicado<T> = class(TInterfacedObject, IPredicado<T>)
  private
    fPredicado: TFuncaoPredicado<T>;
  public
    constructor Create(const predicao: TFuncaoPredicado<T>);

    function validar(const valor: T): Boolean;
  end;

  TRedutor<TAcumulador, TFonte> = class
  (
    TInterfacedObject, IRedutor<TAcumulador, TFonte>
  )
  private
    fReducao: TFuncaoReducao<TAcumulador, TFonte>;
  public
    constructor Create(const reducao: TFuncaoReducao<TAcumulador, TFonte>);

    function reduzir(const valor: TAcumulador; const item: TFonte): TAcumulador;
  end;

  TProjetor<T, TResult> = class(TInterfacedObject, IProjetor<T, TResult>)
  private
    fProjecao: TFuncaoProjecao<T, TResult>;
  public
    constructor Create(const projecao: TFuncaoProjecao<T, TResult>);

    function projetar(const valor: T): TResult;
  end;

  TProjetorIdentidade<T> = class(TInterfacedObject, IProjetor<T, T>)
  public
    function projetar(const valor: T): T;
  end;

  TCorrelacionamento<TEsquerda, TDireita, TChave, TResult> = class
  (
    TInterfacedObject, ICorrelacionamento<TEsquerda, TDireita, TResult>
  )
  private
    fCorrelacao: TResult;
    fResultante: TFuncao2<TEsquerda, TDireita, TResult>;
    fEsquerda  : TFuncaoProjecao<TEsquerda, TChave>;
    fDireita   : TFuncaoProjecao<TDireita, TChave>;
    fComparador: IComparadorIgualdade<TChave>;

    function obterCorrelacao: TResult;
  public
    constructor Create
    (
      const resultante: TFuncao2<TEsquerda, TDireita, TResult>;
      const esquerda  : TFuncaoProjecao<TEsquerda, TChave>;
      const direita   : TFuncaoProjecao<TDireita, TChave>;
      const comparador: IComparadorIgualdade<TChave>
    );

    function correlacionar
    (
      const esquerda: TEsquerda; const direita: TDireita
    ): Boolean;

    property correlacao: TResult read obterCorrelacao;
  end;

implementation
uses
  System.SysUtils, System.Generics.Defaults,
  Recursos.Excecoes;

{ TComparadorIgualdade<T> }

constructor TComparadorIgualdade<T>.Create(const igualdade: TFuncaoIgualdade<T>;
  const hash: TFuncaoHash<T>);
begin
  if ((@igualdade = nil) or (@hash = nil)) then
    fPadrao := IComparadorIgualdade<T>
    (
      _LookupVtableInfo(giEqualityComparer, TypeInfo(T), SizeOf(T))
    );

  fIgualdade := igualdade;
  fHash      := hash;
end;

function TComparadorIgualdade<T>.Equals(const esquerda, direita: T): Boolean;
begin
  if (@fIgualdade = nil) then
    Result := fPadrao.Equals(esquerda, direita)
  else
    Result := fIgualdade(esquerda, direita);
end;

function TComparadorIgualdade<T>.GetHashCode(const valor: T): Integer;
begin
  if (@fHash = nil) then
    Result := fPadrao.GetHashCode(valor)
  else
    Result :=  fHash(valor);
end;

{ TComparador<T> }

function TComparador<T>.Compare(const esquerda, direita: T): Integer;
begin
  Result := fComparacao(esquerda, direita);
end;

constructor TComparador<T>.Create(const comparacao: TFuncaoComparacao<T>);
begin
  if (@comparacao = nil) then
    raise ArgumentoNulo('fun��o compara��o') at ReturnAddress;

  fComparacao := comparacao;
end;

{ TPredicado<T> }

constructor TPredicado<T>.Create(const predicao: TFuncaoPredicado<T>);
begin
  if (@predicao = nil) then
    raise ArgumentoNulo('fun��o predicado') at ReturnAddress;

  fPredicado := predicao;
end;

function TPredicado<T>.validar(const valor: T): Boolean;
begin
  Result := fPredicado(valor);
end;

{ TRedutor<TAcumulador, TFonte> }

constructor TRedutor<TAcumulador, TFonte>.Create(
  const reducao: TFuncaoReducao<TAcumulador, TFonte>);
begin
  if (@reducao = nil) then
    raise ArgumentoNulo('fun��o redu��o') at ReturnAddress;

  fReducao := reducao;
end;

function TRedutor<TAcumulador, TFonte>.reduzir(const valor: TAcumulador;
  const item: TFonte): TAcumulador;
begin
  Result := fReducao(valor, item);
end;

{ TProjetor<T, TResult> }

constructor TProjetor<T, TResult>.Create(
  const projecao: TFuncaoProjecao<T, TResult>);
begin
  if (@projecao = nil) then
    raise ArgumentoNulo('fun��o proje��o') at ReturnAddress;

  fProjecao := projecao;
end;

function TProjetor<T, TResult>.projetar(const valor: T): TResult;
begin
  Result := fProjecao(valor);
end;

{ TProjetorIdentidade<T> }

function TProjetorIdentidade<T>.projetar(const valor: T): T;
begin
  Result := valor;
end;

{ TCorrelacionamento<TEsquerda, TDireita, TChave, TResult> }

function TCorrelacionamento<TEsquerda, TDireita, TChave, TResult>.correlacionar(
  const esquerda: TEsquerda; const direita: TDireita): Boolean;
var
  chaveEsquerda,
  chaveDireita : TChave;
begin
  chaveEsquerda := fEsquerda(esquerda);
  chaveDireita  := fDireita(direita);
  Result        := fComparador.Equals(chaveEsquerda, chaveDireita);

  if (Result) then
    fCorrelacao := fResultante(esquerda, direita);
end;

constructor TCorrelacionamento<TEsquerda, TDireita, TChave, TResult>.Create(
  const resultante: TFuncao2<TEsquerda, TDireita, TResult>;
  const esquerda: TFuncaoProjecao<TEsquerda, TChave>;
  const direita: TFuncaoProjecao<TDireita, TChave>;
  const comparador: IComparadorIgualdade<TChave>);
begin
  if (@resultante = nil) then
    raise ArgumentoNulo('fun��o resultante') at ReturnAddress;
  if (@esquerda = nil) then
    raise ArgumentoNulo('fun��o proje��o da chave esquerda') at ReturnAddress;
  if (@direita = nil) then
    raise ArgumentoNulo('fun��o proje��o da chave direita') at ReturnAddress;
  if (@comparador = nil) then
    raise ArgumentoNulo('fun��o compara��o das chaves') at ReturnAddress;

  fResultante := resultante;
  fEsquerda   := esquerda;
  fDireita    := direita;
  fComparador := comparador;
end;

function TCorrelacionamento<TEsquerda, TDireita, TChave, TResult>.obterCorrelacao: TResult;
begin
  Result := fCorrelacao;
end;

end.
