(*******************************************************************************
 *
 * Arquivo  : Cerne.Constantes.ReducaoMultiplicacao.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-08 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Guarda as inst�ncias do protocolo IRedutor, quando este redutor
 *            for do tipo multiplicativo {operador *} para os tipos primitivos
 *            do Delphi (Object Pascal).
 *
 ******************************************************************************)
unit Cerne.Constantes.ReducaoMultiplicacao;

interface
uses
  System.TypInfo, Cerne.Constantes.Interfaces;

{$region 'Procuradores'}
function ReducaoMultiplicacaoShortInt(instancia: Pointer; const valor, item: ShortInt): ShortInt;
function ReducaoMultiplicacaoByte    (instancia: Pointer; const valor, item: Byte    ): Byte;
function ReducaoMultiplicacaoSmallInt(instancia: Pointer; const valor, item: SmallInt): SmallInt;
function ReducaoMultiplicacaoWord    (instancia: Pointer; const valor, item: Word    ): Word;
function ReducaoMultiplicacaoInteger (instancia: Pointer; const valor, item: Integer ): Integer;
function ReducaoMultiplicacaoCardinal(instancia: Pointer; const valor, item: Cardinal): Cardinal;
function ReducaoMultiplicacaoInt64   (instancia: Pointer; const valor, item: Int64   ): Int64;
function ReducaoMultiplicacaoUInt64  (instancia: Pointer; const valor, item: UInt64  ): UInt64;

function ReducaoMultiplicacaoSingle  (instancia: Pointer; const valor, item: Single  ): Single;
function ReducaoMultiplicacaoDouble  (instancia: Pointer; const valor, item: Double  ): Double;
function ReducaoMultiplicacaoExtended(instancia: Pointer; const valor, item: Extended): Extended;
function ReducaoMultiplicacaoComp    (instancia: Pointer; const valor, item: Comp    ): Comp;
function ReducaoMultiplicacaoCurrency(instancia: Pointer; const valor, item: Currency): Currency;
{$endregion}

{$region 'Instancia��o'}
function SelecionarReducaoMultiplicacaoInteger (info: PTypeInfo): Pointer; inline;
function SelecionarReducaoMultiplicacaoInt64   (info: PTypeInfo): Pointer; inline;
function SelecionarReducaoMultiplicacaoFloat   (info: PTypeInfo): Pointer; inline;
function LocalizarInstanciaReducaoMultiplicacao(info: PTypeInfo; tamanho: Integer): Pointer; inline;
{$endregion}

const
  {$region 'Tabelas Info'}
  TabelaInfoReducaoMultiplicacaoShortInt: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoMultiplicacaoShortInt
  );
  TabelaInfoReducaoMultiplicacaoByte: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoMultiplicacaoByte
  );
  TabelaInfoReducaoMultiplicacaoSmallInt: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoMultiplicacaoSmallInt
  );
  TabelaInfoReducaoMultiplicacaoWord: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoMultiplicacaoWord
  );
  TabelaInfoReducaoMultiplicacaoInteger: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoMultiplicacaoInteger
  );
  TabelaInfoReducaoMultiplicacaoCardinal: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoMultiplicacaoCardinal
  );
  TabelaInfoReducaoMultiplicacaoInt64: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoMultiplicacaoInt64
  );
  TabelaInfoReducaoMultiplicacaoUInt64: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoMultiplicacaoUInt64
  );

  TabelaInfoReducaoMultiplicacaoSingle: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoMultiplicacaoSingle
  );
  TabelaInfoReducaoMultiplicacaoDouble: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoMultiplicacaoDouble
  );
  TabelaInfoReducaoMultiplicacaoExtended: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoMultiplicacaoExtended
  );
  TabelaInfoReducaoMultiplicacaoComp: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoMultiplicacaoComp
  );
  TabelaInfoReducaoMultiplicacaoCurrency: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ReducaoMultiplicacaoCurrency
  );
  {$endregion}

  {$region 'Inst�ncias'}
  InstanciaReducaoMultiplicacaoShortInt: Pointer = @TabelaInfoReducaoMultiplicacaoShortInt;
  InstanciaReducaoMultiplicacaoByte    : Pointer = @TabelaInfoReducaoMultiplicacaoByte;
  InstanciaReducaoMultiplicacaoSmallInt: Pointer = @TabelaInfoReducaoMultiplicacaoSmallInt;
  InstanciaReducaoMultiplicacaoWord    : Pointer = @TabelaInfoReducaoMultiplicacaoWord;
  InstanciaReducaoMultiplicacaoInteger : Pointer = @TabelaInfoReducaoMultiplicacaoInteger;
  InstanciaReducaoMultiplicacaoCardinal: Pointer = @TabelaInfoReducaoMultiplicacaoCardinal;
  InstanciaReducaoMultiplicacaoInt64   : Pointer = @TabelaInfoReducaoMultiplicacaoInt64;
  InstanciaReducaoMultiplicacaoUInt64  : Pointer = @TabelaInfoReducaoMultiplicacaoUInt64;

  InstanciaReducaoMultiplicacaoSingle  : Pointer = @TabelaInfoReducaoMultiplicacaoSingle;
  InstanciaReducaoMultiplicacaoDouble  : Pointer = @TabelaInfoReducaoMultiplicacaoDouble;
  InstanciaReducaoMultiplicacaoExtended: Pointer = @TabelaInfoReducaoMultiplicacaoExtended;
  InstanciaReducaoMultiplicacaoComp    : Pointer = @TabelaInfoReducaoMultiplicacaoComp;
  InstanciaReducaoMultiplicacaoCurrency: Pointer = @TabelaInfoReducaoMultiplicacaoCurrency;
  {$endregion}

implementation

{$region 'Procuradores'}
function ReducaoMultiplicacaoShortInt(instancia: Pointer; const valor, item: ShortInt): ShortInt;
begin
  Result := valor * item;
end;

function ReducaoMultiplicacaoByte(instancia: Pointer; const valor, item: Byte): Byte;
begin
  Result := valor * item;
end;

function ReducaoMultiplicacaoSmallInt(instancia: Pointer; const valor, item: SmallInt): SmallInt;
begin
  Result := valor * item;
end;

function ReducaoMultiplicacaoWord(instancia: Pointer; const valor, item: Word): Word;
begin
  Result := valor * item;
end;

function ReducaoMultiplicacaoInteger (instancia: Pointer; const valor, item: Integer ): Integer;
begin
  Result := valor * item;
end;

function ReducaoMultiplicacaoCardinal(instancia: Pointer; const valor, item: Cardinal): Cardinal;
begin
  Result := valor * item;
end;

function ReducaoMultiplicacaoInt64(instancia: Pointer; const valor, item: Int64): Int64;
begin
  Result := valor * item;
end;

function ReducaoMultiplicacaoUInt64(instancia: Pointer; const valor, item: UInt64): UInt64;
begin
  Result := valor * item;
end;

function ReducaoMultiplicacaoSingle(instancia: Pointer; const valor, item: Single): Single;
begin
  Result := valor * item;
end;

function ReducaoMultiplicacaoDouble(instancia: Pointer; const valor, item: Double): Double;
begin
  Result := valor * item;
end;

function ReducaoMultiplicacaoExtended(instancia: Pointer; const valor, item: Extended): Extended;
begin
  Result := valor * item;
end;

function ReducaoMultiplicacaoComp(instancia: Pointer; const valor, item: Comp): Comp;
begin
  Result := valor * item;
end;

function ReducaoMultiplicacaoCurrency(instancia: Pointer; const valor, item: Currency): Currency;
begin
  Result := valor * item;
end;
{$endregion}

{$region 'Instancia��o'}
function SelecionarReducaoMultiplicacaoInteger(info: PTypeInfo): Pointer;
begin
  case GetTypeData(info)^.OrdType of
    otSByte: Result := @InstanciaReducaoMultiplicacaoShortInt;
    otUByte: Result := @InstanciaReducaoMultiplicacaoByte;
    otSWord: Result := @InstanciaReducaoMultiplicacaoSmallInt;
    otUWord: Result := @InstanciaReducaoMultiplicacaoWord;
    otSLong: Result := @InstanciaReducaoMultiplicacaoInteger;
    otULong: Result := @InstanciaReducaoMultiplicacaoCardinal;
    else     Result := nil;
  end;
end;

function SelecionarReducaoMultiplicacaoInt64(info: PTypeInfo): Pointer;
var
  data: PTypeData;
begin
  data := GetTypeData(info);

  if (data^.MaxInt64Value > data^.MinInt64Value) then
    Result := @InstanciaReducaoMultiplicacaoInt64
  else
    Result := @InstanciaReducaoMultiplicacaoUInt64;
end;

function SelecionarReducaoMultiplicacaoFloat(info: PTypeInfo): Pointer;
begin
  case GetTypeData(info)^.FloatType of
    ftSingle  : Result := @InstanciaReducaoMultiplicacaoSingle;
    ftDouble  : Result := @InstanciaReducaoMultiplicacaoDouble;
    ftExtended: Result := @InstanciaReducaoMultiplicacaoExtended;
    ftComp    : Result := @InstanciaReducaoMultiplicacaoComp;
    ftCurr    : Result := @InstanciaReducaoMultiplicacaoCurrency;
    else        Result := nil;
  end;
end;

function LocalizarInstanciaReducaoMultiplicacao(info: PTypeInfo; tamanho: Integer): Pointer;
begin
  if (info <> nil) then
    case info^.Kind of
      tkInteger    ,
      tkEnumeration: Result := SelecionarReducaoMultiplicacaoInteger(info);
      tkInt64      : Result := SelecionarReducaoMultiplicacaoInt64  (info);
      tkFloat      : Result := SelecionarReducaoMultiplicacaoFloat  (info);
      else           Result := nil;
    end
  else
    Result := nil;
end;
{$endregion}

end.
