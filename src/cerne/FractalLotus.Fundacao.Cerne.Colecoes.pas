(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fundacao.Cerne.Colecoes.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-08 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Cerne da Funda��o: aqui ser�o declaradas os protocolos das cole-
 *            ��es (ou enumer�veis) que s�o as interfaces que definem as regras
 *            das futuras cole��es que constar�o neste projeto. A unit
 *            FractalLotus.Fundacao.Cerne.pas est� sendo quebrada em v�rios ar-
 *            quivos para melhorar a legibilidade e a manuten��o, os arquivos
 *            s�o:
 *              - FractalLotus.Fundacao.Cerne.Funcoes.pas
 *              - FractalLotus.Fundacao.Cerne.Colecoes.pas
 *              - FractalLotus.Fundacao.Cerne.ProtocolosFuncoes.pas
 *              - FractalLotus.Fundacao.Cerne.Estruturas.pas
 *            Para maiores informa��es sobre esses classificadores consulte a
 *            documenta��o completa em www.fractal-lotus.com.br.
 *
 ******************************************************************************)
unit FractalLotus.Fundacao.Cerne.Colecoes;

interface
uses
  System.Generics.Collections;

type
  { Define uma interface padr�o ao projeto para ser alvo da declara��o do la�o }
  { for in.                                                                    }
  IEnumeravel<T> = interface
    ['{C4D78EB7-99A1-4500-BE1E-AA6FBFF7C2A8}']

    function obterTotal         : Integer;
    function obterVazio         : Boolean;
    function obterCheio         : Boolean;
    function obterSomenteLeitura: Boolean;

    function ToString: string;
    function ToArray : TArray<T>;

    function GetEnumerator: TEnumerator<T>;

    property total         : Integer read obterTotal;
    property vazio         : Boolean read obterVazio;
    property cheio         : Boolean read obterCheio;
    property somenteLeitura: Boolean read obterSomenteLeitura;
  end;

  { Define uma interface padr�o ao projeto para criar um protocolo de agrupa-  }
  { mento, usado principalmente pela extens�o TExtensorEnumeravelAgrupamento.  }
  IAgrupavel<TChave, TValor> = interface(IEnumeravel<TValor>)
    ['{71B3F9D1-4957-4923-868F-E288A783D21E}']

    function obterChave: TChave;

    property chave: TChave read obterChave;
  end;

  { Define uma interface para cole��es que se baseiam no princ�pio �ltimo a    }
  { entrar deve ser o primeiro a sair (Last In, First Out: LIFO).              }
  IEmpilhavel<T> = interface(IEnumeravel<T>)
    ['{20D9DE19-FC42-4D68-9666-638971F9F1AD}']

    procedure empilhar(const item: T);
    function  desempilhar: T;
    function  espiar: T;
    procedure limpar;
  end;

  { Define uma interface para cole��es que se baseiam no princ�pio primeiro a  }
  { entrar deve ser o primeiro a sair (First In, First Out: FIFO).             }
  IEnfileiravel<T> = interface(IEnumeravel<T>)
    ['{D254FB16-DC8F-44BB-A63E-866EC4F30DBD}']

    procedure enfileirar(const item: T);
    function  desenfileirar: T;
    function  espiar: T;
    procedure limpar;
  end;

  { Define uma interface para cole��es em que n�o seja necess�rio suporte �    }
  { remo��o de itens. Seu prop�sito � fornecer uma cole��o que possa guardar   }
  { itens e, ent�o, iter�-los, somente, sem se preocupar com outras regras.    }
  IGuardavel<T> = interface(IEnumeravel<T>)
    ['{0CB70390-4EBA-47B4-82ED-556623832E11}']

    function obterPrimeiro: T;
    function obterUltimo  : T;

    procedure guardar(const item: T);               overload;
    procedure guardar(const itens: TArray<T>);      overload;
    procedure guardar(const itens: IEnumeravel<T>); overload;

    property primeiro: T read obterPrimeiro;
    property ultimo  : T read obterUltimo;
  end;

  { Define uma interface para cole��es que possam ser acessadas por �ndices.   }
  IListavel<T> = interface(IEnumeravel<T>)
    ['{61AF955E-613F-4C53-A6A8-71C4439CE3CB}']

    function obterPrimeiro: T;
    function obterUltimo  : T;

    function obterItem(const indice: Integer): T;
    procedure definirItem(const indice: Integer; const valor: T);

    function adicionar(const item: T): Integer;               overload;
    function adicionar(const itens: TArray<T>): Integer;      overload;
    function adicionar(const itens: IEnumeravel<T>): Integer; overload;

    procedure inserir(const indice: Integer; const item: T);               overload;
    procedure inserir(const indice: Integer; const itens: TArray<T>);      overload;
    procedure inserir(const indice: Integer; const itens: IEnumeravel<T>); overload;

    function  contem (const indice: Integer): Boolean;
    function  remover(const indice: Integer): Boolean;
    procedure limpar;

    function obterItens(const inferior, superior: Integer): IEnumeravel<T>;

    property primeiro: T read obterPrimeiro;
    property ultimo  : T read obterUltimo;

    property item[const indice: Integer]: T read obterItem write definirItem; default;
  end;

  { Define uma interface para cole��es cujos itens n�o possam ser duplicados.  }
  { A interface � interpretada tamb�m como um conjunto no sentido matem�tico.  }
  IDistinguivel<TChave> = interface(IEnumeravel<TChave>)
    ['{4D27909C-69D0-47F4-B2AF-FAC822784594}']

    function adicionar(chave: TChave): Boolean;
    function remover  (chave: TChave): Boolean;
    function contem   (chave: TChave): Boolean;

    procedure limpar;

    procedure unir          (colecao: IEnumeravel<TChave>);
    procedure excetuar      (colecao: IEnumeravel<TChave>);
    procedure interseccionar(colecao: IEnumeravel<TChave>);
  end;

  { Define uma interface para uma estrutura de dados de pares chaves-valores   }
  { que suporte tr�s opera��es: inserir um novo par na estrutura; buscar por   }
  { um valor associado a uma determinada chave; e remover uma chave e seu va-  }
  { lor associado.                                                             }
  IDicionarizavel<TChave, TValor> = interface(IEnumeravel<TPair<TChave, TValor>>)
    ['{537526E6-CA0B-4A06-81B2-ED1725C7042B}']

    function  obterValor(const chave: TChave): TValor;
    procedure definirValor(const chave: TChave; const valor: TValor);
    function  obterChaves : IEnumeravel<TChave>;
    function  obterValores: IEnumeravel<TValor>;

    procedure adicionar(const chave: TChave; const valor: TValor);          overload;
    procedure adicionar(const dicionario: IDicionarizavel<TChave, TValor>); overload;

    function  contem (const chave: TChave): Boolean;
    function  remover(const chave: TChave): Boolean;
    procedure limpar;

    function tentarObter(const chave: TChave; out valor: TValor): Boolean;

    property chaves : IEnumeravel<TChave> read obterChaves;
    property valores: IEnumeravel<TValor> read obterValores;
    property valor[const chave: TChave]: TValor read obterValor write definirValor; default;
  end;

  { Define um protocolo que deve ser implementado em cole��es que tenham itens }
  { que se relacionem ordenadamente entre si.                                  }
  IOrdenavel<TChave> = interface
    ['{F165D0C0-0513-45E9-9E35-44C16C411F2E}']
    function obterMinimo: TChave;
    function obterMaximo: TChave;

    function piso(const chave: TChave): TChave;
    function teto(const chave: TChave): TChave;

    function indexar   (const chave : TChave ): Integer;
    function desindexar(const indice: Integer): TChave;

    function contar     (const inferior, superior: TChave): Integer;
    function obterChaves(const inferior, superior: TChave): IEnumeravel<TChave>;

    property minimo: TChave read obterMinimo;
    property maximo: TChave read obterMaximo;
  end;

implementation

end.
