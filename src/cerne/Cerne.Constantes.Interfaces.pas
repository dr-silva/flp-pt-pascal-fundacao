(*******************************************************************************
 *
 * Arquivo  : Cerne.Constantes.Interfaces.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-08 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Guarda os classificadores constantes do Cerne da Funda��o.
 *
 ******************************************************************************)
unit Cerne.Constantes.Interfaces;

interface
{
uses
  System.TypInfo;
}
type
  TMethodPointer = procedure of object;

  PInstanciaSimples = ^TInstanciaSimples;
  TInstanciaSimples = record
    tabelaInfo: Pointer;
    refCount  : Integer;
    tamanho   : Integer;
  end;

  {$region 'Interface'}
  function SemQueryInterface(instancia: Pointer; const IID: TGUID; out Obj): HResult; stdcall;
  function SemAddref        (instancia: Pointer): Integer; stdcall;
  function SemRelease       (instancia: Pointer): Integer; stdcall;
  function MemAddref        (instancia: Pointer): Integer; stdcall;
  function MemRelease       (instancia: Pointer): Integer; stdcall;

  function instanciar(tabelaInfo: Pointer; tamanho: Integer): Pointer;
  {$endregion}

  {$region 'Bin�rio'}
  function BinIsEmpty(const valor: Pointer; const tamanho: Integer): Boolean;
  function BinToStr(const valor: Pointer; const tamanho: Integer): String;
  {$endregion}

implementation
uses
  System.SysUtils;

{$region 'Interface'}
function SemQueryInterface(instancia: Pointer; const IID: TGUID; out Obj): HResult; stdcall;
begin
  Result := E_NOINTERFACE;
end;

function SemAddref(instancia: Pointer): Integer; stdcall;
begin
  Result := -1;
end;

function SemRelease(instancia: Pointer): Integer; stdcall;
begin
  Result := -1;
end;

function MemAddref(instancia: Pointer): Integer; stdcall;
begin
  Result := AtomicIncrement(PInstanciaSimples(instancia)^.refCount);
end;

function MemRelease(instancia: Pointer): Integer; stdcall;
begin
  Result := AtomicDecrement(PInstanciaSimples(instancia)^.refCount);

  if (Result = 0) then
    FreeMem(instancia, PInstanciaSimples(instancia)^.tamanho);
end;

function instanciar(tabelaInfo: Pointer; tamanho: Integer): Pointer;
var
  instancia: PInstanciaSimples;
begin
  GetMem(instancia, SizeOf(instancia^));

  instancia^.tabelaInfo := tabelaInfo;
  instancia^.refCount   := 0;
  instancia^.tamanho    := tamanho;

  Result := instancia;
end;
{$endregion}

{$region 'Bin�rio'}
function BinIsEmpty(const valor: Pointer; const tamanho: Integer): Boolean;
var
  pValor  : PByte;
  vTamanho: Integer;
begin
  pValor   := valor;
  vTamanho := tamanho;
  Result   := True;

  while (Result and (vTamanho > 0)) do
  begin
    Result := (pValor^ = 0);

    Dec(vTamanho);
    Inc(pValor);
  end;
end;

function BinToStr(const valor: Pointer; const tamanho: Integer): String;
var
  pValor  : PByte;
  vTamanho: Integer;
begin
  Result   := EmptyStr;
  pValor   := PByte(valor);
  vTamanho := tamanho;

  while (vTamanho > 0) do
  begin
    Result := Result + IntToHex(pValor^, 2);

    Dec(vTamanho);
    Inc(pValor);
  end;
end;
{$endregion}

end.
