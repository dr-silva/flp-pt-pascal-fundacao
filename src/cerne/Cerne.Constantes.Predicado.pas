(*******************************************************************************
 *
 * Arquivo  : Cerne.Constantes.Predicado.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-08 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Guarda as inst�ncias do protocolo IPredicado para os tipos primi-
 *            tivos do Delphi (Object Pascal).
 *
 ******************************************************************************)
unit Cerne.Constantes.Predicado;

interface
uses
  System.TypInfo, Cerne.Constantes.Interfaces;

{$region 'Procuradores'}
function PredicadoShortInt(instancia: Pointer; const valor: ShortInt): Boolean;
function PredicadoByte    (instancia: Pointer; const valor: Byte    ): Boolean;
function PredicadoSmallInt(instancia: Pointer; const valor: SmallInt): Boolean;
function PredicadoWord    (instancia: Pointer; const valor: Word    ): Boolean;
function PredicadoInteger (instancia: Pointer; const valor: Integer ): Boolean;
function PredicadoCardinal(instancia: Pointer; const valor: Cardinal): Boolean;
function PredicadoInt64   (instancia: Pointer; const valor: Int64   ): Boolean;
function PredicadoUInt64  (instancia: Pointer; const valor: UInt64  ): Boolean;

function PredicadoSingle  (instancia: Pointer; const valor: Single  ): Boolean;
function PredicadoDouble  (instancia: Pointer; const valor: Double  ): Boolean;
function PredicadoExtended(instancia: Pointer; const valor: Extended): Boolean;
function PredicadoComp    (instancia: Pointer; const valor: Comp    ): Boolean;
function PredicadoCurrency(instancia: Pointer; const valor: Currency): Boolean;

function PredicadoString       (instancia: Pointer; const valor: String       ): Boolean;
function PredicadoAnsiString   (instancia: Pointer; const valor: AnsiString   ): Boolean;
function PredicadoWideString   (instancia: Pointer; const valor: WideString   ): Boolean;
function PredicadoUnicodeString(instancia: Pointer; const valor: UnicodeString): Boolean;

function PredicadoBinario (instancia: Pointer; const valor            ): Boolean;
function PredicadoPointer (instancia: Pointer; const valor: NativeUInt): Boolean;
function PredicadoVariant (instancia: Pointer; const valor: Pointer   ): Boolean;
function PredicadoObject  (instancia: Pointer; const valor: TObject   ): Boolean;
function PredicadoDynArray(instancia: Pointer; const valor: Pointer   ): Boolean;
{$endregion}

{$region 'Instancia��o'}
function SelecionarPredicadoInteger (info: PTypeInfo ): Pointer; inline;
function SelecionarPredicadoInt64   (info: PTypeInfo ): Pointer; inline;
function SelecionarPredicadoFloat   (info: PTypeInfo ): Pointer; inline;
function SelecionarPredicadoBinario (tamanho: Integer): Pointer; inline;
function SelecionarPredicadoDynArray(info: PTypeInfo ): Pointer; inline;
function LocalizarInstanciaPredicado(info: PTypeInfo; tamanho: Integer): Pointer; inline;
{$endregion}

const
  {$region 'Tabelas Info'}
  TabelaInfoPredicadoShortInt: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoShortInt
  );
  TabelaInfoPredicadoByte: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoByte
  );
  TabelaInfoPredicadoSmallInt: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoSmallInt
  );
  TabelaInfoPredicadoWord: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoWord
  );
  TabelaInfoPredicadoInteger: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoInteger
  );
  TabelaInfoPredicadoCardinal: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoCardinal
  );
  TabelaInfoPredicadoInt64: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoInt64
  );
  TabelaInfoPredicadoUInt64: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoUInt64
  );

  TabelaInfoPredicadoSingle: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoSingle
  );
  TabelaInfoPredicadoDouble: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoDouble
  );
  TabelaInfoPredicadoExtended: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoExtended
  );
  TabelaInfoPredicadoComp: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoComp
  );
  TabelaInfoPredicadoCurrency: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoCurrency
  );

  TabelaInfoPredicadoString: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoString
  );
  TabelaInfoPredicadoAnsiString: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoAnsiString
  );
  TabelaInfoPredicadoWideString: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoWideString
  );
  TabelaInfoPredicadoUnicodeString: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoUnicodeString
  );

  TabelaInfoPredicadoVariant: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoVariant
  );
  TabelaInfoPredicadoBinario: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @PredicadoBinario
  );
  TabelaInfoPredicadoPointer: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoPointer
  );
  TabelaInfoPredicadoObject: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @PredicadoObject
  );
  TabelaInfoPredicadoDynArray: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @PredicadoDynArray
  );
  {$endregion}

  {$region 'Inst�ncias'}
  InstanciaPredicadoShortInt: Pointer = @TabelaInfoPredicadoShortInt;
  InstanciaPredicadoByte    : Pointer = @TabelaInfoPredicadoByte;
  InstanciaPredicadoSmallInt: Pointer = @TabelaInfoPredicadoSmallInt;
  InstanciaPredicadoWord    : Pointer = @TabelaInfoPredicadoWord;
  InstanciaPredicadoInteger : Pointer = @TabelaInfoPredicadoInteger;
  InstanciaPredicadoCardinal: Pointer = @TabelaInfoPredicadoCardinal;
  InstanciaPredicadoInt64   : Pointer = @TabelaInfoPredicadoInt64;
  InstanciaPredicadoUInt64  : Pointer = @TabelaInfoPredicadoUInt64;

  InstanciaPredicadoSingle  : Pointer = @TabelaInfoPredicadoSingle;
  InstanciaPredicadoDouble  : Pointer = @TabelaInfoPredicadoDouble;
  InstanciaPredicadoExtended: Pointer = @TabelaInfoPredicadoExtended;
  InstanciaPredicadoComp    : Pointer = @TabelaInfoPredicadoComp;
  InstanciaPredicadoCurrency: Pointer = @TabelaInfoPredicadoCurrency;

  InstanciaPredicadoString       : Pointer = @TabelaInfoPredicadoString;
  InstanciaPredicadoAnsiString   : Pointer = @TabelaInfoPredicadoAnsiString;
  InstanciaPredicadoWideString   : Pointer = @TabelaInfoPredicadoWideString;
  InstanciaPredicadoUnicodeString: Pointer = @TabelaInfoPredicadoUnicodeString;

  InstanciaPredicadoVariant : Pointer = @TabelaInfoPredicadoVariant;
  InstanciaPredicadoPointer : Pointer = @TabelaInfoPredicadoPointer;
  InstanciaPredicadoObject  : Pointer = @TabelaInfoPredicadoObject;
  {$endregion}

implementation
uses
  System.Variants;

{$region 'Procuradores'}
function PredicadoShortInt(instancia: Pointer; const valor: ShortInt): Boolean;
begin
  Result := valor <> 0;
end;

function PredicadoByte(instancia: Pointer; const valor: Byte): Boolean;
begin
  Result := valor <> 0;
end;

function PredicadoSmallInt(instancia: Pointer; const valor: SmallInt): Boolean;
begin
  Result := valor <> 0;
end;

function PredicadoWord(instancia: Pointer; const valor: Word): Boolean;
begin
  Result := valor <> 0;
end;

function PredicadoInteger(instancia: Pointer; const valor: Integer): Boolean;
begin
  Result := valor <> 0;
end;

function PredicadoCardinal(instancia: Pointer; const valor: Cardinal): Boolean;
begin
  Result := valor <> 0;
end;

function PredicadoInt64(instancia: Pointer; const valor: Int64): Boolean;
begin
  Result := valor <> 0;
end;

function PredicadoUInt64(instancia: Pointer; const valor: UInt64): Boolean;
begin
  Result := valor <> 0;
end;

function PredicadoSingle(instancia: Pointer; const valor: Single): Boolean;
begin
  Result := valor <> 0.0;
end;

function PredicadoDouble(instancia: Pointer; const valor: Double): Boolean;
begin
  Result := valor <> 0.0;
end;

function PredicadoExtended(instancia: Pointer; const valor: Extended): Boolean;
begin
  Result := valor <> 0.0;
end;

function PredicadoComp(instancia: Pointer; const valor: Comp): Boolean;
begin
  Result := valor <> 0.0;
end;

function PredicadoCurrency(instancia: Pointer; const valor: Currency): Boolean;
begin
  Result := valor <> 0.0;
end;

function PredicadoString(instancia: Pointer; const valor: String): Boolean;
begin
  Result := valor <> '';
end;

function PredicadoAnsiString(instancia: Pointer; const valor: AnsiString): Boolean;
begin
  Result := valor <> '';
end;

function PredicadoWideString(instancia: Pointer; const valor: WideString): Boolean;
begin
  Result := valor <> '';
end;

function PredicadoUnicodeString(instancia: Pointer; const valor: UnicodeString): Boolean;
begin
  Result := valor <> '';
end;

function PredicadoBinario(instancia: Pointer; const valor): Boolean;
begin
  Result := not BinIsEmpty(@valor, PInstanciaSimples(instancia)^.tamanho);
end;

function PredicadoPointer(instancia: Pointer; const valor: NativeUInt): Boolean;
begin
  Result := valor <> 0;
end;

function PredicadoVariant(instancia: Pointer; const valor: Pointer): Boolean;
var
  v: Variant;
begin
  v := PVariant(valor)^;

  Result := System.Variants.VarIsEmpty(v) or
            System.Variants.VarIsNull (v) or
            System.Variants.VarIsClear(v);
end;

function PredicadoObject(instancia: Pointer; const valor: TObject): Boolean;
begin
  Result := valor <> nil;
end;

function PredicadoDynArray(instancia: Pointer; const valor: Pointer): Boolean;
var
  tamanho : Integer;
begin
  if (valor = nil) then
    tamanho := 0
  else
    tamanho := PNativeInt(PByte(valor) - SizeOf(NativeInt))^ *
               PInstanciaSimples(instancia)^.tamanho;

  Result := not BinIsEmpty(valor, tamanho);
end;
{$endregion}

{$region 'Instancia��o'}
function SelecionarPredicadoInteger(info: PTypeInfo): Pointer;
begin
  case GetTypeData(info)^.OrdType of
    otSByte: Result := @InstanciaPredicadoShortInt;
    otUByte: Result := @InstanciaPredicadoByte;
    otSWord: Result := @InstanciaPredicadoSmallInt;
    otUWord: Result := @InstanciaPredicadoWord;
    otSLong: Result := @InstanciaPredicadoInteger;
    otULong: Result := @InstanciaPredicadoCardinal;
    else     Result := nil
  end;
end;

function SelecionarPredicadoInt64(info: PTypeInfo): Pointer;
var
  data: PTypeData;
begin
  data := GetTypeData(info);
  if (data^.MaxInt64Value > data^.MinInt64Value) then
    Result := @InstanciaPredicadoInt64
  else
    Result := @InstanciaPredicadoUInt64;
end;

function SelecionarPredicadoFloat(info: PTypeInfo): Pointer;
begin
  case GetTypeData(info)^.FloatType of
    ftSingle  : Result := @InstanciaPredicadoSingle;
    ftDouble  : Result := @InstanciaPredicadoDouble;
    ftExtended: Result := @InstanciaPredicadoExtended;
    ftComp    : Result := @InstanciaPredicadoComp;
    ftCurr    : Result := @InstanciaPredicadoCurrency;
    else        Result := nil;
  end;
end;

function SelecionarPredicadoBinario(tamanho: Integer): Pointer;
begin
  case tamanho of
    1: Result := @InstanciaPredicadoByte;
    2: Result := @InstanciaPredicadoWord;
    4: Result := @InstanciaPredicadoCardinal;
    {$IFDEF CPUX64}
    8: Result := @InstanciaPredicadoUInt64;
    {$ENDIF}
    else
      Result := instanciar(@TabelaInfoPredicadoBinario, tamanho);
  end;
end;

function SelecionarPredicadoDynArray(info: PTypeInfo): Pointer;
begin
  Result := instanciar(@TabelaInfoPredicadoDynArray, GetTypeData(info)^.elSize);
end;

function LocalizarInstanciaPredicado(info: PTypeInfo; tamanho: Integer): Pointer;
begin
  if (info <> nil) then
    case info^.Kind of
      tkUnknown    ,
      tkChar       ,
      tkWChar      ,
      tkSet        ,
      tkArray      ,
      tkRecord     ,
      tkMethod     : Result := SelecionarPredicadoBinario(tamanho);

      tkInterface  ,
      tkClassRef   ,
      tkProcedure  ,
      tkPointer    : Result := @InstanciaPredicadoPointer;

      tkInteger    ,
      tkEnumeration: Result := SelecionarPredicadoInteger(info);
      tkInt64      : Result := SelecionarPredicadoInt64  (info);

      tkFloat      : Result := SelecionarPredicadoFloat   (info);
      tkDynArray   : Result := SelecionarPredicadoDynArray(info);
      tkVariant    : Result := @InstanciaPredicadoVariant;
      tkClass      : Result := @InstanciaPredicadoObject;

      tkString     : Result := @InstanciaPredicadoString;
      tkLString    : Result := @InstanciaPredicadoAnsiString;
      tkWString    : Result := @InstanciaPredicadoWideString;
      tkUString    : Result := @InstanciaPredicadoUnicodeString;
      else           Result := SelecionarPredicadoBinario(tamanho);
    end
  else
    Result := SelecionarPredicadoBinario(tamanho);
end;
{$endregion}

end.
