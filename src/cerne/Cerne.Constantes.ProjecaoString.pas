(*******************************************************************************
 *
 * Arquivo  : Cerne.Constantes.ProjecaoString.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2020-08-08 <yyyy-mm-dd>
 * Licen�a  : Este c�digo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: Guarda as inst�ncias do protocolo IProjetor para os casos em que
 *            a proje��o � feita de um tipo primivo do Delphi (Object Pascal)
 *            para um string.
 *
 ******************************************************************************)
unit Cerne.Constantes.ProjecaoString;

interface
uses
  System.TypInfo, Cerne.Constantes.Interfaces;

{$region 'Procuradores'}
function ProjecaoStringShortInt(instancia: Pointer; const item: ShortInt): string;
function ProjecaoStringByte    (instancia: Pointer; const item: Byte    ): string;
function ProjecaoStringSmallInt(instancia: Pointer; const item: SmallInt): string;
function ProjecaoStringWord    (instancia: Pointer; const item: Word    ): string;
function ProjecaoStringInteger (instancia: Pointer; const item: Integer ): string;
function ProjecaoStringCardinal(instancia: Pointer; const item: Cardinal): string;
function ProjecaoStringInt64   (instancia: Pointer; const item: Int64   ): string;
function ProjecaoStringUInt64  (instancia: Pointer; const item: UInt64  ): string;

function ProjecaoStringAnsiChar(instancia: Pointer; const item: AnsiChar): string;
function ProjecaoStringWideChar(instancia: Pointer; const item: WideChar): string;

function ProjecaoStringSingle  (instancia: Pointer; const item: Single  ): string;
function ProjecaoStringDouble  (instancia: Pointer; const item: Double  ): string;
function ProjecaoStringExtended(instancia: Pointer; const item: Extended): string;
function ProjecaoStringComp    (instancia: Pointer; const item: Comp    ): string;
function ProjecaoStringCurrency(instancia: Pointer; const item: Currency): string;

function ProjecaoStringString       (instancia: Pointer; const item: string       ): string;
function ProjecaoStringAnsiString   (instancia: Pointer; const item: AnsiString   ): string;
function ProjecaoStringWideString   (instancia: Pointer; const item: WideString   ): string;
function ProjecaoStringUnicodeString(instancia: Pointer; const item: UnicodeString): string;

function ProjecaoStringBinario (instancia: Pointer; const item                ): string;
function ProjecaoStringPointer (instancia: Pointer; const item: NativeUInt    ): string;
function ProjecaoStringVariant (instancia: Pointer; const item: Pointer       ): string;
function ProjecaoStringObject  (instancia: Pointer; const item: TObject       ): string;
function ProjecaoStringMethod  (instancia: Pointer; const item: TMethodPointer): string;
function ProjecaoStringDynArray(instancia: Pointer; const item: Pointer       ): string;
{$endregion}

{$region 'Instancia��o'}
function SelecionarProjecaoStringInteger (info: PTypeInfo ): Pointer;
function SelecionarProjecaoStringInt64   (info: PTypeInfo ): Pointer;
function SelecionarProjecaoStringFloat   (info: PTypeInfo ): Pointer;
function SelecionarProjecaoStringChar    (tamanho: Integer): Pointer;
function SelecionarProjecaoStringBinario (tamanho: Integer): Pointer;
function SelecionarProjecaoStringDynArray(info: PTypeInfo ): Pointer;
function LocalizarInstanciaProjecaoString(info: PTypeInfo; tamanho: Integer): Pointer;
{$endregion}

const
  {$region 'Tabelas Info'}
  TabelaInfoProjecaoStringShortInt: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringShortInt
  );
  TabelaInfoProjecaoStringByte    : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringByte
  );
  TabelaInfoProjecaoStringSmallInt: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringSmallInt
  );
  TabelaInfoProjecaoStringWord    : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringWord
  );
  TabelaInfoProjecaoStringInteger : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringInteger
  );
  TabelaInfoProjecaoStringCardinal: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringCardinal
  );
  TabelaInfoProjecaoStringInt64   : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringInt64
  );
  TabelaInfoProjecaoStringUInt64  : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringUInt64
  );

  TabelaInfoProjecaoStringAnsiChar: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringAnsiChar
  );
  TabelaInfoProjecaoStringWideChar: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringWideChar
  );

  TabelaInfoProjecaoStringSingle  : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringSingle
  );
  TabelaInfoProjecaoStringDouble  : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringDouble
  );
  TabelaInfoProjecaoStringExtended: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringExtended
  );
  TabelaInfoProjecaoStringComp    : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringComp
  );
  TabelaInfoProjecaoStringCurrency: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringCurrency
  );

  TabelaInfoProjecaoStringString       : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringString
  );
  TabelaInfoProjecaoStringAnsiString   : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringAnsiString
  );
  TabelaInfoProjecaoStringWideString   : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringWideString
  );
  TabelaInfoProjecaoStringUnicodeString: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringUnicodeString
  );

  TabelaInfoProjecaoStringBinario : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @ProjecaoStringBinario
  );
  TabelaInfoProjecaoStringPointer : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringPointer
  );
  TabelaInfoProjecaoStringVariant : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringVariant
  );
  TabelaInfoProjecaoStringObject  : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringObject
  );
  TabelaInfoProjecaoStringMethod  : array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @SemAddref,
    @SemRelease,
    @ProjecaoStringMethod
  );
  TabelaInfoProjecaoStringDynArray: array [0..3] of Pointer =
  (
    @SemQueryInterface,
    @MemAddref,
    @MemRelease,
    @ProjecaoStringDynArray
  );
  {$endregion}

  {$region 'Inst�ncias'}
  InstanciaProjecaoStringShortInt: Pointer = @TabelaInfoProjecaoStringShortInt;
  InstanciaProjecaoStringByte    : Pointer = @TabelaInfoProjecaoStringByte;
  InstanciaProjecaoStringSmallInt: Pointer = @TabelaInfoProjecaoStringSmallInt;
  InstanciaProjecaoStringWord    : Pointer = @TabelaInfoProjecaoStringWord;
  InstanciaProjecaoStringInteger : Pointer = @TabelaInfoProjecaoStringInteger;
  InstanciaProjecaoStringCardinal: Pointer = @TabelaInfoProjecaoStringCardinal;
  InstanciaProjecaoStringInt64   : Pointer = @TabelaInfoProjecaoStringInt64;
  InstanciaProjecaoStringUInt64  : Pointer = @TabelaInfoProjecaoStringUInt64;

  InstanciaProjecaoStringAnsiChar: Pointer = @TabelaInfoProjecaoStringAnsiChar;
  InstanciaProjecaoStringWideChar: Pointer = @TabelaInfoProjecaoStringWideChar;

  InstanciaProjecaoStringSingle  : Pointer = @TabelaInfoProjecaoStringSingle;
  InstanciaProjecaoStringDouble  : Pointer = @TabelaInfoProjecaoStringDouble;
  InstanciaProjecaoStringExtended: Pointer = @TabelaInfoProjecaoStringExtended;
  InstanciaProjecaoStringComp    : Pointer = @TabelaInfoProjecaoStringComp;
  InstanciaProjecaoStringCurrency: Pointer = @TabelaInfoProjecaoStringCurrency;

  InstanciaProjecaoStringString       : Pointer = @TabelaInfoProjecaoStringString;
  InstanciaProjecaoStringAnsiString   : Pointer = @TabelaInfoProjecaoStringAnsiString;
  InstanciaProjecaoStringWideString   : Pointer = @TabelaInfoProjecaoStringWideString;
  InstanciaProjecaoStringUnicodeString: Pointer = @TabelaInfoProjecaoStringUnicodeString;

  InstanciaProjecaoStringPointer : Pointer = @TabelaInfoProjecaoStringPointer;
  InstanciaProjecaoStringVariant : Pointer = @TabelaInfoProjecaoStringVariant;
  InstanciaProjecaoStringObject  : Pointer = @TabelaInfoProjecaoStringObject;
  InstanciaProjecaoStringMethod  : Pointer = @TabelaInfoProjecaoStringMethod;
  {$endregion}

implementation
uses
  System.SysUtils, System.Variants;

{$region 'Procuradores'}
function ProjecaoStringShortInt(instancia: Pointer; const item: ShortInt): String;
begin
  Result := IntToStr(item);
end;

function ProjecaoStringByte(instancia: Pointer; const item: Byte): String;
begin
  Result := UIntToStr(item);
end;

function ProjecaoStringSmallInt(instancia: Pointer; const item: SmallInt): String;
begin
  Result := IntToStr(item);
end;

function ProjecaoStringWord(instancia: Pointer; const item: Word): String;
begin
  Result := UIntToStr(item);
end;

function ProjecaoStringInteger(instancia: Pointer; const item: Integer): String;
begin
  Result := IntToStr(item);
end;

function ProjecaoStringCardinal(instancia: Pointer; const item: Cardinal): String;
begin
  Result := UIntToStr(item);
end;

function ProjecaoStringInt64(instancia: Pointer; const item: Int64): String;
begin
  Result := IntToStr(item);
end;

function ProjecaoStringUInt64(instancia: Pointer; const item: UInt64): String;
begin
  Result := UIntToStr(item);
end;

function ProjecaoStringAnsiChar(instancia: Pointer; const item: AnsiChar): string;
begin
  Result := EmptyStr + Char(item);
end;

function ProjecaoStringWideChar(instancia: Pointer; const item: WideChar): string;
begin
  Result := EmptyStr + item;
end;

function ProjecaoStringSingle(instancia: Pointer; const item: Single): String;
begin
  Result := FloatToStr(item);
end;

function ProjecaoStringDouble(instancia: Pointer; const item: Double): String;
begin
  Result := FloatToStr(item);
end;

function ProjecaoStringExtended(instancia: Pointer; const item: Extended): String;
begin
  Result := FloatToStr(item);
end;

function ProjecaoStringComp(instancia: Pointer; const item: Comp): String;
begin
  Result := FloatToStr(item);
end;

function ProjecaoStringCurrency(instancia: Pointer; const item: Currency): String;
begin
  Result := CurrToStr(item);
end;

function ProjecaoStringString(instancia: Pointer; const item: String): String;
begin
  Result := item;
end;

function ProjecaoStringAnsiString(instancia: Pointer; const item: AnsiString): String;
begin
  Result := string(item);
end;

function ProjecaoStringWideString(instancia: Pointer; const item: WideString): String;
begin
  Result := string(item);
end;

function ProjecaoStringUnicodeString(instancia: Pointer; const item: UnicodeString): String;
begin
  Result := item;
end;

function ProjecaoStringBinario(instancia: Pointer; const item): String;
begin
  Result := BinToStr(@item, PInstanciaSimples(instancia)^.tamanho);
end;

function ProjecaoStringPointer(instancia: Pointer; const item: NativeUInt): String;
begin
  Result := IntToHex(item, 8);
end;

function ProjecaoStringVariant(instancia: Pointer; const item: Pointer): String;
begin
  Result := VarToStr(PVariant(item)^);
end;

function ProjecaoStringObject(instancia: Pointer; const item: TObject): String;
begin
  if (item <> nil) then
    Result := item.ToString
  else
    Result := EmptyStr;
end;

function ProjecaoStringMethod(instancia: Pointer; const item: TMethodPointer): String;
var
  metodo: TMethod;
begin
  metodo := TMethod(item);
  Result := Format
  (
    '{"Code": "%s", "Data": "%s"}',
    [
      IntToHex(NativeInt(metodo.Code), 8),
      IntToHex(NativeInt(metodo.Data), 8)
    ]
  );
end;

function ProjecaoStringDynArray(instancia: Pointer; const item: Pointer): String;
var
  tamanho: Integer;
begin
  if (item <> nil) then
    tamanho := PNativeInt(PByte(item) - SizeOf(NativeInt))^ *
               PInstanciaSimples(instancia)^.tamanho
  else
    tamanho := 0;

  Result := BinToStr(item, tamanho);
end;
{$endregion}

{$region 'Instancia��o'}
function SelecionarProjecaoStringInteger(info: PTypeInfo): Pointer;
begin
  case GetTypeData(info)^.OrdType of
    otSByte: Result := @InstanciaProjecaoStringShortInt;
    otUByte: Result := @InstanciaProjecaoStringByte;
    otSWord: Result := @InstanciaProjecaoStringSmallInt;
    otUWord: Result := @InstanciaProjecaoStringWord;
    otSLong: Result := @InstanciaProjecaoStringInteger;
    otULong: Result := @InstanciaProjecaoStringCardinal;
    else     Result := nil;
  end;
end;

function SelecionarProjecaoStringInt64(info: PTypeInfo): Pointer;
var
  data: PTypeData;
begin
  data := GetTypeData(info);

  if (data^.MaxInt64Value > data^.MinInt64Value) then
    Result := @InstanciaProjecaoStringInt64
  else
    Result := @InstanciaProjecaoStringUInt64;
end;

function SelecionarProjecaoStringFloat(info: PTypeInfo): Pointer;
begin
  case GetTypeData(info)^.FloatType of
    ftSingle  : Result := @InstanciaProjecaoStringSingle;
    ftDouble  : Result := @InstanciaProjecaoStringDouble;
    ftExtended: Result := @InstanciaProjecaoStringExtended;
    ftComp    : Result := @InstanciaProjecaoStringComp;
    ftCurr    : Result := @InstanciaProjecaoStringCurrency;
    else        Result := nil;
  end;
end;

function SelecionarProjecaoStringChar(tamanho: Integer): Pointer;
begin
  case tamanho of
      1: Result := @InstanciaProjecaoStringAnsiChar;
      2: Result := @InstanciaProjecaoStringWideChar;
    else Result := nil;
  end;
end;

function SelecionarProjecaoStringBinario(tamanho: Integer): Pointer;
begin
  Result := instanciar(@TabelaInfoProjecaoStringBinario, tamanho);
end;

function SelecionarProjecaoStringDynArray(info: PTypeInfo): Pointer;
begin
  Result := instanciar(@TabelaInfoProjecaoStringDynArray, GetTypeData(info)^.elSize);
end;

function LocalizarInstanciaProjecaoString(info: PTypeInfo; tamanho: Integer): Pointer;
begin
  if (info <> nil) then
    case info^.Kind of
      tkUnknown    ,
      tkSet        ,
      tkArray      ,
      tkRecord     : Result := SelecionarProjecaoStringBinario(tamanho);

      tkInterface  ,
      tkClassRef   ,
      tkProcedure  ,
      tkPointer    : Result := @InstanciaProjecaoStringPointer;

      tkInteger    ,
      tkEnumeration: Result := SelecionarProjecaoStringInteger (info);
      tkInt64      : Result := SelecionarProjecaoStringInt64   (info);
      tkFloat      : Result := SelecionarProjecaoStringFloat   (info);
      tkDynArray   : Result := SelecionarProjecaoStringDynArray(info);
      tkChar       ,
      tkWChar      : Result := SelecionarProjecaoStringChar(tamanho);

      tkVariant    : Result := @InstanciaProjecaoStringVariant;
      tkClass      : Result := @InstanciaProjecaoStringObject;
      tkMethod     : Result := @InstanciaProjecaoStringMethod;

      tkString     : Result := @InstanciaProjecaoStringString;
      tkLString    : Result := @InstanciaProjecaoStringAnsiString;
      tkWString    : Result := @InstanciaProjecaoStringWideString;
      tkUString    : Result := @InstanciaProjecaoStringUnicodeString;
      else           Result := SelecionarProjecaoStringBinario(tamanho);
    end
  else
    Result := SelecionarProjecaoStringBinario(tamanho);
end;
{$endregion}

end.
