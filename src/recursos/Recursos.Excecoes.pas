unit Recursos.Excecoes;

interface
uses
  System.SysUtils;

function PeriodoInvalido  (const nome: string): EArgumentException;
function ArgumentoNulo    (const nome: string): EArgumentNilException;
function ArgumentoNegativo(const nome: string): EArgumentOutOfRangeException;
function IndiceForaLimites(const nome: string; const inferior, superior: Integer): EArgumentOutOfRangeException;
function ChaveDicionario: EArgumentOutOfRangeException;
function ColecaoVazia   : EInvalidOpException;
function ColecaoCheia   : EInvalidOpException;

const
  S_PERIODO_INVALIDO    = 'O par�metro "%s" n�o corresponde a um Per�odo';
  S_ARGUMENTO_NULO      = 'O par�metro "%s" n�o pode ser nulo';
  S_ARGUMENTO_NEGATIVO  = 'O par�metro "%s" deve ser positivo';
  S_INDICE_FORA_LIMITES = 'O par�metro "%s" est� fora dos limites: %d e %d';
  S_CHAVE_DICIONARIO    = 'A chave informada n�o consta no dicion�rio';

  S_OP_COLECAO_VAZIA    = 'Opera��o inv�lida em cole��o vazia';
  S_OP_COLECAO_CHEIA    = 'Opera��o inv�lida em cole��o cheia';

implementation

function PeriodoInvalido(const nome: string): EArgumentException;
begin
  Result := EArgumentException.CreateFmt(S_PERIODO_INVALIDO, [nome]);
end;

function ArgumentoNulo(const nome: string): EArgumentNilException;
begin
  Result := EArgumentNilException.CreateFmt(S_ARGUMENTO_NULO, [nome]);
end;

function ArgumentoNegativo(const nome: string): EArgumentOutOfRangeException;
begin
  Result := EArgumentOutOfRangeException.CreateFmt
  (
    S_ARGUMENTO_NEGATIVO, [nome]
  );
end;

function IndiceForaLimites(const nome: string; const inferior, superior: Integer): EArgumentOutOfRangeException;
begin
  Result := EArgumentOutOfRangeException.CreateFmt
  (
    S_INDICE_FORA_LIMITES, [nome, inferior, superior]
  );
end;

function ChaveDicionario: EArgumentOutOfRangeException;
begin
  Result := EArgumentOutOfRangeException.Create(S_CHAVE_DICIONARIO);
end;

function ColecaoVazia: EInvalidOpException;
begin
  Result := EInvalidOpException.Create(S_OP_COLECAO_VAZIA);
end;

function ColecaoCheia: EInvalidOpException;
begin
  Result := EInvalidOpException.Create(S_OP_COLECAO_CHEIA);
end;

end.
