unit Extensores.ExtensorPadrao;

interface
uses
  FractalLotus.Fundacao.Cerne.ProtocolosFuncoes,
  FractalLotus.Fundacao.Cerne.Colecoes;

type
  TExtensorPadrao = class
  public
    class function toArray <T>(const colecao: IEnumeravel<T>): TArray<T>;
    class function toString<T>(const colecao: IEnumeravel<T>): string; reintroduce; overload;
    class function toString<T>(const colecao: IEnumeravel<T>; const projetor: IProjetor<T, string>): string; reintroduce; overload;
  end;

implementation
uses
  System.SysUtils;

{ TExtensorPadrao }

class function TExtensorPadrao.toArray<T>(
  const colecao: IEnumeravel<T>): TArray<T>;
var
  item  : T;
  indice: Integer;
begin
  if (colecao = nil) then
    Exit(nil);

  indice := 0;
  SetLength(Result, colecao.total);

  for item in colecao do
  begin
    Result[indice] := item;
    Inc(indice);
  end;
end;

class function TExtensorPadrao.toString<T>(
  const colecao: IEnumeravel<T>): string;
var
  projetor: IProjetor<T, string>;
begin
  projetor := nil;

  if (colecao <> nil) then
    projetor := TFabricaProjetor.criarString<T>();

  Result := toString<T>(colecao, projetor);
end;

class function TExtensorPadrao.toString<T>(const colecao: IEnumeravel<T>;
  const projetor: IProjetor<T, string>): string;
var
  item: T;
begin
  if (colecao = nil) then
    Exit('[]');

  Result := '';

  for item in colecao do
    Result := projetor.projetar(item);

  Result.Remove(Result.Length - 2);

  Result := '[' + Result + ']';
end;

end.
